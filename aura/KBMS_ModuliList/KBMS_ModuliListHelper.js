({
	sorting : function(json_object, key_to_sort_by, order)
	    {
            function sortByKey(a, b) 
            {
                var x = a[key_to_sort_by].toLowerCase();
                var y = b[key_to_sort_by].toLowerCase();
                if (order == 'ASC')
                {
                	return ((x < y) ? -1 : ((x > y) ? 1 : 0));    
				}
                else
                {
                    return ((x < y) ? 1 : ((x > y) ? -1 : 0)); 
                }
                
            }
            json_object.sort(sortByKey);
        }
})