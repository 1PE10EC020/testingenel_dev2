({
    afterSVGScriptsLoad: function(component, event, helper) 
    {
        svg4everybody();
	},
    ToggleCollapse : function(component, event, helper) 
    { 
		helper.ToggleCollapseHandler(component, event);               
	},
	doInit : function(component, event) 
    {
        var action = component.get("c.getSezioneContatti");
        // console.log(action);
        action.setCallback(this, function(a) {
        //console.log(JSON.stringyfy(a.getReturnValue()));
        component.set("v.Sezione_Contatti__c", a.getReturnValue());
        });
        $A.enqueueAction(action);    
     },
    
     onSingleSelectChange: function(component) 
     {
         var selectValue = component.find("inputSelectSingle").get("v.value");
         console.log(selectValue);
         var action = component.get("c.getSezioneContatti");
         action.setParams(
            {
                'selectedValue' : selectValue 
            });         
         action.setCallback(this, function(a)                             
         {
            var state = a.getState();
             if(state === "SUCCESS"){
         		component.set("v.Sezione_Contatti__c", a.getReturnValue());
             }
         });
         $A.enqueueAction(action);
        
         //var resultCmp = cmp.find("singleResult");
         //resultCmp.set("v.value", selectCmp.get("v.value"));
	 }
})