({
    validateAccettaPreventivo : function(component, event, helper) {
        
        var errMsg = null;
        var Sottostato = component.get("v.contributi.Sottostato__c");
        var strNumeroTelefono = component.get("v.contributi.NumeroTelefono__c");
        var strCognome =  component.get("v.contributi.Cognome__c");
        var strNome = component.get("v.contributi.Nome__c");
        var strRagioneSociale = component.get("v.contributi.RagioneSociale__c");
        
        if((Sottostato === 'INL.052')&& ($A.util.isEmpty(strNumeroTelefono))){
            
            return 'E obbligatorio specificare il numero di telefono del contatto';
        }
        if((Sottostato === 'INL.052') && (($A.util.isEmpty(strCognome))||($A.util.isEmpty(strNome))) &&($A.util.isEmpty(strRagioneSociale))){
            
            return 'E obbligatorio specificare Nome e Cognome oppure Ragione Sociale del contatto';
        }   
        return errMsg;
    },
    
    throwErrorMsg : function(component,errMsg,helper) 
    { 
        var resultsToast = $A.get("e.force:showToast");
        if(resultsToast)
        {    
            resultsToast.setParams({"title": "Errore","message": errMsg,"type":"error","mode":"sticky"});
            resultsToast.fire();
        }            
    },
    AccettaNoErrorperform : function(component,event,helper){
        
        var contributi = component.get("v.contributi");
        var action = component.get("c.updateACCETTAZIONECONTRIBUTI");
        action.setParams({
            contributi : contributi,
        });
        action.setCallback(this,function(response){
            var state = response.getState(); 
            if(state === "SUCCESS"){
                var retValue = response.getReturnValue();
                if(retValue !== null){
                    component.set("v.contributi", retValue);
                    var cmpTarget = component.find('Modalbox');
                    var cmpBack = component.find('Modalbackdrop');
                    $A.util.removeClass(cmpBack,'slds-backdrop--open');
                    $A.util.removeClass(cmpTarget, 'slds-fade-in-open');
                    var pageurl = window.location.href;
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                        "url": pageurl
                    });
                    urlEvent.fire();
                }
            }
            
        });
        $A.enqueueAction(action);
    },
})