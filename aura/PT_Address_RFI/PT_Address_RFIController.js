({
    doinit : function(component, event, helper) {
       //debugger;
        // Set the address Type Attributes, Initialized picklist field values and assigns Values in the UI fields on Load. 
        var addt= component.get("v.addressType");
		if(addt===$A.get("$Label.c.PT_Address_Type_Fornitura")){
            component.set("v.isFornituraAddress",true);
        }
        else if(addt===$A.get("$Label.c.PT_Address_Type_Esazione")){
            component.set("v.isEsazioneAddress",true);
        }
            else if(addt===$A.get("$Label.c.PT_Address_Type_SedeLegale")){
                component.set("v.isSedeLegaleAddress",true);
            }
        
        var recType = component.get("v.recordType");
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
		          
        if(recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_AN1"))&&
           recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_PD2"))&&
           recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_PD4"))&&
           recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N01"))&&
           recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N02"))&&
           recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP2")))
        {
            component.set("v.displayPOD","true");
        }
        else{
            component.set("v.displayModel","true");
        }
        if(recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP2"))){
            component.set("v.displayaddrSP2","true");
        }
        if(recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M02"))){
            component.set("v.displayaddrM02","true");
            component.set("v.displayverifica","false");
        }
        if(recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N01"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N02")))
        {
            component.set("v.displayaddressfields","true");
        }
        if(recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N01"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N02")))
        {
            component.set("v.reqField","true");
        }
        if(recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N01"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N02"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A01"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A03"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S01"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S02")))
        {
            component.set("v.reqCAPField","true");
        }
        if(recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A01"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A03"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N01"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N02"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP2"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S01"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S02")))
        {
            component.set("v.displayEsazFields","true");
        }
        if(recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A01"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A03"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N01"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N02"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S01"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S02")))
        {
            component.set("v.displayLegaleFields","true");
        }
        if(recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_ACV"))){
            var dateFormat = "yyyy-MM-dd";
            var misuraDate = new Date();
            misuraDate.setDate(misuraDate.getDate());        
            var dateString = $A.localizationService.formatDateTime(misuraDate, dateFormat);  
            component.find("misura").set("v.value",dateString);
            
            var cambioDate = new Date();
            cambioDate.setDate(cambioDate.getDate());        
            var dateString1 = $A.localizationService.formatDateTime(cambioDate, dateFormat);  
            component.find("cambio").set("v.value",dateString1);
        }
        if(recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_ARS"))){
            var dateFormat1 = "yyyy-MM-dd";
            var misuraDate1 = new Date();
            misuraDate1.setDate(misuraDate1.getDate());
            var dateString2 = $A.localizationService.formatDateTime(misuraDate1, dateFormat1);  
            component.find("misura").set("v.value",dateString2);
        }
       
        if(recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_AVO"))){
            var dateFormat2 = "yyyy-MM-dd";
            var misuraDate2 = new Date();
            misuraDate2.setDate(misuraDate2.getDate());        
            var dateString3 = $A.localizationService.formatDateTime(misuraDate2, dateFormat2);  
            component.find("misura").set("v.value",dateString3);
            
            var volturaDate = new Date();
            volturaDate.setDate(volturaDate.getDate());
            var dateString4 = $A.localizationService.formatDateTime(volturaDate, dateFormat2);  
            component.find("voltura").set("v.value",dateString4);
        } 
        
            helper.filterPicklistValues(component, event, helper, recTypeName, 'Toponimo_pod__c','v.Toponimo','','','','');
            helper.filterPicklistValues(component, event, helper, recTypeName, 'Provincia_pod__c','v.Provincia','','','','');
            helper.filterPicklistValues(component, event, helper, recTypeName, 'Nazione_esaz__c','v.NazioneEsaz','','','','');
            helper.filterPicklistValues(component, event, helper, recTypeName, 'Nazione_pod__c','v.NazionePod','','','','');	
            helper.filterPicklistValues(component, event, helper, recTypeName, 'Trattamento__c','v.Trattamento','','','','');
             
    }, 
    
    onTrattamentoFieldChange : function(component, event, helper){
        //Fire Application event to enable fields in the PT_DatiMisura_RFI Component.
        var appEvent1 = $A.get("e.c:PT_DatiMisura_Event");
        appEvent1.setParams({
            "trattamento" : event.getSource().get("v.value") });
        appEvent1.fire(); 
    },
    
    showModal : function(component, event, helper) {
        //On click of edit icon, address detail model pop up gets displayed.
        //debugger;
        var recType = component.get("v.recordType");
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
        var requestObj = component.get("v.requestObj");
            var addt= component.get("v.addressType");
        	document.getElementById("backGroundSectionId").style.display = 'block';
        	document.getElementById(addt).style.display = 'block'; 
            component.set("v.Spinner1",true);
        
            //helper.filterPicklistValues(component, event, helper, recTypeName, 'Toponimo_pod__c','v.Toponimo','','','','');
            /*helper.filterPicklistValues(component, event, helper, recTypeName, 'Provincia_pod__c','v.Provincia','','','','');
            helper.filterPicklistValues(component, event, helper, recTypeName, 'Nazione_esaz__c','v.NazioneEsaz','','','','');
            helper.filterPicklistValues(component, event, helper, recTypeName, 'Nazione_pod__c','v.NazionePod','','','',''); */
            helper.assignFieldValues(component, event, helper); 

                if(component.get("v.isFornituraAddress") && component.get("v.displayaddrSP2")){
                    component.find("nazionePod").set("v.value","IT"); 
                    
                }
                if(component.get("v.isEsazioneAddress") && component.get("v.displayPOD")){
                    if(component.find("copyFromForniture").get("v.value") === ''){
                        component.find("copyFromForniture").set("v.value","no");
                        component.set("v.displayOption","false");
                        if(component.find("copyFromForniture").get("v.value")==="no"){
                            component.set("v.displayEsazioneAddressFields","true");
                            component.find("nazioneEsaz").set("v.value","IT");
                            component.renderEsazioneAddressFields(component, event, helper);
                        }
                    }
                } 
                if(component.get("v.isSedeLegaleAddress") && component.get("v.displayPOD")){
                    if(component.find("copyFromForniture").get("v.value") === ''){
                       // component.find("copyFromForniture").set("v.value","no");
                        //component.set("v.displayOption","false");
                        if(component.find("copyFromForniture").get("v.value")==="no"){
                            component.set("v.displayEsazioneAddressFields","true");
                            component.find("nazioneEsaz").set("v.value","IT");    
                        }
                    }
                } 
     },
    
    hideModal : function(component, event, helper) {
        // This method is invoked on click of save and close button on address popup. It does all the 
        // address field validations prior to saving the address.
        debugger;
        var errorMsg = null;
        if(((component.get("v.addressType")===$A.get("$Label.c.PT_Address_Type_Esazione")) || (component.get("v.addressType")===$A.get("$Label.c.PT_Address_Type_SedeLegale")))){
            //added by Priyanka
            var dispOptValue=component.get("v.displayOption");
            console.log('dispOptValue-->'+dispOptValue);
            if(dispOptValue===true){
            var copyFromAddress = component.find("copyFromForniture").get("v.value");
            }
            else{
                copyFromAddress="no";
            }
            ///
            var inputCmp;
           
            if($A.util.isEmpty(copyFromAddress)){
                // Display and Error if Copy from picklist field is blank
                errorMsg = 'Seleziona un valore.';
                inputCmp = component.find("copyFromForniture");
                inputCmp.set("v.errors", [{message:errorMsg}]);
            }
            else if(copyFromAddress==="si"){
                inputCmp = component.find("copyFromForniture");
                inputCmp.set("v.errors", null);            
            }
                else if(copyFromAddress==="no"){
                    errorMsg = helper.validateAddressFields(component, event, helper);    
                }
        }
        else{
            component.set("v.bypassCodiceViaCheck","false");
            // invoke validation checks across all the address fields
            errorMsg = helper.validateAddressFields(component, event, helper);
        }
        if(!$A.util.isEmpty(errorMsg))
        {
            $A.enqueueAction(action); 
        }
        else
        {
            var requestObj = component.get("v.requestObj");
            var addt= component.get("v.addressType");
            //If all validations are passed then copy the field values to the instance of request object fields.
            if(addt===$A.get("$Label.c.PT_Address_Type_Fornitura")){
                helper.setFornituraFields(component, event, helper);
            }
            else if(addt===$A.get("$Label.c.PT_Address_Type_Esazione")){
                //debugger;
                helper.setEsazioneFields(component, event, helper);
            }
                else if(addt===$A.get("$Label.c.PT_Address_Type_SedeLegale")){
                    helper.setSedeLegaleFields(component, event, helper);
                }
            
            component.set("v.displayAddress",false);
            
            document.getElementById("backGroundSectionId").style.display = "none";
            document.getElementById(addt).style.display = "none";
        }    
    },
    hideAddressModal : function(component, event, helper) {
        document.getElementById("backGroundAddressSectionId").style.display = "none";
        document.getElementById("newAddressSectionId").style.display = "none";
        document.getElementById("backGroundSectionId").style.display = "block";
        document.getElementById(component.get("v.addressType")).style.display = "block";
    },
    closeModal : function(component, event, helper) {
        //On click of close icon, address detail model pop up gets closed.
        var addt= component.get("v.addressType");
        document.getElementById("backGroundSectionId").style.display = "none";
        document.getElementById(addt).style.display = "none";
    },
    deleteAddress : function(component, event, helper) {
        //On click of delete icon, address details are reset.
        var finalAddress = null;
        if(component.get("v.isFornituraAddress")){
            helper.deleteFornitureFields(component, finalAddress, helper);
        }
        if(component.get("v.isEsazioneAddress")){
            //debugger;
            helper.deleteEsazioneFields(component, finalAddress, helper);
        } 
        if(component.get("v.isSedeLegaleAddress")){
            helper.deleteSedeLegaleFields(component, finalAddress, helper);
        }
    },
    verifyAddress : function(component, event, helper){
        //Ref: 420_PT_Logiche_pagine_Inserimento (Common/VERIND_1)
        //debugger;
        var errorMsg = null;
        component.set("v.bypassCodiceViaCheck","true");
        errorMsg = helper.validateAddressFields(component, event, helper); 
        if(!$A.util.isEmpty(errorMsg))
        {
            //$A.enqueueAction(action); 
        }
        else
        {
            var action = component.get("c.retriveAddress");
            action.setParams({
                comune : component.find("comunePod").get("v.value"), 
                cap : component.find("capPod").get("v.value"), 
                via : component.find("viaPod").get("v.value"), 
                codistat : component.find("codIstatPod").get("v.value")
            });
            
            action.setCallback(this,function(a){
                var state = a.getState();
                
                if(state === "SUCCESS"){
                    //debugger;
                    var retValue = a.getReturnValue();
                    if(!$A.util.isEmpty(retValue)){
                        component.set("v.addressList",retValue);
                        component.set("v.displayAddress",true);
                        document.getElementById("backGroundAddressSectionId").style.display = "block";
                        document.getElementById("newAddressSectionId").style.display = "block";
                        document.getElementById("backGroundSectionId").style.display = "none";
                        document.getElementById(component.get("v.addressType")).style.display = "none";
                    }
                    else
                    {
                        helper.throwErrorMsg(component,"Nessun Indirizzo Inserito",helper);
                    }
                    
                }    
            });
            $A.enqueueAction(action); 
        }   
    },
    selectAddress : function(component, event, helper){
        //Ref: 420_PT_Logiche_pagine_Inserimento (Common/VERIND_1)
        var requestObj = component.get("v.requestObj");
        var selected = event.target.getAttribute("data-recId");
        if(!component.get("v.displayaddrSP2")){
            component.find("PPAAAVVVPod").set("v.value",selected);
        }
        requestObj.Toponimo_pod__c = component.find("toponimoPod").get("v.value");
        requestObj.Via_pod__c = component.find("viaPod").get("v.value");
        requestObj.Numero_Civico_pod__c = component.find("numeroCivicoPod").get("v.value");
        requestObj.Provincia_pod__c = component.find("provinciaPod").get("v.value");
        requestObj.Comune_pod__c = component.find("comunePod").get("v.value");
        requestObj.Localita_pod__c = component.find("localitaPod").get("v.value");
        requestObj.Cap_pod__c = component.find("capPod").get("v.value");
        requestObj.CodIstat_pod__c = component.find("codIstatPod").get("v.value");
        if(!component.get("v.displayaddrSP2")){
            requestObj.Scala_pod__c = component.find("scalaPod").get("v.value");
            requestObj.Piano_pod__c = component.find("pianoPod").get("v.value");
            requestObj.Interno_pod__c = component.find("internoPod").get("v.value");
            requestObj.PPAAAVVV_pod__c = component.find("PPAAAVVVPod").get("v.value");
        }
        
        component.set("v.requestObj",requestObj);
        
        requestObj = component.get("v.requestObj");
        var toponimo = requestObj.Toponimo_pod__c;
        var via = requestObj.Via_pod__c;        
        var numeroCivico = requestObj.Numero_Civico_pod__c;
        var CAP = requestObj.Cap_pod__c;
        var Comune = requestObj.Comune_pod__c;
        var Provincia = requestObj.Provincia_pod__c;
        var finalAddress = toponimo + " " + via + ", " + numeroCivico + ", " + CAP + ", " + Comune + ", " + Provincia;
        
        
        component.set("v.addressSummary",finalAddress);
        component.set("v.displayAddress",false);
        document.getElementById("backGroundSectionId").style.display = "block";
        document.getElementById(component.get("v.addressType")).style.display = "block";
        document.getElementById("backGroundAddressSectionId").style.display = "none";
        document.getElementById("newAddressSectionId").style.display = "none";
    },
    renderComuneCodIstat : function(component, event, helper){
        //@reference:		420_PT_Logiche_pagine_Inserimento (Common/FORN_2)
        
        if(!$A.util.isEmpty(component.find("provinciaPod").get("v.value"))){
            var action = component.get("c.getComumneCodIstatPicklistValues");
            action.setParams({
                provinciaPod : component.find("provinciaPod").get("v.value")
            });
            
            action.setCallback(this,function(a){
                var state = a.getState();
                
                if(state === "SUCCESS"){
                    var retValue = a.getReturnValue();
                    if(!$A.util.isEmpty(retValue)){
                        var pickListValue = a.getReturnValue().pickListValues;
                        component.set('v.Comune', pickListValue['Comune_pod__c']); 
                        component.set('v.CodIstat', pickListValue['CodIstat_pod__c']); 
                        component.set("v.CAP","");
                        component.find("capPod").set("v.value","");
                        component.set("v.comuneReadOnly",false);
                        component.set("v.codiceISTATReadOnly",false);
                        component.set("v.capReadOnly",true);
                        component.set("v.Spinner", false);
                    }
                }    
            });
            $A.enqueueAction(action);
        }
        else{
            component.set("v.comuneReadOnly",true);
            component.set("v.codiceISTATReadOnly",true);
            component.set("v.capReadOnly",true);
        }
    },
    setCAPCodIstat : function(component, event, helper){
        //@reference:		420_PT_Logiche_pagine_Inserimento (Common/FORN_2)
        
        if(!$A.util.isEmpty(component.find("comunePod").get("v.value"))){
            component.set("v.codiceISTATReadOnly",true);
            var action = component.get("c.setCAPCodIstatFields");
            action.setParams({
                comunePodVal : component.find("comunePod").get("v.value")
            });
            
            action.setCallback(this,function(a){
                var state = a.getState();
                
                if(state === "SUCCESS"){
                    var retValue = a.getReturnValue();
                    if(!$A.util.isEmpty(retValue)){
                        component.find("codIstatPod").set("v.value",retValue.COD_ISTAT__c);
                        helper.setCAP(component, event, helper);
                    }
                }    
            });
            $A.enqueueAction(action);  
        }
        else{
            component.set("v.codiceISTATReadOnly",false);
        }
    },
    setComuneCodIstat : function(component, event, helper){
        //@reference:		420_PT_Logiche_pagine_Inserimento (Common/FORN_2)
        if(!$A.util.isEmpty(component.find("codIstatPod").get("v.value"))){
            component.set("v.comuneReadOnly",true);
            var action = component.get("c.setComuneCodIstatFields");
            
            action.setParams({
                codIstatPodVal : component.find("codIstatPod").get("v.value")
            });
            
            action.setCallback(this,function(a){
                var state = a.getState();
                
                if(state === "SUCCESS"){
                    var retValue = a.getReturnValue();
                    if(!$A.util.isEmpty(retValue)){
                        component.find("comunePod").set("v.value",retValue.COMUNE__c);
                        helper.setCAP(component, event, helper);
                    }
                }    
            });
            $A.enqueueAction(action);  
        }
        else{
            component.set("v.comuneReadOnly",false);
        }
        
    },
    renderEsazioneAddressFields : function(component, event, helper){
        //@reference:		420_PT_Logiche_pagine_Inserimento (Common/ESA_3)
        debugger;
        var requestObj = component.get("v.requestObj");
        
        if(!$A.util.isEmpty(component.find("copyFromForniture").get("v.value"))){
            if(component.find("copyFromForniture").get("v.value")==="no"){
                //debugger;
                component.set("v.displayEsazioneAddressFields","true");
                var podonLoadvar = component.get("v.podonLoad");
                component.find("toponimoPod").set("v.value", podonLoadvar);
                var viaUpdatevar = component.get("v.viaupdate");
                component.find("viaPod").set("v.value", viaUpdatevar);
                var numCivicovar = component.get("v.numcivicoupdate");
                component.find("numeroCivicoPod").set("v.value", numCivicovar);
                //var provinciavar = component.get("v.provinciaLoad");
                //component.find("provinciaPod").set("v.value", provinciavar);
                //if(!$A.util.isUndefined(provinciavar)){
                    //if(!$A.util.isEmpty(component.find("provinciaPod").get("v.value")))
                    //this.renderComuneCodIstat(component, event, helper);
                //}
                //var comunevar = component.get("v.comuneLoad");
                //component.find("comunePod").set("v.value", comunevar);
                var localitavar = component.get("v.localitaupdate");
                component.find("localitaPod").set("v.value", localitavar);
                //var capPodvar = component.get("v.capupdate");
                //component.find("capPodText").set("v.value", capPodvar);
                //var codIstatvar = component.get("v.coistatupdate");
                //component.find("codIstatPod").set("v.value", codIstatvar);                
                component.find("nazioneEsaz").set("v.value","IT");
            }
            else{
                component.set("v.displayEsazioneAddressFields","false");
            }
        }
        else{
            component.set("v.displayEsazioneAddressFields","false");
        }
    },
    checkNazione : function(component, event, helper){
        //@reference:		420_PT_Logiche_pagine_Inserimento (Common/ESA_3)
        
        if(!$A.util.isEmpty(component.find("copyFromForniture").get("v.value"))){
            if(component.find("nazioneEsaz").get("v.value")!=="IT"){
                component.set("v.disableEsazioneAddressFields",true);
                component.set("v.capReadOnly",false);                
            }
            else{
                component.set("v.disableEsazioneAddressFields",false);
                component.set("v.capReadOnly",true); 
            }
        }
    }, 
    
    checkNazioneFornitura : function(component, event, helper){
        //@reference:		420_PT_Logiche_pagine_Inserimento (SPI_6)
        
        if(component.find("nazionePod").get("v.value")!=="IT"){
            component.set("v.disableEsazioneAddressFields",true);
            component.set("v.capReadOnly",false);                
        }
        else{
            component.set("v.disableEsazioneAddressFields",false);
            component.set("v.capReadOnly",true); 
        }
    },
    
    checkPodFormat : function(component, event, helper){
        var pod_val = component.find("pod").get("v.value");
        var podFormat = new RegExp($A.get("$Label.c.PT_RICHIESTE_POD_REGEX"));
        //alert('pod_val :: ' + pod_val);
        if(pod_val!==null && component.get("v.displayPOD") &&
           component.get("v.addressType")===$A.get("$Label.c.PT_Address_Type_Fornitura")){
            var validatePodFormat = podFormat.test(pod_val);
            if(validatePodFormat){
                var inputCmp = component.find("pod");
                inputCmp.set("v.errors", null);
            }else{
                var errMsg = 'Errore di controllo sul formato per i seguenti campi: POD' ;
                var inputCmp1 = component.find("pod");
                inputCmp1.set("v.errors", [{message:errMsg}]);
                return;
            }
        }
    },
    
    checkCAPFormat : function(component, event, helper){
        var recType = component.get("v.recordType");
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
        
        var cap_val = null;
        var inputCmp = null;
        var inputCmp1 = null;
        
        if(recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M02"))){
            
            if(component.get("v.disableEsazioneAddressFields")){
                cap_val = component.find("capPodText").get("v.value");
            }
            else
            {
                cap_val = component.find("capPod").get("v.value");
            }
            
            var capFormat = new RegExp($A.get("$Label.c.PT_RICHIESTE_CAP_REGEX"));
            
            if(cap_val!==null && component.get("v.displayFornitura") && 
               component.get("v.addressType")===$A.get("$Label.c.PT_Address_Type_Fornitura")){
                var validateCAPFormat = capFormat.test(cap_val);
                if(validateCAPFormat){
                    if(component.get("v.disableEsazioneAddressFields")){
                        inputCmp = component.find("capPodText");
                    }
                    else
                    {
                        inputCmp = component.find("capPod");
                    }
                    inputCmp.set("v.errors", null);
                }else{
                    var errMsg = 'Controllare la validità del campo CAP' ;
                    if(component.get("v.disableEsazioneAddressFields")){
                        inputCmp1 = component.find("capPodText");
                    }
                    else
                    {
                        inputCmp1 = component.find("capPod");
                    }
                    inputCmp1.set("v.errors", [{message:errMsg}]);
                }
            }
        }
    }, 
    
    checkProvinciaFormat : function(component, event, helper){
        var recType = component.get("v.recordType");
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
        
        if(recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M02"))){
            var provincia = component.find("provinciaPod").get("v.value");
            var provinciaFormat = new RegExp($A.get("$Label.c.PT_RICHIESTE_Provincia_REGEX"));
            
            if(provincia!==null && component.get("v.displayFornitura") && 
               component.get("v.addressType")===$A.get("$Label.c.PT_Address_Type_Fornitura")){
                var validateProvinciaFormat = provinciaFormat.test(provincia);
                if(validateProvinciaFormat){
                    var inputCmp = component.find("provinciaPod");
                    inputCmp.set("v.errors", null);
                }else{
                    var errMsg = 'Controllare la validità del campo Provincia' ;
                    var inputCmp1 = component.find("provinciaPod");
                    inputCmp1.set("v.errors", [{message:errMsg}]);
                }
            }
        }
    }, 
    
    onClick: function (component, event, helper) {
        var selected = event.target.value;
       // alert('buttonclick'+selected);
        component.set("v.addselection",selected);

        if(selected==="PodAddress"){
            component.set("v.displayPODsection","true");
            component.set("v.displayPOD", "true");
            component.set("v.displayFornitura","false");
            
        }
        
        if(selected==="IndirizzoFornitura"){
            component.set("v.displayFornitura","true");
            component.set("v.displayPODsection","false");
            component.set("v.displayPOD", "false");
        }
        
    },
    fetchpicklist: function (component, event, helper) {
        var recType = component.get("v.recordType");
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
        if(!component.get("v.toponimoPodload")){
     		//helper.filterPicklistValues(component, event, helper, recTypeName, 'Toponimo_pod__c','v.Toponimo','','','','');
        	component.set("v.toponimoPodload",true);
        }   
    }
})