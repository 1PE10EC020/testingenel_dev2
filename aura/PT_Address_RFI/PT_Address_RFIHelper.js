({
    assignFieldValues :function(component, event, helper)
    {
        //debugger;
        var recType = component.get("v.recordType");
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
        var requestObj = component.get("v.requestObj");
        component.set("v.toponimoPod", requestObj.Toponimo_pod__c);
        component.set("v.viaPod", requestObj.Via_pod__c);
        component.set("v.numeroCivicoPod", requestObj.Numero_Civico_pod__c);
        component.set("v.scalaPod", requestObj.Scala_pod__c);
        component.set("v.pianoPod", requestObj.Piano_pod__c);
        component.set("v.internoPod", requestObj.Interno_pod__c);
        component.set("v.provinciaPod", requestObj.Provincia_pod__c);
        component.set("v.comunePod", requestObj.Comune_pod__c);
        component.set("v.localitaPod", requestObj.Localita_pod__c);	
        component.set("v.capPod", requestObj.Cap_pod__c);
        component.set("v.codIstatPod", requestObj.CodIstat_pod__c);
        if(component.get("v.displayPOD") && component.get("v.addressType")===$A.get("$Label.c.PT_Address_Type_Fornitura")){
            component.set("v.pod", requestObj.POD__c); 
        }
        
        if((recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_ARS")) || 
            recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_AUT"))) && 
           component.get("v.addressType")===$A.get("$Label.c.PT_Address_Type_Fornitura")){
            //debugger;
            component.set("v.Trattamento__c", requestObj.Trattamento__c); 
        }
    },
    
    validateAddressFields :function(component, event, helper, addressType)
    {
        //Ref: 420_PT_ControlliPrevalidazione (Common Fields)
        debugger;
        var errMsg = null;
        var inputCmp = null;
        var CAP = null;
        var hasError = false;
        var requestObj = component.get('v.requestObj'); 
        var toponimo = component.find("toponimoPod").get("v.value");
        var via = component.find("viaPod").get("v.value");        
        var numeroCivico = component.find("numeroCivicoPod").get("v.value");
        if(component.get("v.disableEsazioneAddressFields")){
            CAP = component.find("capPodText").get("v.value");
        }
        else
        {
            CAP = component.find("capPod").get("v.value");
        }
        if(!component.get("v.displayaddrM02")){
            var Comune = component.find("comunePod").get("v.value");
            var codIstat = component.find("codIstatPod").get("v.value");
        }
        var Provincia = component.find("provinciaPod").get("v.value");
        var Localitapod = component.find("localitaPod").get("v.value");
        
        if(component.get("v.addressType")===$A.get("$Label.c.PT_Address_Type_Fornitura") && component.get("v.displayPOD") && !component.get("v.displayaddrM02")){
            var pod = component.find("pod").get("v.value");
        }
        if(component.get("v.addressType")===$A.get("$Label.c.PT_Address_Type_Fornitura") && component.get("v.displayaddrSP2")){
            var NazionePod = component.find("nazionePod").get("v.value");
        }
        if(component.get("v.addressType")===$A.get("$Label.c.PT_Address_Type_Esazione") && component.get("v.displayaddrSP2")){
            var NazioneEsaz = component.find("nazioneEsaz").get("v.value");
        }
        
        var addressLabel = "";
        if(component.get("v.addressType")===$A.get("$Label.c.PT_Address_Type_Fornitura")){
            addressLabel = $A.get("$Label.c.PT_Address_Label_Fornitura");
        }
        else if(component.get("v.addressType")===$A.get("$Label.c.PT_Address_Type_Esazione")){
            addressLabel = $A.get("$Label.c.PT_Address_Label_Esazione");
        }
            else if(component.get("v.addressType")===$A.get("$Label.c.PT_Address_Type_SedeLegale")){
                addressLabel = $A.get("$Label.c.PT_Address_Label_SedeLegale");
            }
        errMsg = $A.get("$Label.c.PT_Address_Page_Validation_Message") + ' ' + addressLabel +': ';
        
        if(component.get("v.addressType")===$A.get("$Label.c.PT_Address_Type_Fornitura") && component.get("v.displayaddrSP2")){
            if((component.find("nazionePod").get("v.value") === 'IT') && ($A.util.isEmpty(toponimo) 
                                                                          || $A.util.isEmpty(Provincia) || $A.util.isEmpty(CAP) || $A.util.isEmpty(Comune) || $A.util.isEmpty(codIstat)))
            {
                errMsg = 'In caso di Impianto Ubicato in Italia è necessario valorizzare i campi Toponimo in Impianto, Provincia in Impianto, Cap in Impianto, Comune in Impianto, Istat in Impianto.';
                hasError = true;
                helper.throwErrorMsg(component,errMsg,helper) ;
                return errMsg;
            }            
        }
        if(component.get("v.addressType")===$A.get("$Label.c.PT_Address_Type_Esazione") && component.get("v.displayaddrSP2")){
            if((component.find("copyFromForniture").get("v.value")==="no") && ($A.util.isEmpty(via) 
                                                                               || $A.util.isEmpty(numeroCivico)) && $A.util.isEmpty(NazioneEsaz))
            {
                errMsg = 'In caso di Impianto Ubicato in Italia è necessario valorizzare i campi Toponimo in Impianto, Provincia in Impianto, Cap in Impianto, Comune in Impianto, Istat in Impianto.';
                hasError = true;
                helper.throwErrorMsg(component,errMsg,helper) ;
                return errMsg;
            }
            else if((component.find("nazioneEsaz").get("v.value") === 'IT') && ($A.util.isEmpty(toponimo) 
                                                                                || $A.util.isEmpty(Provincia) || $A.util.isEmpty(CAP) || $A.util.isEmpty(Comune) || $A.util.isEmpty(codIstat)))
            {
                errMsg = 'Se l\'indirizzo esazione è Ubicato in Italia è necessario valorizzare i campi Toponimo in Impianto, Provincia in Impianto, Cap in Impianto, Comune in Impianto, Istat in Impianto.';
                hasError = true;
                helper.throwErrorMsg(component,errMsg,helper) ;
                return errMsg;
            }             
        }
        if($A.util.isEmpty(toponimo)){
            errMsg = errMsg + 'Toponimo' + ', ';
            hasError = true;
        }
        if($A.util.isEmpty(via)){
            errMsg = errMsg + 'Via' + ', ';
            hasError = true;
        }
        if($A.util.isEmpty(numeroCivico)){
            errMsg = errMsg + 'Numero Civico' + ', ';
            hasError = true;
        }
        if($A.util.isEmpty(CAP)){
            errMsg = errMsg + 'CAP' + ', ';
            hasError = true;
        }
        if($A.util.isEmpty(Comune) && !component.get("v.disableEsazioneAddressFields") && !component.get("v.displayaddrM02")){
            errMsg = errMsg + 'Comune' + ', ';
            hasError = true;
        }
        if($A.util.isEmpty(Provincia) && !component.get("v.disableEsazioneAddressFields")){
            errMsg = errMsg + 'Provincia' + ', ';
            hasError = true;
        }
        if($A.util.isEmpty(Localitapod)){
            errMsg = errMsg + 'Località' + ', ';
            hasError = true;
        }
        if($A.util.isEmpty(codIstat) && !component.get("v.disableEsazioneAddressFields") && !component.get("v.displayaddrM02")){
            errMsg = errMsg + 'Codice Istat' + ', ';
            hasError = true;
        }
        if(hasError === true){
            errMsg = errMsg.slice(0, -2) + '.';
            helper.throwErrorMsg(component,errMsg,helper) ;
            return errMsg;
        }
        if(component.get("v.addressType")===$A.get("$Label.c.PT_Address_Type_Esazione") || component.get("v.addressType")===$A.get("$Label.c.PT_Address_Type_SedeLegale")){
            var Nazione = component.find("nazioneEsaz").get("v.value");
            if($A.util.isEmpty(Nazione)){
                errMsg = 'Valorizzare la nazione per l\'indirizzo di ' + addressLabel;
                hasError = true;
                helper.throwErrorMsg(component,errMsg,helper) ;
                return errMsg;
            }            
        }
        if(component.get("v.addressType")===$A.get("$Label.c.PT_Address_Type_Fornitura") && component.get("v.bypassCodiceViaCheck")==="false" && (!component.get("v.displayaddrSP2") && !component.get("v.displayaddrM02"))){
            
            var codiceVia = component.find("PPAAAVVVPod").get("v.value");
            if($A.util.isEmpty(codiceVia) || codiceVia==="undefined"){
                errMsg = "E' necessario utilizzare la funzionalità di Verifica Indirizzo per determinare un Codice Via per l'indirizzo di fornitura.";
                hasError = true;
                helper.throwErrorMsg(component,errMsg,helper) ;
                return errMsg;                
            }            
        }
        else if(component.get("v.addressType")===$A.get("$Label.c.PT_Address_Type_Fornitura") && component.get("v.bypassCodiceViaCheck")==="true" && (!component.get("v.displayaddrSP2") && !component.get("v.displayaddrM02"))){
            inputCmp = component.find("PPAAAVVVPod");
            inputCmp.set("v.errors", null);     
        }
        if(hasError === false){
            return null;
        }        	
        else{
            return errMsg;
        }
    },
    throwErrorMsg : function(component,errMsg,helper) 
    {
        // This method throws the toast message on any validation error on any field
        var resultsToast = $A.get("e.force:showToast");
        if(resultsToast)
        {    
            resultsToast.setParams({"title": "Errore","message": errMsg,"type":"error", "mode":"sticky"});
            resultsToast.fire();
        }            
    },
    setFornituraFields : function(component, event, helper){
        var requestObj = component.get("v.requestObj");
        
        requestObj.Toponimo_pod__c = component.find("toponimoPod").get("v.value");
        requestObj.Via_pod__c = component.find("viaPod").get("v.value");
        requestObj.Numero_Civico_pod__c = component.find("numeroCivicoPod").get("v.value");
        requestObj.Provincia_pod__c = component.find("provinciaPod").get("v.value");
        requestObj.Comune_pod__c = component.find("comunePod").get("v.value");
        requestObj.Localita_pod__c = component.find("localitaPod").get("v.value");
        requestObj.Cap_pod__c = component.find("capPod").get("v.value");
        requestObj.CodIstat_pod__c = component.find("codIstatPod").get("v.value");
        if(!component.get("v.displayaddrSP2")){
            requestObj.Scala_pod__c = component.find("scalaPod").get("v.value");
            requestObj.Piano_pod__c = component.find("pianoPod").get("v.value");
            requestObj.Interno_pod__c = component.find("internoPod").get("v.value");
            if(!component.get("v.displayaddrM02")){
            	requestObj.PPAAAVVV_pod__c = component.find("PPAAAVVVPod").get("v.value");
            }
        }
        requestObj.Nazione_pod__c = 'IT';
        
        component.set("v.requestObj",requestObj);
        
        var finalAddress = requestObj.Toponimo_pod__c + " " 
        + requestObj.Via_pod__c + ", " 
        + requestObj.Numero_Civico_pod__c + ", " 
        + requestObj.Cap_pod__c + ", " 
        + requestObj.Comune_pod__c + ", " 
        + requestObj.Provincia_pod__c; 
        
        if(finalAddress.includes("undefined")===false){
            component.set("v.addressSummary",finalAddress);    
        }
    },
    setEsazioneFields : function(component, event, helper){
        debugger;
        var requestObj = component.get("v.requestObj");
        //added by Priyanka  
        var dispOptValue=component.get("v.displayOption");
        console.log('dispOptValue-->'+dispOptValue);
        if(dispOptValue===true){
            var copyFromAddress = component.find("copyFromForniture").get("v.value");
        }
        else{
            copyFromAddress="no";
        }
        ////  
        // if(component.find("copyFromForniture").get("v.value")==="no"){
        // added by Priyanka
        if(copyFromAddress==="no"){
            debugger;
            requestObj.Toponimo_esaz__c = component.find("toponimoPod").get("v.value");
            requestObj.Via_esaz__c = component.find("viaPod").get("v.value");
            requestObj.NumCivico_esaz__c = component.find("numeroCivicoPod").get("v.value");
            requestObj.Presso_esaz__c = component.find("pressoEsaz").get("v.value");
            requestObj.Provincia_esaz__c = component.find("provinciaPod").get("v.value");
            requestObj.Comune_esaz__c = component.find("comunePod").get("v.value");
            requestObj.Localita_esaz__c = component.find("localitaPod").get("v.value");
            
            if(component.get("v.disableEsazioneAddressFields")){
                requestObj.Cap_esaz__c = component.find("capPodText").get("v.value");
            }
            else{
                requestObj.Cap_esaz__c = component.find("capPod").get("v.value");
            }
            requestObj.CodIstat_esaz__c = component.find("codIstatPod").get("v.value");
            requestObj.Nazione_esaz__c = 'IT';
        }
        //  else if(component.find("copyFromForniture").get("v.value")==="si"){
        //  added by priyanka
        else if(copyFromAddress==="si"){
            debugger;
            requestObj.Toponimo_esaz__c = requestObj.Toponimo_pod__c;
            requestObj.Via_esaz__c = requestObj.Via_pod__c;
            requestObj.NumCivico_esaz__c = requestObj.Numero_Civico_pod__c;
            requestObj.Provincia_esaz__c = requestObj.Provincia_pod__c;
            requestObj.Comune_esaz__c = requestObj.Comune_pod__c;
            requestObj.Localita_esaz__c = requestObj.Localita_pod__c;
            requestObj.Cap_esaz__c = requestObj.Cap_pod__c;
            requestObj.CodIstat_esaz__c = requestObj.CodIstat_pod__c;
            requestObj.Nazione_esaz__c = requestObj.Nazione_pod__c;
            component.set("v.podonLoad",requestObj.Toponimo_esaz__c);  
            component.set("v.viaupdate",requestObj.Via_esaz__c);  
            component.set("v.numcivicoupdate",requestObj.NumCivico_esaz__c);  
            //component.set("v.provinciaLoad",requestObj.Provincia_esaz__c);
            //component.set("v.comuneLoad",requestObj.Comune_esaz__c);
            component.set("v.localitaupdate",requestObj.Localita_esaz__c);
            //component.set("v.capupdate",requestObj.Comune_esaz__c);
            //component.set("v.coistatupdate",requestObj.Comune_esaz__c);
        }
        component.set("v.requestObj",requestObj);
        var finalAddress =  requestObj.Toponimo_esaz__c + " " 
        + requestObj.Via_esaz__c + ", " 
        + requestObj.NumCivico_esaz__c + ", " 
        + requestObj.Cap_esaz__c + ", " 
        + requestObj.Comune_esaz__c + ", " 
        + requestObj.Provincia_esaz__c; 
        console.log('in final address');
        finalAddress = finalAddress.replace(/undefined/g, "");
        if(finalAddress.includes("undefined")===false){
            component.set("v.addressSummary",finalAddress);    
        }
        
    },
    setSedeLegaleFields : function(component, event, helper){
        var requestObj = component.get("v.requestObj");
        //added by Priyanka  
        var dispOptValue=component.get("v.displayOption");
        console.log('dispOptValue-->'+dispOptValue);
        if(dispOptValue===true){
            var copyFromAddress = component.find("copyFromForniture").get("v.value");
        }
        else{
            copyFromAddress="no";
        }
        ////
        // if(component.find("copyFromForniture").get("v.value")==="no"){
        if(copyFromAddress==="no"){
            requestObj.Toponimo_cl__c = component.find("toponimoPod").get("v.value");
            requestObj.Via_cl__c = component.find("viaPod").get("v.value");
            requestObj.NumCivico_cl__c = component.find("numeroCivicoPod").get("v.value");
            requestObj.Provincia_cl__c = component.find("provinciaPod").get("v.value");
            requestObj.Comune_cl__c = component.find("comunePod").get("v.value");
            requestObj.Localita_cl__c = component.find("localitaPod").get("v.value");
            
            if(component.get("v.disableEsazioneAddressFields")){
                requestObj.Cap_cl__c = component.find("capPodText").get("v.value");
            }
            else{
                requestObj.Cap_cl__c = component.find("capPod").get("v.value");
            }
            requestObj.CodIstat_cl__c = component.find("codIstatPod").get("v.value");
            requestObj.Nazione_cl__c = 'IT';
        }
        // else if(component.find("copyFromForniture").get("v.value")==="si"){
        else if(copyFromAddress==="si"){
            requestObj.Toponimo_cl__c = requestObj.Toponimo_esaz__c;
            requestObj.Via_cl__c = requestObj.Via_esaz__c;
            requestObj.NumCivico_cl__c = requestObj.NumCivico_esaz__c;
            requestObj.Provincia_cl__c = requestObj.Provincia_esaz__c;
            requestObj.Comune_cl__c = requestObj.Comune_esaz__c;
            requestObj.Localita_cl__c = requestObj.Localita_esaz__c;
            requestObj.Cap_cl__c = requestObj.Cap_esaz__c;
            requestObj.CodIstat_cl__c = requestObj.CodIstat_esaz__c;
            requestObj.Nazione_cl__c = requestObj.Nazione_esaz__c;
            component.set("v.podonLoad",requestObj.Toponimo_cl__c);  
            component.set("v.viaupdate",requestObj.Via_cl__c);  
            component.set("v.numcivicoupdate",requestObj.NumCivico_cl__c);  
            //component.set("v.provinciaLoad",requestObj.Provincia_cl__c);
            //component.set("v.comuneLoad",requestObj.Comune_cl__c);
            component.set("v.localitaupdate",requestObj.Localita_cl__c);
            //component.set("v.capupdate",requestObj.Cap_cl__c);
            //component.set("v.coistatupdate",requestObj.CodIstat_cl__c);
        }
        component.set("v.requestObj",requestObj);
        var finalAddress =  requestObj.Toponimo_cl__c + " " 
        + requestObj.Via_cl__c + ", " 
        + requestObj.NumCivico_cl__c + ", " 
        + requestObj.Cap_cl__c + ", " 
        + requestObj.Comune_cl__c + ", " 
        + requestObj.Provincia_cl__c; 
        console.log('>>> Final Address :: ' + finalAddress);
        finalAddress = finalAddress.replace(/undefined/g, "");
        if(finalAddress.includes("undefined")===false){
            component.set("v.addressSummary",finalAddress);    
        }
        
    },
    setCAP : function(component, event, helper){
        if(!$A.util.isEmpty(component.find("comunePod").get("v.value")) || 
           !$A.util.isEmpty(component.find("codIstatPod").get("v.value"))){
            var action = component.get("c.setCAPFieldValue");
            
            action.setParams({
                comunePodVal : component.find("comunePod").get("v.value"),
                codIstatPodVal : component.find("codIstatPod").get("v.value")
            });
            
            action.setCallback(this,function(a){
                var state = a.getState();
                
                if(state === "SUCCESS"){
                    var retValue = a.getReturnValue();
                    if(!$A.util.isEmpty(retValue)){
                        var pickListValue = a.getReturnValue().pickListValues;
                        component.set("v.CAP", pickListValue['Cap_pod__c']); 
                        component.set("v.capReadOnly",false);
                    }
                }    
            });
            $A.enqueueAction(action);
        }
    },
    
    deleteFornitureFields : function(component, finalAddress, helper){
        var requestObj = component.get("v.requestObj");            
        requestObj.Toponimo_pod__c = component.find("toponimoPod").set("v.value","");
        requestObj.Via_pod__c = component.find("viaPod").set("v.value","");
        requestObj.Numero_Civico_pod__c = component.find("numeroCivicoPod").set("v.value","");
        requestObj.Provincia_pod__c = component.find("provinciaPod").set("v.value","");
        requestObj.Comune_pod__c = component.find("comunePod").set("v.value","");
        requestObj.Localita_pod__c = component.find("localitaPod").set("v.value","");
        requestObj.Cap_pod__c = component.find("capPod").set("v.value","");
        requestObj.CodIstat_pod__c = component.find("codIstatPod").set("v.value","");
        
        if(!component.get("v.displayaddrSP2")){
            requestObj.Scala_pod__c = component.find("scalaPod").set("v.value","");
            requestObj.Piano_pod__c = component.find("pianoPod").set("v.value","");
            requestObj.Interno_pod__c = component.find("internoPod").set("v.value","");
            if(!component.get("v.displayaddrM02")){
                requestObj.PPAAAVVV_pod__c = component.find("PPAAAVVVPod").set("v.value","");
            }
        }
        
        component.set("v.requestObj",requestObj);
        //debugger;
        requestObj = component.get("v.requestObj");
        var toponimo = requestObj.Toponimo_pod__c;
        var via = requestObj.Via_pod__c;        
        var numeroCivico = requestObj.Numero_Civico_pod__c;
        var CAP = requestObj.Cap_pod__c;
        var Comune = requestObj.Comune_pod__c;
        var Provincia = requestObj.Provincia_pod__c;
        
        if($A.util.isUndefined(toponimo) && $A.util.isUndefined(via) && $A.util.isUndefined(numeroCivico) && 
           $A.util.isUndefined(CAP) && $A.util.isUndefined(Comune) && $A.util.isUndefined(Provincia)){
            finalAddress = $A.get("$Label.c.PT_NoAddress"); 
            component.set("v.comuneReadOnly",true);
            component.set("v.CAP", ""); 
            component.set("v.capReadOnly",true);
        }        
        component.set("v.addressSummary", finalAddress);
    },    
    
    deleteEsazioneFields : function(component, finalAddress, helper){
        debugger;
        var requestObj = component.get("v.requestObj"); 
        if(component.get("v.displayPOD")){
            requestObj.Toponimo_esaz__c = component.find("toponimoPod").set("v.value","");
            requestObj.Via_esaz__c = component.find("viaPod").set("v.value","");
            requestObj.NumCivico_esaz__c = component.find("numeroCivicoPod").set("v.value","");
            requestObj.Provincia_esaz__c = component.find("provinciaPod").set("v.value","");
            requestObj.Comune_esaz__c = component.find("comunePod").set("v.value","");
            requestObj.Localita_esaz__c = component.find("localitaPod").set("v.value","");
            if(component.get("v.disableEsazioneAddressFields")){
                requestObj.Cap_esaz__c  = component.find("capPodText").set("v.value","");
            }
            else
            {
                requestObj.Cap_esaz__c = component.find("capPod").set("v.value","");
            }
            requestObj.CodIstat_esaz__c = component.find("codIstatPod").set("v.value","");
            requestObj.Presso_esaz__c = component.find("pressoEsaz").set("v.value","");
            requestObj.Nazione_esaz__c = component.find("nazioneEsaz").set("v.value","");
        }
        else{
            if(!$A.util.isEmpty(component.find("copyFromForniture").get("v.value"))){
                if(component.find("copyFromForniture").get("v.value")==="si"){
                    requestObj.Toponimo_esaz__c = null;
                    requestObj.Via_esaz__c = null;
                    requestObj.NumCivico_esaz__c = null;
                    requestObj.Provincia_esaz__c = null;
                    requestObj.Comune_esaz__c = null;
                    requestObj.Localita_esaz__c = null;
                    requestObj.Cap_esaz__c = null;
                    requestObj.CodIstat_esaz__c = null;
                    requestObj.Presso_esaz__c = null;
                    requestObj.Nazione_esaz__c = null;
                    finalAddress = $A.get("$Label.c.PT_NoAddress"); 
                }
                debugger;
                if(component.find("copyFromForniture").get("v.value")==="no" || component.get("v.displayPOD")){
                    debugger;
                    requestObj.Toponimo_esaz__c = component.find("toponimoPod").set("v.value","");
                    requestObj.Via_esaz__c = component.find("viaPod").set("v.value","");
                    requestObj.NumCivico_esaz__c = component.find("numeroCivicoPod").set("v.value","");
                    requestObj.Provincia_esaz__c = component.find("provinciaPod").set("v.value","");
                    requestObj.Comune_esaz__c = component.find("comunePod").set("v.value","");
                    requestObj.Localita_esaz__c = component.find("localitaPod").set("v.value","");
                    if(component.get("v.disableEsazioneAddressFields")){
                        requestObj.Cap_esaz__c  = component.find("capPodText").set("v.value","");
                    }
                    else
                    {
                        requestObj.Cap_esaz__c = component.find("capPod").set("v.value","");
                    }
                    requestObj.CodIstat_esaz__c = component.find("codIstatPod").set("v.value","");
                    requestObj.Presso_esaz__c = component.find("pressoEsaz").set("v.value","");
                    requestObj.Nazione_esaz__c = component.find("nazioneEsaz").set("v.value","");
                }
            }    
        }
        
        component.set("v.requestObj",requestObj);
        requestObj = component.get("v.requestObj");
        var toponimo = requestObj.Toponimo_esaz__c;
        var via = requestObj.Via_esaz__c;        
        var numeroCivico = requestObj.NumCivico_esaz__c;
        var CAP = requestObj.Cap_esaz__c;
        var Comune = requestObj.Comune_esaz__c;
        var Provincia = requestObj.Provincia_esaz__c;
        if($A.util.isUndefined(toponimo) && $A.util.isUndefined(via) && $A.util.isUndefined(numeroCivico) && 
           $A.util.isUndefined(CAP) && $A.util.isUndefined(Comune) && $A.util.isUndefined(Provincia)){
            finalAddress = $A.get("$Label.c.PT_NoAddress"); 
            component.set("v.comuneReadOnly",true);
            component.set("v.CAP", ""); 
            component.set("v.capReadOnly",true);
        }         
        component.set("v.addressSummary", finalAddress);
    },
    
    deleteSedeLegaleFields : function(component, finalAddress, helper){
        var requestObj = component.get("v.requestObj");   
        if(!$A.util.isEmpty(component.find("copyFromForniture").get("v.value"))){
            if(component.find("copyFromForniture").get("v.value")==="si"){
                requestObj.Toponimo_cl__c = null;
                requestObj.Via_cl__c = null;
                requestObj.NumCivico_cl__c = null;
                requestObj.Provincia_cl__c = null;
                requestObj.Comune_cl__c = null;
                requestObj.Localita_cl__c = null;
                requestObj.Cap_cl__c = null;
                requestObj.CodIstat_cl__c = null;
                requestObj.Nazione_cl__c = null;
                finalAddress = $A.get("$Label.c.PT_NoAddress"); 
            }
            if(component.find("copyFromForniture").get("v.value")==="no"){
                requestObj.Toponimo_cl__c = component.find("toponimoPod").set("v.value","");
                requestObj.Via_cl__c = component.find("viaPod").set("v.value","");
                requestObj.NumCivico_cl__c = component.find("numeroCivicoPod").set("v.value","");
                requestObj.Provincia_cl__c = component.find("provinciaPod").set("v.value","");
                requestObj.Comune_cl__c = component.find("comunePod").set("v.value","");
                requestObj.Localita_cl__c = component.find("localitaPod").set("v.value","");
                if(component.get("v.disableEsazioneAddressFields")){
                    requestObj.Cap_cl__c  = component.find("capPodText").set("v.value","");
                }
                else{
                    requestObj.Cap_cl__c = component.find("capPod").set("v.value","");
                }
                requestObj.CodIstat_cl__c = component.find("codIstatPod").set("v.value","");
                requestObj.Nazione_cl__c = component.find("nazioneEsaz").set("v.value","");
            } 
        }       
        component.set("v.requestObj",requestObj);
        requestObj = component.get("v.requestObj");
        var toponimo = requestObj.Toponimo_cl__c;
        var via = requestObj.Via_cl__c;        
        var numeroCivico = requestObj.NumCivico_cl__c;
        var CAP = requestObj.Cap_cl__c;
        var Comune = requestObj.Comune_cl__c;
        var Provincia = requestObj.Provincia_cl__c;
        if($A.util.isUndefined(toponimo) && $A.util.isUndefined(via) && $A.util.isUndefined(numeroCivico) && $A.util.isUndefined(CAP) && $A.util.isUndefined(Comune) && $A.util.isUndefined(Provincia)){
            finalAddress = $A.get("$Label.c.PT_NoAddress"); 
            component.set("v.comuneReadOnly",true);
            component.set("v.CAP", ""); 
            component.set("v.capReadOnly",true);
        }         
        
        component.set("v.addressSummary", finalAddress);
    },
    
    filterPicklistValues : function(component, event, helper, RecordTypeName,FieldAPINAME,atbName,ContFieldAPI,ContFieldVal,ContField2API,ContField2Val){
        var picklistValueSet = new Set();
        var picklistCustomSettingMapTemp = component.get('v.picklistCustomSettingMap');
        var picklistCustomSettingListTemp = picklistCustomSettingMapTemp[FieldAPINAME];
        var allpicklistValuesTemp = component.get('v.picklistValues').pickListValues[FieldAPINAME];             
        
        var x;    
        if(picklistCustomSettingListTemp!==undefined){   
            for(var i = 0; i < picklistCustomSettingListTemp.length; i++) {
                if(ContFieldAPI!=='' && ContFieldVal!==''){
                    if(ContField2API!=='' && ContField2Val!==''){
                        if(picklistCustomSettingListTemp[i].Controlling_Field_API__c === ContFieldAPI && picklistCustomSettingListTemp[i].Controlling_Field_Value__c === ContFieldVal &&
                           picklistCustomSettingListTemp[i].Controlling_Field_2_API__c === ContField2API && picklistCustomSettingListTemp[i].Controlling_Field_2_Value__c === ContField2Val)  {
                            picklistValueSet.add(picklistCustomSettingListTemp[i].Picklist_Value__c); 
                        }
                    }    
                    else{
                        if(picklistCustomSettingListTemp[i].Controlling_Field_API__c === ContFieldAPI && picklistCustomSettingListTemp[i].Controlling_Field_Value__c === ContFieldVal)  {
                            picklistValueSet.add(picklistCustomSettingListTemp[i].Picklist_Value__c); 
                        }
                    }  
                }
                else{    
                    picklistValueSet.add(picklistCustomSettingListTemp[i].Picklist_Value__c);
                } 
            }
        }
        var picklistValueListFinal = allpicklistValuesTemp.slice(0);
        var j = allpicklistValuesTemp.length;
        var picklistKeyValue = new Array(); //This array is being used to render picklist values & labels on component UI
        var counter = 0;
        for(var k = 0; k < allpicklistValuesTemp.length; k++) {
            //alert('allpicklistValuesTemp[k] '+allpicklistValuesTemp[k]);
            var picklistvaluesplit = allpicklistValuesTemp[k].split("~",1); // Splits the Picklist string into Value & Label
            //alert('splitvalue'+splitvalue);
            if(picklistValueSet.has(picklistvaluesplit[0]))//If picklist value is present in the set then add the same in a new array
            {
                //alert ('inside if');
                picklistKeyValue[counter] = new Array(2);
                picklistKeyValue[counter] = allpicklistValuesTemp[k].split("~");
                counter = counter +1;
            }
            else{
                picklistValueListFinal.splice(k-j,1);
            }
        }
        component.set(atbName, picklistKeyValue);       
        
    },  
    /*  renderEsazioneAddressFields : function(component, event, helper){
        //@reference:		420_PT_Logiche_pagine_Inserimento (Common/ESA_3)
        debugger;
        console.log('renderEsazioneAddressFields');
        if(!$A.util.isEmpty(component.find("copyFromForniture").get("v.value"))){
            if(component.find("copyFromForniture").get("v.value")==="no"){
                component.set("v.displayEsazioneAddressFields","true");
                component.find("nazioneEsaz").set("v.value","IT");    
            }
            else{
                component.set("v.displayEsazioneAddressFields","false");
            }
        }
        else{
            component.set("v.displayEsazioneAddressFields","false");
        }
    },*/
    
})