({
    getIntialization : function(component) {
        
        var controllerValues = component.get("c.getWrapper");
        
        controllerValues.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                var responseData = response.getReturnValue(); 
                component.set("v.wrapperList",responseData)
                component.find("Venditore").set("v.value", responseData.Venditore);
                component.set("v.isVenditore",true); 
            }    
        });
        
        $A.enqueueAction(controllerValues);	
    },
    getListOfRecords: function(component, event, helper) {
        debugger;
        var CMsearch = component.get("v.wrapperList");
        CMsearch.TipoRichiesta=component.find("TipoRich").get("v.value");
        CMsearch.Venditore=component.find("Venditore").get("v.value");
        CMsearch.DataDecorrenzaDa=component.find("DataDecorrenzaDa").get("v.value");
        CMsearch.DataDecorrenzaA=component.find("DataDecorrenzaA").get("v.value");
        CMsearch.ContrattodiDisp=component.find("contdispacciamento").get("v.value");
        component.set("v.wrapperList",CMsearch);
        var controllerValues = component.get("c.getQueryall");
        controllerValues.setParams({wrapperobj : JSON.stringify(CMsearch),
                                    strObjectName : component.get("v.objectName"),
                                    strFields : component.get("v.FieldAPIs"),
                                    RecordNum : component.get("v.RecordLimit") 
                                   });
        controllerValues.setCallback(this, function(response){
            var state = response.getState();
            alert(state);
            if (state === "SUCCESS"){
                var responseData = response.getReturnValue(); 
                if(responseData != null){
                    component.set("v.RecordsList", responseData.dataList);
                    component.set("v.LabelList", responseData.labelList);                
                    component.set("v.apiList",responseData.apiList);
                    if(responseData.dataList.length===0){
                        component.set("v.NoRecords",true);
                        component.set("v.Spinner",false);
                        component.set("v.displayRecordList",false);
                        component.set("v.LimitRec",False);
                    }
                    if(responseData.dataList.length>0){
                        component.set("v.displayRecordList",true);
                        component.set("v.NoRecords",false);										                                   
                    }
                    var Reclen=component.get("v.RecordLimit");
                    if(responseData.dataList.length>Reclen){
                        component.set("v.LimitRec",true);
                        component.set("v.NoRecords",false);	 
                        component.set("v.Spinner",false);
                    }                         
                }
            }
            component.set("v.Spinner",false); 
        });
        $A.enqueueAction(controllerValues);  
        component.set("v.Spinner",true); 
    },
    //PREV_REP_01 Validation
    validatePREV_REP_01 : function(component,helper) {
        var ErrorMsgLable=$A.get("$Label.c.PT_PREV_REP_01");
        var errMsg = null;
        var Tipo = component.find("TipoRich").get("v.value");
        if($A.util.isEmpty(Tipo)){
            return ErrorMsgLable;
        }
        
        return errMsg;
    },
    
    getEmpList: function(component) { 
        debugger;
        var action = component.get("c.getUserinfo"); // method in the apex class     
        action.setCallback(this, function(a) {
            var jsonString=a.getReturnValue();
            alert(JSON.stringify(jsonString));
            component.set("v.running", a.getReturnValue());
        });
        $A.enqueueAction(action);
    }, 
    
    throwErrorMsg : function(component,errMsg,helper) 
    { 
        var resultsToast = $A.get("e.force:showToast");
        if(resultsToast)
        {    
            resultsToast.setParams({"title": "Errore","message": errMsg,"type":"error","mode":"sticky"});
            resultsToast.fire();
        }            
    }
    
})