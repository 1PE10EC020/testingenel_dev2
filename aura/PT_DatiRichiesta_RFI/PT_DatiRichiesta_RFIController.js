({
	doInit : function(component, event, helper) {
        //debugger;        
                
        var recType = component.get("v.recordType");
       	var myJSON = JSON.stringify(recType);
        component.set('v.recordTypeId', recType.Id);
        var recTypeId = component.get("v.recordTypeId");
        component.set('v.recTypeName', recType.Name);
		var recTypeNAME = component.get("v.recTypeName");
        helper.filterPicklistValues(component, event, helper, recTypeNAME, 'CategReclamo_cl__c','v.categReclamo','','','','');
        helper.filterPicklistValues(component, event, helper, recTypeNAME, 'RichiestaUtente__c','v.richiestaUtente','','','','');
        helper.filterPicklistValues(component, event, helper, recTypeNAME, 'Categoria__c','v.categoria','','','','');
        helper.filterPicklistValues(component, event, helper, recTypeNAME, 'TipoReclamo_cl__c','v.tipoReclamo','','','','');
        helper.filterPicklistValues(component, event, helper, recTypeNAME, 'TipoRichiesta_cl__c','v.tipoRichiesta','','','','');
        helper.filterPicklistValues(component, event, helper, recTypeNAME, 'MotivRichiesta__c','v.MotivRichiesta','','','','');
        helper.filterPicklistValues(component, event, helper, recTypeNAME, 'TipoRichiedente__c','v.tipologiarichedente','','','','');
        
        if(recTypeNAME===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N02"))||
           recTypeNAME===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S02"))||
           recTypeNAME===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_R01"))||
           recTypeNAME===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_R03"))||
           recTypeNAME===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_MC1"))){
           component.set("v.isTipoRichiedente","true");
           component.set("v.isDecorrenza","true"); 
           var dateFormat = "yyyy-MM-ddTHH:mm:ss.000Z";
           var decorrenzaDate = new Date();
           decorrenzaDate.setHours(decorrenzaDate.getHours());        
           var dateString = $A.localizationService.formatDateTime(decorrenzaDate, dateFormat);  
           component.find("Decorrenza").set("v.value",dateString);
           component.find("TipoRichiedente__c").set("v.value",'T');
            
        }
      
        
        else if(recTypeNAME===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A03"))||
                recTypeNAME===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A01"))||
                recTypeNAME===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_D01"))||
                recTypeNAME===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N01"))||
           		recTypeNAME===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP1"))||
                recTypeNAME===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP2"))||
                recTypeNAME===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M03"))||
                recTypeNAME===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SC1"))||
                recTypeNAME===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S01"))){
                component.set("v.isDecorrenza","true"); 
                var dateFormat1 = "yyyy-MM-ddTHH:mm:ss.000Z";
                var decorrenzaDate1 = new Date();
                decorrenzaDate1.setHours(decorrenzaDate1.getHours());        
                var dateString1 = $A.localizationService.formatDateTime(decorrenzaDate1, dateFormat1);  
                component.find("Decorrenza").set("v.value",dateString1);
         
        }
        else if(recTypeNAME===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M01"))||
                recTypeNAME===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M02"))){
                component.set("v.isDecorrenza","true"); 
                var dateFormat1 = "yyyy-MM-ddTHH:mm:ss.000Z";
                var decorrenzaDate1 = new Date();
                decorrenzaDate1.setHours(decorrenzaDate1.getHours());        
                var dateString1 = $A.localizationService.formatDateTime(decorrenzaDate1, dateFormat1);  
                component.find("Decorrenza").set("v.value",dateString1);
         
        }
    
      else  if(recTypeNAME===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_V02"))||
               recTypeNAME===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_PC1"))||
               recTypeNAME===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_PT1"))){
               component.set("v.isTipoRichiedente","true");
               component.set("v.isDecorrenza","true");
               var dateFormat2 = "yyyy-MM-ddTHH:mm:ss.000Z";
               var decorrenzaDate2 = new Date();
               decorrenzaDate2.setHours(decorrenzaDate2.getHours());        
               var dateString2 = $A.localizationService.formatDateTime(decorrenzaDate2, dateFormat2);  
               component.find("Decorrenza").set("v.value",dateString2);
               component.find("TipoRichiedente__c").set("v.value",'T');
        }
    
      else  if(recTypeNAME===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_V01"))){
            component.set("v.isMotivRichiesta","true");
			component.set("v.isDecorrenza","true");
            var dateFormat3 = "yyyy-MM-ddTHH:mm:ss.000Z";
            var decorrenzaDate3 = new Date();
            decorrenzaDate3.setHours(decorrenzaDate3.getHours());        
            var dateString3 = $A.localizationService.formatDateTime(decorrenzaDate3, dateFormat3);  
            component.find("Decorrenza").set("v.value",dateString3); 
          
        }
      else  if(recTypeNAME===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_AN1"))){
            component.set("v.isTipoRichiedente","true");
            component.set("v.isIdRichAnn","true");
            component.set("v.isDataAnnullamento","true"); 
            var dateFormat3 = "yyyy-MM-ddTHH:mm:ss.000Z";
            var decorrenzaDate3 = new Date();
            decorrenzaDate3.setHours(decorrenzaDate3.getHours());        
            var dateString3 = $A.localizationService.formatDateTime(decorrenzaDate3, dateFormat3);  
            component.find("DataAnnullamento__c").set("v.value",dateString3); 
            component.find("TipoRichiedente__c").set("v.value",'T');
            
        }
    
      else  if(recTypeNAME===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_GD1"))){
            component.set("v.isTipoRichiedente","true");
			component.set("v.isDataRicezioneRichiesta","true");
            var dateFormatnew = "yyyy-MM-ddTHH:mm:ss.000Z";
            var dataRicezioneRichiesta = new Date();
            dataRicezioneRichiesta.setHours(dataRicezioneRichiesta.getHours());        
            var dateStringnew = $A.localizationService.formatDateTime(dataRicezioneRichiesta, dateFormatnew);  
            component.find("DataRicezioneRichiesta__c").set("v.value",dateStringnew);
			component.find("TipoRichiedente__c").set("v.value",'T');
        }
        
       else  if(recTypeNAME===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SC1"))||
                recTypeNAME===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_I01"))||
                recTypeNAME===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SM1"))){
                component.set("v.isDecorrenza","true");
                var dateFormat4 = "yyyy-MM-ddTHH:mm:ss.000Z";
                var decorrenzaDate4 = new Date();
                decorrenzaDate4.setHours(decorrenzaDate4.getHours());        
                var dateString4 = $A.localizationService.formatDateTime(decorrenzaDate4, dateFormat4);  
                component.find("Decorrenza").set("v.value",dateString4);
        }  
    
        else  if(recTypeNAME===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_AVO"))||
               recTypeNAME===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_ACV"))){
               component.set("v.isTipoRichiedente","true");
               component.set("v.isRequired","false");  
               component.find("TipoRichiedente__c").set("v.value",'T');
        
        } 
    
       else  if(recTypeNAME===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_AUT"))||
               recTypeNAME===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_ARS"))){
           		component.set("v.isTipoRichiedente","true");
                component.set("v.isRequired","true");  
                component.find("TipoRichiedente__c").set("v.value",'T');

       }
    },
    
     onTipoRichiestaClienteFinaleFieldChange : function(component, event, helper){
       /* //Fire Application event to enable fields in the PT_Upload_File_Container_Hidefile_Event Component.
      
         var appEvent1 = $A.get("e.c:PT_Upload_File_Container_Hidefile_Event");
         
        appEvent1.setParams({
            "TipoRichiestaClienteFinaleEvent" : event.getSource().get("v.value") });
        appEvent1.fire();*/ 
       helper. ClienteFinaleFieldChange(component,event,helper);
    },
    
    Tipofinaleclient : function(component,event,helper)
    {
        var fldValue = component.find("TipoRichiesta_cl__c").get("v.value");
        var action = component.get("c.tipoClientFinale"); 
         helper. ClienteFinaleFieldChange(component,event,helper);
        action.setParams({
            "fieldValue" : fldValue
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){

                var retValue = response.getReturnValue();
                if(!retValue){
                	component.set("v.finaleAttribute",retValue);
        		}
                else
                {
                    component.set("v.finaleAttribute","true");
                }
            }
            
    	});
    	$A.enqueueAction(action);
     
    }, 
    RichiestaUtente : function(component,event,helper)
    {
        var fldValue = component.find("RichiestaUtente__c").get("v.value");
        
        var action = component.get("c.richiestaUtente");
        action.setParams({
            "fieldValue" : fldValue
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
   
                var rettValue = response.getReturnValue();

                if(!rettValue){
                	component.set("v.utenteAttribute",rettValue);
        		}
                else
                {
                    component.set("v.utenteAttribute","true");
                }
            }
            
    	});
    	$A.enqueueAction(action);
     
    },    
	checkRiferiQuesiti : function(component, event, helper) {
        var RiferiQuesiti = component.find("RiferimentoDocQuesiti__c").get("v.value");        
        var recType = component.get("v.recordType");        
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recTypeName");
        if(recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M02"))&& RiferiQuesiti!==null){
			var RiferiQuesitiFormat = new RegExp($A.get("$Label.c.PT_RICHIESTE_Note_REGEX"));
            var validateRiferiQuesitiFormat = RiferiQuesitiFormat.test(RiferiQuesiti!=null);   
            if(validateRiferiQuesitiFormat){
                var inputCmp = component.find("RiferimentoDocQuesiti__c");
                inputCmp.set("v.errors", null);
            }else{
                var errMsg = 'Controllare la validità del campo Riferimento Doc. Quesiti' ;
                var inputCmp1 = component.find("RiferimentoDocQuesiti__c");
                inputCmp1.set("v.errors", [{message:errMsg}]);
            }    
        }
 	},
	checkRiferiReclamo : function(component, event, helper) {
        var RiferiReclamo = component.find("RiferimentoDocReclamo__c").get("v.value");        
        var recType = component.get("v.recordType");        
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recTypeName");                
        if(recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M02"))&& RiferiReclamo!==null){
			var RiferiReclamoFormat = new RegExp($A.get("$Label.c.PT_RICHIESTE_Note_REGEX"));    
            var validateRiferiReclamoFormat = RiferiReclamoFormat.test(RiferiReclamo);
            if(validateRiferiReclamoFormat){
                var inputCmp = component.find("RiferimentoDocReclamo__c");
                inputCmp.set("v.errors", null);
            }else{
                var errMsg = 'Controllare la validità del campo Riferimento Doc. Reclamo' ;
                var inputCmp1 = component.find("RiferimentoDocReclamo__c");
                inputCmp1.set("v.errors", [{message:errMsg}]);
            }    
        }
 	},
    checkrichiestedaAnnullare : function(component, event, helper) {
        var richiestedaAnnullare = component.find("IdRichAnnullare__c").get("v.value");  
        var recType = component.get("v.recordType");        
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recTypeName");
        if(richiestedaAnnullare!==null && (recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_AN1"))))
        {        
            var richiestedaAnnullareFormat = new RegExp($A.get("$Label.c.PT_RICHIESTE_Richieste_da_Annullare_REGEX"));
            
            var validaterichiestedaAnnullareFormat = richiestedaAnnullareFormat.test(richiestedaAnnullare);
            if(validaterichiestedaAnnullareFormat){
                var inputCmp = component.find("IdRichAnnullare__c");
                inputCmp.set("v.errors", null);
            }else{
                var errMsg = 'Errore di controllo sul formato per i seguenti campi: Id Richiesta da Annullare' ;
                var inputCmp1 = component.find("IdRichAnnullare__c");
                inputCmp1.set("v.errors", [{message:errMsg}]);
            }    
        }
     },

})