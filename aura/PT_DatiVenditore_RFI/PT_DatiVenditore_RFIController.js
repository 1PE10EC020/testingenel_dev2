({  
    doInit: function(component, event, helper) {
        var recType = component.get("v.recordType");
        component.set('v.recordTypeId', recType.Id);
        var recTypeId = component.get("v.recordTypeId");
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
    	helper.getEmpList(component);
        //helper.getpartitaIvaTrader(component); 
    	var action = component.get("c.getCurrentUserinfo"); // method in the apex class     
    	action.setCallback(this, function(a) {
            //alert("Iva-->"+JSON.stringify(a.getReturnValue().Account.PartitaIVA__c));
            if(recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_AN1"))||recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A03"))||
              recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A01"))||recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_D01"))||
              recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_GD1"))||recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_I01"))||
              recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M01"))||recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M02"))||
              recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M03"))||recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_MC1"))||
              recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N01"))||recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N02"))||
              recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_PC1"))||recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_PT1"))||
              recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_R03"))||recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_R01"))||
              recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SC1"))||recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SM1"))||
              recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP1"))||recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP2"))||
              recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S01"))||recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S02"))||
              recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_V01"))||recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_ACV"))||
              recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_ARS"))||recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_AUT"))||
              recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_AVO"))||recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_V02"))){
            var accName=JSON.stringify(a.getReturnValue().Account.Name);
            var accNameSplit = accName.split('"');
            var trader=JSON.stringify(a.getReturnValue().IdTrader__c);
            var traderSplit = trader.split('"');
            alert(accNameSplit[1]);
            component.find("DescrTraderEstesa__c").set("v.value", accNameSplit[1]+"-"+traderSplit[1]); // variable in the component  
            }
            if(recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_ACV"))||
              recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_AVO"))||
              recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_ARS"))||
              recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_AUT")))
        	{  
            var parIva=JSON.stringify(a.getReturnValue().Account.PartitaIVA__c);
            var parIvaSplit = parIva.split('"');
            //alert(parIvaSplit[1]);
            component.find("partitaIvaTrader").set("v.value",parIvaSplit[1]); // variable in the component           
            }
        });
        $A.enqueueAction(action);
       /* var actionParIva = component.get("c.getCurrentUserinfo"); // method in the apex class     
    	action.setCallback(this, function(a) {
            var parIva=JSON.stringify(a.getReturnValue().Account.PartitaIVA__c);
            var parIvaSplit = parIva.split('"');
            //alert(parIvaSplit[1]);
            //component.find("DescrTraderEstesa__c").set("v.value", accNameSplit[1]+"-"+traderSplit[1]); // variable in the component  
			component.find("partitaIvaTrader").set("v.value",parIvaSplit[1]); // variable in the component           
            
        });
        $A.enqueueAction(action,actionParIva);*/
        helper.filterPicklistValues(component, event, helper, recTypeName, 'MandConnessione__c','v.MandConnessione','','','','');        
        if(recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N01")))
        {            
            component.set("v.mandCon","true");
        }
        
        if(recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M03")))
        {            
            component.set("v.mandDiss","false");
        }
    }       
})