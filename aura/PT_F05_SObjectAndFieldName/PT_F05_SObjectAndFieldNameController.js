({
    doInit : function(component, helper) {
     var SObject = component.get('v.SObject');
     var FieldName = component.get('v.fieldName');
     var outputText = component.find("outputTextId");  
     var output=SObject[FieldName];
     outputText.set("v.value",SObject[FieldName]);
     var POD = component.get('v.POD');
     var Rtype = component.get('v.RTYPE');
     
     if(FieldName.includes('DataRicezioneRichiesta__c')){
        if(output!==undefined) {
            var date = new Date(output);
            var day = date.getDate().toString();
            var month = (date.getMonth() + 1).toString();
               // var converteddate = (month[1] ? month : '0' + month[0]) + '-' +(day[1] ? day : '0' + day[0]) + '-' + date.getFullYear();
               var converteddate = (date.getFullYear()) + '-' +(month[1] ? month : '0' + month[0]) + '-' + (day[1] ? day : '0' + day[0]);
               outputText.set("v.value",converteddate);
           }
       }
       if(FieldName.includes('Pod__c')){
          outputText.set("v.value",POD); 
      }
      if(FieldName.includes('Record_Type_Esteso__c')){
          outputText.set("v.value",component.get('v.RTYPE')); 
      }
      


      
  },
  
})