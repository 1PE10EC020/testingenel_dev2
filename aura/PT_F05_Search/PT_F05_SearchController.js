({
    doInit: function(component, event, helper) {
        helper.getIntialization(component);
        //component.find("DataDecorrenzaDa").set("v.value", "2017-07-07T00:00:00.000");
        /*var dateFormat = "yyyy-MM-ddTHH:mm";
           var decorrenzaDate = new Date();
                   
           var dateString = $A.localizationService.formatDateTime(decorrenzaDate, dateFormat);  
           component.find("DataDecorrenzaDa").set("v.value",dateString);*/
        //Fetch RecordType for Tiporich field
        var action = component.get("c.fetchRecordTypeValues");
        action.setCallback(this, function(response) {
            component.set("v.lstOfRecordType", response.getReturnValue());
        });
        $A.enqueueAction(action);
        
        
        
        var actionOne = component.get("c.statoRichValues");
        actionOne.setCallback(this, function(response1) {
            component.set("v.lstOfstatrich", response1.getReturnValue());
        });
        $A.enqueueAction(actionOne);
        var actionpod = component.get("c.podValues");
        actionpod.setCallback(this, function(response2) {
            component.set("v.lstOfpod", response2.getReturnValue());
        });
        $A.enqueueAction(actionpod);
        //Getting header of Downloadable CSV File
        var actionlabel = component.get("c.lstOflabel");
        actionlabel.setCallback(this, function(response) {
            component.set("v.lstOflab", response.getReturnValue());
        });
        $A.enqueueAction(actionlabel);
                //Getting API LIST of Downloadable CSV File
                var actionapi = component.get("c.lstOfAPI");
                actionapi.setCallback(this, function(response) {
                    component.set("v.lstOfAPI", response.getReturnValue());
                });
                $A.enqueueAction(actionapi);
        //test method
        var actionorder = component.get("c.lstOfordermethod");
        actionorder.setCallback(this, function(response) {
            var allvalues = responseData.getReturnValue();
            var optionsList = [];
            alert(allvalues);
            for (var key in allvalues) {
                if (allvalues.hasOwnProperty(key)) {
                    optionsList.push({value: key, label: response[key]});
                }
            };
            alert(optionsList);
            component.set("v.lstOforder", optionsList);
        });
        $A.enqueueAction(actionorder);   
        
        
    },
    
    onValueFieldChange: function(component, event, helper) {
        component.set("v.Spinner", true);
        var StatoRich = component.find("StatoRich").get("v.value");
        if (StatoRich !== "" || StatoRich !== "--None--") {
            component.set("v.issottostatoDisable", false);
        }
        if (StatoRich === "--None--") {
            component.set("v.issottostatoDisable", True);
        }
        var controllerValues = component.get("c.statotoRichValues");
        controllerValues.setParams({
            sataorich: StatoRich
        });
        controllerValues.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var responseData = response.getReturnValue();
                if (responseData != null) {
                    component.set("v.lstSTATO", responseData);
                    component.set("v.Spinner", false);
                }
            }
        });
        $A.enqueueAction(controllerValues);        
        
        component.set("v.Spinner", True);
    },
    search: function(component, event, helper) {
        var key='Search';
        helper.helpersearch(component,helper,key);
        
    },
    //For the cancel button
    cancel: function(component, event, helper) {
        window.location.href = $A.get("$Label.c.PT_Community_Base_URL");
    },
    Runreport: function(component, event, helper) {
     var key='Report';
     helper.helpersearch(component,helper,key);
 },
    //For the downloadCsv button
    downloadCsv: function(component, event, helper) {
       // var stockData = component.get("v.RecordsList");
       // var headerlabel = component.get("v.LabelList");
       // var headerApilist = component.get("v.apiList");
       var csvrecList = component.get("v.csvrecList");
       var csvapi = component.get("v.lstOfAPI");
       var csvlabel = component.get("v.lstOflab");
       
       
       // var csv = helper.convertArrayOfObjectsToCSV(component,stockData, headerlabel, headerApilist);
       var csv = helper.convertArrayOfObjectsToCSV(component,csvrecList, csvlabel, csvapi);
       if (csv == null) {
        return;
    }
    var dateFormat = "THH:mm";
    var dnldname = new Date();
    var dateString = $A.localizationService.formatDateTime(dnldname, dateFormat);
		/*var date = new Date(dateString);
        var day = date.getDate().toString();
        var month = (date.getMonth() + 1).toString();
        var converteddate = (month[1] ? month : '0' + month[0]) + '-' +(day[1] ? day : '0' + day[0]) + '-' + date.getFullYear();*/
        var  filename="Report_F05_" + dateString ;
        var hiddenElement = document.createElement('a');
        var BOM = "\uFEFF"; 
        hiddenElement.href = 'data:text/csv;charset=utf-8,'+BOM + encodeURI(csv);
        hiddenElement.target = '_self';
        hiddenElement.download =filename+ '.csv';
        document.body.appendChild(hiddenElement);
        hiddenElement.click(); // using click() js function to download csv file
    },
    
    //For the navigating into Record
    navigateToRecord: function(component, event, helper) {
        var navEvent = $A.get("e.force:navigateToSObject");
        navEvent.setParams({
            recordId: event.target.id,
            slideDevName: "detail"
        });
        navEvent.fire();
    }
    

    


})