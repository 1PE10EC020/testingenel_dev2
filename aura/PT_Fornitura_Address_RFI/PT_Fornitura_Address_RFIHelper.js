({
	helperMethod : function() {
		
	},
    getAccountlst : function(component, event, helper) {
        var action = component.get("c.getAccountlist");
         
        action.setCallback(this, function(a) {
        if (a.getState() === "SUCCESS") {
        component.set("v.accountlist", a.getReturnValue());
        } else if (a.getState() === "ERROR") {
        $A.log("Errors", a.getError());
        }
        });
         
        $A.enqueueAction(action);
    },
   
	initilizePicklistValues :function(component, event, helper)
    {
                // initlize Richiesta picklist values
                var pickListValue = component.get("v.picklistValues");
                console.log(">>>> Picklist Value :" + pickListValue);
                debugger;
                component.set('v.Toponimo', pickListValue['Toponimo_pod__c']); 
                component.set('v.Provincia', pickListValue['Provincia_pod__c']);
        		component.set('v.NazioneEsaz', pickListValue['Nazione_esaz__c']);
        	
               	debugger;
    },
    assignFieldValues :function(component, event, helper)
    {
		var requestObj = component.get("v.requestObj");
        component.set("v.toponimoPod", requestObj.Toponimo_pod__c);
		component.set("v.viaPod", requestObj.Via_pod__c);
        component.set("v.numeroCivicoPod", requestObj.Numero_Civico_pod__c);
        component.set("v.scalaPod", requestObj.Scala_pod__c);
        component.set("v.pianoPod", requestObj.Piano_pod__c);
        component.set("v.internoPod", requestObj.Interno_pod__c);
        component.set("v.provinciaPod", requestObj.Provincia_pod__c);
        component.set("v.comunePod", requestObj.Comune_pod__c);
        component.set("v.localitaPod", requestObj.Localita_pod__c);	
        component.set("v.capPod", requestObj.Cap_pod__c);
        component.set("v.codIstatPod", requestObj.CodIstat_pod__c);
        component.set("v.PPAAAVVVPod", requestObj.PPAAAVVV_pod__c);
        
        
    },
    validateAddressFields :function(component, event, helper, addressType)
    {
     	debugger;
        var errMsg = null;
        var requestObj = component.get('v.requestObj'); 
        var toponimo = component.find("toponimoPod").get("v.value");;
		var via = component.find("viaPod").get("v.value");        
        var numeroCivico = component.find("numeroCivicoPod").get("v.value");
        var CAP = component.find("capPod").get("v.value");
        var Comune = component.find("comunePod").get("v.value");
        var Provincia = component.find("provinciaPod").get("v.value");
        var Localitapod = component.find("localitaPod").get("v.value");
        var codIstat = component.find("codIstatPod").get("v.value");
        
       // var Localitapod = component.find("localitaPod").get("v.value");
       // var Localitapod = component.find("localitaPod").get("v.value");
        var addressLabel = "";
        if(component.get("v.addressType")==="Fornitura"){
            addressLabel = "Fornitura";
        }
        else if(component.get("v.addressType")==="Esazione"){
            addressLabel = "esazione/recapito";
        }
        else if(component.get("v.addressType")==="SedeLegale"){
            addressLabel = "Sede Legale";
        }
        
        if($A.util.isEmpty(toponimo) && component.get("v.isFornituraAddress")){
            errMsg = 'Valorizzare i seguenti campi dellindirizzo di ' + addressLabel +': Toponimo';
            var inputCmp = component.find("toponimoPod");
			inputCmp.set("v.errors", [{message:errMsg}]);
            
        }
        else{
            var inputCmp = component.find("toponimoPod");
            inputCmp.set("v.errors", null);            
        }
        
        if($A.util.isEmpty(via)){
            errMsg = 'Valorizzare i seguenti campi dellindirizzo di ' + addressLabel +': Via';
            var inputCmp = component.find("viaPod");
			inputCmp.set("v.errors", [{message:errMsg}]);
            
        }
        else{
            var inputCmp = component.find("viaPod");
            inputCmp.set("v.errors", null);            
        }
        
        if($A.util.isEmpty(numeroCivico)){
            errMsg = 'Valorizzare i seguenti campi dellindirizzo di ' + addressLabel +': Numero Civico';
            var inputCmp = component.find("numeroCivicoPod");
			inputCmp.set("v.errors", [{message:errMsg}]);
            
        }
        else{
            var inputCmp = component.find("numeroCivicoPod");
            inputCmp.set("v.errors", null);            
        }
        
        if($A.util.isEmpty(CAP)){
            errMsg = 'Valorizzare i seguenti campi dellindirizzo di ' + addressLabel +': CAP';
            var inputCmp = component.find("capPod");
			inputCmp.set("v.errors", [{message:errMsg}]);
            
        }
        else{
            var inputCmp = component.find("capPod");
            inputCmp.set("v.errors", null);            
        }
        
        if($A.util.isEmpty(Comune) && !component.get("v.disableEsazioneAddressFields")){
            errMsg = 'Valorizzare i seguenti campi dellindirizzo di ' + addressLabel +': Comune';
            var inputCmp = component.find("comunePod");
			inputCmp.set("v.errors", [{message:errMsg}]);
            
        }
        else{
            var inputCmp = component.find("comunePod");
            inputCmp.set("v.errors", null);            
        }
        
        if($A.util.isEmpty(Provincia) && !component.get("v.disableEsazioneAddressFields")){
            errMsg = 'Valorizzare i seguenti campi dellindirizzo di ' + addressLabel +': Provincia';
            var inputCmp = component.find("provinciaPod");
			inputCmp.set("v.errors", [{message:errMsg}]);
            
        }
        else{
            var inputCmp = component.find("provinciaPod");
            inputCmp.set("v.errors", null);            
        }
        
        if($A.util.isEmpty(Localitapod) && component.get("v.isFornituraAddress")){
            errMsg = 'Valorizzare i seguenti campi dellindirizzo di ' + addressLabel +': Località';
            var inputCmp = component.find("localitaPod");
			inputCmp.set("v.errors", [{message:errMsg}]);
            
        }
        else{
            var inputCmp = component.find("localitaPod");
            inputCmp.set("v.errors", null);            
        }
        if($A.util.isEmpty(codIstat) && !component.get("v.disableEsazioneAddressFields")){
            errMsg = 'Valorizzare i seguenti campi dellindirizzo di ' + addressLabel +': Codice Istat';
            var inputCmp = component.find("codIstatPod");
			inputCmp.set("v.errors", [{message:errMsg}]);
            
        }
        else{
            var inputCmp = component.find("codIstatPod");
            inputCmp.set("v.errors", null);            
        }
        
        if(component.get("v.addressType")==="Esazione" || component.get("v.addressType")==="SedeLegale"){
            var Nazione = component.find("nazioneEsaz").get("v.value");
            if($A.util.isEmpty(Nazione)){
                errMsg = 'Valorizzare la nazione per lindirizzo di ' + addressLabel;
                var inputCmp = component.find("nazioneEsaz");
                inputCmp.set("v.errors", [{message:errMsg}]);
                
            }
            else{
                var inputCmp = component.find("nazioneEsaz");
                inputCmp.set("v.errors", null);            
            }
        }
        debugger;
        if(component.get("v.addressType")==="Fornitura" && component.get("v.bypassCodiceViaCheck")==="false"){
            var codiceVia = component.find("PPAAAVVVPod").get("v.value");
            if($A.util.isEmpty(codiceVia) || codiceVia==="undefined"){
                errMsg = "E' necessario utilizzare la funzionalità di Verifica Indirizzo per determinare un Codice Via per l'indirizzo di fornitura.";
                var inputCmp = component.find("PPAAAVVVPod");
                inputCmp.set("v.errors", [{message:errMsg}]);
                
            }
            else{
                var inputCmp = component.find("PPAAAVVVPod");
                inputCmp.set("v.errors", null);            
            }
        }
        else if(component.get("v.addressType")==="Fornitura" && component.get("v.bypassCodiceViaCheck")==="true"){
        	 var inputCmp = component.find("PPAAAVVVPod");
             inputCmp.set("v.errors", null);     
        }
        return errMsg;
    },
    throwErrorMsg : function(component,errMsg,helper) 
    {
        console.log('Error Message :: ' + errMsg);    
        var resultsToast = $A.get("e.force:showToast");
        if(resultsToast)
        {    
			resultsToast.setParams({"title": "Error","message": errMsg,"type":"error"});
			resultsToast.fire();
        }            
    },
    setFornituraFields : function(component, event, helper){
        var requestObj = component.get("v.requestObj");
        
        requestObj.Toponimo_pod__c = component.find("toponimoPod").get("v.value");
        requestObj.Via_pod__c = component.find("viaPod").get("v.value");
        requestObj.Numero_Civico_pod__c = component.find("numeroCivicoPod").get("v.value");
        requestObj.Scala_pod__c = component.find("scalaPod").get("v.value");
        requestObj.Piano_pod__c = component.find("pianoPod").get("v.value");
        requestObj.Interno_pod__c = component.find("internoPod").get("v.value");
        requestObj.Provincia_pod__c = component.find("provinciaPod").get("v.value");
        requestObj.Comune_pod__c = component.find("comunePod").get("v.value");
        requestObj.Localita_pod__c = component.find("localitaPod").get("v.value");
        requestObj.Cap_pod__c = component.find("capPod").get("v.value");
        requestObj.CodIstat_pod__c = component.find("codIstatPod").get("v.value");
        requestObj.PPAAAVVV_pod__c = component.find("PPAAAVVVPod").get("v.value");
        requestObj.Nazione_pod__c = 'IL';
        
        component.set("v.requestObj",requestObj);
        
        var finalAddress = requestObj.Toponimo_pod__c + " " 
        					+ requestObj.Via_pod__c + ", " 
        					+ requestObj.Numero_Civico_pod__c + ", " 
        					+ requestObj.Cap_pod__c + ", " 
        					+ requestObj.Comune_pod__c + ", " 
        					+ requestObj.Provincia_pod__c; 
        
        if(finalAddress.includes("undefined")===false){
            component.set("v.addressSummary",finalAddress);    
        }
    },
    setEsazioneFields : function(component, event, helper){
        var requestObj = component.get("v.requestObj");
        //alert(component.find("copyFromForniture").get("v.value"));
        if(component.find("copyFromForniture").get("v.value")==="no"){
            requestObj.Toponimo_esaz__c = component.find("toponimoPod").get("v.value");
            requestObj.Via_esaz__c = component.find("viaPod").get("v.value");
            requestObj.NumCivico_esaz__c = component.find("numeroCivicoPod").get("v.value");
            requestObj.Presso_esaz__c = component.find("pressoEsaz").get("v.value");
            requestObj.Provincia_esaz__c = component.find("provinciaPod").get("v.value");
            requestObj.Comune_esaz__c = component.find("comunePod").get("v.value");
            requestObj.Localita_esaz__c = component.find("localitaPod").get("v.value");
            requestObj.Cap_esaz__c = component.find("capPod").get("v.value");
            requestObj.CodIstat_esaz__c = component.find("codIstatPod").get("v.value");
            requestObj.Nazione_esaz__c = component.find("nazioneEsaz").get("v.value");
        }
        else if(component.find("copyFromForniture").get("v.value")==="si"){
            requestObj.Toponimo_esaz__c = requestObj.Toponimo_pod__c;
            requestObj.Via_esaz__c = requestObj.Via_pod__c;
            requestObj.NumCivico_esaz__c = requestObj.Numero_Civico_pod__c;
            requestObj.Provincia_esaz__c = requestObj.Provincia_pod__c;
            requestObj.Comune_esaz__c = requestObj.Comune_pod__c;
            requestObj.Localita_esaz__c = requestObj.Localita_pod__c;
            requestObj.Cap_esaz__c = requestObj.Cap_pod__c;
            requestObj.CodIstat_esaz__c = requestObj.CodIstat_pod__c;
            requestObj.Nazione_esaz__c = requestObj.Nazione_pod__c;
        }
        component.set("v.requestObj",requestObj);
        var finalAddress =  requestObj.Toponimo_esaz__c + " " 
        					+ requestObj.Via_esaz__c + ", " 
        					+ requestObj.NumCivico_esaz__c + ", " 
        					+ requestObj.Cap_esaz__c + ", " 
        					+ requestObj.Comune_esaz__c + ", " 
        					+ requestObj.Provincia_esaz__c; 
        if(finalAddress.includes("undefined")===false){
            component.set("v.addressSummary",finalAddress);    
        }
        
    },
    setSedeLegaleFields : function(component, event, helper){
        debugger;
        var requestObj = component.get("v.requestObj");
        //alert(component.find("copyFromForniture").get("v.value"));
        if(component.find("copyFromForniture").get("v.value")==="no"){
            requestObj.Toponimo_cl__c = component.find("toponimoPod").get("v.value");
            requestObj.Via_cl__c = component.find("viaPod").get("v.value");
            requestObj.NumCivico_cl__c = component.find("numeroCivicoPod").get("v.value");
            requestObj.Provincia_cl__c = component.find("provinciaPod").get("v.value");
            requestObj.Comune_cl__c = component.find("comunePod").get("v.value");
            requestObj.Localita_cl__c = component.find("localitaPod").get("v.value");
            requestObj.Cap_cl__c = component.find("capPod").get("v.value");
            requestObj.CodIstat_cl__c = component.find("codIstatPod").get("v.value");
            requestObj.Nazione_cl__c = component.find("nazioneEsaz").get("v.value");
        }
        else if(component.find("copyFromForniture").get("v.value")==="si"){
            requestObj.Toponimo_cl__c = requestObj.Toponimo_esaz__c;
            requestObj.Via_cl__c = requestObj.Via_esaz__c;
            requestObj.NumCivico_cl__c = requestObj.NumCivico_esaz__c;
            requestObj.Provincia_cl__c = requestObj.Provincia_esaz__c;
            requestObj.Comune_cl__c = requestObj.Comune_esaz__c;
            requestObj.Localita_cl__c = requestObj.Localita_esaz__c;
            requestObj.Cap_cl__c = requestObj.Cap_esaz__c;
            requestObj.CodIstat_cl__c = requestObj.CodIstat_esaz__c;
            requestObj.Nazione_cl__c = requestObj.Nazione_esaz__c;
        }
        component.set("v.requestObj",requestObj);
        var finalAddress =  requestObj.Toponimo_cl__c + " " 
        					+ requestObj.Via_cl__c + ", " 
        					+ requestObj.NumCivico_cl__c + ", " 
        					+ requestObj.Cap_cl__c + ", " 
        					+ requestObj.Comune_cl__c + ", " 
        					+ requestObj.Provincia_cl__c; 
        if(finalAddress.includes("undefined")===false){
            component.set("v.addressSummary",finalAddress);    
        }
        
    },
})