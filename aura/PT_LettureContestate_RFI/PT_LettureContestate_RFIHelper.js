({
   	//This method is used to filter picklist values based on the parameters passed and sets the fields attribute name with the picklist values    
 	filterPicklistValues : function(component, event, helper, RecordTypeName,FieldAPINAME,atbName,ContFieldAPI,ContFieldVal,ContField2API,ContField2Val){
        var picklistValueSet = new Set();
        var picklistCustomSettingMapTemp = component.get('v.picklistCustomSettingMap');
        var picklistCustomSettingListTemp = picklistCustomSettingMapTemp[FieldAPINAME];
        var allpicklistValuesTemp = component.get('v.picklistValues').pickListValues[FieldAPINAME];             
        
        var x;    
        if(picklistCustomSettingListTemp!==undefined){   
            for(var i = 0; i < picklistCustomSettingListTemp.length; i++) {
                if(ContFieldAPI!=='' && ContFieldVal!==''){
                    if(ContField2API!=='' && ContField2Val!==''){
                        if(picklistCustomSettingListTemp[i].Controlling_Field_API__c === ContFieldAPI && picklistCustomSettingListTemp[i].Controlling_Field_Value__c === ContFieldVal &&
                           picklistCustomSettingListTemp[i].Controlling_Field_2_API__c === ContField2API && picklistCustomSettingListTemp[i].Controlling_Field_2_Value__c === ContField2Val)  {
                            picklistValueSet.add(picklistCustomSettingListTemp[i].Picklist_Value__c); 
                        }
                    }    
                    else{
                        if(picklistCustomSettingListTemp[i].Controlling_Field_API__c === ContFieldAPI && picklistCustomSettingListTemp[i].Controlling_Field_Value__c === ContFieldVal)  {
                            picklistValueSet.add(picklistCustomSettingListTemp[i].Picklist_Value__c); 
                        }
                    }  
                }
                else{    
                    picklistValueSet.add(picklistCustomSettingListTemp[i].Picklist_Value__c);
                } 
            }
        }
        var picklistValueListFinal = allpicklistValuesTemp.slice(0);
        var j = allpicklistValuesTemp.length;
        var picklistKeyValue = new Array(); //This array is being used to render picklist values & labels on component UI
        var counter = 0;
        for(var k = 0; k < allpicklistValuesTemp.length; k++) {
            //alert('allpicklistValuesTemp[k] '+allpicklistValuesTemp[k]);
            var picklistvaluesplit = allpicklistValuesTemp[k].split("~",1); // Splits the Picklist string into Value & Label
            //alert('splitvalue'+splitvalue);
            if(picklistValueSet.has(picklistvaluesplit[0]))//If picklist value is present in the set then add the same in a new array
            {
                //alert ('inside if');
                picklistKeyValue[counter] = new Array(2);
                picklistKeyValue[counter] = allpicklistValuesTemp[k].split("~");
                counter = counter +1;
            }
            else{
                picklistValueListFinal.splice(k-j,1);
            }
        }
        //alert('array'+f);
        component.set(atbName, picklistKeyValue);
        
    },       
/************************************************************************************************
@Author:Sharan Sukesh
@Description:This method is used to match the entered field value to the regular expression and 
 throws error to the user if a mismatch is found
*************************************************************************************************/
     checkformat : function(component, event, helper,fieldauraid){
        var	fieldValue = component.find(fieldauraid).get("v.value");
        var label = component.find(fieldauraid).get("v.label");
        if(!$A.util.isEmpty(fieldValue)){
        var strVal = fieldValue.toString();
        var lettFormat = /^\d{0,12}(\.\d{0,3})?$/g;
        var validatelettFormat = strVal.match(lettFormat);  
        if(validatelettFormat){
        	var inputCmp = component.find(fieldauraid);
            inputCmp.set("v.errors", null);
        }
        else{
        	var errMsg = 'Controllare la validità del campo '+label;
            var inputCmp1 = component.find(fieldauraid);
            inputCmp1.set("v.errors", [{message:errMsg}]);
         	}
         }
         else{
                var inputCmp2 = component.find(fieldauraid);
                inputCmp2.set("v.errors", null);
         	 }
    	},
})