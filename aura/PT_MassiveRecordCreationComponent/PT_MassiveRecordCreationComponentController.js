({ 
    doInit : function(component, event, helper) {
        helper.initilizePicklistValues(component, event, helper);
        component.set("v.Spinner", true);
        
    },
    validateandcreate : function(component, event, helper) {
        var errorMsg_car = helper.validateDatiCarricamentoRFI(component,helper);
        if(!$A.util.isEmpty(errorMsg_car))
        {
            helper.throwErrorMsg(component,errorMsg_car,helper) ;
            return;
        }
        var errorMsg_file = helper.validateFileUpload(component,helper);
        if(!$A.util.isEmpty(errorMsg_file))
        {
            helper.throwErrorMsg(component,errorMsg_file,helper) ;
            return;
        }
        
        helper.createRequest(component,helper);
    },

    
    formatMismatchChange : function(component,event,helper){
        alert("format mismatch change");
        if(component.get('v.formatMismatch1') ==="true"){
            component.set('v.formatError',true);
        }
        else{
            component.set('v.formatError',false);
        }
        
    },
    
    fileUploadSequenceHandler : function(component,event,helper){
        var request = component.get('v.request');
        var recordId = component.get('v.requestRecId');
        var file1Status = component.get('v.uploadFlag1');
        if(file1Status !== 'not uploaded'){
            component.set("v.Spinner",false);
           	//  debugger;
     	     
        var action = component.get("c.updateFileUploadCheckbox");
        action.setParams({
            "request" : request, 
        });
        
        action.setCallback(this,function(response){
			var navEvt = $A.get("e.force:navigateToSObject");
        	navEvt.setParams({
            	"recordId": recordId,
            	"slideDevName": "detail"
        	});
        	navEvt.fire();
        });
        $A.enqueueAction(action);        
        	
        }
    },
    cancel : function(component, event, helper) 
    {
        //debugger;
        //Navigates user back to community home page on click of cancel button.
        window.location.href = $A.get("$Label.c.PT_Community_Base_URL");        
    },
   showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner      
       component.set("v.Spinner", true);
      
   },
 hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
   },
	
})