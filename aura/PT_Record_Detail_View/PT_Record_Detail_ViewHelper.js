({
   renderfields : function(component, event, helper) {
		   	   var cod = component.get("v.recordId");
		       var controllerValues = component.get("c.getRecordDetails");
		       controllerValues.setParams({
	           strObjectName : component.get("v.objectName"),
	           recordtypeName : component.get("v.recordtypename"),
	           sectionName : component.get("v.sectionname"),
	           objid :component.get("v.recordId"),
	           queryField : component.get("v.QueryField")
	           });
               controllerValues.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        var responseData = response.getReturnValue();
                        // var costiRecord ;
                         if (responseData.recID != null) {
                             //costiRecord = responseData.Costi_Rec;
                         	 component.set("v.RecId", responseData.recID);
                         }
                        if (responseData != null) {
                           component.set("v.layoutRec",responseData.Field_Rec );
                        }
                       
                    }
                });
                $A.enqueueAction(controllerValues);   
   }, 
   initializePagesection : function(component, event, helper) {
        var existingText = component.get("v.collpaseText"); 
        var container = component.find("containerCollapsable") ;
        var closeIcon = component.find("closeIcon") ;
        var openIcon = component.find("openIcon") ;
        var expandSec = component.get("v.expandSec");
        //debugger;
        if(expandSec)
        {
            $A.util.addClass(container, 'slds-hide');  
            $A.util.addClass(closeIcon, 'slds-hide');    
            component.set("v.collpaseText",'chevrondown');
            
        }
        else
        {
            $A.util.addClass(openIcon, 'slds-hide');   
            component.set("v.collpaseText",'chevronright'); 
        }

   },
   toggleCollapseHandler : function(component, event) 
    {  
        
        var existingText = component.get("v.collpaseText"); 
        var container = component.find("containerCollapsable") ;
        var closeIcon = component.find("closeIcon") ;
        var openIcon = component.find("openIcon") ;
        var expandSec = component.find("expandSec");
        
        
        if(existingText === "chevronright")
            {
        component.set("v.collpaseText","chevrondown");
                $A.util.addClass(container, 'slds-hide');            
                $A.util.addClass(closeIcon, 'slds-hide');
                $A.util.removeClass(openIcon, 'slds-hide'); 
            }
            else
            {
                component.set("v.collpaseText","chevronright");
                $A.util.removeClass(container, 'slds-hide');
                $A.util.addClass(openIcon, 'slds-hide');            
                $A.util.removeClass(closeIcon, 'slds-hide');             
            }  
          
            
  } 
})