({
    // Performs UI validations on Dati Venditore RFI component.
    // reference: 420_PT_Logiche_pagine_Inserimento (Common|VEND_1)
    validateDatiRichiestaRFI : function(component,helper) {
        
        var errMsg = null;
        var requestObj = component.get('v.request'); 
        var recType = component.get("v.recordType"); 
        component.set('v.recordTypeName',recType.Name); 
        var recTypeName=component.get("v.recordTypeName");
        var strIdRichiestadaAnnullare = requestObj.IdRichAnn__c; 
        var strDataAnnullamento = requestObj.DataAnnullamento__c;
        var strTipoRichiesta = requestObj.TipoRichiesta_cl__c;
        var strCodReclamo = requestObj.CodReclamo_cl__c;
        var strTipoReclamo = requestObj.TipoReclamo_cl__c;
        var strDataRicReclamo = requestObj.DataRicezioneReclamo__c;
        var strCodRichiesta = requestObj.CodRichiesta__c;
        var strRiferimento = requestObj.RiferimentoDocQuesiti__c;
        
        if(recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_AN1"))){
            if($A.util.isEmpty(strIdRichiestadaAnnullare)){
                return $A.get("$Label.c.PT_ErrorMsg1")+' Id Richiestada Annullare '+$A.get("$Label.c.PT_ErrorMsg2");
                
            }
            if($A.util.isEmpty(strDataAnnullamento)){
                return $A.get("$Label.c.PT_ErrorMsg1")+' Data Annullamento '+$A.get("$Label.c.PT_ErrorMsg2");
            }   
            
        }
        if(recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M01"))||recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M02"))){
            if($A.util.isEmpty(strCodReclamo)){
                return $A.get("$Label.c.PT_ErrorMsg1")+' Codice Reclamo Cliente '+$A.get("$Label.c.PT_ErrorMsg2");
            }
            if($A.util.isEmpty(strTipoRichiesta)){
                return $A.get("$Label.c.PT_ErrorMsg1")+' Cliente Finale '+$A.get("$Label.c.PT_ErrorMsg2");
            }   
            
        }
        
        if(recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_ARS"))){
            if($A.util.isEmpty(strTipoReclamo)){
                return $A.get("$Label.c.PT_ErrorMsg1")+' Tipo Reclamo Cliente '+$A.get("$Label.c.PT_ErrorMsg2");
            }
            if($A.util.isEmpty(strDataRicReclamo)){
                return $A.get("$Label.c.PT_ErrorMsg1")+' Data Ricezione Reclamo '+$A.get("$Label.c.PT_ErrorMsg2");
            }   
            
        }
        //PREV_M02_27//Added as per d-317
        if(recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M02"))){
            if($A.util.isEmpty(strTipoReclamo)){
                if(strTipoRichiesta==='3' || strTipoRichiesta==='4'){
                  if(!$A.util.isEmpty(strRiferimento)){
                    	return 'Non è possibile inserire riferimenti a documenti per reclami telefonici, per proseguire cancellare il campo Riferimento Doc. Quesiti';
                      }
                 }
            } 
        }        
        return errMsg;
    },
    
    validateDatiVenditoreRFI : function(component,helper) {
        
        var errMsg = null;
        var requestObj = component.get('v.request');
        var recType = component.get("v.recordType");
        component.set('v.recordTypeName',recType.Name); 
        var recTypeName=component.get("v.recordTypeName");
        var strContrattodidispacciamento = requestObj.DescrDispaccEstesa__c; 
        var strCodiceRichiestaVenditore = requestObj.IdRichTrader__c;
        
        if($A.util.isEmpty(strContrattodidispacciamento)){
            if(recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_PD2"))&&
               //recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_GD1"))&&
               recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_PD4")))
                return $A.get("$Label.c.PT_ErrorMsg1")+' Contratto di dispacciamento '+$A.get("$Label.c.PT_ErrorMsg2");

        }
        if($A.util.isEmpty(strCodiceRichiestaVenditore)){
            if(recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SM1"))){
                return 'ATTENZIONE: Codice richiesta venditore non presente';
            }
            
            else if(recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_ACV"))&&
                    
                    recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_AUT"))&&
                    recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_AVO"))&&
                     //recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_GD1"))&&
                    recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_PD2"))&&
                    recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_PD4"))){
                return $A.get("$Label.c.PT_ErrorMsg1")+' Codice Richiesta Venditore '+$A.get("$Label.c.PT_ErrorMsg2");
            }
        }
        return errMsg;
    },
    
    validatePODFornituraAddress : function(component,helper) {
        
        var errMsg = null;
        var requestObj = component.get('v.request');
        var strpod = requestObj.POD__c; 
        var strtrattamento = requestObj.Trattamento__c;
        var strDataMisura = requestObj.DataMisura__c;
        var strDataAcquisizione = requestObj.DataAcquisizioneMisura__c;
        var strDataCambioTrader = requestObj.DataCambioTrader__c;
        var strDataVoltura = requestObj.DataVoltura__c;
        var strComune = requestObj.Comune_pod__c;
        var recType = component.get("v.recordType");
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
        
        if(recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_AN1"))&&
           recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N01"))&&
           recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N02"))&&
           recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_PD2"))&&
           recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_PD4"))&&
           recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M02"))&&
           recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP2"))){ 
            if($A.util.isEmpty(strpod)){
                return $A.get("$Label.c.PT_ErrorMsg1")+' POD '+$A.get("$Label.c.PT_ErrorMsg2");
            }
        }
      if(recTypeName ===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M02"))){ 
            if(($A.util.isEmpty(strpod))&&($A.util.isEmpty(strComune))){
                return $A.get("$Label.c.PT_ErrorMsg1")+' POD '+$A.get("$Label.c.PT_ErrorMsg2");
            }
      }
        
        if(recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_AUT"))||
           recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_ACV"))||
           recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_AVO"))){
            if($A.util.isEmpty(strDataMisura)){
                return $A.get("$Label.c.PT_ErrorMsg1")+' Data Misura '+$A.get("$Label.c.PT_ErrorMsg2");
            }
        } 
        
        if(recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_AUT"))){
            if($A.util.isEmpty(strDataAcquisizione)){
                return $A.get("$Label.c.PT_ErrorMsg1")+' Data Acquisizione Misura '+$A.get("$Label.c.PT_ErrorMsg2");
            }
        }
        
        if(recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_AUT"))){
            if($A.util.isEmpty(strDataAcquisizione)){
                return $A.get("$Label.c.PT_ErrorMsg1")+' Data Acquisizione Misura '+$A.get("$Label.c.PT_ErrorMsg2");
            }
        }
        
        if(recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_AVO"))){
            if($A.util.isEmpty(strDataVoltura)){
                return $A.get("$Label.c.PT_ErrorMsg1")+' Data Voltura '+$A.get("$Label.c.PT_ErrorMsg2");
            }
        }
        
        if(recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_AUT"))||
           recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_ARS"))){
            if($A.util.isEmpty(strtrattamento)){
                return $A.get("$Label.c.PT_ErrorMsg1")+' Trattamento '+$A.get("$Label.c.PT_ErrorMsg2");
            }
        } 
        
        return errMsg;
    },
    
    // invokes lightning component event PT_RequestIptypeEvent
    // reference: 420_PT_Logiche_pagine_Inserimento (Common|INT_1)
    validateDatiIntestatarioRFI : function(component, event, helper) {
        
        var errMsg = null;
        var requestObj = component.get('v.request'); 
        var strCodFisc = requestObj.CodFisc_cl__c;
        var recType = component.get("v.recordType");
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
        var selectedRFI=component.get("v.selected");
        var strNome = requestObj.Nome_cl__c;
        var strCognome = requestObj.Cognome_cl__c;
        var strSociale = requestObj.RagSoc_cl__c;
        
        if(recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A03"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A01"))||
           // recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_I01"))||
           // recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_MC1"))||
           // recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M01"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M02"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N01"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N02"))||
           // recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SM1"))||
           //recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP1"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP2"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S02"))||
           // recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_V02"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S01")))
        {
            if(selectedRFI==="NameSurname"){
                component.set("v.request.RagSoc_cl__c","");
                //alert(strNome.size());
                if($A.util.isEmpty(strNome) || $A.util.isEmpty(strCognome))  
                {
                    return 'In caso di intestazione a "Nome e Cognome" è necessario valorizzare i campi Nome e Cognome.';             
                }
            }
            if(selectedRFI==="BusinessName"){
                component.set("v.request.Nome_cl__c","");
                component.set("v.request.Cognome_cl__c","");
                if($A.util.isEmpty(strSociale))  
                {
                    return 'In caso di intestazione a "Ragione Sociale" è necessario valorizzare il campo Ragione Sociale.';             
                    
                }
            }}
        
        if(recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A01"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A03"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M01"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M02"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N01"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N02"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP2"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S01"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S02"))||
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP1"))){
            
            if($A.util.isEmpty(strCodFisc)){
                return $A.get("$Label.c.PT_ErrorMsg1")+' codice fiscale '+$A.get("$Label.c.PT_ErrorMsg2");
            }
        }
        
        //var codiceFiscale = component.find("CodFisc_cl__c").get("v.value");
        /*  var codiceFormat = new RegExp($A.get("$Label.c.PT_RICHIESTE_CodiceFiscale_REGEX"));
        
         if(recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A01"))&&
            recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_ACV"))&&
            recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_AN1"))&&
            recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_AUT"))&&
            recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_AVO"))&&
            recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M03"))&&
            recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_PD2"))&&
            recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_PD4"))&&
            recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SC1"))&&
            recTypeName!==($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP2"))&& strCodFisc!==null){
    			var validateCodiceFormat = codiceFormat.test(strCodFisc);
        			if(!validateCodiceFormat){
        				return 'Errore di controllo sul formato per i seguenti campi: Codice fiscale' ;
        				}
    			 }*/
        return errMsg;
        
    },
    
    // invokes UI validation checks for Dati Contrattuali RFI components.
    // reference: 420_PT_Logiche_pagine_Inserimento (Common|CONTR)    
    validateDatiContrattualiRFI : function(component,helper) {
        
        var errMsg = null;
        var recType = component.get("v.recordType");
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
        var requestObj = component.get('v.request'); 
        var strTipologiadiconnessione = requestObj.IdConnessione__c;
        var strTipologiacontrattorichiesta = requestObj.IdTipoContr__c;
        var strTensioneFaserichiesta = requestObj.Tensione__c;
        var strUsoEnergia = requestObj.IdUsoEnergia__c;
        var strDataFine = requestObj.DataFine__c;
        var strSettoreIndustrialeMerceologico = requestObj.IdSettore__c;
        var strAutocertFormContServdiConn = requestObj.AutocertContrConn__c;
        
        if(($A.util.isEmpty(strTipologiadiconnessione)||(strTipologiadiconnessione === '--- None ---' ))&&
           recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N02")))
        {
            return $A.get("$Label.c.PT_ErrorMsg1")+' Tipologia di connessione '+$A.get("$Label.c.PT_ErrorMsg2");
        }
        
        if(($A.util.isEmpty(strTipologiacontrattorichiesta) ||(strTipologiacontrattorichiesta === '--- None ---' ))&&
           (recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N01"))||
            recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N02"))))
            //Added as part of D-0023 for PT_N02 RT
        {
            return 'Selezionare almeno uno tra Tipologia contratto richiesta e Tipologia contratto in atto; se nessuno dei due viene valorizzato, impossibile proseguire';
        }
        
        if(($A.util.isEmpty(strTensioneFaserichiesta)||(strTensioneFaserichiesta === '--- None ---' ))&&
           (recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N01"))||
            recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N02")))
          ){
            return $A.get("$Label.c.PT_ErrorMsg1")+' Tensione/Fase richiesta '+$A.get("$Label.c.PT_ErrorMsg2");
        }
        
        if(($A.util.isEmpty(strUsoEnergia)||(strUsoEnergia === '--- None ---'))&&
           (recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N01"))||
            recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N02")))
          ){
            return $A.get("$Label.c.PT_ErrorMsg1")+' Uso Energia '+$A.get("$Label.c.PT_ErrorMsg2");
        }
        if(($A.util.isEmpty(strDataFine))&&
           (recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_PC1"))||
            recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_PT1")))
          ){
            return $A.get("$Label.c.PT_ErrorMsg1")+' DataFine '+$A.get("$Label.c.PT_ErrorMsg2");
        }
        
        if(($A.util.isEmpty(strSettoreIndustrialeMerceologico)||(strSettoreIndustrialeMerceologico === '--- None ---'))&&
           (recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N02"))||
            recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S01"))||
            recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S02"))||
            recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A03"))||
            recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A01")))
          ){
            return 'Il Settore Industriale Merceologico deve essere valorizzato';
        }
        if(($A.util.isEmpty(strAutocertFormContServdiConn)||(strAutocertFormContServdiConn === '--- None ---'))&&
           (recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N02"))||
            recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S01"))||
            recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S02"))||
            recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A03"))||
            recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A01")))
          ){
            return $A.get("$Label.c.PT_ErrorMsg1")+' Autocertificazione Formalizzazione Contratto Servizio di Connessione '+$A.get("$Label.c.PT_ErrorMsg2");
        }
        return errMsg;
    },
    // invokes UI validation checks for DatiMisura RFI components.
    validateDatiMisuraRFI : function(component,helper) {
        var errMsg = null;
        var recType = component.get("v.recordType");
        var requestObj = component.get('v.request');
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
        var EnergiaAttivaMonorario = requestObj.EnergiaAttMonorario__c;
        var EnergiaReattMonorario = requestObj.EnergiaReattMonorario__c;
        var PotenzaMonorario = requestObj.PotenzaMonorario__c;
        var Trattamento = requestObj.Trattamento__c;
        var EnergiaaAttF1 = requestObj.EnergiaAtt_F1__c;
        var EnergiaaAttF2 = requestObj.EnergiaAtt_F2__c;
        var EnergiaaAttF3 = requestObj.EnergiaAtt_F3__c;
        
        //debugger;
        if(recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_ACV"))||
           recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_AVO"))){
            if($A.util.isEmpty(EnergiaAttivaMonorario)){
                return $A.get("$Label.c.PT_ErrorMsg1")+' Energia Attiva Monorario '+$A.get("$Label.c.PT_ErrorMsg2");
            }
          /*  if($A.util.isEmpty(EnergiaReattMonorario)){
                return 'Il campo Energia Reattiva Monorario è obbligatorio.';
            }
            if($A.util.isEmpty(PotenzaMonorario)){
                return 'Il campo Potenza Monorario è obbligatorio.';
            } */
        } 
        
        /*if((recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_ARS"))) && (Trattamento === 'M')){
            if($A.util.isEmpty(EnergiaAttivaMonorario)){
            return 'Per il Trattamento Monorario è necessario valorizzare il seguente campo: Energia Attiva Monorario';
           }    
        }
        if((recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_ARS"))) && (Trattamento === 'F')){
            if($A.util.isEmpty(EnergiaaAttF1)){
            return 'Per il Trattamento A Fasce è necessario valorizzare i seguenti campi : Energia Attiva Fascia 1 ';
            }
            if($A.util.isEmpty(EnergiaaAttF2)){
            return 'Per il Trattamento A Fasce è necessario valorizzare i seguenti campi : Energia Attiva Fascia 2 ';
            }
            if($A.util.isEmpty(EnergiaaAttF3)){
            return 'Per il Trattamento A Fasce è necessario valorizzare i seguenti campi : Energia Attiva Fascia 3 ';
            }
      }*/ 
        return errMsg;
    },
    // invokes UI validation checks for Disalimentabile RFI components.
    // reference: 420_PT_Logiche_pagine_Inserimento (Common|DIS_1)
    validateDisalimentabileRFI : function(component,helper) {
        
        var errMsg = null;
        var recType = component.get("v.recordType");
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
        var requestObj = component.get('v.request'); 
        var strDisalimentabile = requestObj.FlagDisalimentabile__c;
        
        if($A.util.isEmpty(strDisalimentabile) && 
           (recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N02"))||
            recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_GD1"))||
            recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S01"))||
            recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_S02"))||
            recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A03"))||
            recTypeName===($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A01")))){
            return $A.get("$Label.c.PT_ErrorMsg1")+' Disalimentabile '+$A.get("$Label.c.PT_ErrorMsg2");
        }
        return errMsg;
    },
    // invokes UI validation checks for Altri Dati RFI components.
    // reference:  
    validateAltriDatiRFI : function(component,helper) {
        
        var errMsg = null;
        var recType = component.get("v.recordType");
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
        var requestObj = component.get('v.request'); 
        var strAutocertificazioneIstanza = requestObj.FlagAutocertIstanza__c;
        var strPresenzaCliente = requestObj.FlagPresenzaCliente__c;
        var strNumerotelefonoRichiedente = requestObj.NumTelRichiedente__c;  
        
        if($A.util.isEmpty(strAutocertificazioneIstanza)&&
           (recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N01"))||
            recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_PC1"))||
            recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_MC1"))||
            recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A01"))||
            recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_A03"))||
            recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N02")))){
            return $A.get("$Label.c.PT_ErrorMsg1")+' Autocertificazione Istanza '+$A.get("$Label.c.PT_ErrorMsg2"); 
        }
        if($A.util.isEmpty(strPresenzaCliente)&& 
           (recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_V01"))||
            recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_V02")))){
            return $A.get("$Label.c.PT_ErrorMsg1")+' Presenza Cliente '+$A.get("$Label.c.PT_ErrorMsg2");
        }
        if($A.util.isEmpty(strNumerotelefonoRichiedente)&&
           (recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_N01")))){
            return $A.get("$Label.c.PT_ErrorMsg1")+' Numero telefono Richiedente '+$A.get("$Label.c.PT_ErrorMsg2");
        }  
        return errMsg;
    },
    
    // invokes UI validation checks for Parametri di Ricerca RFI components.
    // reference:  
    validateParametri : function(component,helper) {
        
        var errMsg = null;
        var recType = component.get("v.recordType");
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
        var requestObj = component.get('v.request'); 
        var strPOD = requestObj.PodRicerche__c; 
        
        if($A.util.isEmpty(strPOD)&&
           (recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_PD4")))){
            return 'POD non presente';
        }
        return errMsg;
    }, 
    
        validateDatiRichestaclienteRFI : function(component,helper) {
        debugger;
		var ErrorMsgLable=$A.get("$Label.c.PT_DatiRich_UI_1");
        var ErrorMsgLable1=$A.get("$Label.c.PT_DatiRich_UI_2");
        var ErrorMsgLable2=$A.get("$Label.c.PT_DatiRich_UI_3");
        var ErrorMsgLable3=$A.get("$Label.c.PT_DatiRich_UI_4");
        var ErrorMsgLable4=$A.get("$Label.c.PT_DatiRich_UI_5");
        var ErrorMsgLable5=$A.get("$Label.c.PT_DatiRich_UI_6");
        var errMsg = null;
        var recType = component.get("v.recordType");
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
        var requestObj = component.get('v.request'); 
        var Tiporich = requestObj.TipologiaRichiesta__c; 
        var Elec = requestObj.ElencoDatiTecniciRichiesti__c; 
        var Richnuo = requestObj.RichiestaNuovoTentativo__c; 
        var Appinto = requestObj.Appuntamento_cl__c; 
        var Corr = requestObj.CorrettCompl_Dati__c;
        var Elecnodati = requestObj.ElencoDati__c; 
        
        /*if(((recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M01"))) ||
 			(recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M02")))) &&
 			($A.util.isEmpty(Tiporich) || Tiporich ==='' )){
             return ErrorMsgLable;
        }*/
         if(recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M01"))){
           if($A.util.isEmpty(Tiporich)){
            	return ErrorMsgLable;
            }
        } 
         if(recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M02"))){
           if($A.util.isEmpty(Tiporich)){
            	return ErrorMsgLable;
            }
        } 
            if(((recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M02"))) || 
            (recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M03")))) && 
             $A.util.isEmpty(Elec)){
				return ErrorMsgLable4;        
        }
             if((recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M02")))&& 
           ($A.util.isEmpty(Corr) || Corr ==='')){
			   return ErrorMsgLable3;
        }
        if((recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M02"))) &&
          (!($A.util.isEmpty(Corr) || Corr ==='' ||Corr ==='N'))&& ($A.util.isEmpty(Elecnodati)) ){
				return ErrorMsgLable5;       
        }
      
        if(((recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M01"))) ||
 			(recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M02")))) &&
 			($A.util.isEmpty(Richnuo) || Richnuo ==='' )){
			return ErrorMsgLable1;
        }
      
        if(((recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M01"))) ||
 			(recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M02")))) &&
 			($A.util.isEmpty(Appinto) || Appinto ==='' )){
				return ErrorMsgLable2;
        }
        
       
       return errMsg;
    }, 
    
    validateDatiServCurveRFI : function(component,helper) {
        var ErrorMsgLable=$A.get("$Label.c.PT_DatiServ_UI_1");
        var ErrorMsgLable1=$A.get("$Label.c.PT_DatiServ_UI_2");
        var errMsg = null;
        var recType = component.get("v.recordType");
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
        var requestObj = component.get('v.request'); 
        var Tiposervizio = requestObj.TipologiaServizio__c;
        var Datadorrenz = requestObj.DataDecorrenzaServizio__c;
        
        if($A.util.isEmpty(Tiposervizio)&&
           (recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SC1")))){
            return ErrorMsgLable;
        }
        if($A.util.isEmpty(Datadorrenz)&&
           (recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SC1")))){
            return ErrorMsgLable1;
        }
        return errMsg;
    },
    
    // invokes UI validation checks for Letture Contestate RFI components.
    // reference:  
    validateLettureContestate : function(component,helper) {
        var ErrorMsgLable=$A.get("$Label.c.PT_LettCon_UI_1");
        var ErrorMsgLable1=$A.get("$Label.c.PT_LettCon_UI_2");
        var ErrorMsgLable2=$A.get("$Label.c.PT_LettCon_UI_3");
        var errMsg = null;
        var recType = component.get("v.recordType");
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
        var requestObj = component.get('v.request'); 
        var strAttFis3 = requestObj.Att_F3_LetCon__c;
        var tipoLett = requestObj.TipologiaLettura__c;
        var datLett = requestObj.DataLettura__c;
        
        if($A.util.isEmpty(strAttFis3)&&
           (recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M01")))){
            return ErrorMsgLable;
        }
        if($A.util.isEmpty(tipoLett)&&
           (recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M01")))){
            return ErrorMsgLable1;
        }
        if($A.util.isEmpty(datLett)&&
           (recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M01")))){
            return ErrorMsgLable2;
        }
        
        return errMsg;
    },
    
    validateFileUpload : function(component,helper){
        debugger;
        var recType = component.get("v.recordType");
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
        var formatCheck = component.get('v.formatError');
        if(formatCheck===true){
            if(recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M03"))){
                var errMsg1 = $A.get("$Label.c.PT_M03_ErrMsg");
                return errMsg1;
            }
            if(recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP2"))){
            	var errMsg1 = $A.get("$Label.c.PT_SP2_ErrMsg");
                return errMsg1;
            }
            if(recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_GD1"))){
				var errMsg1 = $A.get("$Label.c.PT_GD1_ErrMsg");
                return errMsg1;            
            }
            if(recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M01"))){
				var errMsg1 = $A.get("$Label.c.PT_M01_ErrMsg");
                return errMsg1;            
            }
            if(recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M02"))){
				var errMsg1 = $A.get("$Label.c.PT_M02_ErrMsg");
                return errMsg1;            
            }
        }
        return errMsg1;
    },
    
    
    // Throws the error as toast message as part of request field validation checks.
    throwErrorMsg : function(component,errMsg,helper) 
    { 
        var resultsToast = $A.get("e.force:showToast");
        if(resultsToast)
        {    
            resultsToast.setParams({"title": "Errore","message": errMsg,"type":"error","mode":"sticky"});
            resultsToast.fire();
        }            
    },
    
    
    //Performs Pre validation checks across RFI components.
    //reference: 420_PT_ControlliPrevalidazione_v1.0 (Common, NCA, CODFISC_PREV, DATI_FISCALI_PREV, PARTITA_IVA_PREV)
    checkPrevalidationErrors : function(component,helper) {
        debugger;
        var errMsg = null;
        var request = component.get("v.request");
        var action = component.get("c.pt_Prevalidations");
        action.setParams({
            "request" : request, 
        });
        
        action.setCallback(this,function(response){
            
            var state = response.getState(); 
            if(state === $A.get("$Label.c.PT_SUCCESS")){
                errMsg = response.getReturnValue();
                if(!$A.util.isEmpty(errMsg))
                {
                    this.throwErrorMsg(component,errMsg,helper);
                    return;
                }
                else{
                    // If there are no Pre-vaidation errors then invoke logic to insert request record.
                    debugger;
                    this.createRequest(component,helper);
                }
                return errMsg;
            }   
            
        });
        $A.enqueueAction(action);        
    },
    
    interactiveOutcomeManagment : function(component,event,helper){
        var ammissibilita = null;
       
        var recordId = component.get('v.requestRecId');      
        var request = component.get("v.request");
        var action1 = component.get("c.InteractiveOutcomeManagement");
         component.set("v.Spinner",true);  
        action1.setParams({
            requestId : request.Id,
        });
        //debugger;
        action1.setCallback(this,function(result){
            var state = result.getState(); 
            //debugger;
            if(state === $A.get("$Label.c.PT_SUCCESS")){
                //debugger;
                var retValue = result.getReturnValue();
                if(retValue !== null){
                    component.set("v.requestObj", retValue);
                    component.set("v.request",retValue);
                    ammissibilita = retValue.AmmissCodCausale__c;
                    //retValue.AmmissCodCausale__c = '000';
                    //ammissibilita = retValue.AmmissCodCausale__c;
                    component.set("v.request",retValue);
                    if(!$A.util.isUndefined(ammissibilita)){
                        component.set("v.Spinner",false);
                        component.set("v.renderInteractiveComponent","true");  
                    }
                    else{
                        setTimeout($A.getCallback(
                            function(){
                                //debugger;
                                var request = component.get("v.request");
                                var action1 = component.get("c.InteractiveOutcomeManagement");
                                action1.setParams({
                                    requestId : request.Id,
                                });
                                
                                action1.setCallback(this,function(result){
                                    var state = result.getState(); 
                                    //debugger;
                                    if(state === $A.get("$Label.c.PT_SUCCESS")){
                                        var retValue = result.getReturnValue();
                                        if(retValue !== null){
                                            component.set("v.requestObj", retValue);
                                            component.set("v.request",retValue);
                                            ammissibilita = retValue.AmmissCodCausale__c;
                                            if(!$A.util.isUndefined(ammissibilita)){
                                                component.set("v.Spinner",false);
                                                component.set("v.renderInteractiveComponent",true);  
                                            }
                                            else{
                                                setTimeout($A.getCallback(function(){
                                                    //debugger;
                                                    var request = component.get("v.request");
                                                    var action1 = component.get("c.InteractiveOutcomeManagement");
                                                    action1.setParams({
                                                        requestId : request.Id,
                                                    });
                                                    
                                                    action1.setCallback(this,function(result){
                                                        var state = result.getState(); 
                                                        //debugger;
                                                        if(state === $A.get("$Label.c.PT_SUCCESS")){
                                                            var retValue = result.getReturnValue();
                                                            if(retValue !== null){
                                                                component.set("v.requestObj", retValue);
                                                                component.set("v.request",retValue);
                                                                ammissibilita = retValue.AmmissCodCausale__c;
                                                                if(!$A.util.isUndefined(ammissibilita)){
                                                                    component.set("v.Spinner",false);
                                                                    component.set("v.renderInteractiveComponent","true");  
                                                                }
                                                                else{
                                                                    component.set("v.Spinner",false);
                                                                    component.set("v.renderInteractiveComponent","true");
                                                                }
                                                            }
                                                            
                                                        }
                                                    });
                                                    $A.enqueueAction(action1);  
                                                }), 1000);
                                            }
                                        }
                                    }
                                });
                                $A.enqueueAction(action1);  
                            }), 1000);
                    }                      
                }
            }
        });              
        $A.enqueueAction(action1); 
    },
    
    //This method invokes Apex controller method which inserts request object record post all the validation checks.
    createRequest : function(component,helper) {
        var ammissibilita = null;
        var recType = component.get("v.recordType");
        component.set('v.recordTypeName',recType.Name);
        var recTypeName=component.get("v.recordTypeName");
        var request = component.get("v.request");
        var action = component.get("c.createRequestRecord");
        component.set("v.Spinner",true);
        action.setParams({
            request : request,
         });
        //debugger;       
        action.setCallback(this,function(a){
            var state = a.getState();
            //debugger;
            if(state === $A.get("$Label.c.PT_SUCCESS")){
                //debugger;
                var response = a.getReturnValue();
                var responseId = response.request.Id;
                var errorCode = response.errorCode;
                var errorMessage = response.errorMessage;
                if(errorCode==='001'){
                    component.set("v.Spinner",false);
                    helper.throwErrorMsg(component,errorMessage,helper);
                    return;
                }
                
                else{
                    component.set("v.request",response.request);
                   // alert('001----- '+response.request.CodFisc_cl__c);
                    //debugger;
                    //Add record type check to invoke file upload logic
                    if((recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M03"))) ||
                       (recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_GD1"))) ||
                       (recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M01"))&& component.get("v.TipoRichiestaCliente")== true) ||
                       (recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M02"))&& component.get("v.TipoRichiestaCliente")== true) ||
                       (recTypeName === ($A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP2")))){
                        debugger;
                        var appEvent = $A.get("e.c:PT_Upload_File_Container_Event");
                        appEvent.setParams({
                            "responseid" : responseId });
                        appEvent.fire();
                    }
                    
                    else{
        				var request = component.get('v.request');
                        var action = component.get("c.updateFileUploadCheckbox");
                        action.setParams({
                            "request" : request, 
                        });
                        
                        action.setCallback(this,function(response){
                            helper.interactiveOutcomeManagment(component,event,helper);
                            
                        });
                        $A.enqueueAction(action);
                        
                    }  
                }
            } 
            
        });
        $A.enqueueAction(action);
        //alert('003---'+response.request.CodFisc_cl__c);
        component.set("v.Spinner",true);
    },
    
    checkfeasibilty : function(component,helper) {
        debugger;
        //alert('inside Create');
        var request = component.get("v.request");
        var action1 = component.get("c.createRequestRecord");
        //helper.queryRecordType(component);
        //debugger;
        action1.setParams({
            request : request,
            // requestTwo:requestEventParam              
        });
        action1.setCallback(this,function(result){
            var state = result.getState(); 
            //alert('state ::'+ state);
            //debugger;
            if(state === $A.get("$Label.c.PT_SUCCESS")){
                var retValue = result.getReturnValue();
                if(retValue !== null){
                    component.set("v.requestObj", retValue);
                    component.set("v.request",retValue);
                    var ammissibilita = retValue.AmmissCodCausale__c;
                    alert('ammissibilita'+ammissibilita);
                    if(ammissibilita!==null){
                        alert('you have an error in feasibility');
                    }
                }
                //return ammissibilita;
            }
        });
        //var controllerValues = component.get("c.finalcheck");
        //alert('inside controllerValues'+controllerValues);
        //console.log('controllerValues==>'+controllerValues);
        
        /*controllerValues.setCallback(this, function(response){
            var state = response.getState();
            //alert('state==>'+controllerValues-state);
            
            if (state === $A.get("$Label.c.PT_SUCCESS")){
                var responseData = response.getReturnValue(); 
                //console.log('responseData==>'+JSON.stringify(responseData)); 
                //alert('responseData.IdMessaggioPT-->'+responseData.IdMessaggioPT);                       
                if(responseData != null){
                    component.set("v.Message", responseData.IdMessaggioPT );
                    component.set("v.ammissCodCausale", responseData.AmmissCodCausale );                
                    component.set("v.ammissDescrizione",responseData.AmmissDescrizione );
                    console.log('Messa==>'+responseData.IdMessaggioPT);
                    console.log('Messa1==>'+responseData.AmmissCodCausale);
                    console.log('Messa2==>'+responseData.AmmissDescrizione);
                    if(responseData.IdMessaggioPT===0 || responseData.AmmissCodCausale!==null ||
                       responseData.AmmissDescrizione!==null){
                        //Code Goes here
                        errMsg='La richiesta è stata respinta con motivazione';
                        if(!$A.util.isEmpty(errMsg))
                        {
                            this.throwErrorMsg(component,errMsg,helper);
                            return;
                        }
                        else{
                            alert('No error');
                        }
                    }
                }
            }
            debugger;
        });*/
        $A.enqueueAction(action1);
        // component.set("v.Spinner",true);	    
    },
    
    initilizePicklistValues :function(component, event, helper)
    {
        debugger;
        var recTypeId = component.get("v.recordId");  
        var requestObj = component.get('v.request'); 
        var action = component.get("c.getAllRichiestaPicklistValuesCntr");
        //alert('inside');		
        action.setParams({
            'RecordTypeId': recTypeId
            
        });
        action.setCallback(this,function(response){
            var State = response.getState();
            //debugger;
            //alert(State);
            var allpicklistValues = response.getReturnValue().picklistObjectMap;
            //var picklistCustomSettingList = response.getReturnValue().reqPicklistCustomSetting;
            var picklistCustomSettingMap = response.getReturnValue().reqPicklistCustomSettingMap;
            var recTypeDetail = response.getReturnValue().reqrecordTypeDetail;
            var rfiComponentSettings = response.getReturnValue().requestRfiComponentSettings;
            // alert('1');
            component.set('v.picklistValues', allpicklistValues);  
            //component.set('v.picklistCustomSetting', picklistCustomSettingList);
            component.set('v.picklistCustomSettingMap', picklistCustomSettingMap);
            component.set("v.recordType", recTypeDetail);
            component.set("v.checkRFIVisibility",rfiComponentSettings);
            
            var recTypeDetail1 = component.get('v.recordType');  
            var desc = ' - ' + recTypeDetail1.Description;
            component.set("v.rtDesc",desc);
            var RecTypeName = recTypeDetail1.Name;
            //alert('record type name'+RecTypeName);
            helper.setTextDisplay(component,event,helper,RecTypeName);
            component.set("v.recordTypeName",RecTypeName);
            var requestRec = component.get("v.request");
            requestRec.RecordTypeId = recTypeDetail1.Id;
            component.set("v.request",requestRec);
            component.set("v.Spinner",false);
            
        });
        $A.enqueueAction(action);
        
    },
    
    setTextDisplay : function(component,event,helper,RecTypeName){
        if(RecTypeName === $A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M03")){      
            component.set('v.textDisplay',$A.get("$Label.c.PT_Upload_File_Display_M03") );
        }
        if(RecTypeName === $A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_GD1")){      
            component.set('v.textDisplay',$A.get("$Label.c.PT_Upload_File_Display_GD1") );
        }
        if(RecTypeName === $A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_SP2")){      
            component.set('v.textDisplay',$A.get("$Label.c.PT_Upload_File_Display_SP2") );
        }
        if(RecTypeName === $A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M02")){      
            component.set('v.textDisplay',$A.get("$Label.c.PT_Upload_File_Display_M02") );
        }
         if(RecTypeName === $A.get("$Label.c.PT_RICHIESTE_RecordTypeName_PT_M01")){      
            component.set('v.textDisplay',$A.get("$Label.c.PT_Upload_File_Display_M01") );
        }
    },
     
    
})