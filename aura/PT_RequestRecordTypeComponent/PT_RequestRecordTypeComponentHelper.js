({
	// this fuction invokes the recordTypeSelection method in the Apex controller PT_RequestRecordTypeController 
	// to fetch record type details
    queryRecordType : function(component) {
        //debugger;
        var action = component.get("c.getRecordTypeDetails");
        var rcdTypeStrind = component.get("v.rcdTypeName");
        //debugger;
        action.setParams({
            	 "rcdTypeName": rcdTypeStrind
        })
        //debugger;
        action.setCallback(this,function(response){
           var State = response.getState();
            if(response.getReturnValue() != null && response.getReturnValue().length > 0){
                component.set("v.requestRecordTypes", response.getReturnValue());
                document.getElementById("extensionPopup").style.display = "block";
                component.set("v.rcdTypeListflag",true);
            	//debugger;
                component.set("v.Spinner", false);
            }
         });
		$A.enqueueAction(action);
        //debugger;
		component.set("v.Spinner", true);
    },
})