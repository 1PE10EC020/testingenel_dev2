({
	doinit : function(component, event, helper) {
        debugger;
        helper.initilizePicklistValues(component, event, helper); 
	},


	fireComponentEvent : function(component, event, helper) {
		var cmpEvent = component.getEvent("cmpEvent");
        cmpEvent.setParams({
            //"message" : component.get("v.request.Cap_pod__c") 
            "requestEventObj" : component.get("v.request") 
        });
        debugger;
        cmpEvent.fire();
	},
    
    OnChangePick : function (component, event, helper) {
        var selected = event.getSource().get("v.value");
        debugger;
        console.log('Value' +selected);
        component.set("v.inpselection",selected);
        
        if(selected=="TEM"){
            component.set("v.displayDataFields","true");
            component.set("v.displayAltriFields","false");
        }
        
         if(selected!="TEM"){
            component.set("v.displayDataFields","false");
            component.set("v.displayAltriFields","true");
        }
            
     }
})