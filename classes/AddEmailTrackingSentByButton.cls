public Class AddEmailTrackingSentByButton{
@InvocableMethod
	public static void trackEmailSent(list<String> listOfCaseId){
		system.debug(logginglevel.debug, 'case in input : '+listOfCaseId);
		String OwnerRoleName;
		String emailTemplateName;
		String dtrZona;
		
		String teamLeaderRoleId;
		String parentIdTeamLeader;
		String nameRoot;
		
		try{
			list<Case> listOfCase = [Select id,Date_Time_Closed__c,ThreadID__c,Nome__c, Email_Richiedente__c,OwnerId,AssegnatoIILivello__c,AssegnatoAllEspertoDiDTRZona__c,tech_IsStatusIILivello__c, 
											CaseNumber,Subject, CreatedDate, Priority, Status, TipologiaCase__c, Description, OriginForEmail__c, ClosedDate, Eventuali_Note_Richiedente__c, Livello3__c
			 						from case where id in :listOfCaseId];
			System.debug('*** AddEmailTrackingSentByButton listOfCase.OwnerId: '+listOfCase.get(0).OwnerId);
			OwnerRoleName = Case_Helper.getOwnerCase(listOfCase, true);
			System.debug('*** AddEmailTrackingSentByButton OwnerRoleName: '+OwnerRoleName);
			/* [GP 22072016] Aggiunto per tracciare l'email inviata agli operatori delle ditte o ter appartenenti a Enel Info + */
			teamLeaderRoleId = [SELECT Id, Name, parentRoleId FROM UserRole where name =:OwnerRoleName limit 1].parentRoleId;
    		parentIdTeamLeader = [SELECT Id, Name, parentRoleId FROM UserRole where id =:teamLeaderRoleId limit 1].parentRoleId;
        	nameRoot = [SELECT Id, Name, parentRoleId FROM UserRole where id =:parentIdTeamLeader limit 1].name;
        	system.debug('**** teamLeaderRoleId '+teamLeaderRoleId+ ' **** parentIdTeamLeader: '+parentIdTeamLeader+' *** nameRoot: '+nameRoot);
        	if('Gruppo Info+'.equals(nameRoot))
        	{
        		emailTemplateName='Support_Invia_Email_Info_Piu_II_Livello';
        	}
			else
			{
                if (OwnerRoleName.containsIgnoreCase('Ped')){
                    emailTemplateName='Support_Invia_Email_II_Livello';
				  }
			else
			{
				 if (OwnerRoleName.containsIgnoreCase('Back')){
						emailTemplateName='Support_Invia_Email_BO';
				  }
				 else
				 {
				 	if (OwnerRoleName.containsIgnoreCase('Fibra')){
					 	emailTemplateName='Support_Invia_Email_Fibra';
					 }
					 else
					 {
						dtrZona = [SELECT TipoUnitOrganizzativa__c, Name FROM UnitaDiCompetenza__c where name =:OwnerRoleName].get(0).TipoUnitOrganizzativa__c;
						if (dtrZona.equalsIgnoreCase(Constants.ZONA)){
                            emailTemplateName='Support_Invia_Email_II_Livello';	
						}else{
							if (dtrZona.equalsIgnoreCase(Constants.DTR)){
								emailTemplateName='Support_Invia_Email_DTR_II_Livello';	
							}
							//ELSE IF CODA 'Contatori e Fibra'	(AGGIUNGERE l'UNITA ORGANIZZATIVA CONTATORI E FIBRA)
						}
					 }
				 }
               }
			}
			
				 
			SendEmail_Helper.TrackEmail(listOfCase.get(0),emailTemplateName); 
            
		}
		catch(Exception e ){
            system.debug(logginglevel.error, 'Exception in AddEmailTrackingSentByButton :'+e.getMessage());
        }
	}
	
	public static void trackEmailSent(list<Case> listOfCase){
		List<String> listOfCaseId = new List<String>();
		System.debug('*** trackEmailSent(list<Case> listOfCase)');
		for (case inCase:listOfCase){
			System.debug('*** nel For '+inCase.id);
			listOfCaseId.add(inCase.id);
			System.debug('*** dopo add alla lista di id del case ');
		}
		
		trackEmailSent(listOfCaseId);
	}
	
	public static void trackEmailSent(Case inputCase){
		List<String> listOfCaseId = new List<String>();
		System.debug('*** trackEmailSent(Case inputCase)'+inputCase.id);
		listOfCaseId.add(inputCase.id);
		trackEmailSent(listOfCaseId);
	}
	
	
}