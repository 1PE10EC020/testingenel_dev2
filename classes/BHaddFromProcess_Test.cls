@IsTest
public class BHaddFromProcess_Test {

  @IsTest
    static void  CalculateWithNoException16H (){
        
        list<string> listOfCaseID = new list<string>();
        case c = GenerateCase();
        c.AssegnatoIILivello__c = null;
        c.tech_IsStatusIILivello__c = true;
        
        c.tech_case_aperto_manualmente__c = true;
        c.Secondo_Livello_Manuale__c = false;
        insert c;
        listOfCaseID.add(c.id);
        id businesshourid = Constants.mapBusinessHours.get(Constants.DEFAULT_BUSINESS_HOURS);
        long millisec = 16*3600*1000;
            // retrive the createddate 
        datetime created = [select createddate from case where id = : c.id].createddate;
        datetime calculated =  BusinessHours.add(businesshourid,created,millisec);
        list<datetime> result = BHaddFromProcess.AddBusinessHour(listOfCaseID);
        datetime result2 = BHaddFromProcess.AddBusinessHour(c);
        system.assertEquals(calculated,result[0]);
    
    
    }
    
    @IsTest
    static void  CalculateWithNoException40H (){
        
        list<string> listOfCaseID = new list<string>();
        case c = GenerateCase();
        c.AssegnatoIILivello__c = null;
        c.tech_IsStatusIILivello__c = false;
        
        c.tech_case_aperto_manualmente__c = true;
        c.Secondo_Livello_Manuale__c = true;
        
        insert c;
        listOfCaseID.add(c.id);
        id businesshourid = Constants.mapBusinessHours.get(Constants.DEFAULT_BUSINESS_HOURS);
        long millisec = 40*3600*1000;
            // retrive the createddate 
        datetime created = [select createddate from case where id = : c.id].createddate;
        datetime calculated =  BusinessHours.add(businesshourid,created,millisec);
        list<datetime> result = BHaddFromProcess.AddBusinessHour(listOfCaseID);
        datetime result2 = BHaddFromProcess.AddBusinessHour(c);
    
    
    }
  
  
        /*
        * @author : Claudio Quaranta
        * @Date : 2015-06-23
        * @Description : generate new case for test purpose
        */ 
  public static case GenerateCase() {
        case c = new case();
        c.recordTypeId = [select id from recordtype where developername = 'Da_Definire' ].id ;
        c.TipologiaCase__c ='Da Definire';
        c.origin = 'Email';
        c.status='Nuovo';
        c.DescrizioneStato__c='Aperto';
        
        c.AssegnatoIILivello__c = Datetime.newInstance(2015, 3, 1, 9, 0, 0);

        return c;
    
    
    }
}