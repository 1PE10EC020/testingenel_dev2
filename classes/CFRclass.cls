/*
 * @author : Accenture
 * @date : 2011-09-28
 */
public class CFRclass {

    public static Integer indexGen {get; set;}
    public List<Customfieldwrapper> FieldList;
    public Integer numRows {get; set;}
    
    public class Customfieldwrapper {
        
        private CustomFields__c customSetting;
        private Integer index;
        public CustomObjects__c co;

        public Customfieldwrapper() {
            Map<String,String> lstParams = ApexPages.currentPage().getParameters();
            //System.Debug('Keys are ' +lstParams + '-Done-');
            
            String key = '';
            for(String param : lstParams.keySet()) {
                if(param.contains('_lkid')) {
                    key = param;
                    break;
                }
            }
            
            System.Debug('Key is ' + key);
            String strCustomObjectID = lstParams.get(key);
            this.customSetting = new CustomFields__c (Field_Label__c = 'Enter Field Label', Field_Name__c = 'Enter name', Custom_Objects__c = strCustomObjectID );
            String coId = strCustomObjectID;
            
            //this.customSetting = new CustomFields__c (Field_Label__c = 'Enter Field Label', Field_Name__c = 'Enter name', Custom_Objects__c = ApexPages.currentPage().getParameters().get(Label.CL00033));
            //String coId = ApexPages.currentPage().getParameters().get(Label.CL00033);

            try {
                if (coId == null) {
                    String currentRecordID = ApexPages.currentPage().getParameters().get('ID');
                    CustomFields__c[] customFieldList;
                    if (currentRecordID != null)
                        customFieldList = [SELECT Custom_Objects__c FROM CustomFields__c WHERE ID = :CurrentRecordID];
                    if (customFieldList.size() > 0)
                        coId = String.valueOf(customFieldList[0].Custom_Objects__c);
                }
                
                co = [SELECT Enable_History__c FROM CustomObjects__c WHERE Id = :coId]; 
            } catch(Exception ex) {
                ApexPages.addMessages(ex);
            }

            this.index = indexGen++;
        }
        
        public CustomFields__c getcustomSetting() {
            return customSetting;
        }
        
        public Integer getIndex() {
            return index;
        }
    } 
    
    public CFRclass(ApexPages.StandardController controller) {
        if(indexGen == null)
            indexGen = 1;
        FieldList = new List<Customfieldwrapper>();
        numRows = 1;
    }
    
    public List<Customfieldwrapper> getFieldList() {
        return FieldList;
    }


    public PageReference save() {
        try {
            if(!checkHistoryTracking())
                return null;
            
            List<CustomFields__c> tempList = new List<CustomFields__c>();
            
            for(Integer i=0; i<FieldList.size(); ++i) {
                CustomFields__c cc = FieldList[i].getcustomSetting();
                cc.Field_name__c = cc.Field_Name__c.replace(' ', '_');
                // cc.Field_name__c = cc.Field_Name__c.replace('__', '_');
                
                if(!cc.Field_name__c.endsWith('__c'))
                    cc.Field_name__c += '__c';
                    
                if (((cc.Custom_Objects__r.Object_Name__c == 'Account') || (cc.Custom_Objects__r.Object_Name__c == 'Contact') || (cc.Custom_Objects__r.Object_Name__c == 'Opportunity') || (cc.Custom_Objects__r.Object_Name__c == 'Case') || (cc.Custom_Objects__r.Object_Name__c == 'Lead') || (cc.Custom_Objects__r.Object_Name__c == 'Contract') || (cc.Custom_Objects__r.Object_Name__c == 'Campaign') || (cc.Custom_Objects__r.Object_Name__c == 'Solution') || (cc.Custom_Objects__r.Object_Name__c == 'Event') || (cc.Custom_Objects__r.Object_Name__c == 'Task') || (cc.Custom_Objects__r.Object_Name__c == 'Asset')))
                    cc.Enable_History__c = false;
                
                if (FieldList[i].co.Enable_History__c==false && cc.Enable_History__c==true){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: History tracking not enabled in parent object.');
                    ApexPages.addMessage(myMsg);
                    return null;
                }
                
                tempList.add(cc);
            }
            
            try {
                upsert(tempList);
            } catch(Exception ex) {
                if (ex.getMessage().contains('Concat_Object_Field_Names__c duplicates value')) {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Field already exists with same name for this object.');
                    ApexPages.addMessage(myMsg);
                } else {
                    ApexPages.addMessages(ex);
                }
                
                return null;
            }
            
            return new PageReference('/' + ApexPages.currentPage().getParameters().get('retURL'));
        } catch (System.DMLException ex) {
            ApexPages.addMessages(ex);
            return null;
        }
    }

    private boolean checkHistoryTracking() {
        
        if (FieldList.size() == 0)
            return true;
        
        Id parentObject = FieldList[0].getcustomSetting().Custom_Objects__c;
        
        AggregateResult[] ar = [SELECT COUNT(Id) prevHistoryFieldCount
                                FROM CustomFields__c
                                WHERE Custom_Objects__c = :parentObject
                                AND Enable_History__c = true];

        Integer currentHistoryFieldCount = 0;
        for (Customfieldwrapper wrapper: FieldList)
            if (wrapper.getcustomSetting().Enable_History__c == true)
                ++currentHistoryFieldCount;
        Integer overallHistoryCount = 0;
        if(ar != null && ar.size() > 0) {
            overallHistoryCount = (Integer)ar[0].get('prevHistoryFieldCount') + currentHistoryFieldCount;
        }
        if (overallHistoryCount > 20) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: More than 20 fields cannot be history tracked.');
            ApexPages.addMessage(myMsg);
            return false;
        }
        
        return true;
    }

    public void addNewRecord() {
        try {
            if(numRows > 0)
                for(Integer i=0; i<numRows; ++i)
                    FieldList.add(new CustomfieldWrapper());
        } catch (Exception ex) {
            ApexPages.addMessages(ex);
        }
    }
    
    public void InitExistingRecord() {
        try {
            String CurrentRecordID = ApexPages.currentPage().getParameters().get('ID');
            if(CurrentRecordID != null) {
                Customfieldwrapper custFldWrap = new Customfieldwrapper();
                custFldWrap.customSetting = [SELECT Concat_Object_Field_Names__c,Configuration_Environment__c,Custom_Objects__c,Data_Type__c,Decimal_Places__c,Default_Checked__c,Default_Value__c,Description__c,Development_Environment__c,Enable_History__c,External_ID__c,Field_Label__c,Field_Name__c,First_Value_Default__c,Help_Text__c,Length__c,ObjectName__c,Picklist_Values__c,Production_Environment__c,Refrence_No__c,Required__c,SFDC_Track_History__c,Sort__c,UAT_Environment__c,Unique__c,Visible_Lines__c FROM CustomFields__c WHERE ID = :CurrentRecordID];
                FieldList.add(custFldWrap);
                numRows = 1;
            }
        } catch (Exception ex) {
            ApexPages.addMessages(ex);
        }
    }
    
    public void clear() {
        FieldList.clear();
        numRows = 1;
    }
    

    public void deleteRow() {
        try {
            Integer delIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('delRow'));
            
            for(Integer i=0; i<FieldList.size(); ++i)
            if(FieldList[i].index == delIndex) {
                FieldList.remove(i);
                break;
            }
        } catch (Exception ex) {
            ApexPages.addMessages(ex);
        }
    }
}