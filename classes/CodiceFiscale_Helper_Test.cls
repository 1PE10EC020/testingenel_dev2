@IsTest
public class CodiceFiscale_Helper_Test {

@IsTest
   static void  CFValid (){
   
    // first test, male user
    String fiscalCode = 'SPSGNR80A01F839U';
    String firstName = 'Gennaro';
    String lastName = 'Esposito';
   
    String response = CodiceFiscale_Helper.CheckFiscalCode(fiscalCode, firstName,lastName);
	system.assertEquals('OK',response);
	
    // Second test , female user
    fiscalCode = 'DOEJNA80A41H501Z';
    firstName = 'Jane';
    lastName = 'Doe';
   
    response = CodiceFiscale_Helper.CheckFiscalCode(fiscalCode, firstName,lastName);
	system.assertEquals('OK',response);
	
	// third test, last name 1 char
	fiscalCode = 'EXXGNR80A01F839M';
    firstName = 'Gennaro';
    lastName = 'E';
   
    response = CodiceFiscale_Helper.CheckFiscalCode(fiscalCode, firstName,lastName);
	system.assertEquals('OK',response);
	
	
	// fourth test, last name 2 char
	fiscalCode = 'SEXGNR80A01F839W';
    firstName = 'Gennaro';
    lastName = 'ES';
   
    response = CodiceFiscale_Helper.CheckFiscalCode(fiscalCode, firstName,lastName);
	system.assertEquals('OK',response);
	
	// fifth test , name with 1 char
	
	fiscalCode = 'SPSGXX80A01F839F';
    firstName = 'G';
    lastName = 'Esposito';
   
    response = CodiceFiscale_Helper.CheckFiscalCode(fiscalCode, firstName,lastName);
	system.assertEquals('OK',response);
	// sixth test , name with 2 char
	
	fiscalCode = 'SPSGEX80A01F839P';
    firstName = 'Ge';
    lastName = 'Esposito';
   
    response = CodiceFiscale_Helper.CheckFiscalCode(fiscalCode, firstName,lastName);
	system.assertEquals('OK',response);
	
	
	// Test omocodia
	
	
	fiscalCode = 'RSSMRA80A01H5L1F';
    firstName = 'Mario';
    lastName = 'Rossi';
   
    response = CodiceFiscale_Helper.CheckFiscalCode(fiscalCode, firstName,lastName);
	system.assertEquals('OK',response);
	
	fiscalCode = 'RSSMRAULALMHRLMD';
    firstName = 'Mario';
    lastName = 'Rossi';
   
    response = CodiceFiscale_Helper.CheckFiscalCode(fiscalCode, firstName,lastName);
	system.assertEquals('OK',response);
	
   }
   
   @IsTest
   static void FirstNameNotValid(){
   // first test, male user
    String fiscalCode = 'SPSGNR80A01F839U';
    String firstName = 'Giuseppe';
    String lastName = 'Esposito';
   
    String response = CodiceFiscale_Helper.CheckFiscalCode(fiscalCode, firstName,lastName);
	system.assertEquals(Label.CF_First_6_Letter_Not_Valid,response);
	
    // Second test , female user
    fiscalCode = 'DOEJNA80A41H501Z';
    firstName = 'Johanna';
    lastName = 'Doe';
   
    response = CodiceFiscale_Helper.CheckFiscalCode(fiscalCode, firstName,lastName);
	system.assertEquals(Label.CF_First_6_Letter_Not_Valid,response);
   }
   
    @IsTest
   static void LastNameNotValid(){
    // first test, male user
    String fiscalCode = 'SPSGNR80A01F839U';
    String firstName = 'Giuseppe';
    String lastName = 'Rossi';
   
    String response = CodiceFiscale_Helper.CheckFiscalCode(fiscalCode, firstName,lastName);
	system.assertEquals(Label.CF_First_6_Letter_Not_Valid,response);
	
    // Second test , female user
    fiscalCode = 'DOEJNA80A41H501Z';
    firstName = 'Jane';
    lastName = 'Smith';
   
    response = CodiceFiscale_Helper.CheckFiscalCode(fiscalCode, firstName,lastName);
	system.assertEquals(Label.CF_First_6_Letter_Not_Valid,response);
   
   }
    @IsTest
   static void  ControlCharNotValid (){
   
    // first test, male user
    String fiscalCode = 'SPSGNR80A01F839H';
    String firstName = 'Gennaro';
    String lastName = 'Esposito';
   
    String response = CodiceFiscale_Helper.CheckFiscalCode(fiscalCode, firstName,lastName);
	system.assertEquals(Label.CF_Not_Valid,response);
	
    // Second test , female user
    fiscalCode = 'DOEJNA80A41H501A';
    firstName = 'Jane';
    lastName = 'Doe';
   
    response = CodiceFiscale_Helper.CheckFiscalCode(fiscalCode, firstName,lastName);
	system.assertEquals(Label.CF_Not_Valid,response);
	
   }
    @IsTest
    static void  LenghtNotValid (){
   
    // first test, male user
    String fiscalCode = 'SPSGNR80A01F839UAAA';
    String firstName = 'Gennaro';
    String lastName = 'Esposito';
   
    String response = CodiceFiscale_Helper.CheckFiscalCode(fiscalCode, firstName,lastName);
	system.assertEquals(Label.CF_Lenght,response);
	
    // Second test , female user
    fiscalCode = 'DOEJNA80A41H501ZAAA';
    firstName = 'Jane';
    lastName = 'Doe';
   
    response = CodiceFiscale_Helper.CheckFiscalCode(fiscalCode, firstName,lastName);
	system.assertEquals(Label.CF_Lenght,response);
	
   }
    @IsTest
    static void  genericError (){
    	  // first test, male user
    String fiscalCode = '';
    String firstName = 'Gennaro';
    String lastName = 'Esposito';
   
    String response = CodiceFiscale_Helper.CheckFiscalCode(fiscalCode, firstName,lastName);
	system.assertEquals(Label.CF_Not_Valid,response);
	
	fiscalCode = 'SPSGNR80A01F839UAAA';
    firstName = '';
    lastName = 'Esposito';
   
    response = CodiceFiscale_Helper.CheckFiscalCode(fiscalCode, firstName,lastName);
	system.assertEquals(Label.CF_Not_Valid,response);
	
	fiscalCode = 'SPSGNR80A01F839UAAA';
    firstName = 'Gennaro';
    lastName = '';
   
    response = CodiceFiscale_Helper.CheckFiscalCode(fiscalCode, firstName,lastName);
	system.assertEquals(Label.CF_Not_Valid,response);
	
    }
    @IsTest
    static void  checkonlyFiscalCode (){
       // first test,  check ok
    String fiscalCode = 'SPSGNR80A01F839U';
    String response = CodiceFiscale_Helper.CheckFiscalCode(fiscalCode);
	system.assertEquals('OK',response);
	 // second test,  wrong lenght
	fiscalCode = 'SPSGNR80A01F839UAAAA';
	response = CodiceFiscale_Helper.CheckFiscalCode(fiscalCode);
	system.assertEquals(Label.CF_Lenght,response);
	// third test,  wrong check digit
	fiscalCode = 'SPSGNR80A01F839A';
	response = CodiceFiscale_Helper.CheckFiscalCode(fiscalCode);
	system.assertEquals(Label.CF_Not_Valid,response);
	// fourth test, fiscal code null 
	fiscalCode = null;
	response = CodiceFiscale_Helper.CheckFiscalCode(fiscalCode);
	system.assertEquals(Label.CF_Not_Valid,response);
    
    }
    
    
}