public without sharing class CtiData {
	
	// Case number
	public String CASE_NUM 	{ get; set	{
											if ( constants.UNDEFINED.equalsIgnorecase(value))
												CASE_NUM ='';
											else 
												CASE_NUM = value;
									 	}
							}
	// IVR code
	public String COD_IVR 	{ get; set	{
											if ( constants.UNDEFINED.equalsIgnorecase(value))
												COD_IVR ='';
											else 
												COD_IVR = value;
									 	}
							}
	// Fiscal code
	public String CF 	{ get; set	{
										if ( constants.UNDEFINED.equalsIgnorecase(value))
											CF ='';
										else 
											CF = value;
									} 
						}
	//Phone
	public String PHONE { get; set	{
										if ( constants.UNDEFINED.equalsIgnorecase(value))
											PHONE ='';
										else 
											PHONE = value;
									}  
						}
	// Genesys ConnID
	public String CONNID { get; set	{
										if ( constants.UNDEFINED.equalsIgnorecase(value))
											CONNID ='';
										else 
											CONNID = value;
									} 
						 }
	//Genesys DN
	public String DN { get; set	{
										if ( constants.UNDEFINED.equalsIgnorecase(value))
											DN ='';
										else 
											DN = value;
									} 
					 }
	//Genesys Tenant
	public String TENANT { get; set	{
										if ( constants.UNDEFINED.equalsIgnorecase(value))
											TENANT ='';
										else 
											TENANT = value;
									} 
						 }
	// POD
	public String POD { get; set	{
										if ( constants.UNDEFINED.equalsIgnorecase(value))
											POD ='';
										else 
											POD = value;
									} 
					 }
	// ID Pratica
	public String ID_PR { get; set	{
										if ( constants.UNDEFINED.equalsIgnorecase(value))
											ID_PR ='';
										else 
											ID_PR = value;
									} 
						 }
	
		public String RTargetPlaceSelected { get; set	{
										if ( constants.UNDEFINED.equalsIgnorecase(value))
											RTargetPlaceSelected ='';
										else 
											RTargetPlaceSelected = value;
									} 
						 }
}