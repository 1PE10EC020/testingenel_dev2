@isTest

private class CustomContactLookupBAController_Test {

    static testMethod void searcha_Test() {
        CustomContactLookupBAController lookup = new CustomContactLookupBAController();
        lookup.searchString = '5115';
        PageReference pr = lookup.search();

        System.debug('*** results = ' + lookup.results);
        System.assert(lookup.results.isEmpty());
    }  

    static testMethod void checkForStrays_Test() {        
        CustomContactLookupBAController lookup = new CustomContactLookupBAController();
        lookup.inServiceCloudConsole = 'false';
        lookup.checkForStrays();
    }
    
    static testMethod void setWorkspaceConnectorInfo_Test(){
        //CustomContactLookupBAController lookup = new CustomContactLookupBAController();
        String result = CustomContactLookupBAController.setWorkspaceConnectorInfo('123','456');
        System.assert(result == 'success');
    }

}