/*******************
 * Author : Giulia Tedesco
 * Apex Class  :  DownloadContainer_controller
 * CreatedDate :  08/08/2017
 * Description :  This class is DownloadContainer_controller component controller.
 				  It is used to manage Download Columns of the SmartListView component
****************/
public class DownloadContainer_controller{

	 /******************
	 * Apex Method :  getFileConfiguration()
	 * Description :  This method get the configuration of the SmartList_Download_Setup custom setting
	 				  to prepare the file.	
	****************/
	@AuraEnabled
    public static WrapperElements getFileConfiguration(SObject sObjectt, String fieldName, String sObjectApi, String recordId){

    	System.debug('***sObjectt: ' +sObjectt);
		System.debug('***fieldName: ' +fieldName);
		System.debug('***tst sObjectApi: ' +sObjectApi);
		WrapperElements wrapObj = new WrapperElements();

		try{
			//DOWNLOAD CONFIGURATION FILE
			SmartList_Download_Setup__c cs = SmartList_Download_Setup__c.getValues(String.valueOf(fieldName));
			wrapObj.fourID = cs.File_Name__c;
			System.debug('***Id Four: ' +wrapObj.fourID);
			
			wrapObj.extension = cs.Extension__c;
			System.debug('***extension: ' +wrapObj.extension);

			//GET CONTENT OF FILE
			String query = 'SELECT ' +  fieldName + ' ' + 'FROM ' +sObjectApi+ ' WHERE '
							+ 'Id' + ' = ' +'\''+recordId+'\''+' LIMIT 1';
			System.debug('***query: ' + query);

			SObject s = (Database.query(query));
			Object o = s.get(fieldName);

			wrapObj.content = String.valueOf(o);
			System.debug('**TEST content: ' +wrapObj.content);

		}
		catch(Exception e){
			System.debug('Exception: ' +e.getMessage() +' at line: ' + e.getLineNumber());
		}

		return wrapObj;

    }

	public class WrapperElements{
        @AuraEnabled
        public String fourID {get;set;}
        @AuraEnabled
        public String extension {get;set;}
        @AuraEnabled
        public String content{get;set;}
       

       
        public WrapperElements(){

            fourID = '';
            extension = '';
            content = '';

        }
        
    }

    public class PageRender{
        @AuraEnabled
        public Boolean isDMS {get;set;}
        @AuraEnabled
        public Boolean showMessage {get;set;}
        @AuraEnabled
        public Boolean disponibile{get;set;}
        @AuraEnabled
        public Boolean enableDownload {get;set;}
        @AuraEnabled 
        public Attachment attachment{get;set;}
        @AuraEnabled 
        public Boolean exists{get;set;}
        @AuraEnabled
        public String attachmentid{get;set;}
       
        public PageRender(){

            isDMS = false;
            showMessage = false;
           	disponibile = false;
           	enableDownload = false;
           	attachment = new Attachment();
           	exists = false;
        }
        
    }

    /******************
	 * Apex Method :  getStatus()
	 * Description :  This method checks if download type is an External Download(DMS). If it is DMS type, 
	 				  it checks File status and Outbound status everytime the component is refreshed.
	****************/
    @AuraEnabled
    public static PageRender getStatus(String sObjectName, String recId){

    	PageRender pr = new PageRender();
		SmartList_Setup__c cs = SmartList_Setup__c.getValues(sObjectName);
		pr.isDMS = cs.External_Download__c;
		System.debug('**DMS: ' +pr.isDMS);
    	
	    	if(pr.isDMS==true){
	    		PT_Allegato__c allegato = [SELECT Id,SendOutbound__c,FileRichiesto__c,FileDisponibile__c FROM PT_Allegato__c WHERE Id =: recId LIMIT 1];

	    		try{
		    		//**if file is available show the file to download
		    		pr.disponibile = allegato.FileDisponibile__c;
		    		
		    		//**if file is not available 
		    		if(allegato.FileDisponibile__c == false){
		    			
		    			pr.showMessage = allegato.FileRichiesto__c;
		    			if(allegato.FileRichiesto__c==false){
		    				//pr.showMessage = false;
			    			pr.enableDownload = true;
			    		}
		    		}
		    		if(allegato.FileDisponibile__c == true){
		    			//** To take Attachment with Id = Allegato.Id
		    			//Attachment att = [SELECT Id FROM Attachment WHERE ParentId =: recId LIMIT 1];
		    			pr.attachment = [SELECT Id FROM Attachment WHERE ParentId =: recId LIMIT 1];
		    			System.debug('**Attachment ID: ' + pr.attachment);
		    		}
		    		System.debug('***Allegato__c: ' + allegato);
		    		System.debug('***pr.showMessage Config: ' +pr.showMessage);

				} catch(Exception ex){
					pr.showMessage = false;
					pr.disponibile = false;
					pr.enableDownload = true;
	    			allegato.FileDisponibile__c = false;
	    			update allegato;
	    			System.debug('Exception: ' +ex.getMessage() +' at line: ' + ex.getLineNumber());

				}

    		}
	    		
    	

    	return pr;
    	 	
    }


    /******************
	 * Apex Method :  updateAllegato()
	 * Description :  This method is called when user click on Download Button.
	 				  It performes the update on the PT_Allegato SObject
	****************/
    @AuraEnabled
    public static Boolean updateAllegato(String recId){

    	PageRender pr2 = new PageRender();
    	try{

    		PT_Allegato__c all = [SELECT Id,Name FROM PT_Allegato__c WHERE Id=: recId LIMIT 1];
	    	all.SendOutbound__c = true;
	    	update all;

	    	System.debug('**Allegato = ' +all);
	    	pr2.showMessage = true;


    	}
    	catch(Exception e){
    		System.debug('Exception: ' +e.getMessage() +' at line: ' + e.getLineNumber());
    	}
    	
    	return pr2.showMessage;
    }


    /******************
	 * Apex Method :  refreshAttachment()
	 * Description :  This method is called when user click on Richiedi Download Button and performs this check
	 				  every 5 seconds, while Attachment does not exist. 
	****************/
    @AuraEnabled
    public static PageRender refreshAttachment(String recId){

    	System.debug('**id in refreshAttachment: ' +recId);
    	PageRender prx = new PageRender();
    	System.debug('**Attachment trovato: ' +prx.attachment);

    	
    	try{
    		Attachment atc = [SELECT Id FROM Attachment WHERE ParentId =: recId LIMIT 1];
	    	System.debug('**Attachment trovato: ' +atc);
	    	if(atc!=null){
	    		prx.exists = true;
	    		prx.attachment = atc;
	    		prx.attachmentid = atc.Id;
	    	}
    	}
	    catch(Exception e){
	    	//pr.attachment = null;
	    	prx.exists = false;
    		System.debug('Exception: ' +e.getMessage() +' at line: ' + e.getLineNumber());
    	}	
    
    		//System.debug('Exception: ' +e.getMessage() +' at line: ' + e.getLineNumber());
    	

    	return prx;

    }


    @AuraEnabled
    public static void updateFileRichiesto(String recId){

    	
    	try{

    		PT_Allegato__c all3 = [SELECT Id,Name FROM PT_Allegato__c WHERE Id=: recId LIMIT 1];
	    	all3.FileRichiesto__c = false;
	    	update all3;

	    	System.debug('**Allegato = ' +all3);
	    	//pr3.enableDownload = true;

    	}
    	catch(Exception e){
    		System.debug('Exception: ' +e.getMessage() +' at line: ' + e.getLineNumber());
    	}
    	
    	 
    }
   

}