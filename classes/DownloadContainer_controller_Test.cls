@isTest(SeeAllData = true)
public class DownloadContainer_controller_Test{

	static testMethod void TestgetFileConfiguration() {
		
		//CREATE REQUEST
        PT_Richiesta__c request = new PT_Richiesta__c();    
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.PT_Richiesta__c; 
        Map<String,Schema.RecordTypeInfo> richiestaRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
        Id n02_id = richiestaRecordTypeInfo.get('PT_N02').getRecordTypeId();
        request.RecordTypeId= n02_id;
        request.PotImp__c='1.5';
        database.insert(request);

        //PT_NOTIFICA TEST 
        PT_Notifica__c notifica = new PT_Notifica__c();
        notifica.IdNotFour__c = '12345';
        notifica.ProtocolloRic__c = request.Id;
        notifica.DettXml__c = 'Test Test';
        notifica.DettCsv__c = 'test Test';
        notifica.CodFlusso__c = 'TEST TEST';
        notifica.CodServizio__c = 'test test';
        notifica.DataOraNot__c = System.today();
        insert notifica;
        DownloadContainer_controller.WrapperElements wrapObj;
        DownloadContainer_controller.PageRender pRender;

        String fieldname='DettXml__c';
        String sobjectapi= 'PT_Notifica__c';
        String recordId = notifica.Id;

        //PT_ALLEGATO TEST
        String sobjectAllegato = 'PT_Allegato__c';
        PT_Allegato__c allegato = new PT_Allegato__c();
        allegato.IdAllegato__c = 932722;
        allegato.TipoFile__c = 'Fine Opere Cliente';
        allegato.Direzione__c = 'I';
        insert allegato;
        String allegatoId = allegato.Id;

        Boolean testMessage = false;

        Test.startTest();

        wrapObj=DownloadContainer_controller.getFileConfiguration(notifica,fieldname,sobjectapi,recordId);
        pRender=DownloadContainer_controller.getStatus(sobjectAllegato, allegatoId);

        allegato.FileDisponibile__c=true;
        update allegato;
        pRender=DownloadContainer_controller.getStatus(sobjectAllegato, allegatoId);
        
        Attachment a = new Attachment();
        a.ParentId = allegatoId;
        a.Name = 'Test Attachment';
        a.Body = Blob.valueof('Test Body');
        insert a;
        pRender=DownloadContainer_controller.getStatus(sobjectAllegato, allegatoId);

        testMessage=DownloadContainer_controller.updateAllegato(allegatoId);

		Test.stopTest();

	}

}