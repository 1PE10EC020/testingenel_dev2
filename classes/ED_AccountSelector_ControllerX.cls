public with sharing class  ED_AccountSelector_ControllerX {

	public list<Account> AccToDisplay {get;set;}
	public string selectedAccountId {get;set;}
	
	
	private PercorsoTelefonico__c ivrPath;
	private string connId;
	private string dn;
	private string tenant;
	private string idPratica;
	private string pod;
	private string phoneNumber;
	 /* 
    * Autor : Claudio Quaranta <claudio.quaranta@accenture.com>
    * Createddate : 14-07-2015
    * Description : standard controller with no parameter, retrive all parameters from URL
    */
	public ED_AccountSelector_ControllerX (){
		//Retriving inbound phone number for the URL
		phoneNumber = ApexPages.currentPage().getParameters().get(Constants.PHONE_PARAMETER);
		if(String.isNotBlank(phoneNumber)){
			AccToDisplay = Account_Helper.retriveAccountByPhoneNumber(phoneNumber);
		}
		//Retriving inbound IVR Path number for the URL
		string pathIVRSelected = ApexPages.currentPage().getParameters().get(Constants.IVR_PATH_PARAMETER);
		if ( String.isNotBlank(pathIVRSelected))
			ivrPath =  IVR_Helper.findIVRPath(pathIVRSelected);
		connId = ApexPages.currentPage().getParameters().get(Constants.CONNID_PARAMETER);
		if(Constants.UNDEFINED.equals(connId))
			connId='';
		if(Constants.UNDEFINED.equals(dn))
			dn='';
		dn = ApexPages.currentPage().getParameters().get(Constants.DN_PARAMETER);
		if(Constants.UNDEFINED.equals(tenant))
			tenant='';
		tenant = ApexPages.currentPage().getParameters().get(Constants.TENANT_PARAMETER);
		if(Constants.UNDEFINED.equals(idPratica))
			idPratica='';
		idPratica = ApexPages.currentPage().getParameters().get(Constants.IDPRATICA_PARAMETER);
		if(Constants.UNDEFINED.equals(pod))
			pod='';
		pod = ApexPages.currentPage().getParameters().get(Constants.POD_PARAMETER);
		selectedAccountId='';
	}

	public void DummyAction(){
	
		system.debug(logginglevel.debug,'ED_AccountSelector_ControllerX - selectedAccountId '+selectedAccountId);
	}
	
	  /* 
    * Autor : Claudio Quaranta <claudio.quaranta@accenture.com>
    * Createddate : 14-07-2015
    * Description : create a new case with the selected account
    * Input :	Name 					Type					Description
    *			N/A
    *output : 
    *			N/A						Pagereference			Pagereference to redirect the user
    */
    
	public Pagereference CreateCaseWithAccount(){
		if(String.isNotBlank(selectedAccountId)){
			try{
				// Cycle all account in order to find the account selected
				for(Account acc : AccToDisplay){
					if(selectedAccountId.equals(acc.id)){
						Case newCase = Case_Helper.createNewCase(acc,ivrPath,connid,dn,tenant,pod,idPratica,Constants.ORIGIN_TELEFONO,phoneNumber);
						
						if (newCase != null){
							insert newCase;
							pagereference p = new Pagereference('/'+newCase.id);
							return p;						
						}
					}
				}
			
			}
			catch(Exception e){
				ApexPages.addmessage(new Apexpages.Message(ApexPAges.Severity.Error,'Errore : '+e.getMessage()));
				return null; 
			}
		}
		else
			ApexPages.addmessage(new Apexpages.Message(ApexPAges.Severity.Error,System.Label.ACCOUNT_NOT_SELECTED));
			
		return null;
	
	}
	
	
	 /* 
    * Autor : Claudio Quaranta <claudio.quaranta@accenture.com>
    * Createddate : 14-07-2015
    * Description : create a new case without select an account
    * Input :	Name 					Type					Description
    *			N/A
    *output : 
    *			N/A						Pagereference			Pagereference to redirect the user
    */
    
	public pageReference CreateCaseWithoutAccount(){
		try{
			Case newCase = Case_Helper.createNewCase(null,ivrPath,connid,dn,tenant,pod,idPratica,Constants.ORIGIN_TELEFONO,'');
			if (newCase != null){
				insert newCase;
				pagereference p = new Pagereference('/'+newCase.id);
				return p;
			}			
		}
		catch(Exception e){
			ApexPages.addmessage(new Apexpages.Message(ApexPAges.Severity.Error,'Errore : '+e.getMessage()));
		}
		return null;
	}
}