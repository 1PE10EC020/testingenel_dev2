@IsTest
public class ED_AccountSelector_ControllerX_test{

	@testSetup
	private static void setup() {
		Account acc = new Account();
		acc.CodiceFiscale__c =  'SPSGNR80A01F839U';
		acc.name = 'Esposito';
		Acc.nome__c ='Gennaro';
		acc.recordtypeid = [select id from recordtype where developername = :Constants.ACCOUNT_PERSONA_FISICA  limit 1].id;
		acc.phone = '3333111';
		insert acc;
		
		Account acc2 = new Account();
		acc2.CodiceFiscale__c =  'DOEJNA80A41H501Z';
		acc2.name = 'Doe';
		Acc2.nome__c ='Jane';
		acc2.recordtypeid = [select id from recordtype where developername = :Constants.ACCOUNT_PERSONA_FISICA  limit 1].id;
		acc2.phone = '3333111';
		insert acc2;
		
		PercorsoTelefonico__c percorso = new PercorsoTelefonico__C();

	 	percorso.CodIVR__c = '111';
	 	percorso.Contesto__c = 'Test';
	 	insert percorso;
	}


	@isTest
	static void test(){
		
		test.startTest();
		Test.setCurrentPageReference(new PageReference('Page.ED_AccountSelector')); 
		System.currentPageReference().getParameters().put(Constants.PHONE_PARAMETER, '3333111');
		System.currentPageReference().getParameters().put(Constants.IVR_PATH_PARAMETER, '111');
		System.currentPageReference().getParameters().put(Constants.CONNID_PARAMETER, constants.UNDEFINED);
		System.currentPageReference().getParameters().put(Constants.DN_PARAMETER, constants.UNDEFINED);
		System.currentPageReference().getParameters().put(Constants.TENANT_PARAMETER, constants.UNDEFINED);
		System.currentPageReference().getParameters().put(Constants.IDPRATICA_PARAMETER, constants.UNDEFINED);
		System.currentPageReference().getParameters().put(Constants.POD_PARAMETER, constants.UNDEFINED);

		ED_AccountSelector_ControllerX myController = new ED_AccountSelector_ControllerX ();
		myController.CreateCaseWithAccount();
		myController.selectedAccountId = [select id from account limit 1].id;
		boolean errorfound = false;
		try{
			myController.CreateCaseWithAccount();
		}
		catch(Exception e){
			errorfound = true;
		}
		system.assertEquals(false,errorfound);
		
		myController.CreateCaseWithoutAccount();
		test.stopTest();
	
	}


}