public class ED_CaseNotFoundController {

	public string phone {get;set;}
	public string navigation {get;set;}
	public String context {get;set;}
	public String branch {get;set;}
	public String menu {get;set;}
	public String caseNum {get;set;}
	
	public ED_CaseNotFoundController () 
	{
		//Retriving inbound phone number for the URL
    	phone = ApexPages.currentPage().getParameters().get(Constants.PHONE_PARAMETER);
    	if (Constants.UNDEFINED.equals(phone))
    		phone='';
		string pathIVRSelected = ApexPages.currentPage().getParameters().get(Constants.IVR_PATH_PARAMETER);
		casenum  = ApexPages.currentPage().getParameters().get(Constants.CASENUM_PARAMETER);
		system.debug('casenum '+casenum);
    	if ( String.isNotBlank(pathIVRSelected)) {
     	 	PercorsoTelefonico__C ivrPath =  IVR_Helper.findIVRPath(pathIVRSelected);
     	 	if (ivrPath != null){
     	 		navigation = ivrPath.Name;
     	 		context = ivrPath.Contesto__c;
     	 		branch = ivrPath.Ramo__c;
     	 		menu = ivrPath.MenuSecondoLivello__c;
     	 	}
    	}
	}
	public pageReference redirect (){
		return new PageReference('/500');
	}

}