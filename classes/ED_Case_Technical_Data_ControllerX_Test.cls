@isTest
public class ED_Case_Technical_Data_ControllerX_Test {
    
  @testSetup
    private static void setup() {
        
        case c = new case();
        c.recordTypeId = [select id from recordtype where developername = :Constants.CASE_DA_DEFINIRE ].id ;
        c.TipologiaCase__c ='Da Definire';
        c.origin = 'Email';
        c.status='In carico al II Livello';
        c.DescrizioneStato__c='In Lavorazione';
        c.AssegnatoIILivello__c = Datetime.newInstance(2015, 3, 1, 9, 0, 0);
        c.pod__C = 'IT001E020070030';
        c.DirezioneDTR__c = 'Lazio Abruzzo e Molise' ;
        c.UnitaDiCompetenza__c ='ROMA ESTERNA-RIETI';
        c.LivelloTensioneDiFornitura__c ='BT';
        c.livello1__C ='In prelievo';
        c.livello2__C = 'Già Connesso';
        c.livello3__C ='MT/AT' ;
        c.Famiglia__c = ' ';
        insert c;
    }
    
    
    static testMethod void testSave_1() {
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='Operatore CM I Livello' limit 1]; 
        UserRole ur = [select developername, name, id from userRole where developername = 'Front_Office' limit 1];
      
        User u = new User(UserRoleId = ur.Id,Alias = 'pliv', Email='primoliv@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='primoliv@testorg.com');
        System.runAs(u) {
            case c = [select id,TipologiaCase__c, Famiglia__c, CodiceFiscaleTitolarePOD__c,pod__C, status,NomeTitolarePOD__c,CognomeTitolarePOD__c,PartitaIVATitolarePOD__c,UnitaDiCompetenza__c,DirezioneDTR__c,Oraria__c,LivelloTensioneDiFornitura__c from case limit 1 ];
            ApexPages.StandardController sc = new ApexPages.StandardController(c);
            ED_Case_Technical_Data_ControllerX controller = new ED_Case_Technical_Data_ControllerX(sc);
            Test.startTest();
            
            
            controller.saveC();
        
        
        }
    }
}