public with sharing class ED_EMMDashbord_Extension {
    
    public boolean toDisplayEmm {get;set;}
    public boolean toDisplayEMS {get;set;}
    public  string  emm_endURL {get;set;}
    
    /*
    public  string  emm_endURL {
        get{
            string returnURL='';
                
            if(currentCase != null){
                
            
                //returnURL=IntegrationUtils.getFullEndpoint('FOUR_DASHBOARD');
                
                
                if ( Constants.CASE_FAMIGLIA_MOBILITA.equals(currentCase.Famiglia__c))
                {
                    toDisplayEmm = true;
                    toDisplayEMS = false;
                    returnURL = 'http://www.ocweb2.com/OCWeb2/#/login';
                }   
                else
                {
                    if (Constants.CASE_FAMIGLIA_ENEL_INFO_PIU.equals(currentCase.Famiglia__c))
                        toDisplayEmm = false;
                        toDisplayEMS = true;
                        returnURL = 'https://www.google.it';
                }               
                
                SYSTEM.DEBUG('*** EMM currentCase = '+currentCase );
                SYSTEM.DEBUG('*** EMM returnURL = '+returnURL );
                system.debug(logginglevel.debug,'*** emm_endURL - toDisplayEmm : '+toDisplayEmm);
            }
        return returnURL;
        }
    private set;
    }
    */
    /*
    public boolean toDisplayEmm {
        get{
          if ( Constants.CASE_FAMIGLIA_MOBILITA.equals(currentCase.Famiglia__c))
            return true;
          else 
            return false;
        }
    
        
        set;}
    */  
    
    private case currentCase;
    public ED_EMMDashbord_Extension(ApexPages.StandardController controller){
        
        toDisplayEmm = false;
        toDisplayEMS = false;
        //ApexPages.StandardController myController = controller;
        //if(!test.isRunningTest())
        //    controller.addFields(new List<String> {'Famiglia__c','TipologiaCase__c','DirezioneDTR__c','POD__c','IdPratica__c','DirezioneDTR__c','CognomeTitolarePOD__c','UnitaDiCompetenza__c','NomeTitolarePOD__c','LivelloTensioneDiFornitura__c','CodiceFiscaleTitolarePOD__c','Oraria__c','PartitaIVATitolarePOD__c','caseNumber','Account.RecordTypeId','RecordType.developerName','pod__c','Account.codicefiscale__c','Account.PartitaIVA__c','IdPratica__c','CF_Search_DS__c','PIVA_Search_DS__c','status'});

        currentCase = (Case)controller.getRecord();
        /*
        if ( Constants.CASE_FAMIGLIA_MOBILITA.equals(currentCase.Famiglia__c))
        {
            toDisplayEmm = true;
        }   
        else
        {
            toDisplayEmm = false;
        }               
        */
        //emm_endURL = emm_endURL;
        
        if ( Constants.CASE_FAMIGLIA_MOBILITA.equals(currentCase.Famiglia__c))
        {
            toDisplayEmm = true;
            toDisplayEMS = false;
            emm_endURL = 'https://emm2.eneldrive.it/OCWeb2/#/login';

        }   
        else
        {
            if (Constants.CASE_FAMIGLIA_ENEL_INFO_PIU.equals(currentCase.Famiglia__c))
            {
                toDisplayEmm = false;
                toDisplayEMS = true;
                 //emm_endURL = 'https://enelifm--full.cs87.my.salesforce.com/apex/Under_Costruction_Page';
                emm_endURL = '';
            }
            else{
                toDisplayEmm = false;
                toDisplayEMS = false;
            }
        }               
        
        system.debug(logginglevel.debug,'*** ED_FourDashbord_Extension - currentCase.Famiglia__c : '+currentCase.Famiglia__c);
        system.debug(logginglevel.debug,'*** ED_FourDashbord_Extension - toDisplayEmm : '+toDisplayEmm);
        system.debug(logginglevel.debug,'*** ED_FourDashbord_Extension - toDisplayEMS : '+toDisplayEMS);
        system.debug(logginglevel.debug,'*** ED_FourDashbord_Extension - emm_endURL : '+emm_endURL);
    }
}