public class ED_FourDashbord_Extension{

	public string endURL {
		get{
			string returnURL='';
				
			if(currentCase != null){
				toDisplay = true;
			/*	if(String.isBlank(currentCase.pod__c) && String.isBlank(currentCase.Account.codicefiscale__C) && String.isBlank(currentCase.Account.PartitaIVA__c) 
				&& String.isBlank(currentCase.CF_Search_DS__c) && String.isBlank(currentCase.PIVA_Search_DS__c) && String.isBlank(currentCase.IdPratica__c) )
					toDisplay = false;
				*/
				String userFourID = [select MatricolaEnel__c from user where id = : UserInfo.getUserId() limit 1].MatricolaEnel__c;
				
				returnURL=IntegrationUtils.getFullEndpoint('FOUR_DASHBOARD');
				string pod='';
				string accountcf='';
				string accountpiva ='';
				string idPratica ='';
				
				if ( String.isNotBlank(currentCase.pod__C))
					pod =currentCase.pod__C;
				else{
					//if( currentCase.Account != null){
					system.debug('**** currentCase.CF_Search_DS__c: '+currentCase.CF_Search_DS__c);
					if (String.isNotBlank(currentCase.CF_Search_DS__c))
						accountcf=currentCase.CF_Search_DS__c;
					else{
						if (String.isBlank(currentCase.PIVA_Search_DS__c) ){
							// Giorgio: Il codice fiscale dell'account di persona fisica va passato solo se le Informazioni Tecniche di Ricerca (CF o PIVA) sono vuoti
							system.debug(logginglevel.debug,' ***** currentCase.IdPratica__c:'+ currentCase.IdPratica__c);	
							if (String.isBlank(currentCase.IdPratica__c))
							{
								if( currentCase.Account != null)
									if(String.isNotBlank(currentCase.Account.codicefiscale__C))
										accountcf=currentCase.Account.codicefiscale__C;
							}
							else
								idPratica = currentCase.IdPratica__c;
						}
					}			


					system.debug(logginglevel.debug,'  Case.Account.codicefiscale__C :'+currentCase.Account.codicefiscale__C+' *** currentCase.Account.PartitaIVA__c'+currentCase.Account.PartitaIVA__c);	
					system.debug(logginglevel.debug,'  currentCase.Account.recordTypeID :'+ currentCase.Account.recordTypeID);	
					
					system.debug(logginglevel.debug,' *****  currentCase:'+ currentCase);	
					
					if (String.isNotBlank(currentCase.PIVA_Search_DS__c) )
						accountpiva =CurrentCase.PIVA_Search_DS__c;
					else
					if( currentCase.Account != null){
						String rtDevelopername = [select developername from recordtype where id = : currentCase.Account.recordTypeID].developername;
						SYSTEM.DEBUG('rtDevelopername = '+rtDevelopername );
						if(Constants.ACCOUNT_PERSONA_GIURIDICA.equals(rtDevelopername))
							if(String.isNotBlank(CurrentCase.Account.PartitaIVA__c))
								accountpiva =CurrentCase.Account.PartitaIVA__c;
					}
					//}
				
				}
				/*
				if(String.isnotBlank(currentCase.IdPratica__c))
					idPratica =currentCase.IdPratica__c;
				*/
				if(showStorico)
					returnURL = returnURL+ '?utente='+userFourID+'&caseCm='+currentCase.caseNumber+'&visualizzazione=S';
				else
					returnURL = returnURL+ '?utente='+userFourID+'&caseCm='+currentCase.caseNumber+'&pod='+pod+'&codicefiscale='+accountcf+'&partitaIva='+accountpiva+'&idRichiesta='+idPratica+'&visualizzazione=N';
				
			}
			//returnURL = 'http://10.16.171.176:8080/foue/pages/home.jsf?utente=CMCM001&caseCm=0000012345&pod=IT009E00010018&codicefiscale=&partitaIva=&idRichiesta=&visualizzazione=N';
			SYSTEM.DEBUG('currentCase = '+currentCase );
			SYSTEM.DEBUG('returnURL = '+returnURL );
		return returnURL;
		}
	private set;
	}
	
	/*
	public string endURLStorico {
		get{
			string returnURL='';

			if(currentCase != null){
				toDisplay = true;
				if(String.isBlank(currentCase.pod__c) && String.isBlank(currentCase.Account.codicefiscale__C)&& String.isBlank(currentCase.Account.PartitaIVA__c) && String.isBlank(currentCase.CF_Search_DS__c) && String.isBlank(currentCase.PIVA_Search_DS__c) && String.isBlank(currentCase.IdPratica__c)  )
					toDisplay = false;
				String userFourID = [select MatricolaEnel__c from user where id = : UserInfo.getUserId() limit 1].MatricolaEnel__c;
				
				returnURL=IntegrationUtils.getFullEndpoint('FOUR_DASHBOARD');
				string pod='';
				string accountcf='';
				string accountpiva ='';
				string idPratica ='';
					
				if ( String.isNotBlank(currentCase.pod__C))
					pod =currentCase.pod__C;
				else{
						//if( currentCase.Account != null){
					if (String.isNotBlank(currentCase.CF_Search_DS__c))
						accountcf=currentCase.CF_Search_DS__c;
					else
						if( currentCase.Account != null)
							if(String.isNotBlank(currentCase.Account.codicefiscale__C))
								accountcf=currentCase.Account.codicefiscale__C;
					system.debug(logginglevel.debug,'  currentCase.Account.recordTypeID :'+ currentCase.Account.recordTypeID);	
					
					if (String.isNotBlank(currentCase.PIVA_Search_DS__c) )
						accountpiva =CurrentCase.PIVA_Search_DS__c;
					else
					if( currentCase.Account != null){
					String rtDevelopername = [select developername from recordtype where id = : currentCase.Account.recordTypeID].developername;
					SYSTEM.DEBUG('rtDevelopername = '+rtDevelopername );
					if(Constants.ACCOUNT_PERSONA_GIURIDICA.equals(rtDevelopername))
						if(String.isNotBlank(CurrentCase.Account.PartitaIVA__c))
							accountpiva =CurrentCase.Account.PartitaIVA__c;
					}
					//}
				}
				if(String.isnotBlank(currentCase.IdPratica__c))
					idPratica =currentCase.IdPratica__c;
				
				//returnURL=IntegrationUtils.getFullEndpoint('FOUR_DASHBOARD');
				//returnURL = returnURL+ '?utente='+userFourID+'&caseCm='+currentCase.caseNumber+'&pod='+currentCase.pod__C+'&codicefiscale='+currentCase.Account.codicefiscale__C+'&partitaIva='+CurrentCase.Account.PartitaIVA__c+'&idRichiesta='+currentCase.IdPratica__c+'&visualizzazione=S';
				
				returnURL = returnURL+ '?utente='+userFourID+'&caseCm='+currentCase.caseNumber+'&pod='+pod+'&codicefiscale='+accountcf+'&partitaIva='+accountpiva+'&idRichiesta='+idPratica+'&visualizzazione=S';
			}
			//returnURL = 'http://10.16.165.80:8080/foue/pages/home.jsf?utente=AHEP145&caseCm=0000012345&pod=IT009E00010018&codicefiscale=&partitaIva=&idRichiesta=&visualizzazione=S';
			//returnURL ='http://10.16.171.176:8080/foue/pages/home.jsf?utente=CMCM001&caseCm=0000012345&pod=&codicefiscale=&partitaIva=&idRichiesta=&visualizzazione=S';
			SYSTEM.DEBUG('currentCase = '+currentCase );
			SYSTEM.DEBUG('returnURL = '+returnURL );
		return returnURL;
		}
		private set;
	}
	*/
	public boolean toDisplay {
		get{
		  if( String.isBlank(currentCase.pod__c) && String.isBlank(currentCase.Account.codicefiscale__C)&& String.isBlank(currentCase.Account.PartitaIVA__c) && String.isBlank(currentCase.CF_Search_DS__c) && String.isBlank(currentCase.PIVA_Search_DS__c) && String.isBlank(currentCase.idpratica__c) )
		  	return false;
		  else 
		  	return true;
		}
		
		
		set;}
	public boolean showStorico {get;set;}
	private case currentCase;
	public ED_FourDashbord_Extension(ApexPages.StandardController controller){
		showStorico = false;
		toDisplay = true;
		ApexPages.StandardController myController = controller;
		//if(!Test.isRunningTest())
			//myController.addFields(new List<String> {'caseNumber','Account.RecordTypeId','RecordType.developerName','pod__c','Account.codicefiscale__c','Account.PartitaIVA__c','IdPratica__c','CF_Search_DS__c','PIVA_Search_DS__c'});
		currentCase = (Case)controller.getRecord();
	//	if(String.isBlank(currentCase.pod__c) && String.isBlank(currentCase.Account.codicefiscale__C)&& String.isBlank(currentCase.Account.PartitaIVA__c) && String.isBlank(currentCase.CF_Search_DS__c) && String.isBlank(currentCase.PIVA_Search_DS__c) && String.isBlank(currentCase.idpratica__c) )
	//		toDisplay = false;
		system.debug(logginglevel.debug,'ED_FourDashbord_Extension - currentCase.pod__c : '+currentCase.pod__c);
		system.debug(logginglevel.debug,'ED_FourDashbord_Extension - toDisplay : '+toDisplay);
	}
	
	public void revertSelection(){
		showStorico =!showStorico;		
	}
}