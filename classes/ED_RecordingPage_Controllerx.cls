public class ED_RecordingPage_Controllerx{
      Case mycase;
      public Boolean DisableAlternator {get;set;} // enables or disables the button START(false), STOP (true)
    

      public ED_RecordingPage_Controllerx  (ApexPages.StandardController controller){
         if(!Test.isRunningTest())
            controller.addFields(new List<String> {'IsRecordingExist__c','CaseNumber','IVR_ConnID__c','IVR_DN__c','IVR_Tenant__c','TelefonoInputIVR__c','CodiceFiscaleRichiedente__c','pod__c','IdPratica__c','CodicePercorsoTelefonico__c','RecordingID__c'});
         mycase =(case) controller.getRecord();
         DisableAlternator = false;
      }
    /* 
    * Autor : Claudio Quaranta <claudio.quaranta@accenture.com>
    * Createddate : 07-10-2015
    * Description : this method is used to stop the registration
    * Input :   Name                    Type                    Description
    *           N/A
    *output : 
    *           N/A                     ErrorMesage         displayed if there are error invoking the WS to stop the registration
    */
      public void endRec (){
        Registrazione_Helper.RecordingResult  wsresult = Registrazione_Helper.endRecording(myCase.IVR_DN__c);
        if (wsresult.result){
            ApexPages.addmessage(new Apexpages.Message(ApexPAges.Severity.Info,System.label.RECORDING_END));
            this.DisableAlternator = false; // enable the registration button
        }
        else{
           ApexPages.addmessage(new Apexpages.Message(ApexPAges.Severity.Error,wsresult.errorMessage));
           this.DisableAlternator = true; // disable the registration button
           }
      }
      
     /* 
    * Autor : Claudio Quaranta <claudio.quaranta@accenture.com>
    * Createddate : 07-10-2015
    * Description : this method is used to stop the registration
    * Input :   Name                    Type                    Description
    *           registrationid          string                  registration identifier, saved into case object
    *output : 
    *           N/A                     ErrorMesage         displayed if there are error invoking the WS to start the registration
    */
      public void startRec (){
        // only the FO user can use the Vocal Recording Feature
        string userRoleId = UserInfo.getUserRoleId();
        string roleName;
        try{
            roleName = [Select Developername, Name from userrole where id= :userRoleId limit 1].Name;
            system.debug('*** roleName: ' +roleName); 
        }
        catch(Exception e)
        {
            system.debug(logginglevel.error,'Exception Raised in : StartRecording' +e.getMessage()); 
        }
        if(String.isNotBlank(roleName ) && (Constants.ROLE_TECH_FO.equals(roleName) || Constants.ROLE_TECH_BO.equals(roleName) || Constants.ROLE_TL_FO.equals(roleName) || Constants.ROLE_TL_BO.equals(roleName)))
        {
            // calling the recording WS
            system.debug('*** startRec mycase: ' +mycase);
            Registrazione_Helper.RecordingResult  wsresult = Registrazione_Helper.StartRecording (mycase.IVR_ConnID__c,mycase.IVR_DN__c,mycase.IVR_Tenant__c,mycase.TelefonoInputIVR__c ,mycase.CodiceFiscaleRichiedente__c ,mycase.pod__c,mycase.casenumber,mycase.IdPratica__c,mycase.CodicePercorsoTelefonico__c) ;
            if (wsresult.result){
                ApexPages.addmessage(new Apexpages.Message(ApexPAges.Severity.Info,System.label.RECORDING_START));
                try{
                    // saving the recording id recived from WS into case
                    mycase.RecordingID__c = wsresult.recordingId;
                    update mycase;
                    this.DisableAlternator = true; // enable the stop registration button
                }
                catch(Exception e){
                    system.debug(logginglevel.error, 'exception in ED_RecordingPage_Controllerx - startRec :  '+e.getMessage());
                    this.DisableAlternator = false; // disbale the stop registration button
                }
            }
            else{
                    ApexPages.addmessage(new Apexpages.Message(ApexPAges.Severity.Error,wsresult.errorMessage));
                    this.DisableAlternator = false; // disbale the stop registration button
            }   
        }
        
        else{
            ApexPages.addmessage(new Apexpages.Message(ApexPAges.Severity.Warning,Label.REC_NOT_ALLOWED));
        }
      }
      
      
}