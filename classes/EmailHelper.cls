public without sharing class EmailHelper{
 /* 
* Autor : Claudio Quaranta <claudio.quaranta@accenture.com>
* Createddate : 10-07-2015
* Description : Method invocated by a flow, used to send a email to the case owner's role
* Input :	Name 					Type				Description
*			listOfCaseId			ID					Id of input's case number
* Output :  
*			N/A						Email				an email will be sent to case owner's role
* 
*/
	@InvocableMethod
	public static void sendEmailToRole (list<String> listOfCaseId){
		system.debug(logginglevel.info,'EmailHelper - sendEmailToRole');
		system.debug(logginglevel.info,'EmailHelper - sendEmailToRole');
		// check if the value in input is not blank
		if ( listOfCaseId != null && !listOfCaseId.isEmpty()){
			// find the queue name
			list<Case> CaseWithQueueName = [select id,tech_QueueName__c,CaseNumber,Priority ,Status,TipologiaCase__c,Description from case where id in :listOfCaseId];
			system.debug(logginglevel.debug,'EmailHelper - sendEmailToRole - CaseWithQueueName :'+caseWithQueueName);
			// search for the role id with the same name
			if( caseWithQueueName != null && !caseWithQueueName.isEmpty() ){
				list<String> queueName = new list<String>();
				for (Case c : CaseWithQueueName){
					queueName.add(c.tech_QueueName__c);
				}
				// if a queue name is found
				if ( queueName != null && !queueName.isEmpty()){
					system.debug(logginglevel.debug,'EmailHelper - sendEmailToRole - queueName :'+queueName);
					list<UserRole> listOfUserRole = [select id from UserRole where name in :queueName];
					system.debug(logginglevel.debug,'EmailHelper - sendEmailToRole - listOfUserRole :'+listOfUserRole);
					// Search for all user with that role
					if (listOfUserRole != null && !listOfUserRole.isEmpty()){
						list<ID> listOFRoleid = new list<Id> ();
						for (UserRole urole : listOfUserRole)
							listOFRoleid.add(urole.id);
						list<User> userToMail = [Select id, email from user where UserRoleId in : listOFRoleid ];
						if ( userToMail != null && !userToMail.isEmpty()){
							// For all user found send an email
							for (User u : userToMail)
								sendEmail(CaseWithQueueName, u);
						}
					}		
				}
			}
		}
	}
 /* 
* Autor : Claudio Quaranta <claudio.quaranta@accenture.com>
* Createddate : 10-07-2015
* Description : Method used to send an email to a user
* Input :	Name 					Type				Description
*			listOFCase				Case				contains the information used to compose the email
*			userToMail				USer				the user should receve the email.
* Output :  
*			N/A						Email				an email will be sent to the userToMail
*/	
	public Static void sendEmail(list<Case> listOFCase , User userToMail){
		 List<Messaging.SingleEmailMessage> mails =   new List<Messaging.SingleEmailMessage>();
		 list<string> emailRecipent = new list<String> ();
		 emailRecipent.add (userToMail.email);
		 // search for the temlate used to compose the email
		 //Id templateID = [Select id from emailtemplate where developerName = : Constants.emailTemplate limit 1].id;
		for (case c : listOFCase){
			 Messaging.SingleEmailMessage mail =  new Messaging.SingleEmailMessage();
			 mail.setToAddresses(emailRecipent);
			// mail.setTemplateId (templateID);
			// mail.setTargetObjectId(userToMail.id);
			 mail.setSubject('Notifica Assegnazione Case Number: '+c.CaseNumber);
			 
			 string emailtext = 'La seguente email per segnalare  la presenza di un nuovo Case nella coda del II Livello.\nCase Number: '+ c.CaseNumber;
			 emailtext +='\n Dettagli Case: \n';
			 emailtext +='\nPriorità: '+c.Priority;
			 emailtext +='\nStato: '+c.Status;
			 emailtext +='\nTipologia del Case: '+c.TipologiaCase__c;
			 emailtext +='\nLink Salesforce al Case: '+URL.getSalesforceBaseUrl().toExternalForm()+'/'+c.id;
			 emailtext +='\nDescrizione Case: '+c.Description;
			 mail.setPlainTextBody( emailtext);			
			 mail.saveAsActivity  = false;
			 mail.setWhatId (c.id);
			 mails.add(mail);
		}
		Messaging.sendEmail(mails);
	}
 /* 
* Autor : Claudio Quaranta <claudio.quaranta@accenture.com>
* Createddate : 07-09-2015
* Description : Method invoked to create a task when an email arrives
* Input :	Name 					Type				Description
*			inEmail					EmailMessage		input email message
* Output :  
*			N/A						Task				a task is created for each email in input.
*/		
	
	public static void createTaskOnInbounEmail (list <EmailMessage> inEmail) {
		// search if the email in input is populated
	  	if ( inEmail != null && !inEmail.isEmpty()){
			  List <Task> taskEmailRicevuta = new List<Task>();
			  list<String> listOfParentID = new list<String>();
			  // retrive all parentID in input( case) 
			  for (EmailMessage message : inEmail) {
			  	listOfParentID.add(message.ParentId);
			  }
			  	try{
				// create a map for all parent cases
			  	map<id,Case> mapOfCase = new Map<id,Case> ([select id, ownerid, casenumber, status from Case where id in :listOfParentID] );
			  	if (mapOfCase != null){
			  		// retrive all USER that own a case
			  		list<String> listOfOwnerID = new list<String>();
			  		for ( Case c : mapOfCase.values()){
			  			listOfOwnerID.add(c.ownerid);
			  		}
			  		
			  		map<Id,User> mapOfOwner = new map<id,user>( [select id from user where id in :listOfOwnerID and isactive= true ]);
			  		// search for all queue
			  		map<id,Group> mapOfQueue  = new map <id,Group> ( [select name from Group where type='Queue' and  id in  :listOfOwnerID ] ) ;
			  		// map <QueueName, list of user > 
			  		map<String,list<User>> mapRoleUser = new Map <String,list<User>> ();
			  		if (mapOfQueue != null && !mapOfQueue.isEmpty() ) {
						// searc for all role name that own a case
			  			list<String> roleName  = new list<String> ();
			  			for (group g : mapOfQueue.values()){
			  				roleName.add(g.name);
			  			} 
			  			// search for all user that have that role
			  			list<User> userList = [select id, userRole.name from user where userRole.name in :roleName and isactive = true ];
			  			// create the map
			  			if(userList != null  & !userList.isEmpty() ) {
			  				for ( User u : userList){
								if (mapRoleUser.containsKey(u.userRole.name) )	{
									mapRoleUser.get(u.userRole.name).add(u);						
								}
								else{
									list<User> newListUSer = new List<User>();
									newListUSer.add(u);
									mapRoleUser.put(u.userRole.name,newListUSer);
								}		  				
			  				}
			  			}
			  		}
			  		
			  		
			  		// for each email, that have a parent case,  create a new task
				  	for (EmailMessage message : inEmail) {
				  		// create a task only for Inbound email
				  		system.debug(logginglevel.debug,'createTaskOnInbounEmail message is : '+message);
				  		if( message.incoming ==  true && !Constants.EMAIL_STATUS_INVIATO.equals( message.status)){
						  	if (String.isNotBlank(message.ParentId ) &&  mapOfCase.containsKey(message.ParentId ) ){
						  		// if the owner is a USer, only 1 task is created, otherwise a task is created for each queue's user
						  		if (mapOfOwner.containskey( mapOfCase.get( message.ParentId).ownerid) ){
						  			 // if case's status != "new"
								     if (!Constants.CASE_STATUS_NUOVO.Equals( mapOfCase.get( message.ParentId).status) )
								     	taskEmailRicevuta.add(TaskHelper.createTask(message,mapOfOwner.get( mapOfCase.get( message.ParentId).ownerid)));
						  		}
						  		else{
						  			// the current owher is a group, a task should be created for each group's members
						  			if (mapOfQueue.containskey (mapOfCase.get( message.ParentId).ownerid) ){
						  				if ( mapRoleUser.containsKey(mapOfQueue.get(mapOfCase.get( message.ParentId).ownerid ).name ) ){
						  					// cicle all user that have a certain role 
						  					// if case's status != "new"
								   			if (!Constants.CASE_STATUS_NUOVO.Equals( mapOfCase.get( message.ParentId).status) )
						  						for ( User toSend : mapRoleUser.get(mapOfQueue.get(mapOfCase.get( message.ParentId).ownerid ).name)){
						  							taskEmailRicevuta.add(TaskHelper.createTask ( message, toSend) );
						  						}
						  				}
						  			
						  			}
						  		}
						  	}
					  	}
				    }
			  	}
			  	// insert all new task
			  	if ( taskEmailRicevuta != null && !taskEmailRicevuta.isEmpty())
			  		INSERT taskEmailRicevuta;
	  			}
	  			catch(Exception e){
	  				system.debug(logginglevel.error,'Exception in createTaskOnInbounEmail : '+e.getMEssage());
	  			}
	  	} 	
	}
	


}