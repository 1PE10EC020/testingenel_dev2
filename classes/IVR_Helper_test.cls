@IsTest
public class IVR_Helper_test{


 // test case 1 - Case number in input.
 	@IsTest
 	static void  findCaseByNumber (){
	 	 Case c = Four_Helper_Test.GenerateCase(Constants.CASE_DA_DEFINIRE);
	 	 insert c;
	 	 case inputCase = [select id, casenumber from case where id =: c.id limit 1];
	 	 system.debug('created case c :'+c );
	 	 ctiData indata = new ctiData();
	 	 indata.CASE_NUM = inputCase.casenumber;
	 	 String jsonin = JSON.serialize(indata);
	 	 WorkspaceConnectorBAController.IVR_Redirect response = WorkspaceConnectorBAController.manageInboundCallENEL_Distr(jsonin);
	 	 system.debug('CaseID :'+response );
	 	 system.assertEquals(response.caseRef, string.valueof(inputCase.id));
 	}
 	
 	//test if the case created had the right record type
 	@IsTest
 	static void  testRecordTypeNewCase (){
 		
 		PercorsoTelefonico__c per = createPercorso ('CODE1',Constants.IVR_CONNESSIONI);
 		insert per;
 		ctiData indata = new ctiData();
	 	indata.COD_IVR = 'CODE1';
	 	String jsonin = JSON.serialize(indata);
 		WorkspaceConnectorBAController.IVR_Redirect response = WorkspaceConnectorBAController.manageInboundCallENEL_Distr(jsonin);
 		string rtCreated = [select recordtype.developerName from case where id =:response.caseRef limit 1].recordtype.developerName;
 		system.assertEquals(Constants.CASE_CONNESSIONE,rtCreated);
 		
 		PercorsoTelefonico__c per2 = createPercorso ('CODE2',Constants.IVR_MISURE);
 		insert per2;
 		ctiData indata2 = new ctiData();
	 	indata2.COD_IVR = 'CODE2';
	 	jsonin = JSON.serialize(indata2);
 		
 		response = WorkspaceConnectorBAController.manageInboundCallENEL_Distr(jsonin);
 		rtCreated = [select recordtype.developerName from case where id =:response.caseRef limit 1].recordtype.developerName;
 		system.assertEquals(Constants.CASE_MISURA,rtCreated);
 		
 		PercorsoTelefonico__c per3 = createPercorso ('CODE3',Constants.IVR_TRASPORTO);
 		insert per3;
 		
 		ctiData indata3 = new ctiData();
	 	indata3.COD_IVR = 'CODE3';
	 	jsonin = JSON.serialize(indata3);
	 	
 		response = WorkspaceConnectorBAController.manageInboundCallENEL_Distr(jsonin);
 		rtCreated = [select recordtype.developerName from case where id =:response.caseRef limit 1].recordtype.developerName;
 		system.assertEquals(Constants.CASE_TRASPORTO,rtCreated);
 	
 		PercorsoTelefonico__c per4 = createPercorso ('CODE4','GenericError');
 		insert per4;
 		
 		ctiData indata4 = new ctiData();
	 	indata4.COD_IVR = 'CODE4';
	 	jsonin = JSON.serialize(indata4);
	 	
 		response = WorkspaceConnectorBAController.manageInboundCallENEL_Distr(jsonin);
 		rtCreated = [select recordtype.developerName from case where id =:response.caseRef limit 1].recordtype.developerName;
 		system.assertEquals(Constants.CASE_DA_DEFINIRE,rtCreated);
 	}
 	
 	
 	
 	@isTest
 	static void createCaseWithAccountFiscalCode (){
 		system.debug('START TEST createCaseWithAccountFiscalCode');
 		Account acc = createAccount('Gennaro','Esposito','SPSGNR80A01F839U','07643520567',Constants.ACCOUNT_PERSONA_FISICA);
 		insert acc;
 		PercorsoTelefonico__c per = createPercorso ('CODE1',Constants.IVR_CONNESSIONI);
 		insert per;
 		
 		ctiData indata  = new ctiData();
	 	indata.COD_IVR = 'CODE1';
	 	indata.cf = acc.CodiceFiscale__c;
	 	indata.CONNID = 'CONNID';
	 	indata.dn = 'dn';
	 	indata.tenant ='tenatn';
	 	indata.pod='pod';
	 	indata.ID_PR ='ID_PRATICA';
	 	string jsonin = JSON.serialize(indata);
 		
 		WorkspaceConnectorBAController.IVR_Redirect response = WorkspaceConnectorBAController.manageInboundCallENEL_Distr(jsonin);
 		case createdCAse = [select id ,AccountId,Account.CodiceFiscale__c from case where id = :response.caseRef limit 1];
 		system.debug('createdCAse is '+createdCAse);
 		
 		system.assertEquals(acc.CodiceFiscale__c,createdCAse.Account.CodiceFiscale__c);
 		
 		
 	
 	}
 	
 	@isTest
 	static void createCaseWithAccountPhoneNum (){
 		system.debug('START TEST createCaseWithAccountPhoneNum');
 		Account acc = createAccount('Gennaro','Esposito','SPSGNR80A01F839U','07643520567',Constants.ACCOUNT_PERSONA_FISICA);
 		insert acc;
 		
 		Account acc2 = createAccount('Jane','Doe','DOEJNA80A41H501Z','',Constants.ACCOUNT_PERSONA_FISICA);
 		//insert Acc2;
 		
 		
 		PercorsoTelefonico__c per = createPercorso ('CODE1',Constants.IVR_CONNESSIONI);
 		insert per;
 		TEST.startTest();
 		ctiData indata  = new ctiData();
	 	indata.COD_IVR = 'CODE1';
		indata.PHONE = acc.phone;
	 	indata.CONNID = 'CONNID';
	 	indata.dn = 'dn';
	 	indata.tenant ='tenatn';
	 	indata.pod='pod';
	 	indata.ID_PR ='ID_PRATICA';
	 	string jsonin = JSON.serialize(indata);
	 	list<Id> listOFResultQuery = new list<Id>();
	 	listOFResultQuery.add(acc.id);
 		Test.setFixedSearchResults(listOFResultQuery);
 		WorkspaceConnectorBAController.IVR_Redirect response = WorkspaceConnectorBAController.manageInboundCallENEL_Distr(jsonin);
 		
 		case createdCAse = [select id ,AccountId,Account.phone from case where id = :response.caseRef limit 1];
 		system.debug('createdCAse is '+createdCAse);
 		
 		system.assertEquals(acc.phone,createdCAse.Account.phone);
 		
 		acc2.Email__c = 'atsqewqeqw.test.sadasd@gmail.com';
 		insert acc2;
 		listOFResultQuery.add(acc2.id);
 		Test.setFixedSearchResults(listOFResultQuery);
 		response = WorkspaceConnectorBAController.manageInboundCallENEL_Distr(jsonin);
 		system.assertEquals(response.caseRef,'NOT FOUND');
 		
 		
 		test.stopTest();
 	
 	}
 	
 	
 	@isTest
 	static void createCaseWithsoggTerzo (){
 		system.debug('START TEST createCaseWithAccountPhoneNum');
 		Account acc = createAccount('Gennaro','Esposito','SPSGNR80A01F839U','07643520567',Constants.ACCOUNT_PERSONA_FISICA);
 		insert acc;
 		
 		Account acc2 = createAccount('Jane','Doe','DOEJNA80A41H501Z','',Constants.ACCOUNT_PERSONA_FISICA);
 		//insert Acc2;
 		
 		
 		PercorsoTelefonico__c per = createPercorso ('CODE1',Constants.IVR_SOGGTERZO);
 		insert per;
 		TEST.startTest();
 		ctiData indata  = new ctiData();
	 	indata.COD_IVR = 'CODE1';
		indata.PHONE = acc.phone;
	 	indata.CONNID =Constants.UNDEFINED;
	 	indata.dn = Constants.UNDEFINED;
	 	indata.tenant =Constants.UNDEFINED;
	 	indata.pod=Constants.UNDEFINED;
	 	indata.ID_PR =Constants.UNDEFINED;
	 	string jsonin = JSON.serialize(indata);
	 	list<Id> listOFResultQuery = new list<Id>();
	 	listOFResultQuery.add(acc.id);
 		Test.setFixedSearchResults(listOFResultQuery);
 		WorkspaceConnectorBAController.IVR_Redirect response = WorkspaceConnectorBAController.manageInboundCallENEL_Distr(jsonin);
 		
 		case createdCAse = [select id ,AccountId,Account.phone from case where id = :response.caseRef limit 1];
 		system.debug('createdCAse is '+createdCAse);
 		
 		system.assertEquals(acc.phone,createdCAse.Account.phone);
 		
 		acc2.Email__c = 'atsqewqeqw.test.sadasd@gmail.com';
 		insert acc2;
 		listOFResultQuery.add(acc2.id);
 		Test.setFixedSearchResults(listOFResultQuery);
 		response = WorkspaceConnectorBAController.manageInboundCallENEL_Distr(jsonin);
 		system.assertEquals(response.caseRef,'NOT FOUND');
 		
 		
 		test.stopTest();
 	
 	}
 	
 	@isTest
 	static void exceptionInIVR_Helper (){
 		
 	
 		try{
           	PercorsoTelefonico__c res = IVR_Helper.findIVRPath ('test');
        }
        catch(Exception e){
             system.debug('exception '+e.getmessage());
            Boolean expectedExceptionThrown =  e.getMessage().contains(Label.CF_First_6_Letter_Not_Valid) ? true : false;
            system.debug('expectedExceptionThrown '+expectedExceptionThrown );
            System.AssertEquals(expectedExceptionThrown, true);
        
        }
 	
 	}
 	
 	public static PercorsoTelefonico__c createPercorso(string ivrCode, string contesto){
 	PercorsoTelefonico__c per = new PercorsoTelefonico__c();
 	per.CodIVR__c = ivrCode;
 	per.Contesto__c = contesto;
 	return per;
 	}
 	
 	
 	
 	public static account createAccount(string firstname, string lastName, string cf, string piva,string recordtypename ){
 	
 	Account acc = new Account();
 	acc.name = lastname;
 	acc.nome__C = firstname;
 	acc.CodiceFiscale__c = cf;
 	acc.PartitaIVA__c = piva;
 	acc.Email__c='test.test@gmail.com';
 	acc.phone ='3290939117';
 	acc.telefono1__c='3290939117';
 	acc.recordtypeid = [select id from recordtype where developername = :recordtypename limit 1].id;
 	return acc;
 	
 	}
 	

 	

}