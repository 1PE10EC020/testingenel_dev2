@isTest
public class KBMS_ArticleHistoryBatchableSheduledTest 
{

    public Static testMethod void testArticleHistoryScheduled()
    {
    
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        List<KBMS_ArticleHistory__c> articleHistoryList = new List<KBMS_ArticleHistory__c>();
        for (Integer i=0; i<200; i++)
        {
            KBMS_ArticleHistory__c ah = new KBMS_ArticleHistory__c();
            ah.Article_Score__c = 3;
            ah.Article_Summary__c = 'Test Summary';
            ah.Article_Title__c = 'Test Article';
            ah.Article_Total_Views__c = 30;
            ah.Article_Type__c = 'FAQ';
            ah.Article_URL__c = 'TestModalità-di-invio-richiesta-disalimentazione-per-messa-in-sicurezza-impianto';
            ah.Channel__c = 'Customer';
            ah.Publish_Date__c = date.today();
            
            articleHistoryList.add(ah);
        }
        
        insert articleHistoryList;
        
        Test.startTest();
        String jobId = System.schedule('ScheduleApexClassTest', CRON_EXP, new KBMS_ArticleHistoryBatchableSheduled());
        Test.stopTest();
        
    
    }
    public Static testMethod void testArticleHistoryBatch()
    {
    
        List<KBMS_ArticleHistory__c> articleHistoryList = new List<KBMS_ArticleHistory__c>();
        for (Integer i=0; i<100; i++)
        {
            KBMS_ArticleHistory__c ah = new KBMS_ArticleHistory__c();
            ah.Article_Score__c = 3;
            ah.Article_Summary__c = 'Test Summary';
            ah.Article_Title__c = 'Test Article';
            ah.Article_Total_Views__c = 30;
            ah.Article_Type__c = 'FAQ';
            ah.Article_URL__c = 'TestModalità-di-invio-richiesta-disalimentazione-per-messa-in-sicurezza-impianto';
            ah.Channel__c = 'Customer';
            ah.Publish_Date__c = date.today();
            
            articleHistoryList.add(ah);
        }
        
        insert articleHistoryList;
        
        Test.startTest();
        KBMS_ArticleHistoryBatchableSheduled b = new KBMS_ArticleHistoryBatchableSheduled();
        database.executebatch(b);
        Test.stopTest();
        
    
    }
        
    
}