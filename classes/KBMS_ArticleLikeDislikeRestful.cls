@RestResource(urlMapping = '/articleLikeDislikeRestful/*')
//------------------- This REST Class is used for posting vote(up/down) for knowledge article -------//
global with sharing class KBMS_ArticleLikeDislikeRestful {

    @HttpPost
    global static WrapperClass postMethodLikeDislike(String articleId, String userInput) {
        Integer totalUps;
        Integer totalDowns;
        String userInputVoteType = userInput == 'Like'? 'Up': 'Down';
        String userOutPutVoteType;
        System.debug('type is  : '+ userInputVoteType );
        List<Vote> oldVoteDel = [select id, ParentId, type from vote where ParentId =: articleId and CreatedById =: UserInfo.getUserId()];
         if (oldVoteDel.isEmpty()) {
            Vote newVote = new Vote();
            newVote.ParentId = Id.valueof(articleId);
            newVote.type = userInputVoteType;
            try {
                insert newVote;
            }

            catch(Exception e) {
                System.debug('The message is ::' + e.getMessage());
            }
            List < Vote > voteUp = [select id, ParentId, type from vote where ParentId =: articleId and type = 'Up'];
            List < Vote > voteDown = [select id, ParentId, type from vote where ParentId =: articleId and type = 'Down'];
            totalUps = voteUp.size();
            totalDowns = voteDown.size();
            userOutPutVoteType = newVote.type == 'Up'? 'Like' : 'Dislike';
            WrapperClass wp = new WrapperClass();
            wp.articleId = newVote.ParentId;
            wp.userInput = userOutPutVoteType;
            wp.totalLikes = totalUps;
            wp.totalDislikes = totalDowns;
            wp.message = 'Successful Vote';
            
            return wp; 

        }
        
        else{
           userOutPutVoteType = userInputVoteType == 'Up'? 'Like' : 'Dislike';
            List < Vote > voteUp = [select id, ParentId, type from vote where ParentId =: articleId and type = 'Up'];
            List < Vote > voteDown = [select id, ParentId, type from vote where ParentId =: articleId and type = 'Down'];
            totalUps = voteUp.size();
            totalDowns = voteDown.size();
            WrapperClass wp = new WrapperClass();
            wp.articleId = Id.valueof(articleId);
            wp.userInput = userOutPutVoteType;
            wp.totalLikes = totalUps;
            wp.totalDislikes = totalDowns;
            wp.message = 'You have already voted';
            
            return wp; 
        
        
        } 

       
    /*    else {
            if (oldVoteDel[0].type != userInputVoteType) {
                delete oldVoteDel;
                Vote newVote = new Vote();
                newVote.ParentId = Id.valueof(articleId);
                newVote.type = string.valueOf(userInputVoteType);
                try {
                    insert newVote;
                }

                catch(Exception e) {
                    System.debug('The message is ::' + e.getMessage());
                }
                List < Vote > voteUp = [select id, ParentId, type from vote where ParentId =: articleId and type = 'Up'];
                List < Vote > voteDown = [select id, ParentId, type from vote where ParentId =: articleId and type = 'Down'];
                totalUps = voteUp.size();
                totalDowns = voteDown.size();
                userOutPutVoteType = newVote.type == 'Up'? 'Like' : 'Dislike';
                WrapperClass wp = new WrapperClass();
                wp.articleId = newVote.ParentId;
                wp.userInput = userOutPutVoteType;
                wp.totalLikes = totalUps;
                wp.totalDislikes = totalDowns;
                wp.message = 'Successful Vote';
                return wp;


            }

            else {
                List < Vote > voteUp = [select id, ParentId, type from vote where ParentId =: articleId and type = 'Up'];
                List < Vote > voteDown = [select id, ParentId, type from vote where ParentId =: articleId and type = 'Down'];
                totalUps = voteUp.size();
                totalDowns = voteDown.size();
                userOutPutVoteType = oldVoteDel[0].type == 'Up'? 'Like' : 'Dislike';
                WrapperClass wp = new WrapperClass();
                wp.articleId = oldVoteDel[0].ParentId;
                wp.userInput = userOutPutVoteType;
                wp.totalLikes = totalUps;
                wp.totalDislikes = totalDowns;
                wp.message = 'You have already voted the same, so new vote is not counted';
                return wp;

            }

        }  */

    }

    global class WrapperClass {

        public ID articleId;
        public string userInput;
        public Integer totalLikes;
        public Integer totalDislikes;
        public String message;
    
    }


}