@RestResource(urlMapping='/getArticleSearch1/*')
global with sharing class KBMS_ArticleSearchRestful1
{
    
    @HttpGet
    global static KbmsSearchResult  getArticleByCategoryIdArticleType()
    {
        String objectType = RestContext.request.params.get('objectType');
        String limits = RestContext.request.params.get('limit');
        String froms = RestContext.request.params.get('from');
        String categoryId = RestContext.request.params.get('categoryId');
        String searchString= RestContext.request.params.get('textSearch');
        
               
        RestResponse res = RestContext.response;
        
        String language = System.Label.KBMS_Language;
        String publishStatus = System.Label.KBMS_PublishStatus_Online;
        String recordtype = System.Label.KBMS_RecordTypeInSearch;
        String defaultDataCategory = Label.KBMS_AppDataCategory;
        String prePositionWord = System.Label.KBMS_PrepositionWords;
        String appArticle=System.Label.KBMS_AppArticle;
        List<ArticleWrapper> articleWrapperList=new List<ArticleWrapper>();
        Set<String> setSearchString=new Set<String>();      
        RecordType recordtypeQuery=NULL;
        Id recordTypeId=NULL;
        recordtypeQuery = [SELECT Id, Name FROM RecordType WHERE Name = :recordtype LIMIT 1];
        recordTypeId = recordtypeQuery.Id;
        
        List<String> searchStringList = new List<String>();
        List<Contact> contactList=new List<Contact>();
        List<Sezione_Contatti__c> sezioneContattiList=new List<Sezione_Contatti__c>();
    
        String [] contactFields = new String [] {'App__c','Email','Fax','FirstName','LastName','MailingStreet','MailingCity','MailingState','MailingPostalCode','MailingCountry','Name','Phone','Sezione_Contatti__c','Sito__c','Zona__c','MobilePhone'}; 
        String [] sezioneContactFields = new String [] {'Nome_Sezione__c','Tipologia_Richiedente__c'};
        String [] articleFields = new String [] {'Title','UrlName','Summary','KnowledgeArticleId','firstPublishedDate','ArticleType','ArticleNumber'};  
        Boolean isDataCategoryUnselected = FALSE;
           
        if(categoryId == '' || categoryId == NULL || categoryId.length() == 0)
        {
            categoryId = 'Tutto';
            isDataCategoryUnselected = TRUE;
        }
              
        
        if (searchString != NULL && searchString != '')     
            
                
        searchStringList =searchString.split(' ');      
        System.debug('==>'+searchStringList );      
        setSearchString.addAll(searchStringList);       
        
        //preposition word remove       
        String prePositionWords = System.Label.KBMS_prepositionWords;
        String[] splitPrepositionWords = new String[] {};       
        splitPrepositionWords = prePositionWords.split(',');
        System.debug('splitPrepositionWords '+splitPrepositionWords);        
        Set<String> setprepositionWords=new Set<String>();      
        setprepositionWords.addAll(splitPrepositionWords);      
                
        //removeing all preposition words from set      
        setSearchString.removeAll(setprepositionWords);
        System.debug('setSearchString'+setSearchString);     
        searchStringList.Clear();   
        setprepositionWords.Clear();    
        for(String s:setSearchString){      
                
            searchStringList.add('%'+s+'%');        
        }       
        Integer noLimits = limits == null ? 0 : Integer.valueOf(limits);
        Integer offsets = froms == null ? 0 : Integer.valueOf(froms );
        
        String[] splitAppArticle = new String[] {};      
         splitAppArticle = appArticle.split(',');
         Set<String> setAppArticle=new Set<String>();  
         setAppArticle.addAll(splitAppArticle);  
          
        if(setAppArticle.contains('FAQ') && 
            (objectType=='FAQ' || objectType=='Tutto' ) )
        { 
            if (!KBMS_UtilityToCheckFLS.checkCRUDPermission('FAQ__kav', 'isAccessible') ||
                ! (KBMS_UtilityToCheckFLS.checkFLSPermission('FAQ__kav', articleFields, 'isAccessible'))) 
            {
                    res.responseBody = Blob.valueOf('INVALID DATA');
                    res.StatusCode=403;
                    return null;
            }
            else 
            {
                KBMS_CustomSearch__c customSearchFaq = KBMS_CustomSearch__c.getValues('FAQ');
                List<String> faqSearchFields = new List<String>();
                if (customSearchFaq.Fields__c  != null || customSearchFaq.Fields__c != '')
                {
                    faqSearchFields = customSearchFaq.Fields__c.split(',');
                }
                                
                String firstPartQuery = 'SELECT Id, Title, KnowledgeArticleId,LastModifiedDate FROM FAQ__kav ';
                String secondPartQuery = ' (PublishStatus =: PublishStatus AND Language =:language '; 
                String thirdPartQuery='';  
                Integer k = 0;
                for (String s: faqSearchFields)
                {
                    thirdPartQuery  =  thirdPartQuery + s +' LIKE :searchStringList';
                    if (k != faqSearchFields.size())
                    {
                        thirdPartQuery =  thirdPartQuery + ' OR ';
                    }
                    k++;
                }
                 
                thirdPartQuery= thirdPartQuery.removeEnd(' OR ');
                thirdPartQuery = thirdPartQuery+ ' ) ) WITH DATA CATEGORY ' + defaultDataCategory +' BELOW ' + categoryId + '__c AND KBMS__c AT (OT__c) ORDER BY LastModifiedDate DESC LIMIT ' + noLimits + ' OFFSET ' + offsets;
                String query = firstPartQuery + ' WHERE ' + secondPartQuery + ' AND ( ' + thirdPartQuery  ;
                
                system.debug(query);  
           
                List<sObject> knowledgesObject = Database.query(query);
                 //ArticleWrapper
                for (sObject sObjKAV  : knowledgesObject)
                { 
                    articleWrapper aw=new articleWrapper();       
                    aw.title = String.valueOf(sObjKAV.get('title'));
                    aw.id =String.valueOf(sObjKAV.get('KnowledgeArticleId'));
                    aw.categoryId = categoryId;
                    aw.objectType = 'FAQ';
                    articleWrapperList.add(aw);
                }
                
            }
            
        }
        if(setAppArticle.contains('Processi') && 
            (objectType=='Processi' || objectType=='Tutto' ) )
        { 
            /*if (!KBMS_UtilityToCheckFLS.checkCRUDPermission('Processi__kav', 'isAccessible') ||
                ! (KBMS_UtilityToCheckFLS.checkFLSPermission('Processi__kav', articleFields, 'isAccessible'))) 
            {
                    res.responseBody = Blob.valueOf('INVALID DATA');
                    res.StatusCode=403;
                    return null;
            }
            else
            {
                   
                KBMS_CustomSearch__c customSearchProcessi = KBMS_CustomSearch__c.getValues('Processi');
                List<String> processiSearchFields = new List<String>();
                if (customSearchProcessi.Fields__c  != null || customSearchProcessi.Fields__c != '')
                {
                    processiSearchFields = customSearchProcessi.Fields__c.split(',');
                }
                            
                String firstPartQuery = 'SELECT Id, Title, KnowledgeArticleId,LastModifiedDate FROM Processi__kav ';
                String secondPartQuery = ' (PublishStatus =: PublishStatus AND Language =:language '; 
                String thirdPartQuery=''; 
                Integer l = 0;
                for (String s: processiSearchFields)
                {
                    thirdPartQuery  =  thirdPartQuery + s +' LIKE :searchStringList';
                    if (l != processiSearchFields.size())
                    {
                        thirdPartQuery =  thirdPartQuery + ' OR ';
                    }
                    l++;
                }
                         
                thirdPartQuery= thirdPartQuery.removeEnd(' OR ');
                thirdPartQuery = thirdPartQuery+ ' ) ) WITH DATA CATEGORY ' + defaultDataCategory +' BELOW ' + categoryId + '__c AND KBMS__c AT (OT__c) ORDER BY LastModifiedDate DESC LIMIT ' + noLimits + ' OFFSET ' + offsets;
                String query = firstPartQuery + ' WHERE ' + secondPartQuery + ' AND ( ' + thirdPartQuery  ;
                
                system.debug(query);  
           
                List<sObject> knowledgesObject = Database.query(query);
                 //ArticleWrapper
                for (sObject sObjKAV  : knowledgesObject)
                { 
                    articleWrapper aw=new articleWrapper();       
                    aw.title = String.valueOf(sObjKAV.get('title'));
                    aw.id =String.valueOf(sObjKAV.get('KnowledgeArticleId'));
                    aw.categoryId = categoryId;
                    aw.objectType = 'Processi';
                    articleWrapperList.add(aw);
                }
                    
            }*/
        }
        
        if(setAppArticle.contains('Moduli') && 
            (objectType=='Moduli' || objectType=='Tutto' ) )
        { 
            if (!KBMS_UtilityToCheckFLS.checkCRUDPermission('Moduli__kav', 'isAccessible') ||
                ! (KBMS_UtilityToCheckFLS.checkFLSPermission('Moduli__kav', articleFields, 'isAccessible'))) 
            {
                    res.responseBody = Blob.valueOf('INVALID DATA');
                    res.StatusCode=403;
                    return null;
            }
            else
            {
                        
                KBMS_CustomSearch__c customSearchModuli = KBMS_CustomSearch__c.getValues('Moduli');
                List<String> ModuliSearchFields = new List<String>();
                if (customSearchModuli.Fields__c  != null || customSearchModuli.Fields__c != '')
                {
                    ModuliSearchFields = customSearchModuli.Fields__c.split(',');
                }
                            
                String firstPartQuery = 'SELECT Id, Title, KnowledgeArticleId,LastModifiedDate FROM Moduli__kav ';
                String secondPartQuery = ' (PublishStatus =: PublishStatus AND Language =:language '; 
                String thirdPartQuery='';
                Integer l = 0;
                for (String s: ModuliSearchFields)
                {
                    thirdPartQuery  =  thirdPartQuery + s +' LIKE :searchStringList';
                    if (l != ModuliSearchFields.size())
                    {
                        thirdPartQuery =  thirdPartQuery + ' OR ';
                    }
                    l++;
                }
                 
                thirdPartQuery= thirdPartQuery.removeEnd(' OR ');
                thirdPartQuery = thirdPartQuery+ ' ) ) ';
                String fourthQuery =' WITH DATA CATEGORY ' + defaultDataCategory +' BELOW ' + categoryId + '__c AND KBMS__c AT (OT__c) ';
                String fifthQuery = ' ORDER BY LastModifiedDate DESC LIMIT ' + noLimits + ' OFFSET ' + offsets;
                String query;
                query = firstPartQuery + ' WHERE ' + secondPartQuery + ' AND ( ' + thirdPartQuery + fourthQuery + fifthQuery;
                if(isDataCategoryUnselected)
                    query = firstPartQuery + ' WHERE ' + secondPartQuery + ' AND ( ' + thirdPartQuery + fifthQuery;
                
                
                system.debug(query);  
           
                List<sObject> knowledgesObject = Database.query(query);
                 //ArticleWrapper                        
                for (sObject sObjKAV  : knowledgesObject)
                { 
                    articleWrapper aw=new articleWrapper();       
                    aw.title = String.valueOf(sObjKAV.get('title'));
                    aw.id =String.valueOf(sObjKAV.get('KnowledgeArticleId'));
                    aw.categoryId = categoryId;
                    aw.objectType = 'Moduli';
                    articleWrapperList.add(aw);
                }
                    
            }
        }
        
        if(objectType=='Contacts' || objectType=='Tutto')
        {
            if (!KBMS_UtilityToCheckFLS.checkCRUDPermission('Contact', 'isAccessible') ||
                ! (KBMS_UtilityToCheckFLS.checkFLSPermission('Contact', contactFields, 'isAccessible')
                )) 
            {
                res.responseBody = Blob.valueOf('INVALID DATA');
                res.StatusCode=403;
                return null;
            }
            else 
            {
                
                KBMS_CustomSearch__c customSearchContact = KBMS_CustomSearch__c.getValues('Contact');
                List<String> contactSearchFields = new List<String>();
                if (customSearchContact.Fields__c  != null || customSearchContact.Fields__c != '')
                {
                    contactSearchFields = customSearchContact.Fields__c.split(',');
                }
                            
                String contactFieldQuery = 'SELECT App__c, Email, Fax, FirstName, Id, LastName, MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry, Name, Phone, Sezione_Contatti__c, Sito__c, Zona__c, MobilePhone FROM Contact ';
                String whereClouseQuery = ' Sezione_Contatti__c != null '; //RecordTypeId =: RecordTypeId AND
                String whereClouseDynamicQuery = '';
                Integer i = 0;
                for (String s: contactSearchFields)
                {
                    whereClouseDynamicQuery  = whereClouseDynamicQuery + s +' LIKE :searchStringList';
                    if (i != contactSearchFields.size())
                    {
                        whereClouseDynamicQuery = whereClouseDynamicQuery + ' OR ';
                    }
                    i++;
                }
                         
                whereClouseDynamicQuery=whereClouseDynamicQuery.removeEnd(' OR ');
                String contactQuery = contactFieldQuery + ' WHERE ' + whereClouseQuery + ' AND ( ' + whereClouseDynamicQuery + ' ) ';
                //String contactQuery = contactFieldQuery + ' LIMIT 3';
                system.debug(contactQuery);
                contactList= Database.query(contactQuery);   
                    
            }
            
            if (!KBMS_UtilityToCheckFLS.checkCRUDPermission('Sezione_Contatti__c', 'isAccessible') &&
                !(KBMS_UtilityToCheckFLS.checkFLSPermission('Sezione_Contatti__c', sezioneContactFields, 'isAccessible'))) 
            {
                res.responseBody = Blob.valueOf('INVALID DATA');
                res.StatusCode=403;
                return null;
            }
            else 
            {
                KBMS_CustomSearch__c customSearchSezioneContatti = KBMS_CustomSearch__c.getValues('Sezione_Contatti__c');
                List<String> sezioneContattiSearchFields = new List<String>();
                if (customSearchSezioneContatti.Fields__c  != null || customSearchSezioneContatti.Fields__c != '')
                {
                    sezioneContattiSearchFields = customSearchSezioneContatti.Fields__c.split(',');
                }
                            
                String sezioneContattiFieldQuery = 'SELECT Nome_Sezione__c,Tipologia_Richiedente__c, '+ 
                '(SELECT App__c,Email,Fax,FirstName,Id,LastName,MailingStreet,MailingCity,MailingState,MailingPostalCode,MailingCountry,Name,Phone,Sezione_Contatti__c,Sito__c,Zona__c,MobilePhone FROM Contacts_del__r)  ' +
                'FROM Sezione_Contatti__c';
                String whereSezioneContattiQuery = ' Nome_Sezione__c != null ';
                String whereSezioneContattiDynamicQuery = '';
                Integer j = 0;
                for (String s: sezioneContattiSearchFields)
                {
                    whereSezioneContattiDynamicQuery  = whereSezioneContattiDynamicQuery + s +' LIKE :searchStringList';
                    if (j != sezioneContattiSearchFields.size())
                    {
                        whereSezioneContattiDynamicQuery = whereSezioneContattiDynamicQuery + ' OR ';
                    }
                    j++;
                }
             
                whereSezioneContattiDynamicQuery=whereSezioneContattiDynamicQuery.removeEnd(' OR ');
                String sezioneContattiQuery = sezioneContattiFieldQuery + ' WHERE ' + wheresezioneContattiQuery + ' AND ( ' + wheresezioneContattiDynamicQuery + ' ) ';
                system.debug(sezioneContattiQuery);
                sezioneContattiList= Database.query(sezioneContattiQuery);   
            }
        }
    
        KbmsSearchResult KbmsSearchResult= new KbmsSearchResult();
        KbmsSearchResult.contactList=contactList;
        KbmsSearchResult.sezioneContattiList=sezioneContattiList;
        KbmsSearchResult.articleWrapperList=articleWrapperList;
        return KbmsSearchResult;
    }
    
    global class KbmsSearchResult
    {
        public list<Contact> contactList;
        public list<Sezione_Contatti__c> sezioneContattiList;
        public list<ArticleWrapper> articleWrapperList;
    } 
    global class ArticleWrapper
    {
        public String title{get;set;}
        public String id{get;set;}
        public String categoryId{get;set;}
        public String objectType{get;set;}
    } 
    

}