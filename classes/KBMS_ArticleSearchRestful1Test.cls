/**
* @author Atos
* @date  Jan,2017 
* @description This class is test class of KBMS_ArticleSearchRestful1
*/
@isTest()
public class KBMS_ArticleSearchRestful1Test {

    static testMethod void positiveTestFAQ() {
        
        List<FAQ__kav> faqtest= KBMS_TestDataFactory.createFAQTestRecords(2);        
        System.assertEquals(faqtest.size(), 2);
        system.debug('faqtest::'+faqtest);
        
        System.Assert(faqtest[0].Id != null, 'The Test FAQ article typed id not inserted properly');
        
        //Accessing custom Labels
        String defaultDataCategory = Label.KBMS_AppDataCategory;
        String dataCategory=Label.KBMS_DataCategory; 
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String articleType=Label.KBMS_ArticleType;
        String language = Label.KBMS_Language;
        String draft= Label.KBMS_PublishStatus_Draft;
        String limits = '5';
        String froms = '0';
        List<String> searchStringList = new List<String>();
        searchStringList.add('info');
        
        Test.startTest();    
        
        FAQ__DataCategorySelection datacatTest=new FAQ__DataCategorySelection();
        datacatTest.ParentId= faqtest[0].Id;     
        datacatTest.DataCategoryName=dataCategory;
        datacatTest.DataCategoryGroupName=GroupDataCategory;  
        insert datacatTest; 
        
        FAQ__DataCategorySelection datacatTest1=new FAQ__DataCategorySelection();
        datacatTest1.ParentId= faqtest[0].Id;     
        datacatTest1.DataCategoryName='OT';
        datacatTest1.DataCategoryGroupName='KBMS';  
        insert datacatTest1; 

        System.assert(datacatTest.Id!=null,'The Test Data Category did not insert properly');
        System.assertEquals(faqtest[0].Id,datacatTest.ParentId);
        System.assertEquals(dataCategory,datacatTest.DataCategoryName);
        System.assertEquals(GroupDataCategory,datacatTest.DataCategoryGroupName); 
        
        FAQ__kav insertedTestArticle = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE ID = :faqtest[0].Id];
        
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId, true); 
      
        KBMS_CustomSearch__c customSearchFaq = new KBMS_CustomSearch__c();        
        
        customSearchFaq.Name='FAQ';
        customSearchFaq.Fields__c='Title,Summary';
        
        insert customSearchFaq;
        
        
        
        RestRequest restRequestobject = new RestRequest();             
        RestRequestobject.requestURI = '/services/apexrest/getArticleByCategoryId';  
        RestRequestobject.httpMethod = 'GET';  
        RestRequestobject.addParameter('articleType', articleType);
        RestRequestobject.addParameter('limit', '5');
        RestRequestobject.addParameter('from', '0');
        RestRequestobject.addParameter('categoryId',datacatTest.DataCategoryName);
        RestRequestobject.addParameter('textSearch','test');
        RestRequestobject.addParameter('objectType','FAQ'); 
         
        RestContext.request = RestRequestobject;
        RestResponse restResponseObject = new RestResponse(); 
        RestContext.response= restResponseObject;

      KBMS_ArticleSearchRestful1.KbmsSearchResult b = KBMS_ArticleSearchRestful1.getArticleByCategoryIdArticleType();
        //system.assert(condition);
        system.debug('b---'+b);
        
        Test.stopTest();
        
    } 

    static testMethod void positiveTestProcessi() {
        
       List<Processi__kav> Processitest= KBMS_TestDataFactory.createProcessTestRecords(2);        
        System.assertEquals(Processitest.size(), 2);
        system.debug('Processitest::'+Processitest);
        
        //Accessing custom Labels
        String defaultDataCategory = Label.KBMS_AppDataCategory;
        String dataCategory=Label.KBMS_DataCategory; 
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String articleType='Processi';
        String language = Label.KBMS_Language;
        String draft= Label.KBMS_PublishStatus_Draft;
        String limits = '5';
        String froms = '0';
        List<String> searchStringList = new List<String>();
        
        Test.startTest();  
        searchStringList.add('info');
        Processi__DataCategorySelection datacatTest=new Processi__DataCategorySelection();
        
        datacatTest.ParentId= Processitest[0].Id;     
        datacatTest.DataCategoryName=dataCategory;
        datacatTest.DataCategoryGroupName=GroupDataCategory;  
         
        insert datacatTest;  
        
        Processi__DataCategorySelection datacatTest1=new Processi__DataCategorySelection();
        datacatTest1.ParentId= Processitest[0].Id;    
        datacatTest1.DataCategoryName='OT';
        datacatTest1.DataCategoryGroupName='KBMS';  
        insert datacatTest1; 
  
        System.assert(datacatTest.Id!=null,'The Test Data Category did not insert properly');
        System.assertEquals(Processitest[0].Id,datacatTest.ParentId);
        System.assertEquals(dataCategory,datacatTest.DataCategoryName);
        System.assertEquals(GroupDataCategory,datacatTest.DataCategoryGroupName); 

        Processi__kav insertedTestArticle = [SELECT KnowledgeArticleId FROM Processi__kav WHERE ID = :Processitest[0].Id];
        
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId, true); 
      
        KBMS_CustomSearch__c customSearchFaq = new KBMS_CustomSearch__c();        
        
        customSearchFaq.Name='Processi';
        customSearchFaq.Fields__c='Title';
        
        insert customSearchFaq;
      
        RestRequest restRequestobject = new RestRequest();             
        RestRequestobject.requestURI = '/services/apexrest/getArticleByCategoryId';  
        RestRequestobject.httpMethod = 'GET';  
        RestRequestobject.addParameter('articleType', articleType);
        RestRequestobject.addParameter('limit', '5');
        RestRequestobject.addParameter('from', '0');
        RestRequestobject.addParameter('categoryId','Tutto');
        RestRequestobject.addParameter('textSearch','Processi');
        RestRequestobject.addParameter('objectType','Processi'); 
         
         
        RestContext.request = RestRequestobject;
        RestResponse restResponseObject = new RestResponse(); 
        RestContext.response= restResponseObject; 
        
        KBMS_ArticleSearchRestful1.ArticleWrapper artwr= new KBMS_ArticleSearchRestful1.ArticleWrapper();
      
        KBMS_ArticleSearchRestful1.ArticleWrapper art=new KBMS_ArticleSearchRestful1.ArticleWrapper();
        KBMS_ArticleSearchRestful1.getArticleByCategoryIdArticleType();
        
        Test.stopTest();
      
    }
           
    static testMethod void positiveTestModuli() {
        
        List<Moduli__kav> modulitest= KBMS_TestDataFactory.createModuliTestRecords(2);        
        System.assertEquals(modulitest.size(), 2);
        system.debug('faqtest::'+modulitest);
        
        System.Assert(modulitest[0].Id != null, 'The Test FAQ article typed id not inserted properly');
        
        //Accessing custom Labels
        String defaultDataCategory = Label.KBMS_AppDataCategory;
        String dataCategory=Label.KBMS_DataCategory; 
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String articleType=Label.KBMS_ArticleType;
        String language = Label.KBMS_Language;
        String draft= Label.KBMS_PublishStatus_Draft;
        String limits = '5';
        String froms = '0';
        List<String> searchStringList = new List<String>();
        
        Test.startTest();    
        
        searchStringList.add('info');
        Moduli__DataCategorySelection datacatTest=new Moduli__DataCategorySelection();
        
        datacatTest.ParentId= modulitest[0].Id;     
        datacatTest.DataCategoryName=dataCategory;
        datacatTest.DataCategoryGroupName=GroupDataCategory;  
         
        insert datacatTest;  
        
        Moduli__DataCategorySelection datacatTest1 =new Moduli__DataCategorySelection();
        datacatTest1.ParentId= modulitest[0].Id;    
        datacatTest1.DataCategoryName='OT';
        datacatTest1.DataCategoryGroupName='KBMS';  
        insert datacatTest1; 
        
        
        System.assert(datacatTest.Id!=null,'The Test Data Category did not insert properly');
        System.assertEquals(modulitest[0].Id,datacatTest.ParentId);
        System.assertEquals(dataCategory,datacatTest.DataCategoryName);
        System.assertEquals(GroupDataCategory,datacatTest.DataCategoryGroupName); 
        
        
        Moduli__kav insertedTestArticle = [SELECT KnowledgeArticleId FROM Moduli__kav WHERE ID = :modulitest[0].Id];
        
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId, true); 
      
        KBMS_CustomSearch__c customSearchFaq = new KBMS_CustomSearch__c();        
        
        customSearchFaq.Name='Moduli';
        customSearchFaq.Fields__c='Title,Summary';
        
        insert customSearchFaq;
              
        RestRequest restRequestobject = new RestRequest();             
        RestRequestobject.requestURI = '/services/apexrest/getArticleByCategoryId';  
        RestRequestobject.httpMethod = 'GET';  
        RestRequestobject.addParameter('articleType', articleType);
        RestRequestobject.addParameter('limit', '5');
        RestRequestobject.addParameter('from', '0');
        RestRequestobject.addParameter('categoryId',datacatTest.DataCategoryName);
        RestRequestobject.addParameter('textSearch','test');
        RestRequestobject.addParameter('objectType','Moduli'); 
        
         
        RestContext.request = RestRequestobject;
        RestResponse restResponseObject = new RestResponse(); 
        RestContext.response= restResponseObject;
        
        KBMS_ArticleSearchRestful1.ArticleWrapper artwr= new KBMS_ArticleSearchRestful1.ArticleWrapper();
        
        KBMS_ArticleSearchRestful1.ArticleWrapper art=new KBMS_ArticleSearchRestful1.ArticleWrapper();
        KBMS_ArticleSearchRestful1.getArticleByCategoryIdArticleType();
        
        Test.stopTest(); 
    }
  
    static testMethod void positiveTestContact() {
        List<Sezione_Contatti__c> sez= KBMS_TestDataFactory.createSezioneTestRecords(2);    
       // List<Contact> contacttest= KBMS_TestDataFactory.createContactTestRecords(2);        
        //System.assertEquals(contacttest.size(), 2);
        //system.debug('contacttest::'+contacttest);
        List<Contact> contacttest1= new List<Contact>();
        for(Integer i=0;i<2;i++) {
        Contact con = new Contact();
        
                con.FirstName = 'test';
                con.LastName = 'Test Contact';
                con.Email = 'test1'+i+'@test.net';            
                con.Phone  = '983215789';
                con.MobilePhone = '9865897';
                con.Fax = '12345789';
            
            con.Sezione_Contatti__c=sez[0].id;
            
                contacttest1.add(con);
        }
        insert contacttest1;
       System.AssertEquals(contacttest1[0].Id != null,TRUE);
        
        //Accessing custom Labels
        String defaultDataCategory = Label.KBMS_AppDataCategory;
        String dataCategory=Label.KBMS_DataCategory; 
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String articleType=Label.KBMS_ArticleType;
        String language = Label.KBMS_Language;
        String draft= Label.KBMS_PublishStatus_Draft;
        String limits = '5';
        String froms = '0';
        List<String> searchStringList = new List<String>();
        
        Test.startTest();  
        
        searchStringList.add('info');
        
        KBMS_CustomSearch__c customSearchFaq = new KBMS_CustomSearch__c();        
        
        customSearchFaq.Name='Contact';
        customSearchFaq.Fields__c='FirstName,LastName,Email';
        
        insert customSearchFaq;
        
        KBMS_CustomSearch__c customSearchSezioneContatti = new KBMS_CustomSearch__c();        
        
        customSearchSezioneContatti.Name='Sezione_Contatti__c';
        customSearchSezioneContatti.Fields__c='Nome_Sezione__c';
        
        insert customSearchSezioneContatti;
        
              
        RestRequest restRequestobject = new RestRequest();             
        RestRequestobject.requestURI = '/services/apexrest/getArticleByCategoryId';  
        RestRequestobject.httpMethod = 'GET';  
        RestRequestobject.addParameter('articleType', articleType);
        RestRequestobject.addParameter('limit', '5');
        RestRequestobject.addParameter('from', '0');
        //RestRequestobject.addParameter('categoryId',datacatTest.DataCategoryName);
        RestRequestobject.addParameter('textSearch','');
        RestRequestobject.addParameter('objectType','Contacts'); 
         
        RestContext.request = RestRequestobject;
        RestResponse restResponseObject = new RestResponse(); 
        RestContext.response= restResponseObject;    
       
       // KBMS_ArticleSearchRestful1 n=new  KBMS_ArticleSearchRestful1();
        KBMS_ArticleSearchRestful1.KbmsSearchResult b = KBMS_ArticleSearchRestful1.getArticleByCategoryIdArticleType();
                
       
              
        
        KBMS_ArticleSearchRestful1.ArticleWrapper artwr= new KBMS_ArticleSearchRestful1.ArticleWrapper();

        KBMS_ArticleSearchRestful1.ArticleWrapper art=new KBMS_ArticleSearchRestful1.ArticleWrapper();
        KBMS_ArticleSearchRestful1.getArticleByCategoryIdArticleType();
        
               
        Test.stopTest();
         System.debug('Response : '+RestContext.response);
    } 
 
       static testMethod void negativeTestModuli() 
   {
        
        user usr= KBMS_TestDataFactory.createUser();    
       system.assert(usr.id != NULL);
       
        system.runAs(usr)
        {
            
            Test.startTest();     
            
                  
            RestRequest restRequestobject = new RestRequest();             
            RestRequestobject.requestURI = '/services/apexrest/getArticleSearch1';  
            RestRequestobject.httpMethod = 'GET';  
            RestRequestobject.addParameter('articleType', 'Moduli');
            RestRequestobject.addParameter('limit', '5');
            RestRequestobject.addParameter('from', '0');
            RestRequestobject.addParameter('categoryId','Tutto');
            RestRequestobject.addParameter('textSearch','test');
            RestRequestobject.addParameter('objectType','Moduli'); 
            
             
            RestContext.request = RestRequestobject;
            RestResponse restResponseObject = new RestResponse(); 
            RestContext.response= restResponseObject; 
            
            KBMS_ArticleSearchRestful1.ArticleWrapper artwr= new KBMS_ArticleSearchRestful1.ArticleWrapper();
            KBMS_ArticleSearchRestful1.ArticleWrapper art=new KBMS_ArticleSearchRestful1.ArticleWrapper();
            try
         {
        KBMS_ArticleSearchRestful1.getArticleByCategoryIdArticleType();
        }
        catch (exception e){}
            
            Test.stopTest();
        }
   }
       static testMethod void negativeTestFAQ() 
   {
       
       user usr= KBMS_TestDataFactory.createUser();    
       system.assert(usr.id != NULL);
       
        system.runAs(usr)
        {
       
            Test.startTest();
        
        RestRequest restRequestobject = new RestRequest();             
        RestRequestobject.requestURI = '/services/apexrest/getArticleSearch1';  
        RestRequestobject.httpMethod = 'GET';  
        RestRequestobject.addParameter('articleType', 'FAQ');
        RestRequestobject.addParameter('limit', '5');
        RestRequestobject.addParameter('from', '0');
        RestRequestobject.addParameter('categoryId','Tutto');
        RestRequestobject.addParameter('textSearch','test');
        RestRequestobject.addParameter('objectType','FAQ'); 
         
        RestContext.request = RestRequestobject;
        RestResponse restResponseObject = new RestResponse(); 
        RestContext.response= restResponseObject;

        KBMS_ArticleSearchRestful1.ArticleWrapper artwr= new KBMS_ArticleSearchRestful1.ArticleWrapper();
        
        KBMS_ArticleSearchRestful1.ArticleWrapper art=new KBMS_ArticleSearchRestful1.ArticleWrapper();
        try
         {
        KBMS_ArticleSearchRestful1.getArticleByCategoryIdArticleType();
        }
        catch (exception e){}
        
        Test.stopTest();
        }
   }
      static testMethod void negativeTestProcessi() {
        
        user usr= KBMS_TestDataFactory.createUser();    
       system.assert(usr.id != NULL);
       
        system.runAs(usr)
        {
        
        Test.startTest();
        RestRequest restRequestobject = new RestRequest();             
        RestRequestobject.requestURI = '/services/apexrest/getArticleSearch1';  
        RestRequestobject.httpMethod = 'GET';  
        RestRequestobject.addParameter('articleType', 'Processi');
        RestRequestobject.addParameter('limit', '5');
        RestRequestobject.addParameter('from', '0');
        RestRequestobject.addParameter('categoryId','Tutto');
        RestRequestobject.addParameter('textSearch','Processi');
        RestRequestobject.addParameter('objectType','Processi'); 
         
         
        RestContext.request = RestRequestobject;
        RestResponse restResponseObject = new RestResponse(); 
        RestContext.response= restResponseObject; 
        
         KBMS_ArticleSearchRestful1.ArticleWrapper art=new KBMS_ArticleSearchRestful1.ArticleWrapper();
         try
         {
        KBMS_ArticleSearchRestful1.getArticleByCategoryIdArticleType();
        }
        catch (exception e){}
        Test.stopTest();
      }
    
    }
       static testMethod void negativeTest() {
       user usr= KBMS_TestDataFactory.createUser();    
       system.assert(usr.id != NULL);
       
        system.runAs(usr)
        {
        
        List<Contact> contacttest= KBMS_TestDataFactory.createContactTestRecords(2);        
        System.assertEquals(contacttest.size(), 2);
        system.debug('contacttest::'+contacttest);
        
       // System.Assert(contacttest[0].Id != null, 'The Test FAQ article typed id not inserted properly');
        
        //Accessing custom Labels
        String defaultDataCategory = Label.KBMS_AppDataCategory;
        String dataCategory=Label.KBMS_DataCategory; 
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String articleType=Label.KBMS_ArticleType;
        String language = Label.KBMS_Language;
        String draft= Label.KBMS_PublishStatus_Draft;
        String limits = '5';
        String froms = '0';
        List<String> searchStringList = new List<String>();
        
        Test.startTest();  
        
        searchStringList.add('info');
        
        KBMS_CustomSearch__c customSearchFaq = new KBMS_CustomSearch__c();        
        
        customSearchFaq.Name='Contact';
        customSearchFaq.Fields__c='FirstName,LastName,Email';
        
        insert customSearchFaq;
        
        KBMS_CustomSearch__c customSearchSezioneContatti = new KBMS_CustomSearch__c();        
        
        customSearchSezioneContatti.Name='Sezione_Contatti__c';
        customSearchSezioneContatti.Fields__c='Nome_Sezione__c';
        
        insert customSearchSezioneContatti;
        
              
        RestRequest restRequestobject = new RestRequest();             
        RestRequestobject.requestURI = '/services/apexrest/getArticleSearch1';  
        RestRequestobject.httpMethod = 'GET';  
        RestRequestobject.addParameter('articleType', articleType);
        RestRequestobject.addParameter('limit', '5');
        RestRequestobject.addParameter('from', '0');
        //RestRequestobject.addParameter('categoryId',datacatTest.DataCategoryName);
        RestRequestobject.addParameter('textSearch','test');
        RestRequestobject.addParameter('objectType','Contacts'); 
         
        RestContext.request = RestRequestobject;
        RestResponse restResponseObject = new RestResponse(); 
        RestContext.response= restResponseObject;    
        
        KBMS_ArticleSearchRestful1.ArticleWrapper artwr= new KBMS_ArticleSearchRestful1.ArticleWrapper();

        KBMS_ArticleSearchRestful1.ArticleWrapper art=new KBMS_ArticleSearchRestful1.ArticleWrapper();
        KBMS_ArticleSearchRestful1.getArticleByCategoryIdArticleType();
        
        Test.stopTest();
        } 
   }
  
}