@isTest() 
public class KBMS_ArticlesDetailsRestFulTest {  
    static testMethod void testMethod_PositiveVote() {
      
        String language = Label.KBMS_Language;
        
        List<FAQ__kav> faqtest= KBMS_TestDataFactory.createFAQTestRecords(2);
        String defaultDataCategory = Label.KBMS_AppDataCategory;
        System.assertEquals(faqtest.size(),2);
        
        
        List<Moduli__kav> Modulitest= KBMS_TestDataFactory.createModuliTestRecords(2); 
        System.assertEquals(Modulitest.size(), 2);
        
        for(Integer i=0;i<Modulitest.size(); i++){
                Vote v = new Vote();
                v.Type = 'Up';
                Id insertedTestArticleKDId = [SELECT KnowledgeArticleId FROM Moduli__kav WHERE ID = :Modulitest[i].Id].KnowledgeArticleId;
                v.ParentId = insertedTestArticleKDId;
                insert v;
                System.assert(v.id!=null,'The Test Moduli Article did not insert properly');
                System.assertEquals(insertedTestArticleKDId,v.ParentId);
                System.assertEquals('Up',v.Type);
            }
        
        String articleType=Label.KBMS_ArticleType;
        System.Assert(faqtest[0].Id != null, 'The Test FAQ article typed id not inserted properly');       
        System.Assert(Modulitest[0].Id != null, 'The Test FAQ article typed id not inserted properly');  
        FAQ__kav insertedTestArticle = [SELECT id,KnowledgeArticleId FROM FAQ__kav WHERE ID = :faqtest[0].Id];
        Moduli__kav Moduli_insertedTestArticle = [SELECT Id,KnowledgeArticleId FROM Moduli__kav WHERE ID = :Modulitest[0].Id];
    
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId, true);
        KbManagement.PublishingService.publishArticle(Moduli_insertedTestArticle.KnowledgeArticleId,true);
        
        System.Assert(insertedTestArticle.Id != NULL, 'The insertedTestArticle Not inserted properly');
         System.Assert(Moduli_insertedTestArticle.Id != NULL, 'The insertedTestArticle Not inserted properly');
        RestRequest restRequestobject = new RestRequest();             
        RestRequestobject.requestURI = '/services/apexrest/getArticleDetails';  
        RestRequestobject.httpMethod = 'GET';  
        RestRequestobject.addParameter('articleId', Moduli_insertedTestArticle.KnowledgeArticleId);
        RestRequestobject.addParameter('articleType', 'Moduli');
        
        
        RestContext.request = RestRequestobject; 
        RestResponse restResponseObject = new RestResponse(); 
        RestContext.response= restResponseObject;
        
        Map<Id,Integer> mapTotalUpVote=new Map<Id,Integer>();
        Map<Id,Integer> mapTotalDownVote=new Map<Id,Integer>(); 
        
        List<KnowledgeArticleVersion> knowledgeArticle = new List<KnowledgeArticleVersion>();   
        
        knowledgeArticle = [SELECT Id FROM KnowledgeArticleVersion WHERE KnowledgeArticleId =:Moduli_insertedTestArticle.KnowledgeArticleId AND PublishStatus = 'Online'];
         
		System.debug('knowledgeArticle' +knowledgeArticle.size());    
        
        Test.startTest(); 
        
        KBMS_ArticlesDetailsRestFul.ArticleWrapper art= new KBMS_ArticlesDetailsRestFul.ArticleWrapper();

        KBMS_ArticlesDetailsRestFul.getArticleDetails();
       
        Test.stopTest();
        
    }
    
    static testMethod void testMethod_NegativeVote() {
      
        String language = Label.KBMS_Language;
        
        List<FAQ__kav> faqtest= KBMS_TestDataFactory.createFAQTestRecords(2);
        String defaultDataCategory = Label.KBMS_AppDataCategory;
        System.assertEquals(faqtest.size(),2);
        
        
        List<Moduli__kav> Modulitest= KBMS_TestDataFactory.createModuliTestRecords(2); 
        System.assertEquals(Modulitest.size(), 2);
        
        for(Integer i=0;i<Modulitest.size(); i++){
                Vote v = new Vote();
                v.Type = 'Down';
                Id insertedTestArticleKDId = [SELECT KnowledgeArticleId FROM Moduli__kav WHERE ID = :Modulitest[i].Id].KnowledgeArticleId;
                v.ParentId = insertedTestArticleKDId;
                insert v;
                System.assert(v.id!=null,'The Test Moduli Article did not insert properly');
                System.assertEquals(insertedTestArticleKDId,v.ParentId);
                System.assertEquals('Down',v.Type);
            }
        
        String articleType=Label.KBMS_ArticleType;
        System.Assert(faqtest[0].Id != null, 'The Test FAQ article typed id not inserted properly');       
         System.Assert(Modulitest[0].Id != null, 'The Test FAQ article typed id not inserted properly');  
        FAQ__kav insertedTestArticle = [SELECT id,KnowledgeArticleId FROM FAQ__kav WHERE ID = :faqtest[0].Id];
        Moduli__kav Moduli_insertedTestArticle = [SELECT Id,KnowledgeArticleId FROM Moduli__kav WHERE ID = :Modulitest[0].Id];
    
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId, true);
        KbManagement.PublishingService.publishArticle(Moduli_insertedTestArticle.KnowledgeArticleId,true);
        
        System.Assert(insertedTestArticle.Id != NULL, 'The insertedTestArticle Not inserted properly');
         System.Assert(Moduli_insertedTestArticle.Id != NULL, 'The insertedTestArticle Not inserted properly');
        RestRequest restRequestobject = new RestRequest();             
        RestRequestobject.requestURI = '/services/apexrest/getArticleDetails';  
        RestRequestobject.httpMethod = 'GET';  
        RestRequestobject.addParameter('articleId', Moduli_insertedTestArticle.KnowledgeArticleId);
        RestRequestobject.addParameter('articleType', 'Moduli');
        
        
        RestContext.request = RestRequestobject; 
        RestResponse restResponseObject = new RestResponse(); 
        RestContext.response= restResponseObject;
        
        Map<Id,Integer> mapTotalUpVote=new Map<Id,Integer>();
        Map<Id,Integer> mapTotalDownVote=new Map<Id,Integer>(); 
        
        List<KnowledgeArticleVersion> knowledgeArticle = new List<KnowledgeArticleVersion>();   
        
        knowledgeArticle = [SELECT Id FROM KnowledgeArticleVersion WHERE KnowledgeArticleId =:Moduli_insertedTestArticle.KnowledgeArticleId AND PublishStatus = 'Online'];
         
		System.debug('knowledgeArticle' +knowledgeArticle.size());        	
       	//System.asserteQUALS(knowledgeArticle.size(),1);
        
        
     	//String kavId = knowledgeArticle.Id;
        
		//String query = 'SELECT Summary, Title, UrlName, Id, KnowledgeArticleId, LastModifiedDate FROM Moduli__kav WHERE publishStatus=\'online\' and Language =:language and Id =: kavId';
        //sObject res = Database.query(query);
        
        //System.debug('***res query****' +res);  
        
      	
        
        Test.startTest(); 
        
        KBMS_ArticlesDetailsRestFul.ArticleWrapper art= new KBMS_ArticlesDetailsRestFul.ArticleWrapper();

        KBMS_ArticlesDetailsRestFul.getArticleDetails();
        
       
       
        Test.stopTest();
        
    }
    
  
}