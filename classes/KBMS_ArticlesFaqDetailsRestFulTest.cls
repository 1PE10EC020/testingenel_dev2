/**
* @author Atos
* @date  Sept,2016
* @description This class is test class of KBMS_ArticlesFaqDetailsRestFul
*/

@isTest()
private class KBMS_ArticlesFaqDetailsRestFulTest{
    
    static testMethod void testMethod1() {
      
        List<FAQ__kav> faqtest= KBMS_TestDataFactory.createFAQTestRecords(2);
        String defaultDataCategory = Label.KBMS_AppDataCategory;
        System.assertEquals(faqtest.size(), 2);
        String articleType=Label.KBMS_ArticleType;
         System.Assert(faqtest[0].Id != null, 'The Test FAQ article typed id not inserted properly');       
        
        FAQ__kav insertedTestArticle = [SELECT id,KnowledgeArticleId FROM FAQ__kav WHERE ID = :faqtest[0].Id];
        
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId, true);
         
        System.Assert(insertedTestArticle.Id != NULL, 'The insertedTestArticle Not inserted properly');
        
        RestRequest restRequestobject = new RestRequest();             
        RestRequestobject.requestURI = '/services/apexrest/getFaqDetailId';  
        RestRequestobject.httpMethod = 'GET';  
        RestRequestobject.addParameter('articleId', insertedTestArticle.id);
        RestContext.request = RestRequestobject;
        RestResponse restResponseObject = new RestResponse(); 
        RestContext.response= restResponseObject;
        
        Test.startTest();
        KBMS_ArticlesFaqDetailsRestFul.getCategoryList(); 
        Test.stopTest();
        
    }
    
    
    
}