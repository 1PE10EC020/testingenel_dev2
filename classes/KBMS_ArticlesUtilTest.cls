/*
 * @author Atos India Pvt Ltd.
 * @date  Sept,2016
 * @description This class is test class of KBMS_ArticlesUtil
 */
@isTest(seeAllData = true)
public class KBMS_ArticlesUtilTest{  

static testMethod void testMethod_Title(){        
        
        List<FAQ__kav> lstFaq= KBMS_TestDataFactory.createFAQTestRecords(2); 
        System.assertEquals(lstFaq.size(), 2);
        String lang= Label.KBMS_Language;    
        String draft= Label.KBMS_PublishStatus_Draft;    
        
        for(Integer i=0;i<lstFaq.size(); i++){
                Vote v = new Vote();
                v.Type = 'Up';
                Id insertedTestArticleKDId = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE ID = :lstFaq[i].Id].KnowledgeArticleId;
                v.ParentId = insertedTestArticleKDId;
                insert v;
            }
        
        system.debug('values are' +lstFaq);
    	Set<Id> articleKAVIds = new Set<Id>();
    	articleKAVIds.add(lstFaq[0].Id);
        articleKAVIds.add(lstFaq[1].Id);
        //List<FAQ__kav> faqdata = [SELECT Id,KnowledgeArticleId,title,summary,UrlName,ArticleType FROM FAQ__kav WHERE PublishStatus = 'DRAFT' AND Language ='it' LIMIT 4];
        List<FAQ__kav> faqdata = Database.query('SELECT Id,KnowledgeArticleId,title,summary,UrlName,ArticleType FROM FAQ__kav WHERE PublishStatus =:draft AND Language =:lang AND Id In: articleKAVIds ');
        system.debug('values are' +faqdata);
        system.debug('size' +faqdata.size()); 
        
        String dataCategory=Label.KBMS_DataCategory; 
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String articleType=Label.KBMS_ArticleType;
        
        FAQ__DataCategorySelection datacatTest=new FAQ__DataCategorySelection();
        
        datacatTest.ParentId= faqdata[0].Id;     
        datacatTest.DataCategoryName='Connessione';
        datacatTest.DataCategoryGroupName=GroupDataCategory;
        
        insert datacatTest;
    
     	FAQ__DataCategorySelection datacatTest1=new FAQ__DataCategorySelection();
        datacatTest1.ParentId= faqdata[0].Id;     
        datacatTest1.DataCategoryName='OT';
        datacatTest1.DataCategoryGroupName='KBMS';  
        insert datacatTest1; 
    
    	FAQ__kav insertedTestArticle = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE ID = :faqdata[0].Id];
        
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId, true); 
        
         
       // String articleType = Label.KBMS_ArticleType;        
        String srcArticleId = null; 
        String dstArticleId = faqdata[1].Id;     
        system.debug('articleType is' +articleType); 
        //KBMS_ArticlesUtil.cloneArticle(articleType, srcArticleId, dstArticleId);
        
        articleType = Label.KBMS_ArticleType;       
        srcArticleId = faqdata[0].Id; 
        dstArticleId = '';     
        system.debug('articleType is' +articleType); 
        
        Test.startTest();
        KBMS_ArticlesUtil.ArtcileWrapper art= new KBMS_ArticlesUtil.ArtcileWrapper();
     	
    
    	KBMS_ArticlesUtil.getArticlesList(articleType,'5','0','TestFAQ test','Tutto__c','Title'); 
		
		KBMS_ArticlesUtil.getArticlesList(articleType,'5','0','TestFAQ','Connessione__c','Title');  
    
    	KBMS_ArticlesUtil.getArticlesList(articleType,'5','0','TestFAQ test','Tutto__c','LastModifiedDate'); 
		
		KBMS_ArticlesUtil.getArticlesList(articleType,'5','0','TestFAQ','Connessione__c','LastModifiedDate');  
    
    	KBMS_ArticlesUtil.getArticlesList(articleType,'5','0','TestFAQ test','Tutto__c','View'); 
		
		KBMS_ArticlesUtil.getArticlesList(articleType,'5','0','TestFAQ','Connessione__c','View'); 
    
    	KBMS_ArticlesUtil.getArticlesList(articleType,'5','0','TestFAQ test','Tutto__c','Like'); 
		
		KBMS_ArticlesUtil.getArticlesList(articleType,'5','0','TestFAQ','Connessione__c','Like'); 
        //KBMS_ArticlesUtil.dataCategories data=new KBMS_ArticlesUtil.dataCategories();
       
    	/*
        data.categoryName = 'test_datacategory'; 
        art.dataCategoryLabel='fghfgh';  
        art.parentName='fghgfhgh';
        art.likeScore=4.0;
        art.viewCount=3; 
        art.parentLabel='test';
        art.dataCategoryName='DataCategoryName';
    
    	KBMS_ArticlesUtil.getArticlesList(articleType,'5','0','TestFAQ',dataCategory+'__c','Title');
		
        data.categoryName = 'test_datacategory'; 
        art.dataCategoryLabel='fghfgh';  
        art.parentName='fghgfhgh';
        art.likeScore=4.0;
        art.viewCount=3; 
        art.parentLabel='test';
        art.dataCategoryName='DataCategoryName';
    
        KBMS_ArticlesUtil.getArticlesList(articleType,'5','0','TestFAQ',dataCategory+'__c','LastModifiedDate');
        data.categoryName = 'test_datacategory'; 
        art.dataCategoryLabel='fghfgh';  
        art.parentName='fghgfhgh';
        art.likeScore=4.0;
        art.viewCount=3; 
        art.parentLabel='test';
        art.dataCategoryName='DataCategoryName'; 
    
    	KBMS_ArticlesUtil.getArticlesList(articleType,'5','0','searchtext test',dataCategory+'__c','LastModifiedDate');
        data.categoryName = 'test_datacategory'; 
        art.dataCategoryLabel='fghfgh';  
        art.parentName='fghgfhgh';
        art.likeScore=4.0;
        art.viewCount=3; 
        art.parentLabel='test';
        art.dataCategoryName='DataCategoryName';
        
        KBMS_ArticlesUtil.getArticlesList(articleType,'5','0','searchtext',dataCategory+'__c','Like');
        data.categoryName = 'test_datacategory'; 
        art.dataCategoryLabel='fghfgh';  
        art.parentName='fghgfhgh';
        art.likeScore=4.0;
        art.viewCount=3; 
        art.parentLabel='test';
        art.dataCategoryName='DataCategoryName';
    
    	KBMS_ArticlesUtil.getArticlesList(articleType,'5','0','searchtext test',dataCategory+'__c','Like');
        data.categoryName = 'test_datacategory'; 
        art.dataCategoryLabel='fghfgh';  
        art.parentName='fghgfhgh';
        art.likeScore=4.0;
        art.viewCount=3; 
        art.parentLabel='test';
        art.dataCategoryName='DataCategoryName';
                
        KBMS_ArticlesUtil.getArticlesList(articleType,'5','0','test',dataCategory+'__c','View');
        data.categoryName = 'test_datacategory'; 
        art.dataCategoryLabel='fghfgh';  
        art.parentName='fghgfhgh';
        art.likeScore=4.0;
        art.viewCount=3; 
        art.parentLabel='test';
        art.dataCategoryName='DataCategoryName';
    
    	KBMS_ArticlesUtil.getArticlesList(articleType,'5','0','searchtext test',dataCategory+'__c','View');
        data.categoryName = 'test_datacategory'; 
        art.dataCategoryLabel='fghfgh';  
        art.parentName='fghgfhgh';
        art.likeScore=4.0;
        art.viewCount=3; 
        art.parentLabel='test';
        art.dataCategoryName='DataCategoryName';
    */
        Test.stopTest();
        
        
        
    }
    
    

    
}