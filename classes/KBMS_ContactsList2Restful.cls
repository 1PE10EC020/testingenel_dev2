@RestResource(urlMapping='/getContactsList2/*')
global with sharing class KBMS_ContactsList2Restful
{
    @HttpGet
    global static List<ApplicantWrapper> getContactList()
    {
        List<SectionWrapper> sectionWrappersClients = new List<SectionWrapper>();
        List<SectionWrapper> sectionWrappersManufacturers = new List<SectionWrapper>();
        
        String firstPartQuery = 'SELECT Name,Nome_Sezione__c,Tipologia_Richiedente__c ';
        //String secondPartQuery = 'FROM Sezione_Contatti__c WHERE IsDeleted = false ORDER BY Id,Nome_Sezione__c';
        String secondPartQuery = 'FROM Sezione_Contatti__c WHERE IsDeleted = false ORDER BY Priority__c';
        String query = firstPartQuery + secondPartQuery;
        system.debug(query);
        List<sObject> sectionObjects = Database.query(query);
        
        
        
        for (sObject sObjSect  : sectionObjects)
        {
            SectionWrapper sectionWrapper = new SectionWrapper();
            sectionWrapper.sectionId = String.valueOf(sObjSect.get('Name'));
            sectionWrapper.sectionName = String.valueOf(sObjSect.get('Nome_Sezione__c'));
            sectionWrapper.sectionApplicant = String.valueOf(sObjSect.get('Tipologia_Richiedente__c'));
            
            
            String firstPartNestedQuery = 'SELECT Id,Name,FirstName,LastName,Phone,MobilePhone,Email,Fax,Zona__c,MailingStreet,MailingCity,MailingCountry,MailingPostalCode,MailingState,Sezione_Contatti__r.Name,Sezione_Contatti__r.Nome_Sezione__c,Sito__c,App__c ';
            String secondPartNestedQuery = 'FROM Contact WHERE Sezione_Contatti__r.Name = \''+sectionWrapper.sectionId+'\' AND Sezione_Contatti__c != null AND IsDeleted = false AND Name != null ORDER BY Sezione_Contatti__r.Nome_Sezione__c,Name';
        
            String nestedQuery = firstPartNestedQuery + secondPartNestedQuery;
            system.debug(nestedQuery);
            List<sObject> contactObjects = Database.query(nestedQuery);
            List<ContactWrapper> contactsTmp = new List<ContactWrapper>();
            
            for (sObject sObjContact  : contactObjects)
            {
                ContactWrapper contact = new ContactWrapper();
                contact.id = String.valueOf(sObjContact.get('Id'));
                contact.name = String.valueOf(sObjContact.get('Name'));
                contact.firstname = String.valueOf(sObjContact.get('FirstName'));
                contact.lastname = String.valueOf(sObjContact.get('LastName'));
                contact.phone = String.valueOf(sObjContact.get('Phone'));
                contact.mobilephone = String.valueOf(sObjContact.get('MobilePhone'));
                contact.email = String.valueOf(sObjContact.get('Email'));
                contact.fax = String.valueOf(sObjContact.get('Fax'));
                contact.mailingstreet = String.valueOf(sObjContact.get('MailingStreet'));
                contact.mailingcity = String.valueOf(sObjContact.get('MailingCity'));
                contact.mailingcountry = String.valueOf(sObjContact.get('MailingCountry'));
                contact.mailingpostalcode = String.valueOf(sObjContact.get('MailingPostalCode'));
                contact.mailingstate = String.valueOf(sObjContact.get('MailingState'));
                contact.site = String.valueOf(sObjContact.get('Sito__c'));
                contact.app = String.valueOf(sObjContact.get('App__c'));
                contact.Zona= String.valueOf(sObjContact.get('Zona__c'));
                
                
                
                contactsTmp.add(contact);
            }
            
            sectionWrapper.contacts = contactsTmp;
            if(sectionWrapper.sectionApplicant != null && sectionWrapper.sectionApplicant.contains('Clienti')){
                sectionWrappersClients.add(sectionWrapper);
            }
            if(sectionWrapper.sectionApplicant !=null && sectionWrapper.sectionApplicant.contains('Produttori')){
                sectionWrappersManufacturers.add(sectionWrapper);
            }
                
        
        }
        
        
        List<ApplicantWrapper> aws = new List<ApplicantWrapper>();
        ApplicantWrapper awClients = new ApplicantWrapper();
        awClients.sections = sectionWrappersClients;
        awClients.applicantName='Clienti';
        aws.add(awClients);
        
        ApplicantWrapper awManufacturers = new ApplicantWrapper();
        awManufacturers.sections = sectionWrappersManufacturers;
        awManufacturers.applicantName='Produttori';
        aws.add(awManufacturers);
        
        
        return aws;
    }
    
    global class ApplicantWrapper
    {
        public List<SectionWrapper> sections {get;set;}
        public String applicantName{get;set;}
    } 
    
    global class SectionWrapper
    {
        public String sectionId {get;set;}
        public String sectionName{get;set;}
        public String sectionApplicant{get;set;}
        public List<ContactWrapper> contacts{get;set;}
    } 
    
    private class ContactWrapper
    {
        public String id {get;set;}
        public String name {get;set;}
        public String firstname {get;set;}
        public String lastname {get;set;}
        public String phone {get;set;}
        public String mobilephone {get;set;}
        public String email {get;set;}
        public String fax {get;set;}
        public String mailingstreet {get;set;}
        public String mailingcity {get;set;}
        public String mailingcountry {get;set;}
        public String mailingpostalcode {get;set;}
        public String mailingstate {get;set;}
        public String site {get;set;}
        public String app {get;set;}
        public String Zona{set;get;}
    } 
}