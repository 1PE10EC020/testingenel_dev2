/**
* @author Atos India Pvt Ltd.
* @date  Sept,2016
* @description This class is test class of KBMS_DataCategoryListArticleCount
*/

@isTest() 
private class KBMS_DataCategoryListArticleCountTest{
        
    static testMethod void testMethod1() {
        
        List<FAQ__kav> faqtest= KBMS_TestDataFactory.createFAQTestRecords(2);
        String defaultDataCategory = Label.KBMS_AppDataCategory;
        System.assertEquals(faqtest.size(), 2);
        
        //Accessing custom Labels
        String dataCategory=Label.KBMS_DataCategory; 
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String articleType=Label.KBMS_ArticleType;
        
        FAQ__DataCategorySelection datacatTest=new FAQ__DataCategorySelection();
        
        datacatTest.ParentId= faqtest[0].Id;     
        datacatTest.DataCategoryName=dataCategory;
        datacatTest.DataCategoryGroupName=GroupDataCategory;
        
        insert datacatTest;  
        FAQ__kav insertedTestArticle = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE ID = :faqtest[0].Id];
        
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId, true); 
        
        
        RestRequest restRequestobject = new RestRequest();             
        RestRequestobject.requestURI = '/services/apexrest/getCategoryListArticleCount';  
        RestRequestobject.httpMethod = 'GET';  
        RestRequestobject.addParameter('articleType', articleType);
        RestRequestobject.addParameter('limit', '5');
        RestRequestobject.addParameter('from', '0');
        RestRequestobject.addParameter('categoryId',datacatTest.DataCategoryName);
        
        RestContext.request = RestRequestobject;
        RestResponse restResponseObject = new RestResponse(); 
        RestContext.response= restResponseObject;
        
        Test.startTest();
        
      /*  KBMS_ArticleByCategoryIdRestful.ArtcileWrapper art=new KBMS_ArticleByCategoryIdRestful.ArtcileWrapper();
        art.dataCategoryLabel='fghfgh';  
        art.parentName='fghgfhgh';
       
        art.parentLabel=RestContext.request.params.get('ParentId');
        art.dataCategoryName=RestContext.request.params.get('DataCategoryName');*/
        
        KBMS_DataCategoryListArticleCount.doGet();
       
        
        Test.stopTest();
        
    }
    
}