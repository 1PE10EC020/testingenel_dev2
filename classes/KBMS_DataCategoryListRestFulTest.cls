/**
 * @author Atos India Pvt Ltd.
 * @date  Sept,2016
 * @description This class is test class of KBMS_DataCategoryListRestFul
 */

@isTest()
private class  KBMS_DataCategoryListRestFulTest {

    static testMethod void testMethod1() {

        RestRequest restRequestobject = new RestRequest();             
        RestRequestobject.requestURI = '/services/apexrest/getCategoryList';  
        RestRequestobject.httpMethod = 'GET';              
        RestContext.request = RestRequestobject;
        RestResponse restResponseObject = new RestResponse(); 
        RestContext.response= restResponseObject;
        Test.startTest();

        KBMS_DataCategoryListRestFul.doGet();
        
        Test.stopTest();

       
    }
   
    
 }