public with sharing class  KBMS_ModuliController
{
   @AuraEnabled    
   public static List<sObject> findAll() 
   {
       List<sObject> moduliList = new  List<sObject>(); 
      
       String lang = Label.KBMS_Language;
       System.debug('lang is: '+lang );
       
       String query = 'SELECT ArticleNumber, Id, LastModifiedDate, LastPublishedDate, Summary, Title, UrlName ' +  
                     ' FROM Moduli__kav ' +  
                     ' WHERE PublishStatus = \'online\' AND Language =:lang  ORDER BY LastPublishedDate DESC NULLS FIRST';
        
       moduliList = database.query(query);              
       return  moduliList;
    } 
}