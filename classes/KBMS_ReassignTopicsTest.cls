@IsTest(SeeAllData=true)
public class KBMS_ReassignTopicsTest{
    Static testMethod void testWithDataCategory(){
    
         FAQ__kav faqtst = new FAQ__kav();
            faqtst.Title = 'TestFAQ';
            faqtst.Summary = 'KB Summary ';
            faqtst.URLName = 'TestFAQ';                
            faqtst.QUESITO__c='testno';
            faqtst.RISPOSTA__c='testno';
            faqtst.Language= Label.KBMS_Language;
        insert faqtst;       
        
        String dataCategory = Label.KBMS_DataCategory;
        String GroupDataCategory=Label.KBMS_GroupDataCategory;
        String selectedArticle=Label.KBMS_ArticleType;
        
        FAQ__DataCategorySelection datacatTest=new FAQ__DataCategorySelection();
        
        datacatTest.ParentId= faqtst.Id;     
        datacatTest.DataCategoryName=dataCategory;
        datacatTest.DataCategoryGroupName=GroupDataCategory;
        
        insert datacatTest;
      
        FAQ__kav insertedTestArticle = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE ID = :faqtst.Id];
        
        
        
        KbManagement.PublishingService.publishArticle(insertedTestArticle.KnowledgeArticleId,true);
        PageReference pageRef = new PageReference('/'+faqtst.Id);
        Test.setCurrentPage(pageRef);
        
        
        Processi__kav Processitest = new Processi__kav();
            Processitest.Title = 'TestFAQ';
            Processitest.Summary = 'KB Summary ';
            Processitest.URLName = 'TestProcessi';   
            
            Processitest.Language=Label.KBMS_Language;
        insert Processitest;       
        
        Processi__DataCategorySelection datacatTest1=new Processi__DataCategorySelection();
        
        datacatTest1.ParentId= Processitest.Id;     
        datacatTest1.DataCategoryName=dataCategory;
        datacatTest1.DataCategoryGroupName=GroupDataCategory;
        
        insert datacatTest1;
      
        Processi__kav insertedprocessiArticle = [SELECT KnowledgeArticleId FROM Processi__kav WHERE ID = :Processitest.Id];
                
        
        KbManagement.PublishingService.publishArticle(insertedprocessiArticle.KnowledgeArticleId,true);
        PageReference pageRef1 = new PageReference('/'+Processitest.Id);
        Test.setCurrentPage(pageRef1);    
        
        
        List<FAQ__DataCategorySelection> dataCategorySelections = new list<FAQ__DataCategorySelection>();
        dataCategorySelections.add(datacatTest);
        
        List<Processi__DataCategorySelection> processi_dataCategorySelections = new list<Processi__DataCategorySelection>();
        processi_dataCategorySelections.add(datacatTest1);
                
        List<String> topicNamesList = new List<String>();
        topicNamesList.add('KBMS_Mercato_elettrico');
        topicNamesList.add('KBMS Mercato elettrico');
        topicNamesList.add('KBMS_Operai_Tecnici');
        topicNamesList.add('KBMS Operai Tecnici');
        ConnectApi.TopicNamesInput tpi = new ConnectApi.TopicNamesInput();
        tpi.topicNames =topicNamesList;
        
             
        
        //ConnectApi.Topics.reassignTopicsByName('0DB410000004sAk', faqtst.Id,tpi);
        Test.startTest();
        KBMS_ReassignTopics.reassignTopics(processi_dataCategorySelections);
        KBMS_ReassignTopics.reassignTopics(dataCategorySelections);
        
        //KBMS_ReassignTopics.DeleteTopics(articlewithNoDataCategory);
        
        Test.stopTest();
          
        
    }

}