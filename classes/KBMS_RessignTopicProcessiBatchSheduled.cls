global class KBMS_RessignTopicProcessiBatchSheduled implements Schedulable, Database.Batchable<sObject>{
  //Execute menthod of Schedulable interface
    global void execute(SchedulableContext scMain) 
    {
         KBMS_RessignTopicProcessiBatchSheduled b = new KBMS_RessignTopicProcessiBatchSheduled();
         database.executebatch(b,1);

    }
    
    //Start Method
    global Database.Querylocator start (Database.BatchableContext BC) 
    {
        //Query which will be determine the scope of Records fetching the same
        String communityLanguage =Label.KBMS_Language;
        String communityPublishStatus = Label.KBMS_PublishStatus_Online;
        String query='SELECT ArticleNumber, ArticleType, Id, Language, OwnerId, PublishStatus, Summary, Title, UrlName, (SELECT DataCategoryName,ParentId,DataCategoryGroupName,Id from  DataCategorySelections) FROM Processi__kav WHERE PublishStatus =:communityPublishStatus AND Language =:communityLanguage';
        if(Test.isRunningTest())
         {
                query += ' Limit 1';
         }
return Database.getQueryLocator(query);
        //return Database.getQueryLocator('SELECT ArticleNumber, ArticleType, Id, Language, OwnerId, PublishStatus, Summary, Title, UrlName, (SELECT DataCategoryName,ParentId,DataCategoryGroupName,Id from  DataCategorySelections) FROM Processi__kav WHERE PublishStatus =:communityPublishStatus AND Language =:communityLanguage');
    }
    
    //Execute method
    global void execute (Database.BatchableContext BC,List<Processi__kav> articleList) 
    {
        List<Processi__DataCategorySelection> dataCategorySelections = new List<Processi__DataCategorySelection>();
        Set<Id> articlewithNoDataCategory = new Set<Id>();
        for(Processi__kav  articleRecord : articleList)
        {
            if(articleRecord.DataCategorySelections.size() == 0)
            {
                articlewithNoDataCategory.add(articleRecord.id);
            }
            for(Processi__DataCategorySelection categoryRecord : articleRecord.DataCategorySelections)
            {
                dataCategorySelections.add(categoryRecord);
            }
            
         }  
          
        KBMS_ReassignTopics.reassignTopics(dataCategorySelections);
        KBMS_ReassignTopics.DeleteTopics(articlewithNoDataCategory); 
    }
    
    //Finish Method
    global void finish(Database.BatchableContext BC)
    {
    
    }
}