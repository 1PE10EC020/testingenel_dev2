// This is the test class of  KBMS_SezioniContattiController class
@isTest(seeAllData = false)
public class KBMS_SezioniContattiControllerTest
{

     static testmethod void SezioniTest1()
     {
         List<Sezione_Contatti__c> lstsezioni = new List<Sezione_Contatti__c>();
         Sezione_Contatti__c sezioniobj = new Sezione_Contatti__c();
        
         sezioniobj.Nome_Sezione__c = 'Richiesta di informazioni';
         sezioniobj.Tipologia_Richiedente__c = 'Produttori';
         sezioniobj.Priority__c= 2;
                          
         lstsezioni.add(sezioniobj);    
         insert lstsezioni;
         system.debug('listsz::'+lstsezioni);
         String selectedValue= 'Tutto';
         system.debug('selected val::'+selectedValue);
         
         List<contact> lstcontact = new List<contact>();
         Contact conobj = new Contact();
        
         conobj.FirstName= 'Atos_test222';
         conobj.LastName= '32';
         conobj.MailingCity = 'Boston';
         conobj.MailingStreet = 'Road-1';
         conobj.MailingState = 'TYC';
         conobj.MailingPostalCode = '';
         conobj.MailingCountry = 'cou';
         conobj.Sito__c = '';
         conobj.App__c = '';
         conobj.Zona__c ='';
         conobj.Phone = '';
         conobj.MobilePhone = '';
         conobj.Email = '';
         conobj.Fax ='';
         conobj.Sezione_Contatti__c = lstsezioni[0].id;
                          
         lstcontact.add(conobj);    
         insert lstcontact ;
         Id scId = lstsezioni[0].id;
         Test.startTest();
         KBMS_SezioniContattiController cont = new KBMS_SezioniContattiController();
         KBMS_SezioniContattiController.getSezioneContatti(selectedValue);
         KBMS_SezioniContattiController.findContact(scId);
         Test.stopTest();
     }
     static testmethod void SezioniTest2()
     {
         List<Sezione_Contatti__c> lstsezioni = new List<Sezione_Contatti__c>();
         Sezione_Contatti__c sezioniobj = new Sezione_Contatti__c();         
         sezioniobj.Nome_Sezione__c = 'Invio Comunicazione Fine Opere';
         sezioniobj.Tipologia_Richiedente__c = 'Clienti';
         sezioniobj.Priority__c= 2;
                 
         lstsezioni.add(sezioniobj);    
         insert lstsezioni;
         system.debug('listsz::'+lstsezioni);
         String selectedValue= 'Clienti';
         system.debug('selected val::'+selectedValue);
          
         Test.startTest();
         KBMS_SezioniContattiController cont = new KBMS_SezioniContattiController();
         KBMS_SezioniContattiController.getSezioneContatti(selectedValue);
         Test.stopTest();
     }

}