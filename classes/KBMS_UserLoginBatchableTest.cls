/**
* @author Atos India Pvt Ltd.
* @date  Nov,2016
* @description This class is to Test class of KBMS_UserLoginBatchable class.
*/
@isTest()
public class  KBMS_UserLoginBatchableTest
{ 
    public Static testMethod void testUserLoginHistoryScheduled()
    {  
        Test.startTest();
        KBMS_UserLoginBatchable  b = new KBMS_UserLoginBatchable();
        database.executebatch(b); 
        Test.stopTest();
        
        List<KBMS_UserLogin__c> userLogins = [SELECT Id FROM KBMS_UserLogin__c];
        
        System.assert(userLogins.size() > 0); 
     }
}