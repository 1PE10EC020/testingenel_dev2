/**
* @author Atos India Pvt Ltd.
* @date  Nov,2016
* @description This class is to Test class of KBMS_UserLoginDeleteBatchableSheduled class.
*/
@isTest 
public class KBMS_UserLoginDeleteBatchSheduledTest {
    public Static testMethod void testUserLoginHistoryScheduled()
    {
            Test.startTest();
        SchedulableContext SC;
        Database.BatchableContext  bc;
        User usr=[Select id ,name,profile.name from User where profile.name='Operaio' AND isActive = true LIMIT 1];
        system.runAs(usr){
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        List<KBMS_UserLogin__c> loginHistoryList = new List<KBMS_UserLogin__c>();
       for(integer i=0;i<10;i++){
                    KBMS_UserLogin__c ul=new KBMS_UserLogin__c();
                    ul.ApiType__c='';
                    ul.Application__c='Browser';
                    ul.Browser__c='Chrome 54';
                    ul.LoginTime__c=date.today();
                    ul.LoginType__c='Remote Access 2.0';
                    ul.LoginUrl__c='test.salesforce.com';
                    ul.NetworkId__c='';
                    ul.Platform__c='Windows 10';
                    ul.Profile__c='Operaio';                    
                    ul.User_Name__c=usr.id ;
                   
            
            loginHistoryList.add(ul);
            }
        
        insert loginHistoryList;
        
       
        KBMS_UserLoginDeleteBatchableSheduled b = new KBMS_UserLoginDeleteBatchableSheduled();
      Database.executeBatch(b);
        }
        //String jobId = System.schedule('ScheduleApexClassTest', CRON_EXP, new KBMS_UserLoginDeleteBatchableSheduled());
        
        Test.stopTest();
    }  
    
    public Static testMethod void testUserLoginHistoryScheduled1()
    {
           
        SchedulableContext SC;
        Database.BatchableContext  bc;
        User usr=[Select id ,name,profile.name from User where profile.name='Operaio' AND isActive = true LIMIT 1];
        
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        List<KBMS_UserLogin__c> loginHistoryList = new List<KBMS_UserLogin__c>();
       for(integer i=0;i<10;i++){
                    KBMS_UserLogin__c ul=new KBMS_UserLogin__c();
                    ul.ApiType__c='';
                    ul.Application__c='Browser';
                    ul.Browser__c='Chrome 54';
                    ul.LoginTime__c=date.today();
                    ul.LoginType__c='Remote Access 2.0';
                    ul.LoginUrl__c='test.salesforce.com';
                    ul.NetworkId__c='';
                    ul.Platform__c='Windows 10';
                    ul.Profile__c='Operaio';                    
                    ul.User_Name__c=usr.id ;
                   
            
            loginHistoryList.add(ul);
            }
        
        insert loginHistoryList;
        
       
        
           Test.startTest();
        String jobId = System.schedule('ScheduleApexClassTest', CRON_EXP, new KBMS_UserLoginDeleteBatchableSheduled());
        Test.stopTest();
    }   
        
}