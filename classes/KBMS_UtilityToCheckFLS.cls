/**
* @author Atos India Pvt Ltd.
* @date  Jan,2017
* @description This class is utility class which checks  CRUD & FLS 
*/

public with sharing class KBMS_UtilityToCheckFLS{
    
    
    public static Boolean checkCRUDPermission(String sObjectName,String operation){
        
        Map<String,Schema.SObjectType>  descripeResult=Schema.getGlobalDescribe();
        system.debug('@@@@@@@-----'+descripeResult);
        Schema.DescribeSObjectResult result=descripeResult.get(sObjectName).getDescribe();
        
        if(operation=='isCreateable')
        {
            return result.isCreateable();
        }
        else if(operation=='isQueryable')
        {
            return result.isQueryable();
        }
        else if(operation=='isUndeletable')
        {
            return result.isUndeletable();
        }
        else if(operation=='isUpdateable')
        {
            return result.isUpdateable();
        }
        else if(operation=='isDeletable')
        {
            return result.isDeletable();
        } 
        else if(operation=='isAccessible')
        {
            return result.isAccessible();
        }     
        else{
            return false;
        }
    }
    
    public static Boolean checkFLSPermission(String sobjectName,String[] lstfields,String level){
        
        Boolean flag=false;
        Map<String,Schema.SObjectType>  schemaMap=Schema.getGlobalDescribe();
        Schema.SObjectType objectSchema=schemaMap.get(sobjectName); 
        Schema.SObjectType.Contact.fields.getMap();
        
        Map<String,Schema.SObjectField> m = objectSchema.getDescribe().fields.getMap();
        System.assert(m!=null);
        
        if(level=='isAccessible'){
            
            for(String fieldToCheck : lstfields ){
                
                Schema.SObjectField f=m.get(fieldToCheck);
                if (f!=null){
                    
                    if(!m.get(fieldToCheck).getDescribe().isAccessible()){
                        
                        flag=false;
                        break;
                    }else{
                        
                        flag=true;
                    }
                }
            }
        }
        if(level=='isCreateable'){
            
            for(String fieldToCheck : lstfields ){
                
                Schema.SObjectField f=m.get(fieldToCheck);
                if (f!=null){
                    
                    if(!m.get(fieldToCheck).getDescribe().isCreateable()){
                        
                        flag=false;
                        break;
                    }else{
                        
                        flag=true;
                    }
                }
            }
        }
        if(level=='isUpdateable'){
            
            for(String fieldToCheck : lstfields ){
                
                Schema.SObjectField f=m.get(fieldToCheck);
                if (f!=null){
                    
                    if(!m.get(fieldToCheck).getDescribe().isUpdateable()){
                        
                        flag=false;
                        break;
                    }else{
                        
                        flag=true;
                    }
                }
            }
        }return flag;
        
    }
}