public with sharing class ObjectController{
    
    @AuraEnabled
    public static void fetchRecordId(){
        Id objRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Trader').getRecordTypeId();
    }
    
    @AuraEnabled
    public static List<Id> fetchRecordId1(){
        Id objRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Trader').getRecordTypeId();
        List<Id> recTypeId = new List<Id>();
        recTypeId.add(objRecordTypeId);
        return recTypeId;
    }
    
    @AuraEnabled
    public static Account fetchAccount(){
        //List<Account> acc = new List<Account>();
        Account acc = [Select Id, Name from Account];
        return acc;
    }
}