public class PT_Attachment_Management_Helper {
    
    public static void setAllegatoFields(Map<Id,Attachment> triggerNew){
        List<Pt_Allegato__c> listAllegato = new List<Pt_Allegato__c>();
        list<string> idAllegato = new list<string>();
        try{
            Map<id,Attachment> mapAttach =new Map<ID,Attachment> ([Select id, Parent.Type,parentid
                                                                   from Attachment where id in :triggerNew.keySet()]);
            
            
            if(!mapAttach.isEmpty()){
                for(Attachment attach : mapAttach.values()){
                    if('PT_Allegato__c'.equals(attach.parent.type))
                		idAllegato.add(attach.parentid);
            	}
                if(!idAllegato.isEmpty())
                {
                    listAllegato =[Select id,FileDisponibile__c,FileRichiesto__c from PT_Allegato__c where id in :idAllegato];
                	system.debug('*** PT_Attachment_Management_Helper.setAllegatoFields listAllegato: '+listAllegato);
                }
                
            }
            if (!listAllegato.isEmpty())
            {
                for(PT_Allegato__c allegato: listAllegato){                    
                        allegato.FileDisponibile__c = true;
                        allegato.FileRichiesto__c = false;                
            	}
                update listAllegato;
            } 
            
        }catch (Exception e){
             system.debug(logginglevel.error,'Exception in PT_Attachment_Management_Helper.setAllegatoFields: ' + e.getMessage() );
        } 
            
    }

}