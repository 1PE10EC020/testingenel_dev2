public with sharing class PT_Cancella_Attachment {
	
    @InvocableMethod
    public static void deleteAttachment_onAllegato(List<PT_Allegato__c> listAllegati){
        List<String> listId = new List<String>();
        for(PT_Allegato__c all : listAllegati){
            listId.add(all.id);
        }
        List<Attachment> listAtt = [Select id from Attachment where parentid in :listId];
        delete listAtt;        
    }
}