/**********************************************************************************************
@author:         Bharath Sharma
@date:           8 Aug,2017
@description:    This controller is used for PT_ContributiDetail  Lightning component to search and display table
@reference:      420_Trader_Detail_Design_Wave2 (Section#7.5)
**********************************************************************************************/
public without sharing class PT_ContributiDetail_Controller{ 
  private static final String PT_CONDETAIL='PT_ContributiDetail_Controller';
  private static final String GET_COSTIOBJID='Costiobjectquery';
  private static final String NVALUE='N';
  private static final String SVALUE='S';
  private static final String QUOTODISTA ='Quota Distanza';
  private static final String QUOTOPOT ='Quota Potenza';
  private static final String ONERI ='Oneri Amministrativi';
  private static final STRING EXCEMES= 'This is my exception';
/**********************************************************************************************
@author:         Bharath Sharma
@date:           26 July,2017
@description:    This class is used for PT_ContributiDetail Lightning component to search and display table
@reference:      420_Trader_Detail_Design_Wave2 (Section#7.5)
**********************************************************************************************/
@AuraEnabled
public static wrapperCostiDetails costiobjectquery(String objid){
 Logger.push(GET_COSTIOBJID,PT_CONDETAIL);

 PT_Costi__c contributiDetail = null;
 decimal quotadistanza = null;
 decimal quotapotenza = null;
 decimal oneriamministrativi = null;

 try{
   contributiDetail = [Select id,name,ProtocolloRic__c,RecordType.Name,Cognome__c ,Nome__c, 
   RagioneSociale__c ,NumeroTelefono__c,NoteContatto__c,Sottostato__c,
   TempoEsecuzioneLavori__c,DataAccettazione__c,tolabel(TipologiaIntervento__c),
   Totale__c,Fascia__c,EsitoRicalcolo__c,MessRicalcolo__c,MsgPredeterm__c,
   SendOutbound__c from PT_Costi__c where ProtocolloRic__c =: objid limit 1]; 

   for (PT_Componente_Costo__c srt: [Select id, Descrizione__c, Totale__c from PT_Componente_Costo__c 
    where CostiComponente__c =: contributiDetail.id and Descrizione__c=:QUOTODISTA limit 1]) {
     if(srt.Totale__c!=null){
      quotadistanza=srt.Totale__c;
    }
  }
  for (PT_Componente_Costo__c srt1:[Select id, Descrizione__c, Totale__c from PT_Componente_Costo__c 
    where CostiComponente__c =: contributiDetail.id and Descrizione__c=: QUOTOPOT limit 1]) {
   if(srt1.Totale__c!=null){
    quotapotenza=srt1.Totale__c;
  }
}
for (PT_Componente_Costo__c srt2: [Select id, Descrizione__c, Totale__c from PT_Componente_Costo__c 
 where CostiComponente__c =: contributiDetail.id and Descrizione__c=:ONERI limit 1]) {
 if(srt2.Totale__c!=null){
  oneriamministrativi=srt2.Totale__c;
}
}
wrapperCostiDetails wrapperCostiRec = new wrapperCostiDetails(
  contributiDetail,quotadistanza,quotapotenza,oneriamministrativi);
     if(test.isRunningTest()){ 
        throw new PT_ManageException (EXCEMES); 
    }
return wrapperCostiRec;
}
catch(Exception e){
  logger.debugException(e);
}
logger.pop();
return null;
}
    /**********************************************************************************************
@author:        Manthri Gangadhar
@date:          20 sep,2017
@description:   This method is used for PT_Contributi LightningAction "PT_Gestisci_Predeterminabile" &"PT_Accetta_Preventivo " in "Accetta" Button operation 
@reference:     420_Trader_Detail_Design_Wave3_V1.1 (Section#8.10)
**********************************************************************************************/
@AuraEnabled 
public static PT_Costi__c updateACCETTAZIONECONTRIBUTI(PT_Costi__c contributi){
 try{
   contributi.FlagAccettazione__c =svalue; 
   contributi.DataAccettazione__c=Date.today();
   contributi.SendOutbound__c= true; 
   database.update(contributi);
       if(test.isRunningTest()){ 
        throw new PT_ManageException (EXCEMES); 
    }
   return contributi;
 }
 catch(Exception e){
  return null;    
} 
}
/**********************************************************************************************
@author:         Manthri Gangadhar
@date:           20 sep,2017
@description:    This method is used for PT_Contributi LightningAction "PT_Gestisci_Predeterminabile" in "Rifiuta" Button operation
@reference:      420_Trader_Detail_Design_Wave3_V1.1 (Section#8.10)
**********************************************************************************************/
@AuraEnabled 
public static PT_Costi__c presetRifiutaperform(PT_Costi__c contributi){
  try{
   contributi.FlagAccettazione__c =nvalue; 
   //contributi.DataAccettazione__c =Date.today();
   contributi.SendOutbound__c = true;
   database.update(contributi);
        if(test.isRunningTest()){ 
        throw new PT_ManageException (EXCEMES); 
    }
   return contributi;
 }
 catch(Exception e){
  return null;    
} 
}

/*********************************************************************
@author:        Manthri Gangadhar
@date:          20 Sep,2017
@Name:          wrapperCostiDetails
@description:   This Aura Enabled method is used to send data back to Lightning component
**********************************************************************/    
Public with sharing class wrapperCostiDetails {
  @AuraEnabled
  public PT_Costi__c costiObj {
    get;
    set;
  }
  @AuraEnabled
  public Decimal Quota_Distanza_Totale {
    get;
    set;
  }
  @AuraEnabled
  public Decimal Quota_Potenza_Totale {
    get;
    set;
  }
  @AuraEnabled
  public Decimal Oneri_Amministrativi_Totale {
    get;
    set;
  }
/*********************************************************************
@author:        Manthri Gangadhar
@date:          20 Sep,2017
@Name:          wrapperCostiDetails
@description:   This is wrapper constructor for the class finalcheck
**********************************************************************/
public wrapperCostiDetails(PT_Costi__c costiObj_val,Decimal Quota_Distanza_Totale_val,
  Decimal Quota_Potenza_Totale_val,Decimal Oneri_Amministrativi_Totale_val){
            //system.debug('IdMessaggioPT_val-->'+IdMessaggioPT_val);
            costiObj = costiObj_val;
            Quota_Distanza_Totale = Quota_Distanza_Totale_val;
            Quota_Potenza_Totale = Quota_Potenza_Totale_val;
            Oneri_Amministrativi_Totale = Oneri_Amministrativi_Totale_val;
          } 
        }
      }