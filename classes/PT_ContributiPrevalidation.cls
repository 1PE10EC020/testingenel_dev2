public class PT_ContributiPrevalidation {
 
    //public static String ErrMsg ;
    public static final String FlagAccettazionpic = 'FlagAccettazione__c';
    public static final String Sottostatopic = 'Sottostato__c';

 @AuraEnabled 
public static String validateACCETTAZIONECONTRIBUTI(PT_Costi__c request){
     String ErrMsg ='';
     if(request.Sottostato__c == 'INL.052' && (request.NumeroTelefono__c == null || request.NumeroTelefono__c == '' )){
            ErrMsg = 'E obbligatorio specificare il numero di telefono del contatto';
            return ErrMsg;
        }
      
	  if(request.Sottostato__c == 'INL.052' && ((request.Nome__c == null || request.Nome__c == '' )||
	                                            (request.Cognome__c == null || request.Cognome__c == ''))
										    &&(request.RagioneSociale__c == null || request.RagioneSociale__c == '')){
            ErrMsg = 'E obbligatorio specificare Nome e Cognome oppure Ragione Sociale del contatto';
            return ErrMsg;
        }
	 return null;
    }
 
@AuraEnabled 
public static PT_Costi__c updateACCETTAZIONECONTRIBUTI(PT_Costi__c contributi){
    /*if(contributi != null){
        contributi.Nome__c = Contributi. ProtocolloRic__r.Nome_cl__c;	
        contributi.Cognome__c = Contributi.ProtocolloRic__r.Cognome_cl__c;
        contributi.RagioneSociale__c = Contributi.ProtocolloRic__r.RagSoc_cl__c;
         contributi.NumeroTelefono__c = Contributi.ProtocolloRic__r.NumTelRichiedente__c;
        contributi.NoteContatto__c = Contributi.ProtocolloRic__r.Note_rich_cl__c;
        //database.update(contributi);
        update contributi;
    }*/ 
       contributi.FlagAccettazione__c ='S'; 
        contributi.DataAccettazione__c=Date.today();
        contributi.SendOutbound__c=True;
       system.debug('------------------------->>'+contributi);
   update contributi;
    return contributi;
       

  }
       
    
    @AuraEnabled
    public static String getRecordDetails(String rcdid){
        List<PT_Costi__c> recordTypeList = new List<PT_Costi__c>(); 
        String recTypeName;
        if(rcdid!=null){
           // List<String> recTypeAPINames = rcdTypeName.split(SEMICOLON);

        	try{
           		     recordTypeList = [select id,RecordType.Name from PT_Costi__c
                                                   where id=:rcdid limit 1];
                	recTypeName=recordTypeList[0].RecordType.Name;
                	
       		}
            catch(Exception e){
                return null;
       		}
        }
        
       	return recTypeName;
    }    
  /*  @AuraEnabled
    public static User getCurrentinfooffields(){
        
        try{
      User toReturn = [SELECT Id,name, FirstName,account.name,IdTrader__c,account.Parent.IdTrader__c, 
                       account.PartitaIVA__c,LastName FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
       //system.debug('------------------------->>'+ toReturn);             
      
       return toReturn;
        }
        catch(Exception e){
            return null;            
        }
  }*/
    
    
   /* @AuraEnabled 
     public static PT_PicklistAuraComponent getcostiPicklistValues()
    {
        try{
        PT_PicklistAuraComponent  obj = new PT_PicklistAuraComponent();
            
        obj.pickListValues.put(FlagAccettazionpic,PT_Utility.getPickListValues(PT_Costi,FlagAccettazionpic));
        obj.pickListValues.put(Sottostatopic,PT_Utility.getPickListValues(PT_Costi,Sottostatopic));
          return obj;  
        }
         catch(Exception e){
           // system.debug('Error Message'+ e.getMessage());  
            return null;
        } 
    }*/
}