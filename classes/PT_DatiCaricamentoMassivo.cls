/**********************************************************************************************
@author:        Jyotsna Tiwari
@date:          24 Sep,2017
@Method Name:   createRequestRecord
@description:   This Aura Enabled method inserts request object instance post successfull validation checks across all RFI components
@reference:     420_Trader_Detail_Design_Wave0 (Section#2.8.1.1)
**********************************************************************************************/
public without sharing class PT_DatiCaricamentoMassivo {
    
    public static final String CREATECARICAMENTOMASSIVORECORD='createCaricamentoMassivoRecord';
    public static final String PTDATICARICAMENTOMASIVO='PT_DatiCaricamentoMassivo';
    public static final String GETALLRICHPICKVALS = 'getAllRichiestaPicklistValuesCntr';
    
    
    /**********************************************************************************************
@author:        Jyotsna Tiwari
@date:          24 Sep,2017
@Method Name:   createRequestRecord
@description:   This Aura Enabled method inserts request object instance post successfull validation checks across all RFI components
@reference:     420_Trader_Detail_Design_Wave0 (Section#2.8.1.1)
**********************************************************************************************/    
    @AuraEnabled
    public static PT_Caricamento_Massivo__c createCaricamentoMassivoRecord(PT_Caricamento_Massivo__c caricamento){
Logger.push(CREATECARICAMENTOMASSIVORECORD,PTDATICARICAMENTOMASIVO);
try{
    if(caricamento != null){
        database.insert(caricamento);
        return caricamento;
    }
            // return null;
        }
        catch(Exception e){
            logger.debugException(e);   
        } 
        logger.pop();
        return null;
    }
    
    /**********************************************************************************************
@author:        Jyotsna Tiwari
@date:          25 Sep,2017
@Method Name:   renderRFIComponents
@description:   This Aura Enabled method is used to fetch PT_MIC_Component_Visibility_Settings__c custom setting value to get the mapping between 
each request record type and RFI component.
@reference:     
**********************************************************************************************/
@AuraEnabled
public static PT_MIC_Component_Visibility_Settings__c renderRFIComponents(String recordTypeName){
    try{
        if(recordTypeName!=null){
            PT_MIC_Component_Visibility_Settings__c rfiComponentSettings = PT_MIC_Component_Visibility_Settings__c.getValues(recordTypeName);        
            return rfiComponentSettings; 
        }
        return null;
    }        
    catch(Exception e){
        return null;    
    } 
}
}