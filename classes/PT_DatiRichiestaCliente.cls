/**********************************************************************************************
@author:       	Sharan Sukesh
@date:         	10 Aug,2017
@Class Name:  	PT_DatiRichiestaCliente
@description:  	This class is used to get the user details for the DatiRichiestaCliente RFI Lightning Component.
@reference:		420_PT_Logiche_pagine_Inserimento_Wave2
**********************************************************************************************/
public without sharing class PT_DatiRichiestaCliente {

	public static final String HYPHEN = '-'; 
    public static final String PT_M01 = 'PT_M01';
    public static final String PT_M02 = 'PT_M02';
    PRIVATE STATIC FINAL STRING REQUESTOBJNAME='PT_Richiesta__c';
/**********************************************************************************************
@author:       	Sharan Sukesh
@date:         	24 Jun,2017
@Method Name:  	getCurrentUserinfo
@description:  	This Aura Enabled method is used to get current user login details and return as string.
@reference:		420_PT_Logiche_pagine_Inserimento_Wave0
**********************************************************************************************/       
    @AuraEnabled
    public static List<RecordType> getRecordTypeDetails(String recordTypeName){
        List<RecordType> recordTypeList = new List<RecordType>();    
        
        if(recordTypeName.equals(PT_M01)){
        try{
     	     recordTypeList = [select id,name,developerName,description from RecordType
                                                   where sobjectType=:REQUESTOBJNAME and developername in ('PT_M01','PT_M02','PT_M03') limit 3];
       		}
            catch(Exception e){
                return null;
       		}
        }
        
        if(recordTypeName.equals(PT_M02)){
        try{
     	     recordTypeList = [select id,name,developerName,description from RecordType
                                                   where sobjectType=:REQUESTOBJNAME and developername in ('PT_M02') limit 1];
       		}
            catch(Exception e){
                return null;
       		}
        } 
        
        return recordTypeList;
        
    }
}