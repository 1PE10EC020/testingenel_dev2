/**********************************************************************************************
@author:       	Sushant Yadav
@date:         	24 Jun,2017
@Class Name:  	PT_DatiVenditore
@description:  	This class is used to get the user details for the DatiVenditore RFI Lightning Component.
@reference:		420_PT_Logiche_pagine_Inserimento_Wave0
**********************************************************************************************/
public without sharing class PT_DatiVenditore {

	public static final String HYPHEN = '-';  
        PRIVATE STATIC FINAL STRING EXCEMES= 'This is my exception';
/**********************************************************************************************
@author:       	Sushant Yadav
@date:         	24 Jun,2017
@Method Name:  	getCurrentUserinfo
@description:  	This Aura Enabled method is used to get current user login details and return as string.
@reference:		420_PT_Logiche_pagine_Inserimento_Wave0
**********************************************************************************************/       
    @AuraEnabled
    public static User getCurrentUserinfo(){
        
        try{
      User toReturn = [SELECT Id,name, FirstName,account.name,IdTrader__c,account.Parent.IdTrader__c, 
                       account.PartitaIVA__c,LastName FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
       //system.debug('------------------------->>'+ toReturn.name);             
       // return toReturn.account.name + HYPHEN + toReturn.IdTrader__c;
        if(test.isRunningTest()){ 
                        throw new PT_ManageException (EXCEMES); 
                    }
       return toReturn;
        }
        catch(Exception e){
            return null;            
        }
  }
/**********************************************************************************************
@author:       	Priyanka Sathyamurthy
@date:         	18 Sep,2017
@Method Name:  	getPartitaIvaTraderValue
@description:  	This Aura Enabled method is used to get Partita Iva Trader and return as string.
**********************************************************************************************/       
   /* @AuraEnabled
    public static String getPartitaIvaTraderValue(){
        
        try{
      User toReturn = [SELECT Id,name,account.PartitaIVA__c, FirstName,account.name,IdTrader__c,account.Parent.IdTrader__c, 
                       LastName FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        //system.debug('------------------------->>'+ toReturn.IdTrader__c);             
        return toReturn.account.PartitaIVA__c;
        }
        catch(Exception e){
            return null;            
        }
  }*/
/**********************************************************************************************
@author:       	Sushant Yadav
@date:         	24 Jun,2017
@Method Name:  	getUserId
@description:  	This Aura Enabled method is used to get list of PT_Dispacciamento__c where login id is IdTrader__c and concatinate some values
@reference:		420_PT_Logiche_pagine_Inserimento_Wave0
**********************************************************************************************/   
    @AuraEnabled
    /*public static List<PT_Dispacciamento__c> getUserinfo() {
        try{
      User toReturnn = [SELECT Id, FirstName,account.name,IdTrader__c,account.IdTrader__c, 
                        LastName FROM User WHERE Id = :UserInfo.getUserId() Limit 1];
       //system.debug('-->>'+ toReturnn.account.name); 
       List<PT_Dispacciamento__c> options = new list<PT_Dispacciamento__c>();
       List<PT_Dispacciamento__c> disp = [SELECT Id,IdDispacciamento__c,DescrDispacc__c,IdTrader__c,DataInizioValid__c,DataFineValid__c
                                    FROM PT_Dispacciamento__c where IdTrader__c = :toReturnn.IdTrader__c and 
                                    DataInizioValid__c <=:system.now() and DataFineValid__c>=:system.now() LIMIT 500];
       //system.debug('-->>'+ disp +'--->'); 
        return disp;
        }
        catch(Exception e){
            return null;
        }
  }*/
     public static List<PT_Dispacciamento__c> getUserinfo() {
        try{
      User toReturnn = [SELECT Id, FirstName,account.name,IdTrader__c,account.IdTrader__c,account.Distributore__c, 
                        LastName FROM User WHERE Id = :UserInfo.getUserId() Limit 1];
      // system.debug('-->>'+ toReturnn); 
       List<PT_Dispacciamento__c> options = new list<PT_Dispacciamento__c>();
       List<PT_Dispacciamento__c> disp = [SELECT Id,IdDispacciamento__c,trader__r.Distributore__c,DescrDispacc__c,IdTrader__c,DataInizioValid__c,DataFineValid__c
                                    FROM PT_Dispacciamento__c where IdTrader__c = :toReturnn.IdTrader__c and trader__r.Distributore__c =:toReturnn.account.Distributore__c and 
                                    DataInizioValid__c <=:system.now() and DataFineValid__c>=:system.now() LIMIT 500];
       //system.debug('-->>'+ disp +'--->'); 
       if(test.isRunningTest()){ 
                        throw new PT_ManageException (EXCEMES); 
                    }
        return disp;
        }
        catch(Exception e){
            return null;
        }
  }
}