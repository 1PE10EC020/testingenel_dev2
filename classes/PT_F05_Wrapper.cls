/**********************************************************************************************
@author:         Bharath Sharma
@date:           26 July,2017
@description:    This Aura Enabled method used to hold data entered in PT_F05_Search Lightning component 
                 used in PT_F05_Search class
@reference:    420_Trader_Detail_Design_Wave1 (Section#4.2.2)
**********************************************************************************************/

public class PT_F05_Wrapper {
       @AuraEnabled
        public string TipoRich {
            get;
            set;
        }

        @AuraEnabled
        public Decimal CodRichDistribA {
            get;
            set;
        }

        @AuraEnabled
        public Decimal CodRichDistribDa {
            get;
            set;
        }

        @AuraEnabled
        public string CodRichVend {
            get;
            set;
        }

        @AuraEnabled
        public string StatoRich {
            get;
            set;
        }

        @AuraEnabled
        public string SottostatoRich {
            get;
            set;
        }

        @AuraEnabled
        public Date DataDecorrenzaDa {
            get;
            set;
        }

        @AuraEnabled
        public Date DataDecorrenzaA {
            get;
            set;
        }

        @AuraEnabled
        public string DataDecorrenzaSwitchDa {
            get;
            set;
        }

        @AuraEnabled
        public string DataDecorrenzaSwitchA {
            get;
            set;
        }

        @AuraEnabled
        public Date DataEvasioneDa {
            get;
            set;
        }

        @AuraEnabled
        public Date DataEvasioneA {
            get;
            set;
        }

        @AuraEnabled
        public string POD {
            get;
            set;
        }

        @AuraEnabled
        public string NumPresa {
            get;
            set;
        }

        @AuraEnabled
        public string Eneltel {
            get;
            set;
        }

        @AuraEnabled
        public string Venditore {
            get;
            set;
        }

        @AuraEnabled
        public string ContrattodiDisp {
            get;
            set;
        }

        @AuraEnabled
        public string CFRich {
            get;
            set;
        }

        @AuraEnabled
        public string PIVARich {
            get;
            set;
        }

        @AuraEnabled
        public string Nome {
            get;
            set;
        }

        @AuraEnabled
        public string Cognome {
            get;
            set;
        }

        @AuraEnabled
        public string RagSociale {
            get;
            set;
        }

        @AuraEnabled
        public string ProvinciaPOD {
            get;
            set;
        }

        @AuraEnabled
        public string Ordinaper {
            get;
            set;
        }
    
}