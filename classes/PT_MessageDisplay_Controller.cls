/*******************
 * Author : Giulia Tedesco
 * Apex Class  :  PT_MessageDisplay_Controller
 * CreatedDate :  15/09/2017
 * Description :  This class is PT_MessageDisplay component controller. It is used to get Records of Messaggio Utente SObject
 ****************/

 public class PT_MessageDisplay_Controller {

   
    @AuraEnabled
    public static PageWrapper getRecords(Integer sObjOffset, List<String> fieldApiName, Integer maxNoOfRecord, String profiles){
        
        // System is throwing internal salesforce error so converting to string to add 1 
        String maxNoOfRecordStr =  maxNoOfRecord+'';                        
        Integer maxRec = (Integer.valueOf(maxNoOfRecordStr)+ 1);    
        
        List<Sobject> sobjLst = null;
        PageWrapper wrapObj = new PageWrapper();
        
        //getObjName('PT_Messaggio_Utente__c');
        sobjLst  = Database.query(generateSoql(sObjOffset,fieldApiName,maxRec,profiles));
        
        
        if(sobjLst.size() == maxRec ){
            for(integer i =0; i< maxRec-1;i++){
                wrapObj.sObjLst.add(sobjLst.get(i));                    
            }    
            }else{
             wrapObj.sObjLst.addALl(sobjLst);    
             wrapObj.hasNext = true;        
         }
         
         

         return wrapObj;
     }
     
     
     
     private  static String generateSoql(Integer sObjOffset,List<String> fieldApiName, Integer maxNoOfRecord, String profiles) 
     {
        String queryStr = 'SELECT ';
        String fields  = ' ';
        
        // Add 1 just to Check if it has more number of records.
        //   maxNoOfRecord = maxNoOfRecord + 1;                
        if(fieldApiName != null){
            for(String str : fieldApiName){
                fields +=  str + ',';
            }
        }

        // remove last comma from field string         
        fields  = fields.substring(0, fields.length()-1); 
        
        queryStr = queryStr + fields + ',ownerId FROM ' + 'PT_Messaggio_Utente__c';
        
        //***[G.Tedesco] - Filter to get context page
        String filter = '';
        
        
        if(profiles <> null && profiles.trim()<>'' ){
            filter = 'ListaProfili__c' + ' INCLUDES (' +profiles + ')';
        }

        if(filter <> null && filter.trim()<>''){
            
            queryStr += ' WHERE '+filter;
            
        }

        //queryStr += ' AND DataInizValidita__c <= '+System.Today()+' AND DataFineValidita__c >= '+System.Today();
        queryStr += ' AND DataInizValidita__c <= TODAY AND DataFineValidita__c >= TODAY';
        
        queryStr = queryStr + ' LIMIT ' + maxNoOfRecord + ' OFFSET '   + sObjOffset;
		system.debug('*** queryStr: '+queryStr);
        return queryStr;


    }
    

    public class PageWrapper{
        @AuraEnabled
        public Boolean hasNext {get;set;}
        @AuraEnabled
        public List<Sobject> sobjLst {get;set;}
        

        public PageWrapper(){
            hasNext = false;
            sobjLst  = new List<Sobject>();
        }
        
    }
}