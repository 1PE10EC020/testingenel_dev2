@isTest(SeeAllData = true)
public class PT_MessageDisplay_Controller_Test{

	static testMethod void TestgetRecords(){

		Integer sObjOffset = 0;
		PT_MessageDisplay_Controller.PageWrapper pWrapper;

		PT_Messaggio_Utente__c mu = new PT_Messaggio_Utente__c();
		mu.DataInizValidita__c = System.today();
		mu.TestoMsg__c = 'Test testo messaggio';
		mu.ListaProfili__c = 'ICT;V01;VRE';
		insert mu;

		List<String> strList = new List<String>();
        strList.add('DataInizValidita__c');
        strList.add('TestoMsg__c');
        Integer maxNumRec = 8;
        String profiles = mu.ListaProfili__c;



		Test.startTest();
			pWrapper = PT_MessageDisplay_Controller.getRecords(sObjOffset, strList, maxNumRec, profiles);
		Test.stopTest();
	}
}