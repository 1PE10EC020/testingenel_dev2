/**********************************************************************************************
@author:       	Sushant Yadav
@date:         	26 Jun,2017
@description:  	This is the wrapper class for picklist values
@reference:		
**********************************************************************************************/
public without sharing class PT_PicklistEntryWrapper{

/**********************************************************************************************
@author:       	Sushant Yadav
@date:         	26 Jun,2017
@description:  	This method acts as a wrapper for get and set of picklist values
**********************************************************************************************/    
   public String active {get;set;}
   public String defaultValue {get;set;}
   public String label {get;set;}
   public String value {get;set;}
   public String validFor {get;set;}
}