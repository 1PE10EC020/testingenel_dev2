/*********************************************************************************************
@Author : Akansha Lakhchaura
@date : 04 July 2017
@description : Test class for PT_RequestRecordCreator.
**********************************************************************************************/

@isTest(SeeAllData = false)
/*********************************************************************************************
@Author : Akansha Lakhchaura
@date : 04 July 2017
@description : Test class for PT_RequestRecordCreator.
**********************************************************************************************/
private class PT_RequestRecordCreatorTest{
    
    private static final String ALIAS = 'unEx14';
    private static final String PTN02 = System.Label.PT_RICHIESTE_RecordTypeName_PT_N02;
    
    /* 
    Author : Akansha Lakhchaura
    Apex Method : createRequestRecord
    CreatedDate : 04/07/2017
    Description : createRequestRecord is a method to invoke methods from PT_RequestRecordCreator.
    */
    private static testMethod void createRequestRecord(){
        
        User runningUser=PT_TestDataFactory.userDataFetch(ALIAS);
        //database.insert(runningUser);
       
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.PT_Richiesta__c; 
      Map<String,Schema.RecordTypeInfo> richiestaRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
      Id ptpc1_id = richiestaRecordTypeInfo .get('PT_PC1').getRecordTypeId();
        
        PT_Feasibility_Check__c fesCheck= new PT_Feasibility_Check__c();
        fesCheck.Name='PT_PC1';
        System.runAs(runningUser){
        	database.insert(fesCheck);
        }
        fesCheck.Controllo_Comp_Fornit__c=false;
        fesCheck.Controllo_Rich_Duplicato__c=false;
        fesCheck.Controllo_Trader_Moroso__c=false;
        System.runAs(runningUser){
        	database.update(fesCheck);
        }
        
		//User usr = PT_TestDataFactory.userDataFetch('Test1');
       // system.debug('usr->'+usr);
        
       // Contact con = [Select id,Name,Account.id,Account.Distributore__c FROM Contact WHERE id=:usr.ContactId];
        
        PT_Richiesta__c request = PT_TestDataFactory.createRequest();
       // request.AccountDisId__c = con.Account.Distributore__c;
       // request.AccountTrId__c = con.Account.id;
        //PC1 record type id is passed since it fails process builder condition
        request.MandConnessione__c='S';
        request.RecordTypeId= ptpc1_id;
	
        System.runAs(runningUser){
            database.insert(request);
        }
        System.assertEquals(ptpc1_id,request.RecordTypeId);
        //PT_RequestRecordCreator.createRequestRecord(request);        
        //PT_RequestRecordCreator.getRichiestaPicklistValues();
         
        System.runAs(runningUser){
            PT_RequestRecordCreator.getRichiestaPicklistValues();
            PT_RequestRecordCreator.createRequestRecord(request);
           
            }
        }
    
   /* 
    Author : Akansha Lakhchaura
    Apex Method : getRichiestaPicklistValues
    CreatedDate : 04/07/2017
    Description : pt_Prevalidations is a method to invoke getRichiestaPicklistValues method from PT_RequestRecordCreator.
    */  
    private static testMethod void getRichiestaPicklistValues(){
      User runningUser=PT_TestDataFactory.userDataFetch(ALIAS);
        //database.insert(runningUser);   
        
        PT_Richiesta__c request = PT_TestDataFactory.createRequest();  
        system.assertEquals('ROSSI', request.Cognome_cl__c);
       // database.insert(request);
        System.runAs(runningUser){
         PT_RequestRecordCreator.getRichiestaPicklistValues();
        }
       }
    
    /* 
    Author : Akansha Lakhchaura
    Apex Method : getAllRichiestaPicklistValuesCntr
    CreatedDate : 30/08/2017
    Description : pt_Prevalidations is a method to invoke getAllRichiestaPicklistValuesCntr method from PT_RequestRecordCreator.
    */  
    private static testmethod void getAllRichiestaPicklistValues(){
      User runningUser=PT_TestDataFactory.userDataFetch(ALIAS);
        
        PT_Richiesta__c request = PT_TestDataFactory.createRequestF05();
        Id recordtypeid = Schema.SObjectType.PT_Richiesta__c.getRecordTypeInfosByName().get('PT_PD2').getRecordTypeId();
        request.RecordTypeId=recordtypeid;
        
        Richieste_Picklist_Values__c pickVal = new Richieste_Picklist_Values__c();
        List<Richieste_Picklist_Values__c> reqAllPicklist = [SELECT Id, Name, Record_Type__c, Picklist_Api_Name__c, Picklist_Value__c, Controlling_Field_API__c,
                                                                 Controlling_Field_Value__c, Controlling_Field_2_API__c, Controlling_Field_2_Value__c 
                                                                 FROM Richieste_Picklist_Values__c where Record_Type__c=:'PT_N02' order by Picklist_Api_Name__c limit 100];
         List <Richieste_Picklist_Values__c> asrList = new Richieste_Picklist_Values__c [1];
       asrList[0] = PT_TestDataFactory.insertrichesta( 'ACV-1', '', '', '',
     '', '','', '','TipoRichiedente__c','M');

       Database.Insert(asrList );   
        //System.debug('asrList'+asrList);
        Map<String,List<Richieste_Picklist_Values__c>> reqPicklistMap = new Map<String,List<Richieste_Picklist_Values__c>>();
        system.assertEquals('ROSSI', request.Cognome_cl__c);
        database.insert(request);
        //system.debug('request==>'+request);
        System.runAs(runningUser){
         PT_RequestRecordCreator.getAllRichiestaPicklistValuesCntr(request.RecordTypeId);
        
        }
       }
    
    
    /* 
    Author : Sharan Sukesh
    Apex Method : richiestaUtentetest
    CreatedDate : 30/08/2017
    Description : pt_Prevalidations is a method to invoke richiestaUtentetest method from PT_RequestRecordCreator.
    */  
    private static testmethod void richiestaUtentetest(){
     User runningUser=PT_TestDataFactory.userDataFetch(ALIAS);
        //database.insert(runningUser);   
        
        PT_Richiesta__c request = PT_TestDataFactory.createRequest();  
        system.assertEquals('ROSSI', request.Cognome_cl__c);
       // database.insert(request);
        System.runAs(runningUser){
         PT_RequestRecordCreator.richiestaUtente('2');
        }
       }
    
    
    /* 
    Author : Sharan Sukesh
    Apex Method : TipoClientFinale
    CreatedDate : 30/08/2017
    Description : pt_Prevalidations is a method to invoke richiestaUtentetest method from PT_RequestRecordCreator.
    */  
    private static testmethod void tipoClientFinale(){
      User runningUser=PT_TestDataFactory.userDataFetch(ALIAS);
        //database.insert(runningUser);   
        
        PT_Richiesta__c request = PT_TestDataFactory.createRequest();  
        system.assertEquals('ROSSI', request.Cognome_cl__c);
       // database.insert(request);
        System.runAs(runningUser){
         PT_RequestRecordCreator.tipoClientFinale('5');
        }
       }

    
     /* 
    Author : Akansha Lakhchaura
    Apex Method : renderRFIComponents1
    CreatedDate : 04/07/2017
    Description : renderRFIComponents1 is a method to invoke renderRFIComponents method from PT_RequestRecordCreator.
    */
    private static testMethod void renderRFIComponents1() {
          User runningUser=PT_TestDataFactory.userDataFetch(ALIAS);
        //database.insert(runningUser);
        
        PT_RFI_Component_Visibility__c css = new PT_RFI_Component_Visibility__c();
        css.Name='PT_N02';
        css.IdTipoRichiesta__c = 'NCA';
        database.insert(css);
       // PT_RequestRecordCreator.renderRFIComponents(PT_RFI_Component_Visibility__c.getValues('PT_N02').Name);
        System.assertEquals('PT_N02', css.name);
        System.runAs(runningUser){
              PT_RequestRecordCreator.renderRFIComponents(PT_RFI_Component_Visibility__c.getValues('PT_N02').Name);
            }
    }
     /* 
    Author : Akansha Lakhchaura
    Apex Method : getRecTypeDetail
    CreatedDate : 04/07/2017
    Description : getRecTypeDetail is a method to invoke getRecTypeDetail method from PT_RequestRecordCreator.
    */
    private static testMethod void getRecTypeDetail(){
        User runningUser=PT_TestDataFactory.userDataFetch(ALIAS);
       // database.insert(runningUser);
        System.runAs(runningUser){
            RecordType recordTypeDetail = [select ID from RecordType where developername=:PTN02 limit 1];
            recordTypeDetail = PT_RequestRecordCreator.PT_GetRecTypeDetail(recordTypeDetail.Id);
            System.assertEquals(recordTypeDetail.id, recordTypeDetail.id);
        }
    }
   /* 
    Author : Priyanka Sathyamurthy
    Apex Method : updateFileUploadCheckbox
    CreatedDate : 10/16/2017
    Description : updateFileUploadCheckbox is a method to invoke updateFileUploadCheckbox method from PT_RequestRecordCreator.
    */
    private static testMethod void updateFileUploadCheckbox(){
        User runningUser=PT_TestDataFactory.userDataFetch(ALIAS);
       // database.insert(runningUser);
       Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.PT_Richiesta__c; 
      Map<String,Schema.RecordTypeInfo> richiestaRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
      Id ptpc1_id = richiestaRecordTypeInfo .get('PT_PC1').getRecordTypeId();
        
      PT_Feasibility_Check__c fesCheck= new PT_Feasibility_Check__c();
        fesCheck.Name='PT_PC1';
        database.insert(fesCheck);
        fesCheck.Controllo_Comp_Fornit__c=false;
        fesCheck.Controllo_Rich_Duplicato__c=false;
        fesCheck.Controllo_Trader_Moroso__c=false;
        database.update(fesCheck);
        System.runAs(runningUser){
        /*User usr = PT_TestDataFactory.userDataFetch('Test');
        Contact con = [SELECT Name,Id,Account.id,Account.Distributore__c  FROM Contact WHERE Id=:usr.ContactId];*/
       	PT_Richiesta__c request = PT_TestDataFactory.createRequest();
       /* request.AccountTrId__c = con.Account.id;
        request.AccountDisId__c = con.Account.Distributore__c;*/
        request.MandConnessione__c='S';
        request.RecordTypeId=ptpc1_id;
        request.PotImp__c='3';
        database.insert(request);
        System.assertEquals('3', request.PotImp__c);
        PT_RequestRecordCreator.updateFileUploadCheckbox(request);
        PT_RequestRecordCreator.InteractiveOutcomeManagement(request.Id);
        }
    }   
/* 
    Author : Priyanka Sathyamurthy
    Apex Method : createMassiveRecord
    CreatedDate : 10/16/2017
    Description : createMassiveRecord is a method to invoke createCaricamentoMassivoRecord method from PT_RequestRecordCreator.
    */
    private static testMethod void createMassiveRecord(){
        User runningUser=PT_TestDataFactory.userDataFetch(ALIAS);
       // database.insert(runningUser);

        System.runAs(runningUser){
       	PT_Caricamento_Massivo__c request = PT_TestDataFactory.createMassiveRecord();
        //database.insert(request);
        PT_RequestRecordCreator.createCaricamentoMassivoRecord(request);
        System.assertEquals('N02', request.TipoRich__c);
        }
    }


    
    
}