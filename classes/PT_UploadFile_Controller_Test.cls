/*********************************************************************************************
@Author : Sharan Sukesh
@date : 11 October 2017
@description : Test class for creation of record with File Upload capability.
**********************************************************************************************/
@isTest
private class PT_UploadFile_Controller_Test{
/*********************************************************************************************
@Author : Sharan Sukesh
@date : 11 October 2017
@description : Test class for creation of record with File Upload capability.
**********************************************************************************************/
PRIVATE STATIC FINAL STRING S02='S02';    
private static final String ALIAS = 'unEx14';
/*********************************************************************************************
@Author : Sharan Sukesh
@date : 11 October 2017
@description : Test class for creation of record with File Upload capability.
**********************************************************************************************/    
     @testSetup static void createTestRecords() {
    list<PT_Caricamento_Massivo__c > lstInsertService = new list<PT_Caricamento_Massivo__c >();
     User runningUser=PT_TestDataFactory.userDataFetch(ALIAS);
     PT_Caricamento_Massivo__c cmaer= new PT_Caricamento_Massivo__c();
      cmaer.TipoRich__c=S02;
      lstInsertService.add(cmaer);
      system.assertEquals( cmaer.TipoRich__c , 'S02');
         system.runAs(runningUser){
        database.insert(lstInsertService);
      }
    }
/*********************************************************************************************
@Author : Sharan Sukesh
@date : 11 October 2017
@description : Test method for getting the attachment and upload the attachment.
**********************************************************************************************/
    static testMethod void callUploadFile() {
        User runningUser = PT_TestDataFactory.userDataFetch(ALIAS);
  		PT_UploadFile_Controller myControllerObj = new PT_UploadFile_Controller();
        Test.startTest();
        myControllerObj.message = 'File Uploaded Successfully';
        myControllerObj.messageType = 'Success';
        system.assertEquals(  myControllerObj.messageType  ,'Success' );
        system.runAs(runningUser){
        myControllerObj.getObjAttachment();       
 		myControllerObj.uploadFile();
        }
        test.stopTest();
      }
     
 }