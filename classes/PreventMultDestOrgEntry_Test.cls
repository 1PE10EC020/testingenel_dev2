@isTest
public class PreventMultDestOrgEntry_Test {
    static testMethod void prevendEntry() {
         Destination_Org_Credentials__c doc1 =
                 new Destination_Org_Credentials__c(Username__c = 'x_inspire_x',
                                                    Password__c = 'x_password_x',
                                                    Environment_Url__c = 'x_url_x');

         Destination_Org_Credentials__c doc2 =
                 new Destination_Org_Credentials__c(Username__c = 'x_inspire2_x',
                                                    Password__c = 'x_password_x',
                                                    Environment_Url__c = 'x_url_x');

         Test.startTest();
         insert doc1;
         
         try {
             insert doc2;
         } catch (Exception ex) {}
         Test.stopTest();
    }
}