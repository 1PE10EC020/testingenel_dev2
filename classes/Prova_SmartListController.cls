public class Prova_SmartListController {
   

    @AuraEnabled
    public static String recordId{get;set;}
    

  /*
  Description: This method is returning records to the lightning Component.
  */
    @AuraEnabled
    public static PaginationWrapper getRecords(String otherSoql,Integer sObjOffset, 
                                               List<String> fieldApiName, Integer maxNoOfRecord, 
                                               String sobjApiName, 
                                               Boolean createdByMe, String recordId,Boolean isDownloadable,
                                               List<String> downloadFields, String whereClause)
    {        
       
        // System is throwing internal salesforce error so converting to string to add 1 
        String maxNoOfRecordStr =  maxNoOfRecord+'';                        
        Integer maxRec = (Integer.valueOf(maxNoOfRecordStr)+ 1);    
        
        List<Sobject> sobjLst = null;
        PaginationWrapper wrapObj = new PaginationWrapper();
        //Pass parameters to create query String and fetch the records
        sobjLst  = Database.query(generateSoql(false,null,null,sObjOffset,fieldApiName,
                                               maxRec,sobjApiName,createdByMe,recordId, isDownloadable, 
                                               downloadFields, whereClause
                                               )
                                 );
        
        if(sobjLst.size() == maxRec ){
            for(integer i =0; i< maxRec-1;i++){
                wrapObj.sObjLst.add(sobjLst.get(i));                    
            }    
        }else{
            wrapObj.sObjLst.addALl(sobjLst);    
            wrapObj.hasNext = true;        
        }

        
        System.debug('**recordId: ' +recordId);
        System.debug('**sObjLst: ' +sObjLst);

        return wrapObj;
    }
    
    /*
  Description: This method will return search result. 
  */
  /*
    @AuraEnabled
    public static PaginationWrapper getRecordsForSearch(String otherSoql,Integer sObjOffset,
                                                        List<String> fieldApiName, Integer maxNoOfRecord,
                                                        String sobjApiName,
                                                        List<String> searchField,
                                                        Boolean createdByMe) 
    {        
        // System is throwing internal salesforce error so converting to string to add 1 
        String maxNoOfRecordStr =  maxNoOfRecord+'';                        
        Integer maxRec = (Integer.valueOf(maxNoOfRecordStr)+ 1);  
        PaginationWrapper wrapObj = new PaginationWrapper(); 
        
        //get record list
        List<Sobject> sobjLst = null;
        sobjLst  = Database.query(generateSoql(true,searchField,null,sObjOffset,
                                               fieldApiName,maxRec,sobjApiName,createdByMe,recordId)
                                 );
        if(sobjLst.size() == maxRec ){
            for(integer i =0; i< maxRec-1;i++){
                wrapObj.sObjLst.add(sobjLst.get(i));                    
            }
        }else{
            wrapObj.sObjLst.addALl(sobjLst);   
            wrapObj.hasNext = true;        
        }


        
        return wrapObj;                
    } */
    
    /*
  Description: This method will generate query String
  */
    private  static String generateSoql(Boolean isSearch,List<String> serarchField, 
                                        String searchTxt,Integer sObjOffset,
                                        List<String> fieldApiName, Integer maxNoOfRecord,
                                        String sobjApiName, Boolean createdByMe, String recordId, Boolean isDownloadable,
                                        List<String> downloadFields, String whereClause
                                       ) 
    {
        String queryStr = 'Select ';
        String fields  = ' ';
        // Add 1 just to Check if it has more number of records.
        //   maxNoOfRecord = maxNoOfRecord + 1;                
        if(fieldApiName != null){
            for(String str : fieldApiName){
                fields +=  str + ',';
            }
        }

        //***[G.T.] - to get download content
        System.debug('***isDownloadable: ' +isDownloadable);
        if(isDownloadable){
           for(String str2 : downloadFields){
                fields +=  str2 + ',';
            } 
        }

        //***[G.T.] - For Object Componente Costo
        if(sobjApiName == 'PT_Componente_Costo__c'){
            fields = fields + 'ProtocolloRic__c' + ',';
        }
           
        // remove last , from field string         
        fields  = fields.substring(0, fields.length()-1);                  
        queryStr = queryStr + fields + ',ownerId from ' + sobjApiName;
        
        /*if(isSearch){
            // Add 
            if(searchTxt!= null && searchTxt != ''){                        
                queryStr = queryStr  +' where ';
                for(String fieldName  : serarchField){
                    queryStr = queryStr + ' ' + fieldName + ' like \'%'+ searchTxt +'%\' OR ';
                }
                
                // Remove last OR operator 
                queryStr = queryStr.substring(0, queryStr.length()-3);
                
                
            }
        } */        
        
        if(createdByMe){
            Id runninguserId = UserInfo.getUserId();
            queryStr = queryStr  +' where createdById='+'\''+runninguserId+'\'';
        }
        
        //***[G.Tedesco] - Filter to get context page
        System.debug('**recordId: ' +recordId);
        String filter='ProtocolloCaricamento__c =' +'\''+recordId+'\'';

        if(filter <> null && filter.trim()<>''){
            if(createdByMe){
                queryStr +=' AND '+filter;
            }
            else{
                queryStr += ' where '+filter;
            }
        }

        System.debug('***whereClause: ' +whereClause);

        //[G.Tedesco] - Another filter inserted in design
        if(whereClause != ''){
            queryStr += ' AND '+whereClause;
            System.debug('***queryStr+whereClause: ' +queryStr);
            System.debug('***whereClause: ' +whereClause);

        }

        queryStr = queryStr + ' LIMIT ' + maxNoOfRecord + ' OFFSET '   + sObjOffset;

        system.debug('*** queryStr: '+queryStr);
        List<SObject> contentlst2 = new List<SObject>();

        contentlst2 = Database.query(queryStr);

        System.debug('*** contentlst2: '+contentlst2);

       
        return queryStr;


    }    
    
    // Wrapper Class for the pagination
    public class PaginationWrapper{
        @AuraEnabled
        public Boolean hasNext {get;set;}
        @AuraEnabled
        public List<Sobject> sobjLst {get;set;}
        @AuraEnabled
        public List<Sobject> sobjLst2 {get;set;}

       
        public PaginationWrapper(){
            hasNext = false;
            sobjLst  = new List<Sobject>();
            sobjLst2  = new List<Sobject>();
        }
        
    }


    @AuraEnabled
    public static void getDownloadableContent(String sobjApiName, String recordId, List<String> downloadField){

        System.debug('***sobjApiName: ' +sobjApiName);
        System.debug('***recordId: ' +recordId);
        System.debug('***downloadField: ' +downloadField);

        String queryStr = 'Select ';
        String fields  = ' ';
        
        for(String str : downloadField){
            fields +=  str + ',';
        }

        fields  = fields.substring(0, fields.length()-1);
        queryStr = queryStr + fields + ' from ' + sobjApiName;

        String whereClause='ProtocolloRic__c =' +'\''+recordId+'\'';

        queryStr += ' where '+whereClause;

        System.debug('*** queryStr: '+queryStr);
        
        List<SObject> contentlst = new List<SObject>();

        contentlst = Database.query(queryStr);

        System.debug('*** contentlst: '+contentlst);


    }


    
}