@isTest 
private class RecordTypeControllerTest
{
    static testMethod void testMethod1() 
    {
        RecordTypeController.fetchRecordTypeValues();
        RecordTypeController.getRecTypeId('Master');
        RecordTypeController.fetchRecordTypeValuesReq();
        RecordTypeController.getRecTypeIdReq('Master');
    }
     /* 
    Author : Priyanka Sathyamurthy
    Apex Method : richiestaCreation
    CreatedDate : 28/07/2017
    Description : Test class for richiestaCreation 
   */   
    private static testMethod void richiestaCreation(){
			 PT_Richiesta__c request = PT_TestDataFactory.createRequest();        
        	request.RecordTypeId='0127E00000092Vi';
        	RecordTypeController.CreateNewRichiesta(request);
    } 
}