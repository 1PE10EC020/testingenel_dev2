@IsTest
public class TaskHelper_Test {
  @testSetup
  private static void setup() {
  
	
	User frontOffice = CreateTestUser('Operatore CM I Livello', 'Front Office');
     frontOffice.username = 'classTestEmailHelperFO@enel.test.com';
    insert frontOffice;
    
    User backOffice = CreateTestUser('Operatore CM I Livello', 'Back Office');
    backoffice.username = 'classTestEmailHelperBO@enel.test.com';
    insert backOffice;
    
    User secondoliv = CreateTestUser('Operatore CM II Livello', 'Lazio Abruzzo e Molise');
    secondoliv.username = 'classTestEmailHelperSEcondliv@enel.test.com';
    insert secondoliv;
    user secliv = CreateTestUser('Operatore CM II Livello', 'Lazio Abruzzo e Molise');
    secliv.username = 'classTestEmailHelperSEcondliv2@enel.test.com';
    insert secLiv;
    
    
    
  }

	private static  EmailMessage generateEmailMessage(Case c){
		EmailMessage email = new EmailMessage();
		email.parentId = c.id;
		email.incoming = true;
		//email.status ='Read';
		return email;
	}
	
	 private static User CreateTestUser(string profileName, string rolename){
            User u = new User();
            u.username = 'classTestEmailHelperBO@enel.test.com';
            u.firstname = 'User';
            u.lastName = 'test';
            u.email = 'userTestEmailHelper@enel.test.com';
            u.alias ='alias';
            u.TimeZoneSidKey = 'GMT';
            u.LocaleSidKey = 'it_IT'; 
            u.EmailEncodingKey='ISO-8859-1';
            u.profileid =  [Select id from profile where name = :profileName limit 1].id;
            u.UserRoleId = [Select id from Userrole where name = : rolename limit 1 ].id;
            u.LanguageLocaleKey ='it';
            return u;
        }
	
	@isTest
	public static void testReciveMail (){
		User secondoliv = [select id from user where username = 'classTestEmailHelperSEcondliv@enel.test.com'];
		case c = new case();
    	c.recordTypeId = [select id from recordtype where developername = :'Da_Definire' ].id ;
    	c.TipologiaCase__c ='Da Definire';
    	c.origin = 'Email';
    	//c.ownerid =  [select id from Group where type='Queue' and  name  = 'Lazio Abruzzo e Molise' limit 1].id;
    	c.ownerid = secondoliv.id;
    	c.status='In carico al II Livello';
    	c.DescrizioneStato__c='In Lavorazione';
    	c.AssegnatoIILivello__c = Datetime.newInstance(2015, 3, 1, 9, 0, 0);
    	c.pod__C = 'testPod';
    	c.DirezioneDTR__c = 'Lazio Abruzzo e Molise' ;
    	c.UnitaDiCompetenza__c ='ROMA ESTERNA-RIETI';
    	c.LivelloTensioneDiFornitura__c ='BT';
    	c.livello1__C ='In prelievo';
    	c.livello2__C = 'Già Connesso';
    	c.livello3__C ='MT/AT' ;
		insert c;
	
		User runningUser =  [select id from user where username = 'classTestEmailHelperSEcondliv@enel.test.com'];
		test.startTest();
		system.runAs(runningUser){
			 c = [select id from case limit 1];
			Emailmessage mail = generateEmailMessage(c);
			insert mail;
		
		}
		test.stopTest();
	}
	
	
	
	
	@isTest
	public static void testCloseTask (){
		User secondoliv = [select id from user where username = 'classTestEmailHelperSEcondliv@enel.test.com'];
		case c = new case();
    	c.recordTypeId = [select id from recordtype where developername = :'Da_Definire' ].id ;
    	c.TipologiaCase__c ='Da Definire';
    	c.origin = 'Email';
    	//c.ownerid =  [select id from Group where type='Queue' and  name  = 'Lazio Abruzzo e Molise' limit 1].id;
    	c.ownerid = secondoliv.id;
    	c.status='In carico al II Livello';
    	c.DescrizioneStato__c='In Lavorazione';
    	c.AssegnatoIILivello__c = Datetime.newInstance(2015, 3, 1, 9, 0, 0);
    	c.pod__C = 'testPod';
    	c.DirezioneDTR__c = 'Lazio Abruzzo e Molise' ;
    	c.UnitaDiCompetenza__c ='ROMA ESTERNA-RIETI';
    	c.LivelloTensioneDiFornitura__c ='BT';
    	c.livello1__C ='In prelievo';
    	c.livello2__C = 'Già Connesso';
    	c.livello3__C ='MT/AT' ;
		insert c;
	
		User runningUser =  [select id from user where username = 'classTestEmailHelperSEcondliv@enel.test.com'];
		test.startTest();
		system.runAs(runningUser){
			 c = [select id from case limit 1];
			Emailmessage mail = generateEmailMessage(c);
			insert mail;
			task t = [select id, status from task limit 1];
			t.status = Constants.TASK_STATUS_COMPLETED;
			update t ;
		
		}
		test.stopTest();
	}
	
}