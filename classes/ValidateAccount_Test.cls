@IsTest
public class ValidateAccount_Test {
private final static string RT_PERSONA_FISICA = 'Persona_Fisica';
private final static string RT_PERSONA_GIURIDICA = 'Persona_Giuridica';

@IsTest
    public static void  testPersonaFisica_NoErr () {
        list <Account> listAccount = new List<Account>();
        Account acc = CreateAccount(RT_PERSONA_FISICA);
        acc.partitaiva__C = null ;
        acc.email__c = 'gennaro.Esposito@gmail.com';
        listAccount.add(acc);
        acc = CreateAccount(RT_PERSONA_FISICA);
        acc.codicefiscale__C = 'DOEJNA80A41H501Z';
        acc.Nome__C = 'Jane';
        acc.Name = 'Doe';
        acc.email__c = 'Jane.Doe@gmail.com';
        listAccount.add(acc);
        Boolean expectedExceptionThrown = false ;
        try{
            insert listAccount;
        }
        catch(Exception e){
            expectedExceptionThrown = true;
            system.debug('Exception : '+e.getmessage());
        }
        System.AssertEquals(expectedExceptionThrown, false);
    }   
    
@IsTest
    public static void  testPersonaFisica_withErr () {
    list <Account> listAccount = new List<Account>();
        Account acc = CreateAccount(RT_PERSONA_FISICA);
        acc.partitaiva__C = null ; 
        acc.email__c = 'Gennaro.esposito@gmail.com';
        listAccount.add(acc);
        acc = CreateAccount(RT_PERSONA_FISICA);
        acc.codicefiscale__C = 'DOEAAA80A41H501L';
        acc.email__c = 'Jane.Doe@gmail.com';
        acc.Nome__C = 'Jane';
        acc.Name = 'Doe';
        listAccount.add(acc);
        
        try{
            insert listAccount;
        }
        catch(Exception e){
             system.debug('exception '+e.getmessage());
            Boolean expectedExceptionThrown =  e.getMessage().contains(Label.CF_First_6_Letter_Not_Valid) ? true : false;
            system.debug('expectedExceptionThrown '+expectedExceptionThrown );
            System.AssertEquals(expectedExceptionThrown, true);
        
        }
    
    }

@IsTest
    public static void  testPersonaGiuridica_NoErr () {
        list <Account> listAccount = new List<Account>();
        Account acc = CreateAccount(RT_PERSONA_GIURIDICA);
        acc.codicefiscale__C = '13378520152';
        listAccount.add(acc);
        acc = CreateAccount(RT_PERSONA_GIURIDICA);
        acc.codicefiscale__C = '07643520567';
        acc.partitaiva__C = '07643520567';
        acc.Nome__C = 'Jane';
        acc.Name = 'Doe';
        acc.email__c = 'Jane.Doe@gmail.com';
        listAccount.add(acc);
            Boolean expectedExceptionThrown = false ;
        try{
            insert listAccount;
        }
        catch(Exception e){
            expectedExceptionThrown = true;
        }
        System.AssertEquals(false,expectedExceptionThrown);
    }   

@IsTest
    public static void  testPersonaGiuridica_withErr () {
        list <Account> listAccount = new List<Account>();
        Account acc = CreateAccount(RT_PERSONA_GIURIDICA);
        acc.codicefiscale__C = '13378520152' ; 
        listAccount.add(acc);
        acc = CreateAccount(RT_PERSONA_GIURIDICA);
        acc.codicefiscale__C = '07643520567';
        acc.partitaiva__C = '07643520569';
        acc.Nome__C = 'Jane';
        acc.Name = 'Doe';
        acc.email__c = 'Jane.Doe@gmail.com';
        listAccount.add(acc);
    
        try{
            insert listAccount;
        }
        catch(Exception e){
                    Boolean expectedExceptionThrown =  e.getMessage().contains(Label.PIVA_CheckDigit_Wrong) ? true : false;
            System.AssertEquals(true,expectedExceptionThrown);
        }
        
    }   
    
    
    private static map<String,Id> MapRTAccountName_ID {
        get {
                if ( MapRTAccountName_ID == null){
                     MapRTAccountName_ID = new map<string,id>();
                    list<RecordType> listOfrecordtype = [Select id, developerName from RecordType where SobjectType = 'Account'];
                    if ( listOfrecordtype != null &&  !listOfrecordtype.isEmpty()){
                        for (RecordType rt : listOfrecordtype )
                            MapRTAccountName_ID.put(rt.developerName, rt.id);
                    
                    }
                    
                }
                return MapRTAccountName_ID;
        
        }
        private set;
    
    }
    
    private static Account CreateAccount (string RT_DeveloperName){
    
        Account acc = new Account();
        acc.recordtypeid = MapRTAccountName_ID.get(RT_DeveloperName);
        acc.codicefiscale__C = 'SPSGNR80A01F839U';
        acc.nome__C ='Gennaro';
        acc.name ='Esposito';
        acc.partitaiva__C = '13378520152';
        
        return acc; 
    
    
    }

}