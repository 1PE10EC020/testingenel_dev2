@IsTest
public class WSPedCase_Test {

     @testSetup
    private static void setup() {   
        
        Account acc = new Account();
        acc.CodiceFiscale__c =  'BNCMHL88A01H146I';
        acc.name = 'Bianchi';
        Acc.nome__c ='Michele';
        acc.email__c='prova@test.com';
        acc.recordtypeid = [select id from recordtype where developername = :Constants.ACCOUNT_PERSONA_FISICA  limit 1].id;
        acc.phone = '3333111';
        insert acc;
        
            
        case parentCase = new Case ();
        case c = new case();
        c.recordTypeId = [select id from recordtype where developername = :'Da_Definire' ].id ;
        c.TipologiaCase__c ='Da Definire';
        c.origin = 'Email';
        c.status='In carico al II Livello';
        c.DescrizioneStato__c='In Lavorazione';
        c.AssegnatoIILivello__c = Datetime.newInstance(2015, 3, 1, 9, 0, 0);
        //c.pod__C = 'testPod';
        //c.DirezioneDTR__c = 'Lazio Abruzzo e Molise' ;
        //c.UnitaDiCompetenza__c ='ROMA ESTERNA-RIETI';
        c.LivelloTensioneDiFornitura__c ='BT';
        c.livello1__C ='In prelievo';
        c.livello2__C = 'Già Connesso';
        c.livello3__C ='MT/AT' ;
        
        /**/
        
        c.Subject = 'test Ped';
        c.Priority = '';
        c.Argomenti__c='Da Definire';
        c.Famiglia__c='';
        c.SituazionePrestazioneSottesa__c='';
        c.SottofamigliaSituazionePrestazionesot__c='';
        
        c.AssegnatoIILivello__c = null;
        c.tech_IsStatusIILivello__c = true;
        c.Description = 'Descrizione Zona';
        c.Eventuali_Note_Richiedente__c = 'Note richiedente';
        c.Famiglia__c = '';
        c.POD__c ='IT001E020070030';
        c.DirezioneDTR__c = 'Piemonte';
        
        
        
       
        c.UnitaDiCompetenza__c = 'Triveneto';
        c.IdPratica__c = '2302300';
        c.Danni__c = 'danni';
        c.ReiteroDaAltroCanale__c = true;
        
        
        
        //User backOffice = CreateTestUser(Constants.PROFILE_I_LIV, 'Back Office');
        
        
        /*
        
        c.OwnerId= [select name, id from Group where type='Queue' and name like 'ALESSANDRIA%' limit 1].Id;
        //c.OwnerId = '00G26000000UX3ZEAW';
        system.debug(logginglevel.debug,' *** Test OwnerId :'+c.OwnerId);
        */
        
        UnitaDiCompetenza__c unitaDiCompetenza = new UnitaDiCompetenza__c();
        unitaDiCompetenza.name = 'ALESSANDRIA-ASTI';
        unitaDiCompetenza.TipoUnitOrganizzativa__c = 'Zona';
        insert unitaDiCompetenza;
       
        /**/
        
        insert c;

    }
    @isTest
    static void testCaseCreation (){
        WSPedCase.WSPedCase_Input input = new WSPedCase.WSPedCase_Input();
        // create case with minimal information 
        input.Email='prova@test.com';
        input.Cognome='';
        input.Nome='';
        input.CodiceFiscale='';
        input.Telefono='321333321';
        input.Tipo='Informazioni';
        input.Livello1='In prelievo';
        input.Livello2='In Connessione';
        input.Livello3='Non Orario';
        //input.CaseRecordType='Misura';
        input.ParentCase='';
        input.Descrizione='test';
        input.ServizioFunzionalita='test';
        input.DataOccorrenzaProblema='test';
        input.POD='';
        
        input.CaseRecordType = 'PROGETTI';
        input.Oggetto = 'test Ped';
        input.Priorita = '';
        input.Argomenti='Info+';
        input.Famiglia='';
        input.SituazionePrestazioneSottesa='';
        input.SottofamigliaSituazionePrestazionesot='';
        
        WSPedCase.WSPedCase_output  output;
        output = WSPedCase.CreatePEDCase(input);
        System.assertEquals ('000', output.codice);
    }
    
    @isTest
    static void testCaseCreation5 (){
        WSPedCase.WSPedCase_Input input = new WSPedCase.WSPedCase_Input();
        // create case with minimal information 
        input.Email='prova@test.com';
        input.Cognome='Antonio';
        input.Nome='Magrelli';
        input.CodiceFiscale='CPTSDS32F537L';
        input.Telefono='321333321';
        input.Tipo='Informazioni';
        input.Livello1='In prelievo';
        input.Livello2='In Connessione';
        input.Livello3='Non Orario';
        //input.CaseRecordType='Misura';
        input.ParentCase='';
        input.Descrizione='test';
        input.ServizioFunzionalita='test';
        input.DataOccorrenzaProblema='test';
        input.POD='';
        
        input.CaseRecordType = 'Connessione';
        input.Oggetto = 'test Ped';
        input.Priorita = '';
        input.Argomenti='Connessione';
        input.Famiglia='';
        input.SituazionePrestazioneSottesa='';
        input.SottofamigliaSituazionePrestazionesot='';
        
        WSPedCase.WSPedCase_output  output;
        output = WSPedCase.CreatePEDCase(input);
        System.assertEquals ('000', output.codice);
    }
    
        @isTest
    static void testCaseCreation1 (){
        WSPedCase.WSPedCase_Input input = new WSPedCase.WSPedCase_Input();
        
        // create case without a parent case  and no RT 
        input.Email='prova@test.com';
        input.Cognome='Bianchi';
        input.Nome='Michele';
        input.CodiceFiscale='BNCMHL88A01H146I';
        input.Telefono='321333321';
        input.Tipo='Informazioni';
        input.Livello1='In prelievo';
        input.Livello2='In Connessione';
        input.Livello3='Non Orario';
        input.CaseRecordType='Misura';
        input.ParentCase='';
        input.Descrizione='test';
        input.ServizioFunzionalita='test';
        input.DataOccorrenzaProblema='test';
        input.POD='';
        input.Oggetto = 'test Ped';
        input.Priorita = '';
        input.Argomenti='Misura';
        input.Famiglia='';
        input.SituazionePrestazioneSottesa='';
        input.SottofamigliaSituazionePrestazionesot='';
        
        WSPedCase.WSPedCase_output  output;
        output =  WSPedCase.CreatePEDCase(input);
        System.assertEquals ('000', output.codice);
    }
    
    @isTest
    static void testCaseCreation2 (){
        WSPedCase.WSPedCase_Input input = new WSPedCase.WSPedCase_Input();
        // create case without a parent case  and wrong RT 
        input.Email='prova@test.com';
        input.Cognome='Bianchi';
        input.Nome='Michele';
        input.CodiceFiscale='';
        input.Telefono='321333321';
        input.Tipo='Informazioni';
        input.Livello1='In prelievo';
        input.Livello2='In Connessione';
        input.Livello3='Non Orario';
        input.CaseRecordType='Misura';
        input.ParentCase='';
        input.Descrizione='test';
        input.ServizioFunzionalita='test';
        input.DataOccorrenzaProblema='test';
        input.POD='';
        
        input.Oggetto = 'test Ped';
        input.Priorita = '';
        input.Argomenti='Misura';
        input.Famiglia='';
        input.SituazionePrestazioneSottesa='';
        input.SottofamigliaSituazionePrestazionesot='';
        WSPedCase.WSPedCase_output  output;
        output =  WSPedCase.CreatePEDCase(input);
        System.assertEquals ('000', output.codice);
    }
        @isTest
    static void testCaseCreation3 (){
        WSPedCase.WSPedCase_Input input = new WSPedCase.WSPedCase_Input();
        // create case with a parent case  
        input.Email='test@provaaccenture.com';
        input.Cognome='Rolando';
        input.Nome='Cavalli';
        input.CodiceFiscale='RLNCLL87A01H501Z ';
        input.Telefono='321333321';
        input.Tipo='Informazioni';
        input.Livello1='In prelievo';
        input.Livello2='In Connessione';
        input.Livello3='Non Orario';
        input.CaseRecordType='Misura';
        input.ParentCase= [select casenumber from case limit 1].casenumber;
        input.Descrizione='test';
        input.ServizioFunzionalita='test';
        input.DataOccorrenzaProblema='test';
        input.POD='';
        
        input.Oggetto = 'test Ped';
        input.Priorita = '';
        input.Argomenti='Misura';
        input.Famiglia='';
        input.SituazionePrestazioneSottesa='';
        input.SottofamigliaSituazionePrestazionesot='';
        
        WSPedCase.WSPedCase_output  output;
        output =  WSPedCase.CreatePEDCase(input);
        
        System.assertEquals ('000', output.codice);
    }
    @isTest
    static void testCaseCreation4 (){
        WSPedCase.WSPedCase_Input input = new WSPedCase.WSPedCase_Input();
        // create a case with pod
        FOURTypes.DashboardArrReplay_element expetedResponse = Four_Helper_Test.generateFakeResponse ('N',Constants.LIVELLO_TENSIONE_FORNTITURA_BT,Constants.WS_FASCIA_ORARIA,'Lazio Abruzzo e Molise','ROMA ESTERNA-RIETI', 'SPSGNR80A01F839U','07643520567','s','Gennaro','Esposito','');
        Test.setMock(WebServiceMock.class, new FOURService_Moch(expetedResponse));  
        input.Email='prova@test.com';
        input.Cognome='Bianchi';
        input.Nome='Michele';
        input.CodiceFiscale='';
        input.PartitaIva='12345678998';
        input.Telefono='321333321';
        input.Tipo='Informazioni';
        input.Livello1='In prelievo';
        input.Livello2='In Connessione';
        input.Livello3='Non Orario';
        input.CaseRecordType='Misura';
        input.ParentCase= [select casenumber from case limit 1].casenumber;
        input.Descrizione='test';
        input.ServizioFunzionalita='test';
        input.DataOccorrenzaProblema='test';
        input.POD='UTAK00001';
        input.Oggetto = 'test Ped';
        input.Priorita = '';
        input.Argomenti='Misura';
        input.Famiglia='';
        input.SituazionePrestazioneSottesa='';
        input.SottofamigliaSituazionePrestazionesot='';
        
        WSPedCase.WSPedCase_output  output;
        output =  WSPedCase.CreatePEDCase(input);
        System.assertEquals ('000', output.codice);
    
    }
}