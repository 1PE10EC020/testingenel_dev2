global class WorkspaceConnectorBAController {
     
    @RemoteAction
    global static String retrieveWorkspaceConnectorInfo(String reason)
    {   
        system.debug('*** retrieveWorkspaceConnectorInfo');
        String myId = UserInfo.getUserId();
        myId = 'GenesysConnector' + myId;
        try{
            WorkspaceConnectorInfo__c connector = [SELECT iD, objectId__c, interactionId__c From WorkspaceConnectorInfo__c WHERE genesysId__c = :myId];
            system.debug('*** connector = '+connector);
            String ret = '{"sfdcObjectId":"' + connector.objectId__c + '","id":"' + connector.interactionId__c + '"}';
            return ret;   
        }
        catch(QueryException e){
            return 'none';
        }
    }
    
    
    
    @RemoteAction
    global static void setConnIdWorkspaceConnectorInfo(String jsonData){
        system.debug(logginglevel.debug,'setConnIdWorkspaceConnectorInfo.setConnIdWorkspaceConnectorInfo - manageInboundCall - inbound parameter jsonData : '+jsonData);
        
        CtiData inData = (CtiData) JSON.deserialize(jsonData, CtiData.class);
        system.debug(logginglevel.debug, 'WorkspaceConnectorBAController.setConnIdWorkspaceConnectorInfo - manageInboundCall - inData :'+inData);
        setWorkspaceConnectorInfo(inData.CONNID, inData.CONNID);
    }
    
    
    
    
    
    @RemoteAction
    global static boolean isPresentWorkspaceConnectorInfo(String connectionId)
    {   
        system.debug('*** isPresentWorkspaceConnectorInfo');
        try{
            WorkspaceConnectorInfo__c connector = [SELECT iD, objectId__c, interactionId__c From WorkspaceConnectorInfo__c WHERE interactionId__c = :connectionId];
            system.debug('*** connector = '+connector);
            if (connector != null)
                return true;
            else
                return false;
        }
        catch(QueryException e){
            return false;
        }
    }
    
    
    @RemoteAction
    global static string setWorkspaceConnectorInfo(String objectId, String interactionId)
    {           
        system.debug('*** setWorkspaceConnectorInfo objectId = '+objectId+' ,interactionId = '+interactionId);
        try{
            String myId = UserInfo.getUserId();
            myId = 'GenesysConnector' + myId;
            WorkspaceConnectorInfo__c myCustomObject = new WorkspaceConnectorInfo__c (
                name = 'GenesysConnector', interactionId__c = interactionId, objectId__c = objectId,genesysId__c = myId);
            //update/insert myCustomObject; 
            upsert myCustomObject genesysId__c;
            system.debug('*** myCustomObject = ' + myCustomObject );
            String urlForDetailPage = new PageReference('/' + myCustomObject.id).getUrl();
            system.debug('*** url = '+ urlForDetailPage );
            return 'success';   
        }
        catch(QueryException e){
            return 'error';
        }
    }

    
    @RemoteAction
    global static Case findCaseFromNumber(String num)
    {
        system.debug('[WSC] findCaseFromNumber using  ' + num);       
        try {
           List<Case> cases = [SELECT CaseNumber FROM Case WHERE CaseNumber= :num];            
           if(!cases.isEmpty()) {
               // return the first case found (should only be one)                            
               for (Case c : cases) {
                   return c;
               } 
           }
        } catch(QueryException e){
            return null; 
        }
        return null; 
    }
 
   
/*
    @RemoteAction
    global static Object findObjectFromANI(String ANI)
    {
        system.debug('*** findObjectFromANIfor '+ANI);       
        try{
            
            List<List<SObject>> objects = [FIND :ANI IN PHONE FIELDS RETURNING Account];
            if (!objects.isEmpty()){
              for (List<SObject> objList : objects)
                  for (SObject obj : objList){
                    system.debug('*** findObjectFromANI account = '+ obj.Id);
                    return obj;
                  } 
            }                
            
            try{
            List<List<SObject>> cobjects = [FIND :ANI IN PHONE FIELDS RETURNING Contact];
            if (!cobjects.isEmpty()){
              for (List<SObject> objList : cobjects)
                  for (SObject cobj : objList){
                    system.debug('*** findObjectFromANI contact = '+ cobj.Id);
                    return cobj;
                  } 
            }                
            return null; 
            }
            catch(QueryException e){
                return null; 
            }
        }
        catch(QueryException e){
            return null; 
        }        
    }
*/    
    
    @RemoteAction
    global static Object findContactFromANI(String ANI)
    {
        system.debug('*** findContactFromANI '+ANI);       
        try{                                  
            List<List<SObject>> cobjects = [FIND :ANI IN PHONE FIELDS RETURNING Contact];
            Integer listSize = cobjects.size();
            system.debug('*** listSize = ' + listSize);
            if(listSize > 1){
                    return 'multiple found'; //not expected
            }
            if (!cobjects.isEmpty()){
              List<Contact> contacts = ((List<Contact>)cobjects[0]);
              listSize = contacts.size();
              system.debug('*** contacts listSize = ' + listSize);
              if(listSize > 1){
                    return 'multiple found';
              }
              if(listSize == 0){
                  return 'not found';
              }
              for (List<SObject> objList : cobjects)
                  for (SObject cobj : objList){
                    system.debug('*** findContactFromANI contact = '+ cobj.Id);
                    return cobj;
                  } 
            }                
            return null; 

        }
        catch(QueryException e){
            return null; 
        }        
    }

    @RemoteAction
    global static Object findContactFromEmailAddress(String address)
    {
        system.debug('*** findObjectFromEmailAddress' + address);       
        try {
            List<Contact> objects = [select name from contact where email= :address ];
            if (!objects.isEmpty()) {
                  for (Contact obj : objects) {
                    system.debug('*** findContactFromEmailAddress contact = '+ obj.Name);
                    return obj;
                  } 
            }                
            return null; 
        }
        catch(QueryException e){
            return null; 
        }        
    }
    
    @RemoteAction
    global static Object findContactFromChatAddress(String theName)
    {
        system.debug('*** findObjectFromChatAddress' + theName);       
        try {
            List<Contact> objects = [select name from contact where name= :theName];
            if (!objects.isEmpty()) {
                  for (Contact obj : objects) {
                    system.debug('*** findObjectFromChatAddresscontact = '+ obj.Name);
                    return obj;
                  } 
            }                
            return null; 
        }
        catch(QueryException e){
            return null; 
        }        
    }

        
    @RemoteAction
    global static Object findContactFromWorkItemAddress(String theName)
    {
        system.debug('*** findContactFromWorkItemAddress' + theName);       
        try {
            List<Contact> objects = [select name from contact where name= :theName];
            if (!objects.isEmpty()) {
                  for (Contact obj : objects) {
                    system.debug('*** findContactFromWorkItemAddress = '+ obj.Name);
                    return obj;
                  } 
            }                
            return null; 
        }
        catch(QueryException e){
            return null; 
        }        
    }    


    @RemoteAction
    global static Object findContactFromOpenMediaAddress(String theName)
    {
        system.debug('*** findContactFromOpenMediaAddress' + theName);       
        try {
            List<Contact> objects = [select name from contact where name= :theName];
            if (!objects.isEmpty()) {
                  for (Contact obj : objects) {
                    system.debug('*** findContactFromOpenMediaAddress = '+ obj.Name);
                    return obj;
                  } 
            }                
            return null; 
        }
        catch(QueryException e){
            return null; 
        }        
    }
 

    @RemoteAction global static String createActivity(Map<String,String> activityMap){
        system.debug('*** createActivity' );
        String result = 'not found';
        
         String ANI = activityMap.get('ANI');
        String lookupSource = activityMap.get('LOOKUP');
        system.debug('*** createActivity for '+lookupSource + ' - ANI = '+ANI);
        system.debug('*** duration = ' + activityMap.get('Call Duration'));
        String callType = activityMap.get('IXN Type');
        String mediaType = activityMap.get('Media Type');
        String subject = callType + ' ' + mediaType + ' ' + activityMap.get('DATE'); 
        String objectToUse = '';
        String duration = activityMap.get('Call Duration');
        String hours = duration.substring(0,2);
        String mins  = duration.substring(3,5);
        String secs  = duration.substring(6,8);
        //system.debug('*** hours = '+hours +',mins = '+mins+ ',secs = '+secs);
        
        Integer hh = Integer.valueOf(hours);
        Integer mm = Integer.valueOf(mins);
        Integer ss = Integer.valueOf(secs);  
             
        
        Integer durationInSecs = ss + (mm*60) + (hh*3600);
        system.debug('*** durationInSecs = '+ durationInSecs );
                
          if(activityMap.get('sfdc Object Id')!= ''){
                system.debug('*** createActivity sfdc Object Id = ' + activityMap.get('sfdc Object Id'));
                objectToUse = activityMap.get('sfdc Object Id');
        }
        
        if(objectToUse == ''){
           object objectFromLookup;

            if(callType == 'Email')
            {
                objectFromLookup = findContactFromEmailAddress(lookupSource);
            }
            else if(callType == 'Chat')
            {
                objectFromLookup = findContactFromChatAddress(lookupSource);
            }
            else if(callType == 'InteractionWorkItem')
            {    
                objectFromLookup = findContactFromWorkItemAddress(lookupSource);
            }
            else if(callType == 'OpenMedia')
            {
                objectFromLookup = findContactFromOpenMediaAddress(lookupSource);
            }
            else
            {
                objectFromLookup = findContactFromANI(lookupSource);
            }


          if(objectFromLookup != null && objectFromLookup != 'not found' && objectFromLookup != 'multiple found'){
                    Contact c = (Contact)objectFromLookup;
                    if(c != null){
                        system.debug('*** createActivity c Id = ' + c.Id);
                        objectToUse = c.Id;
                    }
          }

        }
        
        system.debug('*** createActivity for object ' + objectToUse);
        
        String accountPrefix = Schema.SObjectType.Account.getKeyPrefix();
        String contactPrefix = Schema.SObjectType.Contact.getKeyPrefix();
        
        String prefix = '';
        if(objectToUse != '')
            prefix = objectToUse.substring(0, 3);
        system.debug('*** prexix = '+prefix);
          
        if(prefix == contactPrefix)
        {
          Contact contact= [SELECT AccountId, Id FROM Contact WHERE Id= :objectToUse];
          system.debug('*** create task for contact');
          Task t = new Task (WhatId = contact.AccountId,
            WhoId = objectToUse,
            Type = 'Call',
            Status = 'Completed',
            Subject = subject,
            CallDurationInSeconds = durationInSecs,            
            Interaction_Type__c = callType,
            Description = activityMap.get('Comments'),
            CallDisposition = activityMap.get('Disposition'),
            CallObject = activityMap.get('GenesysId')
          );
                  
        String mySFDCfield = '';
        String mySFDCvalue = '';
        if (activityMap.get('SFDC1field') != '' && (activityMap.get('SFDC1field') != null) && activityMap.get('SFDC1value') != '' && activityMap.get('SFDC1value') != null)
        {
            mySFDCfield = activityMap.get('SFDC1field');
            mySFDCvalue = activityMap.get('SFDC1value');
            system.debug('*** mySFDCfield1 ' + mySFDCfield + ' -- ' + mySFDCvalue);
            t.put(mySFDCfield,mySFDCvalue);
        }
        if (activityMap.get('SFDC2field') != '' && (activityMap.get('SFDC2field') != null) && activityMap.get('SFDC2value') != '' && activityMap.get('SFDC2value') != null)
        {
            mySFDCfield = activityMap.get('SFDC2field');
            mySFDCvalue = activityMap.get('SFDC2value');
            system.debug('*** mySFDCfield2 ' + mySFDCfield + ' -- ' + mySFDCvalue);
            t.put(mySFDCfield,mySFDCvalue);
        }
        if (activityMap.get('SFDC3field') != '' && (activityMap.get('SFDC3field') != null) && activityMap.get('SFDC3value') != '' && activityMap.get('SFDC3value') != null)
        {
            mySFDCfield = activityMap.get('SFDC3field');
            mySFDCvalue = activityMap.get('SFDC3value');
            system.debug('*** mySFDCfield3 ' + mySFDCfield + ' -- ' + mySFDCvalue);
            t.put(mySFDCfield,mySFDCvalue);
        }
        if (activityMap.get('SFDC4field') != '' && (activityMap.get('SFDC4field') != null) && activityMap.get('SFDC4value') != '' && activityMap.get('SFDC4value') != null)
        {
            mySFDCfield = activityMap.get('SFDC4field');
            mySFDCvalue = activityMap.get('SFDC4value');
            system.debug('*** mySFDCfield4 ' + mySFDCfield + ' -- ' + mySFDCvalue);
            t.put(mySFDCfield,mySFDCvalue);
        }
        if (activityMap.get('SFDC5field') != '' && (activityMap.get('SFDC5field') != null) && activityMap.get('SFDC5value') != '' && activityMap.get('SFDC5value') != null)
        {
            mySFDCfield = activityMap.get('SFDC5field');
            mySFDCvalue = activityMap.get('SFDC5value');
            system.debug('*** mySFDCfield5 ' + mySFDCfield + ' -- ' + mySFDCvalue);
            t.put(mySFDCfield,mySFDCvalue);
        }            
            
          try{                        
                insert t;
          }
          catch(QueryException e){
                    return 'not found'; 
          }
          system.debug('*** Contact Task id = '  +t.Id);
          result = t.Id;
          return result;
        }
        
        if(prefix == '')
        {
          Task t = new Task (
            // WhatId = objectToUse,
            // WhoId = acc.Id,
            Type = 'Call',
            Status = 'Completed',
            Subject = subject,
            CallDurationInSeconds = durationInSecs,            
            Interaction_Type__c = callType,
            Description = activityMap.get('Comments'),
            CallDisposition = activityMap.get('Disposition'),
            CallObject = activityMap.get('GenesysId')
          );
          try{                        
                insert t;
          }
          catch(QueryException e){
                    return 'not found'; 
          }
          system.debug('*** Orphan Task id = '  +t.Id);
          result = t.Id;                        
        }
   
        return result;
     }   


/*
   @RemoteAction
    global static String findAccount(String searchFieldName, String searchFieldValue){
        system.debug('*** findAccount: searchFieldName - ' + searchFieldName +', searchFieldValue - ' + searchFieldValue);
        try{

           String query = 'SELECT Id, Name, Phone FROM Account WHERE ' + searchFieldName  + ' = \'' + searchFieldValue + '\'';
           system.debug('*** findAccount: query = ' + query);            
           List<Account> accounts = Database.query(query);
           //List<Account> accounts = [SELECT Id, Name, Phone FROM Account WHERE Phone = :searchFieldValue]; 
            if (!accounts.isEmpty()){
                Integer listSize = accounts.size();
                if(listSize > 1){
                    return 'multiple found';
                }
                for (Account a : accounts){
                    system.debug('***  Id = ' + a.Id);
                    system.debug('***  Name = '+ a.Name);
                    system.debug('*** Phone = '+ a.Phone);
                    //return json string id  and full name
                    return '{"id":"' + a.Id + '","name":"' + a.Name + '"}';
                }
            }
        }
        catch(QueryException e){
                return 'QueryException ' + e; 
        }
        //check contact
        String query = 'SELECT Id, Name, Phone FROM Contact WHERE ' + searchFieldName  + ' = \'' + searchFieldValue + '\'';
        system.debug('*** findAccount - contact: query = ' + query);            
        List<Contact> contacts = Database.query(query);
        if (!contacts.isEmpty()){
                Integer listSize = contacts.size();
                if(listSize > 1){
                    return 'multiple found';
                }
                for (Contact c: contacts ){
                    system.debug('***  Id = ' + c.Id);
                    system.debug('***  Name = '+ c.Name);
                    system.debug('*** Phone = '+ c.Phone);
                    //return json string id  and full name
                    return '{"id":"' + c.Id + '","name":"' + c.Name + '"}';
                }
            }
        
        return 'not found';
    }    
*/


    /* 
    * Autor : Claudio Quaranta <claudio.quaranta@accenture.com>
    * Createddate : 13-07-2015
    * Description : class used to wrap the manageInboundCallENEL_Distr response.
    *               
    */ 

    global class IVR_Redirect{
        public string caseRef;
        public string phone;
        public string ivr;
        public string connid ;
        public string dn;
        public string tenant ;
        public string idpratica ;
        public string pod;
        public string caseNum;
        /*
        *  constructor with caseID or CaselistId argument
        */
        public IVR_Redirect (string resIn){
            caseRef = resIn;
             phone='';
             ivr='';
             connid ='';
             dn='';
             tenant ='';
             idpratica ='';
             pod='';
             caseNum='';
        }
        
        /*
        *  constructor with no argument
        */
        public IVR_Redirect (){
             caseRef='';
             phone='';
             ivr='';
             connid ='';
             dn='';
             tenant ='';
             idpratica ='';
             pod='';
             caseNum='';
        }
    }



    /* 
    * Autor : Claudio Quaranta <claudio.quaranta@accenture.com>
    * Createddate : 13-07-2015
    * Description : remote action , used to parse the CTI data in input from the genesys bar.
    *               All the data are parsed and :
    *           1) if a case number is given in input , the user is redirected to the case detail page (if present), otherwise :
    *           2)  search for an existing account with a given fiscal code ( is present), if an account is found a new case is created else 
                3) search for an existing account with a given phone number, if :
                        - zero account found --> create a case without an account associated
                        - 1 account found --> create a case with the given account
                        - 2 or more --> redirect the user to the custom pate ED_AccountSelector
    * Input :   Name                    Type                    Description
    *           jsonData                String                  a JSON string , contains all userdata passed in input from the IVR
    *output : 
    *           N/A                     String                  the URL where the user should be redirected.
    */  
    
    @RemoteAction
        global static IVR_Redirect manageInboundCallENEL_Distr(String jsonData){
            system.debug(logginglevel.debug,'WorkspaceConnectorBAController - manageInboundCall - inbound parameter jsonData : '+jsonData);
            
            CtiData inData = (CtiData) JSON.deserialize(jsonData, CtiData.class);
            system.debug(logginglevel.debug, 'WorkspaceConnectorBAController - manageInboundCall - inData :'+inData);
        // if CaseNumber is passed, search for the case number
            try{
                if(string.isNotBlank(inData.CASE_NUM)){
                    Case caseFound = Case_Helper.retriveCaseFromCaseNum(inData.CASE_NUM);
                    if (caseFound != null)
                        return new IVR_Redirect(caseFound.id);//(String)caseFound.id;   // return the case Found
                    else
                    {
                        IVR_Redirect result = new IVR_Redirect();
                        result.caseRef =Constants.CASE_NUM_NOT_FOUND;
                        result.phone=inData.PHONE;
                        result.ivr=inData.COD_IVR;
                        result.connid=inData.CONNID;
                      //  result.dn=inData.DN;
                        result.dn = inData.RTargetPlaceSelected; // C.Q 19/11/2015 change requested by IVR
                        result.tenant=inData.TENANT;
                        result.idpratica=inData.ID_PR;
                        result.pod=inData.POD;
                        result.caseNum = inData.CASE_NUM;
                        system.debug('case not found : result is'+result);
                        return result;
                    }
                }
                else{
                    
                    // search Account on fiscal Code is present
                    Account accFound ;
                    if (String.isNotBlank(inData.CF) && inData.CF != null ){//[GP 18102016] aggiunto nell'if && inData.CF != null
                        accFound= Account_Helper.retriveAccountByFiscalCode(inData.CF);
                        system.debug(logginglevel.debug,'WorkspaceConnectorBAController - manageInboundCall -  account retrived by fiscal code : '+accFound);
                    }
                    // if no account were found , search account by phoneNumber
                    if (accFound == null && String.isNotBlank(inData.PHONE)){
                        system.debug(logginglevel.debug,' WorkspaceConnectorBAController - manageInboundCall - searching account by phone');
                        list<Account> listAccountByPhone = Account_Helper.retriveAccountByPhoneNumber(inData.PHONE);
                        if(listAccountByPhone != null && listAccountByPhone.size()>1){
                        // More than 1 account has found. TO-DO manage custom scenario.     
                            /*Pagereference p = Page.ED_AccountSelector;
                            p.getParameters().put(Constants.PHONE_PARAMETER,inData.PHONE);
                            p.getParameters().put(Constants.IVR_PATH_PARAMETER,inData.COD_IVR);
                            p.getParameters().put(Constants.CONNID_PARAMETER,inData.CONNID);
                            p.getParameters().put(Constants.DN_PARAMETER,inData.DN);
                            p.getParameters().put(Constants.TENANT_PARAMETER,inData.TENANT);
                            p.getParameters().put(Constants.IDPRATICA_PARAMETER,inData.ID_PR);
                            p.getParameters().put(Constants.POD_PARAMETER,inData.POD);
                            
                            
                            system.debug(logginglevel.debug,' WorkspaceConnectorBAController - manageInboundCall - url generated :'+p.getUrl());
                            */
                            IVR_Redirect result = new IVR_Redirect();
                            result.caseRef ='NOT FOUND';
                            result.phone=inData.PHONE;
                            result.ivr=inData.COD_IVR;
                            result.connid=inData.CONNID;
                            //result.dn=inData.DN;
                            result.dn = inData.RTargetPlaceSelected; // C.Q 19/11/2015 change requested by IVR
                            result.tenant=inData.TENANT;
                            result.idpratica=inData.ID_PR;
                            result.pod=inData.POD;
                            result.caseNum = inData.CASE_NUM;
                            return result;
                        }
                        if(listAccountByPhone != null && listAccountByPhone.size() == 1){
                            accFound = listAccountByPhone[0];
                        }
                    }
                    system.debug('########## accFound '+accFound);
                    // no case number recived in input, new case should be created.
                    PercorsoTelefonico__c ivrPathSelected = IVR_Helper.findIVRPath(inData.COD_IVR);
                    //Case newCase = Case_Helper.createNewCase(accFound,ivrPathSelected,inData.CONNID,inData.DN,inData.TENANT,inData.POD,inData.ID_PR, Constants.ORIGIN_TELEFONO,inData.PHONE);
                    Case newCase = Case_Helper.createNewCase(accFound,ivrPathSelected,inData.CONNID,inData.RTargetPlaceSelected,inData.TENANT,inData.POD,inData.ID_PR, Constants.ORIGIN_TELEFONO,inData.PHONE);
                    if ( newCase != null){
                        insert newCase;
                        return  new IVR_Redirect(newCase.id);//return the case Found
                    }
                    else 
                    return new IVR_Redirect('500'); 
                }
            }
            catch(Exception e){
                system.debug(logginglevel.error, 'Exception in WorkspaceConnectorBAController - manageInboundCall : '+e.getMessage());
            }
            // return the data in order to redirect on case list view
            return new IVR_Redirect('500'); 
        
        }
        

}