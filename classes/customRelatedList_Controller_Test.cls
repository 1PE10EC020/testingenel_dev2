@IsTest
public class customRelatedList_Controller_Test {
    @testSetup
  	private static void setup() {
  	Account acc = new Account();
  	acc.Nome__c = 'Claudio' ; 
    acc.Name = 'Quaranta';
    acc.codicefiscale__c  ='QRNCLD85B26F839L';
    acc.phone='12312312312';
    insert acc;
  	}
  	
  	
  	@isTest
    static void testController (){
    customRelatedList_Controller contr = new customRelatedList_Controller();
    contr.baseObject ='Account';
    contr.blockTitle ='Account';
    contr.namefields='Id,Nome__c';
    system.debug('displayTable is '+contr.displayTable);
    system.debug('elementToDisplay is '+contr.elementToDisplay);
    system.debug('hasNext is : '+contr.hasNext);
    system.debug('hasPrevious is : '+contr.hasPrevious);
    system.debug('elementToDisplay is '+contr.elementToDisplay);
    system.debug('totalPageNumber is '+contr.totalPageNumber);
    system.debug('currentPageNumber is '+contr.currentPageNumber);
    contr.Next();
    contr.previous();
    }
    
    @isTest
    static void testExceptinController (){
    	customRelatedList_Controller contr = new customRelatedList_Controller();
    	contr.Next();
    	contr.previous();
    	try{
    		system.debug('totalPageNumber is '+contr.totalPageNumber);
    	}
    	catch(Exception e){
    	}
    	try{
    		system.debug('elementToDisplay is '+contr.elementToDisplay);
    	}
    	catch(Exception e){
    	}
    	
    	try{
   			 system.debug('hasPrevious is : '+contr.hasPrevious);
    	}
    	catch(Exception e){
    	}
    	
    	try{
   			 system.debug('hasNext is : '+contr.hasNext);
    	}
    	catch(Exception e){
    	}
    	
    	try{
		    system.debug('elementToDisplay is '+contr.elementToDisplay);
    	}
    	catch(Exception e){
    	}
    	
    	try{
    		system.debug('displayTable is '+contr.displayTable);
    	}
    	catch(Exception e){
    	}
    
    }
}