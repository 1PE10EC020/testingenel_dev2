/* Controller associated with pages rendering the KPI survey.
 * Used by KPI_TEST page
 */

public with sharing class kpi_Controller {
    
    private ApexPages.StandardController stdControl;
    public String nameSurvey{get;set;}
     
    public boolean choiceSurvey{get;set;}
    //Reneder parameter
    public boolean Showtable{get;set;}
    public boolean showTotaleAverage{get;set;}
    public boolean showResponse{get;set;}
    public boolean showQuestion{get;set;}
    
    
    public List<Selectoption> listSurveyName {get;set;}
    public List<Survey_Question__c> listQuestions{get;set;}
    
    
    public questionsDisplayed questionView;
    public List<questionsDisplayed> allQuestions {get;set;}
    
    public List<SurveyQuestionResponse__c> allResponse{get;set;}
    public Integer mediaResponse {get;set;}
    public Integer mediaTotaleResponse {get;set;}
    
    public responseSurvey respSurvey{get;set;}
    public List<responseSurvey> listRespSurvey {get;set;}
    
    public List<SurveyQuestionResponse__c> listResponse;
    public responseSurvey respTotalSurvey{get;set;}
    
    public String nameMonth{get;set;}
    public Integer year{get;set;}
   
    //Parameter for read survey and display in picklist
     public static map<String,id> mapSurveyName {
            get{
                    if(mapSurveyName == null){
                    
                        list<Survey__c> listOfSurvey = [SELECT id,Name FROM Survey__c];
                        if (listOfSurvey != null && !listOfSurvey.isEmpty() ){
                            mapSurveyName = new map<String,id> ();
                            for (Survey__c s :listOfSurvey )
                                mapSurveyName.put(s.Name, s.id);
                        }
                    }
                
                return mapSurveyName;
            }
            private set;
     }
     
         //Parameter for display month in picklist
     public static List<SelectOption> monthNameSelection {
            get{                           
          		List<SelectOption> monthOption = new List<SelectOption>();
       			 monthOption.add(new SelectOption('','-None-'));
       			 monthOption.add(new SelectOption('1', 'Gennaio'));
       			 monthOption.add(new SelectOption('2','Febbraio'));
       			 monthOption.add(new SelectOption('3','Marzo')); 
       			 monthOption.add(new SelectOption('4','Aprile'));
       			 monthOption.add(new SelectOption('5','Maggio'));
       			 monthOption.add(new SelectOption('6','Giugno'));
       			 monthOption.add(new SelectOption('7','Luglio'));
       			 monthOption.add(new SelectOption('8','Agosto'));
       			 monthOption.add(new SelectOption('9','Settembre'));
       			 monthOption.add(new SelectOption('10','Ottobre'));
       			 monthOption.add(new SelectOption('11','Novembre'));
       			 monthOption.add(new SelectOption('12','Dicembre'));
       			 
              return monthOption;                  
            }
            private set;
     }
     
     
     public kpi_Controller (ApexPages.StandardController controller){
        map<String,id> mapSurvey = kpi_Controller.mapSurveyName;
        List<SelectOption> months =  kpi_Controller.monthNameSelection;
        showTotaleAverage = false;
        Showtable = false;
        showResponse = false;
        system.debug('*** Showtable: '+Showtable);
 	}
 	
    //Search the question associate to the value Survey pickilist selected in page 
     public PageReference searchQuestions()
    {
        system.debug('*** mapSurveyName: '+mapSurveyName);
        system.debug('*** Id Survey Selected: '+nameSurvey);
        Showtable = false;
        showResponse = false;
        showQuestion = false;
        allQuestions = new List<questionsDisplayed>();
        
        Integer i = 0;
        
        boolean viewQuestion;
        
        try{
            listQuestions = [SELECT Choices__c, IsDeleted, Name, Question__c, Id, Survey__c, SystemModstamp, Total_Responses__c, Type__c, Survey__r.Name FROM Survey_Question__c where Survey__r.id =:nameSurvey];
              
            Showtable = true;
            for (Survey_Question__c q : listQuestions)
            {
            //inizializzo a false il tipo numerico di domanda
            viewQuestion = false;	
            	
            system.debug('*** Questions passed to veirfyResponse: '+q);	
            //Selected the Questions to display in page 
            viewQuestion = veirfyResponse(q);
            system.debug('*** viewQuestion: '+viewQuestion); 
            
            system.debug('*** Questions: '+listQuestions.get(i).Question__c+' *** Choice: '+q.Choices__c); 
           if (viewQuestion)
                {
                    questionView = new questionsDisplayed(q);
                    system.debug('*** checkbox: '+questionView.choiceSurvey);
                    allQuestions.add(questionView);
                    showQuestion = true;
                }  
            i++;
            }
            system.debug('*** Questions associated to Survey choice'+allQuestions);
    	}
    
        catch(Exception e){
            System.debug('Exception in Query Questions is '+e.getMessage());
        }
       
     
        return null;
    }
    
    //Found the Response with numeric value associated to Questions choice in page
    
    public void searchResponse(){
    	//allQuestions = new List<questionsDisplayed>();
    	allResponse = new List<SurveyQuestionResponse__c>();
    	//respSurvey = new responseSurvey();
    	listRespSurvey = new List<responseSurvey>();
    	mediaTotaleResponse = 0;
    	Integer nResponse = 0;
    	//showTotaleAverage = true;
    	//showResponse=true;
    	
    	for (questionsDisplayed qd : allQuestions){
            	system.debug('*** User Choices: '+qd.choiceSurvey); 
            	//Verify which are the questions user choice
            	if (qd.choiceSurvey)
            	{
            		Integer intMonth = Integer.valueOf(nameMonth);
    				checkDate();
            		allResponse = [SELECT Survey_Question__r.id, response__c, CreatedDate FROM SurveyQuestionResponse__c where Survey_Question__r.id =:qd.id AND CALENDAR_MONTH(CreatedDate)=: intMonth AND CALENDAR_YEAR(CreatedDate)=:year];
            		//system.debug('*** n: '+n); 
	            	system.debug('*** Response List: '+allResponse); 
	            	mediaResponse = 0;	
	            	Integer i = 0;
	            	
	            	system.debug('*** mediaResponse: '+mediaResponse); 
	            	for (SurveyQuestionResponse__c sq : allResponse){
	            		system.debug('*** Response: '+sq.response__c); 
	            		if (sq.response__c != null ){//AGGIUNGERE IL CONTROLLO SULLE RESPONSE CREATE NELL'ULTIMO MESE
	            			mediaResponse = mediaResponse + Integer.valueOf(sq.response__c);
	            			i++;
	            		}
	            		
	            	}
	            	if (mediaResponse != 0){
	            		mediaResponse = mediaResponse/i;
	            		//respSurvey.setResponseSurvey(qd.question,mediaResponse);
	            		respSurvey = new responseSurvey(qd.question,mediaResponse);
	            		system.debug('*** For Question: '+respSurvey.surveyQuestion+ ' Media Vote is: '+respSurvey.mediaVoteResponse); 
	            		listRespSurvey.add(respSurvey);
	            		showResponse=true;
	            	}
	            	 	
	            	system.debug('*** List of media vote: '+listRespSurvey);	
            	}
           	
        }
        //Calculate general Media value response
        for (responseSurvey rs : listRespSurvey){
        	mediaTotaleResponse = mediaTotaleResponse + rs.mediaVoteResponse;
        	nResponse++;
        }
        
        if(mediaTotaleResponse !=0){
        	mediaTotaleResponse = mediaTotaleResponse/nResponse;
        	respTotalSurvey = new responseSurvey('Media Totale Survey',mediaTotaleResponse);
        	system.debug('*** Total Average: '+mediaTotaleResponse);
        	system.debug('*** respTotalSurvey: '+respTotalSurvey);
        	showTotaleAverage = true;
        }
        
    }
    
    //Verify if there is one response with nueric value
    private boolean veirfyResponse(Survey_Question__c question){
    	listResponse = new List<SurveyQuestionResponse__c>();
    	SurveyQuestionResponse__c singleResponse = new SurveyQuestionResponse__c();
    	boolean numericType = false;
    	Integer numericResponse;
    	    	
    	//for (Survey_Question__c qd : lisQuestions){
    	system.debug('*** In veirfyResponse question.id: '+question.id); 
    		Integer intMonth = Integer.valueOf(nameMonth);
    		checkDate();
    		system.debug('*** intYear:' +year);
    		system.debug('*** intMonth: '+intMonth);
    		listResponse = [SELECT Survey_Question__r.id, response__c FROM SurveyQuestionResponse__c where Survey_Question__r.id =:question.id and CALENDAR_MONTH(CreatedDate)=: intMonth and CALENDAR_YEAR(CreatedDate)=:year];
    	system.debug('*** In veirfyResponse listResponse: '+listResponse); 
    		if (listResponse == null || listResponse.isEmpty())
    			return numericType;
    		else{
    			singleResponse = listResponse.get(0);
    			try{
    				numericResponse = Integer.valueOf(singleResponse.response__c);
    				numericType = true;
    				return numericType;
    			}
    			catch(Exception e){
            		System.debug('Exception, Response is a String '+e.getMessage());
            		return numericType;
        		}
    		return numericType;	
    		}
    		
    	//}
    	
    	
    	
    }
    
    
    
    
    
    
    public class responseSurvey{
    	public String surveyQuestion {get;set;}
    	public Integer mediaVoteResponse {get;set;}
    	/*
    	public responseSurvey(){
    	
    	}
    	*/
    	public responseSurvey(String question, Integer mediaVote){
    		this.surveyQuestion = question;
    		this.mediaVoteResponse = mediaVote; 
    	}
    	
    	
    	public void setResponseSurvey (String question, Integer mediaVote ){
    		this.surveyQuestion = question;
    		this.mediaVoteResponse = mediaVote; 
    	}
    	
    }
    
    
    public class questionsDisplayed{
        public String   name                   {get; set;}
        public String   id                           {get; set;}
        public String   question               {get; set;}
        public boolean   choices                {get; set;}
        public String   questionType           {get; set;}  
        public boolean choiceSurvey{get;set;}
        
        
        public questionsDisplayed(Survey_Question__c sq) {
            name = sq.Name;
            id = sq.Id;
            question = sq.Question__c;
            //orderNumber = String.valueOf(sq.OrderNumber__c+1);
            choices = Boolean.valueOf(sq.Choices__c);
            questionType = sq.Type__c;
            choiceSurvey = false;
            /*
            singleOptionsForChart = ' ';
            selectedOption = '';
            selectedOptions = new List<String>();
            if (sq.Type__c=='Multi-Select--Vertical'){        
                        renderSelectCheckboxes='true';
                        
                      
                selectedOption = '';
                selectedOptions = new List<String>();
              }
             else if (sq.Type__c=='Single Select--Horizontal'){   
                  renderSelectCheckboxes='false';
                  rowOptions = stringToSelectOptions(choices);
                  
                  
                selectedOption = '';
                selectedOptions = new List<String>();     
              }
            //responses= getResponses();
              */
          } 
        
    }
    
    //method to choose correct year according to month selected
    private void checkDate(){
    				Date myDate = date.today();

                    String month = myDate.month().format();
               /*     if (month.length() == 1){
                                 month = '0'+month;
                               }*/
                               
                    Integer currentYear = myDate.year();
                    
                    if( nameMonth <= month){
                    	year = currentYear;
                    }else year = currentYear-1;                   
   	
    }
    
    
    
}