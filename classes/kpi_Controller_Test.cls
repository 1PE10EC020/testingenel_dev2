@IsTest

private class kpi_Controller_Test {
       
	static testMethod void search_Test() { 
		Survey__c surv = new Survey__c();
   		Survey_Question__c quest = new Survey_Question__c();
   		SurveyQuestionResponse__c resp = new SurveyQuestionResponse__c(); 
   		SurveyTestingUtil sUtil = new SurveyTestingUtil();
   		
   		surv.id = sUtil.surveyId;
   		update surv;
   			
   		quest.id = sUtil.questionIds[0];
   		update quest; 
   			
   		resp.Response__c = '2';	
   		resp.SurveyTaker__c = sUtil.surveyTakerId;
   		resp.Survey_Question__c = quest.id;
   		insert resp;
   			
		test.startTest();
		Test.setCurrentPageReference(new PageReference('Page.KPI_TEST')); 
		ApexPages.StandardController sc = new ApexPages.standardController(surv);
			
        kpi_Controller kpi = new kpi_Controller(sc);
       	kpi.nameSurvey = surv.id;
       	kpi.searchQuestions();   
        
        for(kpi_Controller.questionsDisplayed q : kpi.allQuestions){
        	q.choiceSurvey = true;
        	
        }
        kpi.searchResponse();
        
        test.stopTest();
        		
	}

}