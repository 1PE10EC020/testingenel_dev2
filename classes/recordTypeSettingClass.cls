/*
 * @author: Accenture
 * @date: 2011-09-21
 */
public class recordTypeSettingClass {

    public static Integer indexGen {get; set;}
    
     public class settingwrapper {
    
        private RecordTypeSetting__c recordsetting;
        private Integer index;  

         public settingwrapper() {
         Map<String,String> lstParams = ApexPages.currentPage().getParameters();
            //System.Debug('Keys are ' +lstParams + '-Done-');
            String key = '';
            for(String param : lstParams.keySet()) {
                if(param.contains('_lkid')) {
                    key = param;
                    break;
                }
            }
            System.Debug('Key is ' + key);
            String strParentObjectID = lstParams.get(key);
            this.recordsetting = new RecordTypeSetting__c(Name__c = 'Enter name', Profile__c = ApexPages.currentPage().getParameters().get(label.CL00043),
            Object__c = strParentObjectID);
            //Object__c = ApexPages.currentPage().getParameters().get(label.CL00044));
            this.index = indexGen++;
        }
        
        public RecordTypeSetting__c getrecordsetting() {
            return recordsetting;
        }
        
        public Integer getIndex() {
            return index;
        }
    } 

    public List<settingwrapper> settinglist;
    public Integer numRows {get; set;}
    
     
     public recordTypeSettingClass(ApexPages.StandardController controller) {
        if(indexGen == null) indexGen = 1;
        
        settinglist = new List<settingwrapper>();
        numRows = 1;
    }
    
    public List<settingwrapper> getsettinglist() {
        return settinglist;
}
 

public PageReference save() {
        try {
            List<RecordTypeSetting__c> tempList = new List<RecordTypeSetting__c>();
            for(Integer i=0; i<settinglist.size(); ++i)
                tempList.add(settinglist[i].getrecordsetting());
            upsert(tempList);
            
        } catch (System.DMLException ex) {
            ApexPages.addMessages(ex);
        }
        PageReference requestPage = new PageReference('/' + ApexPages.currentPage().getParameters().get('retURL'));
        requestPage.setRedirect(false);
        return requestPage;
    }


        


    public void addNewRecord() {
        try {
            if(numRows > 0)
                for(Integer i=0; i<numRows; ++i)
                    settinglist.add(new settingwrapper());
        } catch (Exception ex) {
        ApexPages.addMessages(ex);}
    }
    public void InitExistingRecord() {
       try {
          String CurrentRecordID = ApexPages.currentPage().getParameters().get('ID');
            if(CurrentRecordID != null) {
                settingwrapper settingWrap = new settingwrapper();
                settingWrap.recordsetting = [SELECT Configuration_Environment__c,Development_Environment__c,Name__c,Object__c,Production_Environment__c,Profile__c,Record_Type_Setting__c,Refrence_No__c,UAT_Environment__c FROM RecordTypeSetting__c WHERE ID = :CurrentRecordID];
                settinglist.add(settingWrap);
                numRows = 1;
            }
        } catch (Exception ex) {
             ApexPages.addMessages(ex);
        }
    }
    
    public void clear() {
        settinglist.clear();
        numRows = 1;
    }
    
  
    public void deleteRow() {
        try {
            Integer delIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('delRow'));
            
            for(Integer i=0; i<settinglist.size(); ++i)
                if(settinglist[i].index == delIndex) {
                    settinglist.remove(i);
                    break;
                }
        } catch (Exception ex) {
            ApexPages.addMessages(ex);
        }
    }
}