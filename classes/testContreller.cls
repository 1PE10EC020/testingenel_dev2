public class testContreller {

    Case mycase;
    
    public string pod {get;set;}
    public string idpratica {get;set;} 
    public string cognomePod {get;set;}
    public string direzione {get;set;}
    public string nomePod{get;set;}
    public string unita {get;set;}
    public string codiceFiscalepod {get;set;}
    public string livello {get;set;}
    public string pivapod {get;set;}
    public boolean oraria {get;set;}
    
    public testContreller (ApexPages.StandardController controller){
        controller.addFields(new List<String> {'DirezioneDTR__c'});
        mycase =(case) controller.getRecord();
        refreshinfo(mycase.id);
    
    }

    public void dummy(){
    /*  system.debug('dummy ');
        system.debug('case '+mycase);
     testValue = mycase.DirezioneDTR__c;
     case c = [select id,DirezioneDTR__c from case where id = :mycase.id limit 1];
    system.debug('new case :'+c);
    testValue2 = c.DirezioneDTR__c; */
    refreshinfo(mycase.id);
    }
    public void saveC(){
        //mycase.pod__c= pod;
       // mycase = Four_Helper.Arricchisci_Case(mycase);
        if( myCase != null)
        	update mycase;
    }
    
    public void  refreshinfo (string caseid){
        case c = [select POD__c,IdPratica__c,DirezioneDTR__c,CognomeTitolarePOD__c,UnitaDiCompetenza__c,NomeTitolarePOD__c,LivelloTensioneDiFornitura__c,CodiceFiscaleTitolarePOD__c,Oraria__c,PartitaIVATitolarePOD__c from case where id = :caseid limit 1];
        if ( c!= null){
            pod= c.pod__c;
            idpratica = c.IdPratica__c;
            cognomePod = c.CognomeTitolarePOD__c;
            direzione = c.DirezioneDTR__c;
            unita=c.UnitaDiCompetenza__c;
            nomePod = c.NomeTitolarePOD__c;
            livello = c.LivelloTensioneDiFornitura__c;
            codiceFiscalepod = c.CodiceFiscaleTitolarePOD__c;
            oraria = c.Oraria__c;
            pivapod = c.PartitaIVATitolarePOD__c;
        }       
    
    
    }
    
}