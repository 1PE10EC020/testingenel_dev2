trigger PT_Richiesta_Trigger on PT_Richiesta__c (before insert,before update) {
    
    if(Trigger.isBefore){
        if (Trigger.isInsert) { 
                for(PT_Richiesta__c a: Trigger.New) {
                            PT_Richesta_Helper.validateConditions(Trigger.new);
                        }
          }
     if(Trigger.isBefore){
        if (Trigger.isUpdate) { 
        System.debug('*** PT_Richiesta_Trigger isBefore ****');
        PT_SetRTEsteso.setDescriptionRT(Trigger.newMap);
                }
       }
       
    }
    
}