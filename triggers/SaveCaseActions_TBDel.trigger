trigger SaveCaseActions_TBDel on Case (after insert, before update) {
String OwnerQueueName;
String OwnerRoleName;
String emailTemplateName; 
String dtrZona; 
boolean SmeIILiv = false;
String OwnerRoleNameOld;
String OwnerQueueNameOld;
String userProfileName;
String userProfileId;
list<Group> listQueueOwner;
Group queueOwner;



if (Constants.TRIGGER_ON.equalsIgnoreCase(Label.EXC_TRIGGER_ON_OFF))
{
	 if ( Trigger.isInsert)
    {       
      /*[GP 11012017 Modifica per disabiltare il trigger a portale Fibra]*/
      userProfileId = System.UserInfo.getProfileId();
      try{
         userProfileName = [SELECT Id, Name FROM Profile where id =:userProfileId limit 1].name;
      }catch(Exception e)
      {
        system.debug(logginglevel.error,'no Profile found trigger before isInsert: '+e.getMessage() );
      }
      if(Constants.PROFILE_I_LIV.equalsIgnoreCase(userProfileName) || Constants.PROFILE_II_LIV.equalsIgnoreCase(userProfileName) || Constants.PROFILE_FIBRA.equalsIgnoreCase(userProfileName)
            || Constants.PROFILE_II_LIV_INFOPIU.equalsIgnoreCase(userProfileName) || Constants.Amministratore_CM.equalsIgnoreCase(userProfileName)
            || Constants.Amministratore_KB_CM.equalsIgnoreCase(userProfileName) || Constants.Amministratore_Report_e_Survey.equalsIgnoreCase(userProfileName)
            || Constants.Integration_User.equalsIgnoreCase(userProfileName) || Constants.System_Administrator.equalsIgnoreCase(userProfileName))
      {
      /*END [GP 11012017 Modifica per disabiltare il trigger a portale Fibra]*/
           //Case_Helper.manageCaseOnTrigger(Trigger.new);
          // Case_Helper.CallFourService (Trigger.new);
          System.debug('*** nel trigger prima di Case_Helper.getOwnerCase(Trigger.new);'+Trigger.new.get(0));
          System.debug('*** nel trigger INSERT SmeIILiv: '+SmeIILiv);
          try{
          	/*[GP 16032017] Tolta l'invocazione alla Case_Helper.getOwnerCase per limitare le query, sostiutito prendendo il tech_queueName__c*/
              //OwnerRoleName = Case_Helper.getOwnerCase(Trigger.new, SmeIILiv);
              OwnerRoleName = Trigger.new.get(0).tech_queueName__c;
        	  System.debug('*** track BO OwnerRoleName '+OwnerRoleName);
              if (OwnerRoleName != null && !String.isBlank(OwnerRoleName) && OwnerRoleName.containsIgnoreCase('Back')){
                System.debug('*** track BO emailTemplateName '+emailTemplateName);
                    emailTemplateName='Support_Invia_Email_BO';
                    SendEmail_Helper.TrackEmail(Trigger.new.get(0),emailTemplateName); 
                    
              }
              
          }
          catch(Exception e ){
                    system.debug(logginglevel.error, 'Exception OwnerRoleName is null beacause case assigned to user :'+e.getMessage());
          }
      }/*END [GP 11012017 Modifica per disabiltare il trigger a portale Fibra]*/
      //SendEmail_Helper.TrackEmail(Trigger.new.get(0),emailTemplateName); 
      
    }
    system.debug('Case_Helper.flagProcess: '+Case_Helper.flagProcess + ' Case_Helper.flagTrigger: '+Case_Helper.flagTrigger);
    if ( Trigger.isUpdate && Trigger.isBefore && ((Case_Helper.flagProcess == false && Case_Helper.flagTrigger ==  false) || (Case_Helper.flagProcess == true && Case_Helper.flagTrigger ==  false)) )
    {
    /*[GP 07122016] Evita il controllo per fare accetta da Amministratore CM*/
        userProfileId = System.UserInfo.getProfileId();
        userProfileName = [SELECT Id, Name FROM Profile where id =:userProfileId limit 1].name;
        if (Case_Helper.flagProcess == true)
       {
       		Case_Helper.flagTrigger =  true;
       		Trigger.new.get(0).tech_change_owner__c = true;
       		//Trigger.new.get(0).tech_IsStatusLocked__c = true;
       }
        
        /*[GP 11012017 Modifica per disabiltare il trigger a portale Fibra]*/
        if(Constants.PROFILE_I_LIV.equalsIgnoreCase(userProfileName) || Constants.PROFILE_II_LIV.equalsIgnoreCase(userProfileName) || Constants.PROFILE_FIBRA.equalsIgnoreCase(userProfileName)
            || Constants.PROFILE_II_LIV_INFOPIU.equalsIgnoreCase(userProfileName) || Constants.Amministratore_CM.equalsIgnoreCase(userProfileName)
            || Constants.Amministratore_KB_CM.equalsIgnoreCase(userProfileName) || Constants.Amministratore_Report_e_Survey.equalsIgnoreCase(userProfileName)
            || Constants.Integration_User.equalsIgnoreCase(userProfileName) || Constants.System_Administrator.equalsIgnoreCase(userProfileName))
        {
            System.debug('*** DENTRO IF SOLO PER CM');
            /*END [GP 11012017 Modifica per disabiltare il trigger a portale Fibra]*/           
            // Nino 18/12/16 per Accettare i case in coda CallBack da Operatori BO
            /*
            list<String> listOwnerID = new list<String>();
            
            for ( Case c : Trigger.old){
                        listOwnerID.add(c.ownerid);
                        }
            try{
                        listQueueOwner = [select name from Group where type='Queue' and  id IN : listOwnerID];
                        //queueOwner = listQueueOwner.get(0);
                
                        if (!listQueueOwner.isEmpty()){
                            queueOwner = listQueueOwner.get(0);
                            OwnerRoleNameOld = queueOwner.name;
                        } else{
                            OwnerRoleNameOld ='';
                        }
                
             }catch(Exception e){
                        system.debug(logginglevel.error,'no queue found on c.ownerID : '+e.getMessage() );
                    }
          // Fine Nino 18/12/16 
          */
          //[GP 06022017] sostituisce la modifica 'Nino 18/12/16 per Accettare i case in coda CallBack da Operatori BO'
          OwnerQueueNameOld = Trigger.old.get(0).tech_QueueName__c;
          if(OwnerQueueNameOld == null) // il case è assegnato ad una coda, cerco il nome della coda a cui è assegnato il case
          {
           		OwnerQueueNameOld = '';//Trigger.old.get(0).owner.name;
          }
          
          //System.debug('*** Trigger.old.get(0).tech_rolename__c: '+Trigger.old.get(0).tech_rolename__c+' *** Trigger.new.get(0).tech_rolename__c'+Trigger.new.get(0).tech_rolename__c);
          //System.debug('*** Trigger.old.get(0).tech_queueName__c: '+Trigger.old.get(0).tech_queueName__c+' *** Trigger.new.get(0).tech_queueName__c'+Trigger.new.get(0).tech_queueName__c);
          //System.debug('*** Trigger.old.get(0).tech_change_owner__c: '+Trigger.old.get(0).tech_change_owner__c+' *** Trigger.new.get(0).tech_change_owner__c'+Trigger.new.get(0).tech_change_owner__c);
         
          OwnerRoleNameOld = Trigger.old.get(0).tech_RoleName__c;//Trigger.old.get(0).owner.UserRole.Name; //Case_Helper.getOwnerCase(Trigger.old, true);
          System.debug('*** Trigger.old.get(0).owner.UserRole.Name: '+Trigger.old.get(0).owner.UserRole.Name);
          //OwnerRoleName = Trigger.new.get(0).tech_RoleName__c;//Trigger.new.get(0).owner.UserRole.Name;//Case_Helper.getOwnerCase(Trigger.new, true);
          //System.debug('*** prima di vedere se è una coda OwnerRoleNamee: '+OwnerQueueName);
          //if(OwnerRoleNameOld == null) // il case è assegnato ad una coda, cerco il nome della coda a cui è assegnato il case
          //{
            //  OwnerRoleNameOld = Trigger.old.get(0).tech_QueueName__c;//Trigger.old.get(0).owner.name;
          //}
          //if(OwnerRoleName == null) // il case è assegnato ad una coda, cerco il nome della coda a cui è assegnato il case
          //{
                OwnerQueueName = Trigger.new.get(0).tech_QueueName__c;//Trigger.new.get(0).owner.name;
          //}
          //String OwnerQueueNameOld = Trigger.old.get(0).tech_QueueName__c; //Case_Helper.getOwnerCase(Trigger.old, true);
          System.debug('*** OwnerRoleNameOld: '+OwnerRoleNameOld);
          //String OwnerQueueName = Trigger.new.get(0).tech_QueueName__c;//Case_Helper.getOwnerCase(Trigger.new, true);
          System.debug('*** OwnerQueueName: '+OwnerQueueName);
          //System.debug('*** case.trigger Trigger.oldMap.get(Trigger.new.get(0).id).AssegnatoAllEspertoDiDTRZona__c : '+Trigger.oldMap.get(Trigger.new.get(0).id).AssegnatoAllEspertoDiDTRZona__c );
          /*[GP 20032017 S00000036] Tracciamento email per Invio Email automatica su case Chiuso */
		  System.debug('*** case.trigger Trigger.oldMap.get(Trigger.new.get(0).id).Status : '+Trigger.oldMap.get(Trigger.new.get(0).id).Status);
		  System.debug('*** case.trigger Trigger.new.get(0).Status : '+Trigger.new.get(0).Status);
		  System.debug('*** case.Trigger.new.get(0).tech_new_email_case_chiuso__c : '+Trigger.new.get(0).tech_new_email_case_chiuso__c);
		  if(Trigger.new.get(0).status.equals(Trigger.oldMap.get(Trigger.new.get(0).id).status) && Constants.CASE_STATUS_CHIUSO.equals(Trigger.new.get(0).status) 
			  && Trigger.new.get(0).tech_new_email_case_chiuso__c && SendEmail_Helper.sent == false)
			  {
				  EmailMessage eMessage = [SELECT Incoming, TextBody FROM EmailMessage where parentId =: Trigger.new.get(0).id limit 1];
				  if(eMessage.TextBody.Contains('Si prega di non rispondere a questa mail') && eMessage.Incoming)
				  {
					  SendEmail_Helper.TrackEmail(Trigger.new.get(0),'Support_Case_Closure_New_Email_Richiedente'); 
				  }
			  }
          /*END [GP 20032017 S00000036] Tracciamento email per Invio Email automatica su case Chiuso */
          if (OwnerRoleNameOld != null && OwnerQueueName!= null && !OwnerRoleNameOld.equalsIgnoreCase(OwnerQueueName) && Trigger.oldMap.get(Trigger.new.get(0).id).AssegnatoAllEspertoDiDTRZona__c != null)
          {
              System.debug('*** SaveCaseActions_TBDel trigger is Before nell if ruolo iniziale e finale sono uguali, owner iniziale: '+OwnerRoleNameOld+' owner finale: '+OwnerQueueName);
              Trigger.new.get(0).AssegnatoAllEspertoDiDTRZona__c = null;
              Trigger.new.get(0).tech_IsStatusLocked__c = false;
              //Federica Aloia 28/02/2017 aggiunto controllo su Back Office: se ho un case con SME e voglio passarlo a
              //Back Office, lo stato non deve essere messo a In carico al II Livello
              if(!Trigger.new.get(0).tech_queueName__c.equals('Back Office') )
              	Trigger.new.get(0).status = 'In Carico al II Livello';  
              //Trigger.new.get(0).tech_IsStatusLocked__c = true;
          }
          
          /*
          else{
            if (Constants.CASE_STATUS_INCARICO_ESPERTO_DI_ZONA.equalsIgnoreCase(Trigger.old.get(0).Status) && !Case_Helper.justChangeIIlivToSme)
            {
                Trigger.new.get(0).Status = Constants.CASE_STATUS_INCARICO_AL_II_LIVELLO;
                Case_Helper.justChangeIIlivToSme = true;
                Trigger.new.get(0).tech_IsStatusLocked__c = false;
            }
             
          }
          */ 
          /*
          try{
            OwnerRoleNameOld = Case_Helper.getOwnerCase(Trigger.old, SmeIILiv);
            
          }catch(Exception e ){
                    system.debug(logginglevel.error, 'Exception OwnerRoleName is null beacause case assigned to user :'+e.getMessage());
          }
          */
           //System.debug('*** OwnerQueueNameOld: '+OwnerQueueNameOld+' *** ped contain? '+OwnerQueueNameOld.containsIgnoreCase(Constants.ROLE_CONTAIN_PED));
		   //[GP 14022017] tolto /*userProfileName != 'Amministratore CM' && per obbligare ad acettare il case anche a amministratore cm
           if(/*userProfileName != 'Amministratore CM' && */OwnerQueueNameOld != 'CallBack' &&  OwnerQueueNameOld != null && !OwnerQueueNameOld.containsIgnoreCase(Constants.ROLE_CONTAIN_PED))
            {
            /*END [GP 07122016] Evita il controllo per fare accetta da Amministratore CM*/
            // is the update came from a future call, skip the check
            if(!System.isFuture())
                {
                    //Case_Helper.manageCaseOnTrigger(Trigger.new, Trigger.oldMap.get(Trigger.new[0].Id).POD__c);
                    if (!Case_Helper.invokeVerifyOwnerChanged)//[GP 2510/2016 Aggiunto if per non far richiamare più volte il metodo Case_Helper.BlockModifyIfNotOwed]
                        Case_Helper.BlockModifyIfNotOwed (Trigger.new,Trigger.oldMap);
                    Case_Helper.VerifyOwnerChanged(Trigger.new,Trigger.oldMap);
                    /*Giorgio Riporta lo status dallo SME al II Livello, calcola il nuovo tempo di escaltion per il WF 40h, spedisce l'email di II livello, tiene traccia dell'email spedita */
                    System.debug('*** Case_Helper.sent: '+Case_Helper.sent);
                    if (Case_Helper.sent == false)
                        Case_Helper.VerifyComeBackToIILivello(Trigger.new,Trigger.oldMap);
                    //Case_Helper.VerifyPodChanged (Trigger.new, Trigger.oldMap);
                    /*for (case c : trigger.new){
                        string status = Trigger.newMap.get(c.id).status;
                        if(Constants.CASE_STATUS_CHIUSO.equals(status) || Constants.CASE_STATUS_ANNULLATO.equals(status) || Constants.CASE_STATUS_CHIUSO_PENDING.equals(status))
                            if (Trigger.newMap.get(c.id).ownerid != Userinfo.g  etUserId() )
                                c.addError('errore, non sei autorizzato a modificare il case');
                    }*/
                  
                    
                    System.debug('*** caseForEmailNew: '+Trigger.new.get(0).status+' *** caseForEmailOld'+Trigger.oldMap.get(Trigger.new.get(0).id).status);
                    /* Se il case è aperto con la Email To Case allora la track email di invio al BO va fatta in update     */  
                   
                   try{
                        //OwnerRoleName = Case_Helper.getOwnerCase(Trigger.new, SmeIILiv);
                        //System.debug('*** OwnerRoleName: '+OwnerRoleName);
                       // Nino 19-12-16 Blocco Assegnazione alla coda Callback da parte di tutti gli operatori
                        if( OwnerQueueName== 'CallBack'){
                        Trigger.new.get(0).addError('Assegnazione a coda Callback non consentita');
                        system.debug(logginglevel.error,'raised error: Assegnazione a coda Callback non consentita');
                        } 
                       //Fine Nino 19-12-16
                       else if (OwnerQueueName!= null && !String.isBlank(OwnerQueueName)  && OwnerQueueName.containsIgnoreCase('Back') && Constants.ORIGIN_EMAIL.equalsIgnoreCase(Trigger.new.get(0).Origin) && Constants.CASE_STATUS_NUOVO.equalsIgnoreCase(Trigger.new.get(0).Status)
                            && Trigger.new.get(0).Status.equalsIgnoreCase(Trigger.oldMap.get(Trigger.new.get(0).id).Status))
                        {
                            System.debug('*** track BO EmailToCase ');
                            SendEmail_Helper.TrackEmail(Trigger.new.get(0),'Support_Invia_Email_BO'); 
                            
                        }
                        
                        /*
                        else
                        {// per mandare l'email al II livello tornando da esperto di zona
                            if (OwnerRoleName != null && String.isBlank(OwnerRoleName)  && Constants.CASE_STATUS_INCARICO_AL_II_LIVELLO.equalsIgnoreCase(Trigger.new.get(0).Status)
                            && !Trigger.new.get(0).Status.equalsIgnoreCase(Trigger.oldMap.get(Trigger.new.get(0).id).Status) && Constants.CASE_STATUS_INCARICO_ESPERTO_DI_ZONA.equalsIgnoreCase(Trigger.oldMap.get(Trigger.new.get(0).id).status))
                            {
                                System.debug('*** per mandare lemail al II livello tornando da esperto di zona'+Trigger.new);
                                AddEmailTrackingSentByButton.trackEmailSent(Trigger.new);
                            }
                        }
                        */
                   }catch(Exception e ){
                        system.debug(logginglevel.error, 'Exception in Trigger.isUpdate OwnerRoleName is null beacause case assigned to user :'+e.getMessage());
                    }
                    
                    
                    /* fine blocco track bo per email to Case*/
                    
                    /*Per tenere traccia dell'email spedite per stato Chiuso e Chiuso Pending*/
                    System.debug(' *** SendEmail_Helper.sent: '+SendEmail_Helper.sent+'*** Prima del blocco Chiuso Trigger.new.get(0):'+Trigger.new.get(0).status+
                                 ' **** vecchio valore: '+Trigger.oldMap.get(Trigger.new.get(0).id).Status+' *** Email_Richiedente__c: '+Trigger.new.get(0).Email_Richiedente__c);
                    //if (Trigger.new.get(0).Status.equalsIgnoreCase('Chiuso') && !Trigger.new.get(0).Status.equalsIgnoreCase(Trigger.oldMap.get(Trigger.new.get(0).id).Status) && SendEmail_Helper.sent == false
                    //  && !String.isBlank(Trigger.new.get(0).Email_Richiedente__c) && !Trigger.new.get(0).Famiglia__c.equalsIgnoreCase('Mobilità'))
                    if (Trigger.new.get(0).Status.equalsIgnoreCase('Chiuso') && !Trigger.new.get(0).Status.equalsIgnoreCase(Trigger.oldMap.get(Trigger.new.get(0).id).Status) && SendEmail_Helper.sent == false
                    && !String.isBlank(Trigger.new.get(0).Email_Richiedente__c) && ( String.isNotBlank(Trigger.new.get(0).Famiglia__c) &&  !Trigger.new.get(0).Famiglia__c.equalsIgnoreCase('Mobilità'))) //FIX TRACCIAMENTO EMAIL CHIUSURA MOBILITA' [GP 29_11_2016]
                    {
                        System.debug('*** caseForEmailNew: '+Trigger.new.get(0).Status);
                        SendEmail_Helper.TrackEmail (Trigger.new.get(0), 'Support_Case_Closure_Survey_Richiedente');                
                    }else{
                        if (Trigger.new.get(0).status.equalsIgnoreCase('Chiuso Pending') && !Trigger.new.get(0).Status.equalsIgnoreCase(Trigger.oldMap.get(Trigger.new.get(0).id).Status) 
                            && SendEmail_Helper.sent == false && !String.isBlank(Trigger.new.get(0).Email_Richiedente__c)&& ( String.isNotBlank(Trigger.new.get(0).Famiglia__c) &&  !Trigger.new.get(0).Famiglia__c.equalsIgnoreCase('Mobilità'))){//FIX TRACCIAMENTO EMAIL CHIUSURA MOBILITA' [GP 29_11_2016]
                            System.debug('*** caseForEmailNew: '+Trigger.new);
                            SendEmail_Helper.TrackEmail (Trigger.new.get(0), 'Support_Case_ClosurePending_Survey_Richiedente');                 
                        }
                    }
                    /* fine blocco chiuso e chiuso pending*/
                    
                    /*Per tenere traccia dell'email spedite all'Esperto di Zona*/
                    
                    System.debug('*** Trigger.new.get(0).AssegnatoAllEspertoDiDTRZona__c : '+Trigger.new.get(0).AssegnatoAllEspertoDiDTRZona__c +
                                ' *** Trigger.oldMap.get(Trigger.new.get(0).id).AssegnatoAllEspertoDiDTRZona__c) '+Trigger.oldMap.get(Trigger.new.get(0).id).AssegnatoAllEspertoDiDTRZona__c);
                                
                    if ((Trigger.new.get(0).AssegnatoAllEspertoDiDTRZona__c != null) && !String.isBlank(Trigger.new.get(0).AssegnatoAllEspertoDiDTRZona__c) &&
                            /*Constants.CASE_STATUS_INCARICO_ESPERTO_DI_ZONA.equalsIgnoreCase(Trigger.new.get(0).status)&& */                   
                            !Trigger.new.get(0).AssegnatoAllEspertoDiDTRZona__c.equals(Trigger.oldMap.get(Trigger.new.get(0).id).AssegnatoAllEspertoDiDTRZona__c) )
                    {
                        /*[GP 12122016] S00000030 email sme per ped*/
                        if(!Constants.ORIGIN_WEB.equalsIgnoreCase(Trigger.new.get(0).Origin))
                        {
                            System.debug('*** nell if SME caseAfterEmailNew: '+Trigger.new.get(0));
                            SendEmail_Helper.TrackEmail (Trigger.new.get(0), 'Support_Invia_Email_Esperto_Zona'); 
                        }
                        else
                        {
                            SendEmail_Helper.TrackEmail (Trigger.new.get(0), 'Support_Invia_Email_Esperto_Zona_PED'); 
                        }
                        /*END [GP 12122016] S00000030 email sme per ped*/
                    }
                     /* fine blocco Esperto di Zona*/
                    
                    /* tiene traccia delle email scalate al Team Leader*/
                    try{
                        System.debug('***TL BO caseAfterEmailNew: '+Trigger.new.get(0));
                                                                                                                             
                        System.debug('***Trigger.new.get(0).IsEscalated: '+Trigger.new.get(0).IsEscalated+' *** old value :'+Trigger.oldMap.get(Trigger.new.get(0).id).IsEscalated );
                        System.debug('*** STATUS: '+Trigger.new.get(0).status+' *** STATUS OLD: '+Trigger.oldMap.get(Trigger.new.get(0).id).Status);
                        //OwnerRoleName = Case_Helper.getOwnerCase(Trigger.new);
                        
                        //System.debug('*** nell escalation OwnerRoleName: '+OwnerRoleName);
                        //if (OwnerRoleName != null && !String.isBlank(OwnerRoleName) && OwnerRoleName.containsIgnoreCase('Back')){
                            if ((Trigger.new.get(0).IsEscalated == true) && (Trigger.new.get(0).IsEscalated != Trigger.oldMap.get(Trigger.new.get(0).id).IsEscalated) &&
                                Constants.CASE_STATUS_SCALATO.equalsIgnoreCase(Trigger.new.get(0).status)
                                && !Trigger.new.get(0).Status.equalsIgnoreCase(Trigger.oldMap.get(Trigger.new.get(0).id).Status))
                            {
                                System.debug('*** nell if Scalato TL BO caseAfterEmailNew: '+Trigger.new.get(0));
                                SendEmail_Helper.TrackEmail (Trigger.new.get(0), 'Support_Escalation_Email_Team_Leader'); 
                            } 
                        //}
                        /*
                        else
                        {
                            System.debug('***TL II livello else caseAfterEmailNew: '+Trigger.new.get(0));
                             if (OwnerRoleName != null && !String.isBlank(OwnerRoleName) && !OwnerRoleName.containsIgnoreCase('Back'))
                             {
                                if ((Trigger.new.get(0).IsEscalated == true) && (Trigger.new.get(0).IsEscalated != Trigger.oldMap.get(Trigger.new.get(0).id).IsEscalated) &&
                                Constants.CASE_STATUS_SCALATO.equalsIgnoreCase(Trigger.new.get(0).status)
                                && !Trigger.new.get(0).Status.equalsIgnoreCase(Trigger.oldMap.get(Trigger.new.get(0).id).Status))
                                {
                                    System.debug('*** nell else Scalato TL II caseAfterEmailNew: '+Trigger.new.get(0));
                                    SendEmail_Helper.TrackEmail (Trigger.new.get(0), 'Support_Invia_Email_II_Livello'); //Probabilmete da sostituire con un tempalate ad Hoc per TL II Livello
                                } 
                             }
                        }
                        */
                        /* Fine email al team leader*/
                    }catch(Exception e ){
                        system.debug(logginglevel.error, 'Exception in Trigger.isUpdate TL OwnerRoleName is null beacause case assigned to user :'+e.getMessage());
                    }  
                   
                }
            } else 
                if(OwnerQueueNameOld != null && OwnerQueueNameOld == 'CallBack' &&  (Trigger.new.get(0).IsEscalated == true) && (Trigger.new.get(0).IsEscalated != Trigger.oldMap.get(Trigger.new.get(0).id).IsEscalated) &&
                            Constants.CASE_STATUS_SCALATO.equalsIgnoreCase(Trigger.new.get(0).status)
                            && !Trigger.new.get(0).Status.equalsIgnoreCase(Trigger.oldMap.get(Trigger.new.get(0).id).Status ))
                        {
                            System.debug('*** nell if Scalato TL BO caseAfterEmailNew: '+Trigger.new.get(0));
                            SendEmail_Helper.TrackEmail (Trigger.new.get(0), 'Support_Escalation_Email_Team_Leader'); 
                        }
            // Federica Aloia 01/02/2017 se l'utente loggato è amministratoreCM non può assegnare il case ad AtosFibra
            else if (!Case_Helper.AmministratoreCMfound(Trigger.new)){
                 Trigger.new.get(0).addError(LABEL.CM_EXC_ILLEGAL_ASS_ATOS);
            }
            
        }
        /*END [GP 11012017 Modifica per disabiltare il trigger a portale Fibra]*/           
    }
}
    
}