<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Copia_Unita_di_Competenza</fullName>
        <field>Unita_di_Competenza2__c</field>
        <formula>UnitaDiCompetenza__r.Name</formula>
        <name>Copia Unita di Competenza</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>WF Copia Unita di Competenza</fullName>
        <actions>
            <name>Copia_Unita_di_Competenza</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Esperto DTR/Zona</value>
        </criteriaItems>
        <description>Copia l&apos;Unita di Competenza da lookup a testo</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
