<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SurveyResponseVale</fullName>
        <field>ResponseValue__c</field>
        <formula>value(Response__c)</formula>
        <name>SurveyResponseVale</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>WF_SurveResponseValue</fullName>
        <actions>
            <name>SurveyResponseVale</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISNUMBER( Response__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
